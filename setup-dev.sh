#!/usr/bin/sh
docker exec orderbly-api rake db:create
docker exec orderbly-api rake db:migrate
docker exec orderbly-api rake db:seed