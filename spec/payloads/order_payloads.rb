module OrderPayload

  def new_order_payload
    {
      delivery_date: Date.tomorrow,
      supplier_id: supplier.id,
      customer_id: customer.id,
      order_items_attributes: [{
        product_id: product.id, 
        company_id: supplier.id,
        product_variant_id: product_variant.id,
        quantity: 23,
        gross_price: 22.61,
        vat_price: 2.61,
        net_price: 20
      }]
    }
  end

end