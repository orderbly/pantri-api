# == Schema Information
#
# Table name: products
#
#  company_id
#  tax_id
#  title
#  description
#  image
#  got_variants
#  created_at
#  updated_at


require 'rails_helper'

RSpec.describe Product, type: :model do
  let(:product) { build(:product) }

  describe '#relationships' do
    it { should belong_to(:company) }
    it { should belong_to(:tax) }
    it { should have_many(:product_variants) }

    context 'have correct validations' do
      it { should validate_presence_of(:tax) }
    end
  end

  context '#attributes' do
    let(:attributes) { product.attributes.symbolize_keys }

    it { expect(attributes).to include(:got_variants) }
    it { expect(attributes).to include(:title) }
    it { expect(attributes).to include(:description) }
    it { expect(attributes).to include(:tax_id) }
    it { expect(attributes).to include(:image) }


    context 'have correct validations' do
      it { should validate_presence_of(:title) }
      it { should validate_presence_of(:tax) }
      # TODO: Complete validations
    end
  end

  context 'product with tags' do
    let(:company) { create(:company) }
    let(:tax) { create(:tax, company: company) }
    let(:product) { create(:product, company: company, tax: tax) }
    let(:tag) { create(:tag, company: company) }

    it 'should find this product with a tag via scope' do
      product.tags << tag
      expect(Product.all.by_tag(tag.description).count).to eq(1)
    end
  end
end
