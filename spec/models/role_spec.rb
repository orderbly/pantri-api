# == Schema Information
#
# Table name: tags
#
#  id          :bigint           not null, primary key
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Role, type: :model do
  let(:role) { build(:role) }

  context '#attributes' do
    let(:attributes) { role.attributes.symbolize_keys }

    it { expect(attributes).to include(:level) }
    it { expect(attributes).to include(:description) }

  end
end
