# == Schema Information
#
# Table name: companies
#
#  id          :bigint           not null, primary key
#  title       :string
#  address_one :string
#  country     :string
#  city        :string
#  postal_code :string
#  contact     :string
#  website     :string
#  description :string
#  image       :string
#  colour      :string
#  category    :string
#  supplier    :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Company, type: :model do
  let(:company) { build(:company) }
  describe '#relationships' do
  it { should have_many(:users) }
  it { should have_many(:members) }
  it { should have_many(:taxes) }
  it { should have_many(:products) }
  it { should have_many(:discount_books) }
  it { should have_many(:discount_book_memberships) }
  it { should have_many(:members) }
  it { should have_many(:tags) }
  it { should have_many(:integrations) }
  it { should have_many(:addresses) }
  it { should have_many(:users).through(:members) }
  end
  
  describe '#attributes' do
    let(:attributes) { company.attributes.symbolize_keys }
    context 'have correct validations' do
      it { should validate_presence_of(:title) }
      it { expect(attributes).to include(:address_one) }
      it { expect(attributes).to include(:country) }
      it { expect(attributes).to include(:city) }
      it { expect(attributes).to include(:postal_code) }
      it { expect(attributes).to include(:contact) }
      it { expect(attributes).to include(:description) }
      it { expect(attributes).to include(:image) }
      it { expect(attributes).to include(:colour) }
      it { expect(attributes).to include(:is_active) }
      it { expect(attributes).to include(:email) }

    end
  end

  describe "is created different types" do
    let(:company) { build :company, :as_supplier }
    it "is created as a supplier" do
      expect(company.supplier).to eq(true)
    end
  end

  describe "it creates a restuarant" do
    let(:company) { build :company }
    it "is created as a restuarant" do
      expect(company.supplier).to eq(false)
    end
  end


end