require 'rails_helper'

RSpec.describe Member, type: :model do
  let(:member) { build(:member) }

  describe '#relationships' do
    it { should belong_to(:user) }
    it { should belong_to(:company) }
    it { should belong_to(:role) }
  end

end
