# == Schema Information
#
# Table name: orders
#
#  id            :bigint           not null, primary key
#  customer_id   :integer
#  supplier_id   :integer
#  delivery_date :datetime
#  notes         :text
#  order_number  :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Order, type: :model do
  let!(:customer) {  create(:company) }
  let!(:supplier) {  create(:company) }
  let!(:customer_user) { create(:user, current_company: customer.id) }
  let!(:supplier_user) { create(:user, current_company: supplier.id) }
  let!(:customer_membership) { create(:member, user: customer_user, company: customer) }
  let!(:supplier_membership) { create(:member, user: supplier_user, company: supplier) }
  let!(:address) { create(:address, company: supplier) }
  let!(:order) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }

  describe '#relationships' do
    it { should belong_to(:customer).class_name('Company')  }
    it { should belong_to(:supplier).class_name('Company') }

    context 'have correct validations' do
      it { should validate_presence_of(:customer) }
      it { should validate_presence_of(:supplier) }
    end
  end

  context '#attributes' do
    let(:attributes) { order.attributes.symbolize_keys }
    it { expect(attributes).to include(:delivery_date) }
    it { expect(attributes).to include(:notes) }
    it { expect(attributes).to include(:order_number) }
    it { expect(attributes).to include(:status) }
    it { expect(attributes).to include(:balance) }
    it { expect(attributes).to include(:is_paid) }
    it { expect(attributes).to include(:delivery_status) }
    it { expect(attributes).to include(:value) }
    it { expect(attributes).to include(:accounting) }
    it { expect(attributes).to include(:is_recurring) }
    it { expect(attributes).to include(:signature_image) }
    it { expect(attributes).to include(:received_by) }
  
  end

  it 'verifies user customer permission' do
    expect(Order.isCustomer(order.id, customer_user.id)).to be(true)
    expect(Order.isCustomer(order.id, supplier_user.id)).to be(false)
  end
  
  it 'verifies user supplier permission' do
    expect(Order.isSupplier(order.id, customer_user.id)).to be(false)
    expect(Order.isSupplier(order.id, supplier_user.id)).to be(true)
  end
end
