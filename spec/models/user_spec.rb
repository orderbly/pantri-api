# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  company_id      :bigint
#  email           :string
#  password_digest :string
#  first_name      :string
#  last_name       :string
#  username        :string
#  contact_number  :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build(:user) }

  describe '#relationships' do
    it { should have_many(:members) }
    it { should have_many(:companies) }

    context 'have correct validations' do
    end
  end

  context '#attributes' do
    let(:attributes) { user.attributes.symbolize_keys }

    it { expect(attributes).to include(:email) }
    it { expect(attributes).to include(:password_digest) }
    it { expect(attributes).to include(:first_name) }
    it { expect(attributes).to include(:last_name) }
    it { expect(attributes).to include(:current_company) }
    it { expect(attributes).to include(:contact_number) }

    context 'have correct validations' do
      it { should validate_presence_of(:first_name) }
      it { should validate_presence_of(:last_name) }
      it { should validate_presence_of(:email) }
      it { should validate_uniqueness_of(:email).case_insensitive }
    end
  end

  it "should create a new membership" do
    role =  create(:role)
    company =  create(:company)
    user = create(:user)
    
    expect(User.create_invite_membership(user.email, company.id, role.id, {})).to eq({type: "success", message: "Invite was successful"})
    expect(User.create_invite_membership(user.email, company.id, role.id, {})).to eq({:message=>"This membership already exists.", :type=>"error"})
  end
end

