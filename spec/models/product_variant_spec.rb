supplier = Company.create(
          title: "Test Supplier",
          contact: "(021) 434 6565",
          description: "The worlds best milk",
          image: "https://www.foodbev.com/wp-content/uploads/2019/06/Hart-Dairy.jpg",
          supplier: true,
          is_active: true,
          email: 'test+1@test.com'
        )
        customer = Company.create(
          title: "Test Company",
          contact: "(021) 434 6565",
          description: "The worlds best milk",
          image: "https://www.foodbev.com/wp-content/uploads/2019/06/Hart-Dairy.jpg",
          supplier: true,
          is_active: true,
          email: 'test+2@test.com'
        )
        tax = Tax.create( company_id: supplier.id, category: 2, description: "No Tax", percentage: 0.0)
        product = Product.create(
          company_id: supplier.id,
          tax_id: tax.id,
          title: "Steak",
          description: "High marbling Wagyu",
          image: "https://buyfresh.co.za/wp-content/uploads/2019/06/Wagyu-Rump-Web-600x600.jpg"
        )
        product_variant = ProductVariant.create(
          product_id: product.id,
          description: "200g",
          available_stock: 100.0,
          stock: 100.0,
          unit_amount: 200.0,
          tax_amount: 26.09,
          track_stock: true,
        )
  
        order = Order.create(
          customer_id: customer.id,
          supplier_id: supplier.id,
          delivery_date: Date.tomorrow,
          order_number: "12",
          status: "pending",
          balance: 300,
          is_paid: "false",
          delivery_status: "order_pending",
          value: 300,
          is_recurring: false,
          parent_credit_note_id: nil
        )
  
        order_items = OrderItem.create(
          company_id: customer.id,
          product_id: product.id,
          order_id: order.id,
          quantity: 3,
          gross_price: 300,
          net_price: 300,
          vat_price: 0,
          product_variant_id: product_variant.id,
          received_amount: 0,
        )