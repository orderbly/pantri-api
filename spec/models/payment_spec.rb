require 'rails_helper'

RSpec.describe Payment, type: :model do

  describe '#relationships' do
    it { should have_many(:payment_allocations) }
    it { should have_many(:orders).through(:payment_allocations) }
    it { should belong_to(:order).optional }
    it { should belong_to(:supplier).class_name('Company').with_foreign_key('supplier_id') }
    it { should belong_to(:customer).class_name('Company').with_foreign_key('customer_id') }
  
  end
end
