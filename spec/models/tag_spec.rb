# == Schema Information
#
# Table name: tags
#
#  id          :bigint           not null, primary key
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Tag, type: :model do
  let(:tag) { build(:tag) }

  describe '#relationships' do
    it { should belong_to(:company) }

  end

  context '#attributes' do
    let(:attributes) { tag.attributes.symbolize_keys }

    it { expect(attributes).to include(:description) }
    it { expect(attributes).to include(:company_id) }

  end
end
