# == Schema Information
#
# Table name: taxes
#
#  id          :bigint           not null, primary key
#  category    :integer          default("standard_tax_rate")
#  description :string
#  company_id  :bigint
#  percentage  :float            default(0.0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Tax, type: :model do
  let(:tax) { build(:tax) }
  
  describe '#relationships' do
  it { should belong_to(:company) }

end 

  context '#attributes' do
    let(:attributes) { tax.attributes.symbolize_keys }

    it { expect(attributes).to include(:company_id) }
    it { expect(attributes).to include(:category) }
    it { expect(attributes).to include(:description) }
    it { expect(attributes).to include(:percentage) }

  end
end
