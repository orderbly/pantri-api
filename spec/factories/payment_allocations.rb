FactoryBot.define do
  factory :payment_allocation do
    order_id { 1 }
    payment_id { 1 }
    value { 1.5 }
  end
end
