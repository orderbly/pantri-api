FactoryBot.define do
  factory :delivery_zone_area do
    name { "Base Address" }
    street_code { "8001"}
    postal_code { "8000" }

    trait :cape_town do
      postal_code { "8001" }
      street_code { "8001" }
      name { "CAPE TOWN" }
      province { "Western Cape" }
    end

    trait :johannesburg do
      postal_code { "2000" }
      street_code { "2000" }
      name { "JOHANNESBURG" }
      province { "Gauteng" }
    end

  end
end
