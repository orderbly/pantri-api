FactoryBot.define do
  factory :delivery_zone do
    name { "DZ 1" }
    delivery_days { ["Monday"] }
    cut_off_time { "08:00" }
    cut_off_day { 1 }
    uuid { SecureRandom.uuid }

    trait :early_cut_off do
      cut_off_time {"06:00"}
    end

    trait :quantity_based do
      fee_based_on { 1 }
    end

    trait :late_cut_off do
      cut_off_time {"16:00"}
    end

    trait :same_day_cutoff do
      cut_off_day { 0 }
    end

    trait :previous_day_cutoff do
      cut_off_day { 1 }
    end

    trait :two_days_prior_cutoff do
      cut_off_day { 2 }
    end

    trait :weekly_delivery_days do
      delivery_days { ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"] }
    end

    trait :weekday_delivery_days do
      delivery_days { ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"] }
    end

    trait :tuesday_and_thursday_delivery_days do
      delivery_days { ["Tuesday", "Thursday" ] }
    end

    trait :monday_and_wednesday_delivery_days do
      delivery_days { ["Tuesday", "Thursday" ] }
    end
  
  end
end
