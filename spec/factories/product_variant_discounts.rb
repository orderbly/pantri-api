FactoryBot.define do
  factory :product_variant_discount do
    discount_book_id { "" }
    product_variant_id { "" }
    discount_percentage { 1.5 }
  end
end
