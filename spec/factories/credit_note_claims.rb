FactoryBot.define do
  factory :credit_note_claim do
    credit_note_id { 1 }
    order_id { 1 }
    value { 1.5 }
    description { "MyString" }
  end
end
