FactoryBot.define do
  factory :order_item do
    association :product
    association :order
    association :product_variant

    gross_price { 100 }
    net_price  { 100 }
    vat_price { 0 }
    quantity { 10 }

    trait :with_incorrect_stock do
      quantity { 30 }
    end

  end
end


