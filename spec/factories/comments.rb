FactoryBot.define do
  factory :comment do
    date { "2019-11-26 16:41:03" }
    order_id { 1 }
    company_id { 1 }
    user_id { 1 }
    message { "MyText" }
  end
end
