FactoryBot.define do
  factory :payment do
    supplier_id { 1 }
    customer_id { 1 }
    order_id { 1 }
    value { 1.5 }
    account_sale_id { 1 }
  end
end
