FactoryBot.define do
  factory :company_membership do
    supplier_id { 1 }
    customer_id { 1 }
    key { "MyString" }
  end
end
