FactoryBot.define do
  factory :pending_invite do
    email_address { "MyString" }
    role { 1 }
  end
end
