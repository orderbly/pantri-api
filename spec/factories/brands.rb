FactoryBot.define do
  factory :brand do
    title { "MyString" }
    company_id { 1 }
  end
end
