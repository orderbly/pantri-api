FactoryBot.define do
  factory :account_limit_membership do
    customer_id { 1 }
    supplier_id { 1 }
    account_limit { 1.5 }
  end
end
