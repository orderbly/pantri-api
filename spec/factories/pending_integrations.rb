FactoryBot.define do
  factory :pending_integration do
    company_id { 1 }
    name { "MyString" }
  end
end
