FactoryBot.define do
  factory :discount_book_membership do
    customer_id { 1 }
    discount_book_id { 1 }
  end
end
