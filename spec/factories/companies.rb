# == Schema Information
#
# Table name: companies
#
#  id          :bigint           not null, primary key
#  title       :string
#  address_one :string
#  country     :string
#  city        :string
#  postal_code :string
#  contact     :string
#  website     :string
#  description :string
#  image       :string
#  colour      :string
#  category    :string
#  supplier    :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :company do
    title { "#{Faker::Lorem.word} #{rand()}" }
    address_one { Faker::Address.street_address }
    country { Faker::Address.country }
    city { Faker::Address.city }
    postal_code { Faker::Address.postcode }
    contact { Faker::PhoneNumber.phone_number }
    website { Faker::Internet.domain_name }
    description { Faker::Lorem.sentence }
    image { "https://www.thecafedistributors.com.au/assets/full/MILKLAB.png" }
    colour { "primary" }
    category { "" }
    is_active { true }
    default_account_limit { 200000000 }
    uuid { SecureRandom.uuid }
    email { Faker::Internet.email}
    email_list { [Faker::Internet.email, Faker::Internet.email, Faker::Internet.email] }
    minimum_order_value { 200 }

    trait :as_supplier do
      supplier { true }
    end

    trait :low_credit_limit do
      default_account_limit { 20 }
    end

  end
end
