FactoryBot.define do
  factory :address do
    address_one { "MyString" }
    suburb { "MyString" }
    city { "MyString" }
    province { "MyString" }
    country { "MyString" }
    postal_code { "MyString" }
    company_id { 1 }
    latitude { "9.99" }
    longitude { "9.99" }


    trait :johannesburg do
      postal_code { "2000" }
      name { "JOHANNESBURG" }
      province { "GAUTENG" }
    end

    trait :cape_town do
      suburb { "CAPE TOWN" }
      city { "CAPE TOWN" }
      country { "SOUTH AFRICA" }
      postal_code { "8001" }
      name { "CAPE TOWN" }
      province { "Western Cape" }
    end

  end
end
