# == Schema Information
#
# Table name: product_variants
#
#  id                  :bigint           not null, primary key
#  product_id          :bigint
#  description         :string
#  status              :string
#  available_stock     :float            default(0.0)
#  stock               :float            default(0.0)
#  unit_amount         :float            default(0.0)
#  tax_amount          :float            default(0.0)
#  discount_percentage :float            default(0.0)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

FactoryBot.define do
  factory :product_variant do
    association :product

    description { Faker::Lorem.sentence }
    status { Faker::Lorem.characters(10) }
    available_stock { 0 }
    stock { Faker::Number.non_zero_digit }
    unit_amount { rand(1.0..16.0) }
    tax_amount { 0 }
    discount_percentage { rand(1.0..16.0) }
    track_stock { true }

    trait :with_stock do
      available_stock { 1000000 }
      track_stock { true }
    end

    trait :with_no_stock do
      available_stock { 0 }
      track_stock { true }
    end

  end
end
