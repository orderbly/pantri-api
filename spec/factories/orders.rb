# == Schema Information
#
# Table name: orders
#
#  id            :bigint           not null, primary key
#  customer_id   :integer
#  supplier_id   :integer
#  delivery_date :datetime
#  notes         :text
#  order_number  :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  factory :order do
    delivery_date { Time.now + 2.days }
    notes { Faker::Lorem.sentence }
    order_number { "123" }
    uuid { SecureRandom.uuid }
    status { 0 }
    balance { 1000 }
    is_paid { 'false' }
    delivery_status { 0 }
    value { 1000 }
    accounting { {} }
    is_recurring { false }

    trait :delivered do
      delivery_status {4}
    end

    trait :paid_order do
      is_paid { 'true' }
    end
    
    trait :partially_paid do
      is_paid {'partial'}
    end

    trait :value_greater_than_thousand do
      value {2000}
    end

    trait :balance_greater_than_thousand do
      balance {2000}
    end

    trait :value_smaller_than_thousand do
      value {800}
    end

    trait :balance_smaller_than_thousand do
      balance {800}
    end

    trait :value_thousand do
      value {1000}
    end

    trait :balance_thousand do
      balance {1000}
    end
    
    trait :large_balance do
      balance {10000}
    end

    trait :large_value do
      value {11000}
    end

    trait :is_accepted do 
      status { 2 }
    end

  end
end
