FactoryBot.define do
  factory :area do
    name { "MyString" }
    postal_code { "8001" }
    street_code { "8001" }
    city { "Cape Town" }
    province { "Western Cape" }
  end
end
