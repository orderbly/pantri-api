# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  company_id      :bigint
#  email           :string
#  password_digest :string
#  first_name      :string
#  last_name       :string
#  username        :string
#  contact_number  :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryBot.define do
  factory :user do
    first_name { 'Bilbo' }
    last_name { 'Baggins' }
    current_company { 1 }
    email { "user#{Random.rand.to_s[2..10]}@example.com" }
    password { 'password' }
  end
end
