FactoryBot.define do
  factory :delivery_zone_fee do
    value_min { 0 }
    value_max { 200 }
    fee_based_on { 0 }
    name { 'Delivery Fee' }

    trait :zero_to_hundred_value do
      value_min { 0 }
      value_max { 100 }
      fee_based_on { 0 }
    end
    
    trait :hundred_rand_fee do
      price { 100 }
    end

    trait :two_hundred_rand_fee do
      price { 200 }
    end

    trait :three_hundred_rand_fee do
      price { 300 }
    end

    trait :zero_rand_fee do
      price { 0 }
    end

    trait :hundred_to_thousand_value do
      value_min { 101 }
      value_max { 120 }
      fee_based_on { 0 }
    end

    trait :zero_to_ten_quantity do
      value_min { 0 }
      value_max { 10 }
      fee_based_on { 1 }
    end

    trait :eleven_to_twenty_quantity do
      value_min { 0 }
      value_max { 120 }
      fee_based_on { 1 }
    end

  end
end
