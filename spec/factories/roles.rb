FactoryBot.define do
  factory :role do
    level { 'admin' }
  end
end
