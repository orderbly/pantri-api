FactoryBot.define do
  factory :custom_client do
    address_one { Faker::Address.street_name }
    city { Faker::Address.city }
    suburb { Faker::Address.community }
    postal_code { Faker::Address.postcode }
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    uuid { SecureRandom.uuid }
    email { "test@test.com" }
    contact { "0844381188" }
  end
end
