FactoryBot.define do
  factory :credit_note do
    order_id { 1 }
    customer_id { 1 }
    balance { 1.5 }
    remaining_balance {1}
    credit_note_number {"12"}
  end

  trait :with_balance_zero do
    remaining_balance {0}
  end

  trait :balance_greater_than_thousand do
    balance {1500}
  end

  trait :remaining_balance_greater_than_thousand do
    remaining_balance {1500}
  end

  trait :balance_lower_than_thousand do
    balance {700}
  end

  trait :remaining_balance_lower_than_thousand do
    remaining_balance {700}
  end
  
  trait :large_remaining_balance do
    remaining_balance {5000}
  end

end
