FactoryBot.define do
  factory :credit_limit do
    supplier_id { 1 }
    customer_id { 1 }
    credit_limit { 1000 }

    trait :with_high_value do 
      credit_limit { 1000000 }
    end
  end
end
