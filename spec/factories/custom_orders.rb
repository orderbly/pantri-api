FactoryBot.define do
  factory :custom_order do
    customer_id { 1 }
    supplier_id { 1 }
    delivery_date { "2020-10-06 03:51:17" }
    notes { "MyText" }
    order_number { "MyString" }
    status { 1 }
    balance { 1.5 }
    is_paid { 1 }
    delivery_status { 1 }
    value { 1.5 }
    accounting { "" }
    order_schedule_id { 1 }
    is_recurring { false }
    schedule_id { 1 }
    signature_image { "MyString" }
    delivery_user_id { 1 }
    parent_credit_note_id { 1 }
    received_by { "MyString" }
    uuid { "" }
    delivery_address_id { 1 }
    delivery_fee { "" }
    order_reconciliation { false }
    is_delivery_buyer_actionable { false }
    inventory { "" }
    reorder_balance { "9.99" }
    delivery { "" }
  end
end
