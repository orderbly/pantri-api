# == Schema Information
#
# Table name: products
#
#  id           :bigint           not null, primary key
#  company_id   :bigint
#  tax_id       :bigint
#  title        :string
#  description  :string
#  image        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  got_variants :boolean
#

FactoryBot.define do
  factory :product do
    title { Faker::Food.dish }
    description { Faker::Food.description }
    got_variants { Faker::Boolean.boolean }
    image { Faker::Internet.url }
    exclusive_companies { [] }
  end
end
