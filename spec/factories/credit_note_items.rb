FactoryBot.define do
  factory :credit_note_item do
    company_id { 1 }
    product_id { 1 }
    order_id { 1 }
    quantity { 1.5 }
    gross_price { 1.5 }
    net_price { 1.5 }
    vat_price { 1.5 }
    product_variant_id { 1 }
    received_amount { 1.5 }
  end
end
