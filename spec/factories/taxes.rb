# == Schema Information
#
# Table name: taxes
#
#  id          :bigint           not null, primary key
#  category    :integer          default("standard_tax_rate")
#  description :string
#  company_id  :bigint
#  percentage  :float            default(0.0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :tax do
    association :company
    
    category { :standard_tax_rate }
    description { 'Standard Vat rate' }
    percentage { 0.15 }
  end
end
