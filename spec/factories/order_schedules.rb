FactoryBot.define do
  factory :order_schedule do
    order_id { 1 }
    start_date { "" }
    end_date { "" }
    active { false }
    next_date { "" }
    frequency { 1 }
  end
end
