FactoryBot.define do
  factory :delivery_fee_bracket do
    min_distance { "0" }
    max_distance { "100" }
    price { "100" }
  end
end
