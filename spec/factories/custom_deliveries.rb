FactoryBot.define do
  factory :custom_delivery do
    order_number { Faker::Number.number(digits: 10) }
    delivery_status { 0 }
    trait :delivered do
      delivery_status { 4 }
    end 
  end
end
