# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TagsController, type: :controller do
  let(:headers) { {} }
  let(:company) { create(:company) }
  let(:member) { create(:member, company: company) }
  let(:user) { member.user }

  describe 'GET /taxes' do
    before { sign_in_as(user) }
    context 'with authentication token' do

      it 'responds with 200 and an (empty) array of products' do
        get :index
        expect(response).to have_http_status(200)
        expect(response.body).to eq "[]"
      end
    end
  end

  describe 'without authentication token' do
    it 'responds with a 401' do
      get :index
      expect(response).to have_http_status(401)
    end
  end

  describe "POST create" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      let(:company) { create(:company) }
      let(:tag) { create(:tag, company: company) }


    it 'should create a new tag' do
      new_company =  create(:company)
      post :create,  params: { tag: { description: "New Tag", company_id: new_company.id.to_i } }
      assert_response :success
      end
    end
  end

  describe "PUT update" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      let(:company) { create(:company) }
      let(:tag) { create(:tag, company: company) }

      it 'should update an existing tag description' do
        patch :update, params: { id: tag.id, tag: { id: tag.id, description: 'My Description' } }
        assert_response :success
        expect(Tag.find(tag.id).description).to eq('My Description')
      end
    end
  end
end
