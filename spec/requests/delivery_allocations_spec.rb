require 'rails_helper'

RSpec.describe DeliveryAllocationsController, type: :controller do
  let(:headers) { {} }
  let(:company) { create(:company) }
  let(:supplier) { create(:company) }
  let(:customer) { create(:company) }
  let(:member) { create(:member, company: supplier) }
  let(:user) { member.user }
  let(:general_user) { create(:user) }
  let!(:address) { create(:address, company: supplier) }
  let!(:order) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }

  describe "Create Delivery Allocation" do
    before { sign_in_as(user) }

    it 'should create successful delivery_allocations' do
      post :create, params: {:delivery_allocation => {:users => [user.id], :orders => [order.id] }}
      assert_response :success
      expect(response.status).to eq(201)
    end

    it 'should fail to create a delivery_allocations with non-member' do
      post :create, params: {:delivery_allocation => {:users => [general_user.id], :orders => [order.id] }}
 
      expect(response.status).to eq(422)
      expect(JSON.parse(response.body)["status"]).to eq("failed")
    end

    it 'should to fail create a delivery_allocations with incorrect order ID' do
      post :create, params: {:delivery_allocation => {:users => [general_user.id], :orders => [343223] }}

      expect(response.status).to eq(404)
    end
  end

  describe "Deletes Delivery Allocation" do
    before { sign_in_as(user) }
    it 'should successful delete a  delivery_allocations' do
      post :delete_delivery_allocation, params: {:delivery_allocation => {:user_id => user.id, :order_id => order.id }}
      
      assert_response :success
      expect(response.status).to eq(200)
    end

  end

end
