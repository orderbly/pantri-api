# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  let(:headers) { {} }
  let(:company) { create(:company) }
  let(:member) { create(:member, company: company) }
  let(:user) { member.user }

  describe 'POST #create' do
    let(:user_params) { {user: { email: 'test@email.com', password: 'password', password_confirmation: 'password', first_name: 'Jean', last_name: 'Malan' }} }
    before { sign_in_as(user) }
    it 'returns http success' do
      post :signup, params: user_params

      expect(response).to be_successful
    end

    it 'returns unauthorized for invalid params' do
      post :signin, params: { auth: {email: user.email, password: 'incorrect' }}
      
      expect(response).to have_http_status(401)
    end

    it 'creates a new user' do
      expect do
        post :signup, params: user_params
      end.to change(User, :count).by(1)
    end
  end


  describe 'POST /invite_user' do
    before { sign_in_as(user) }
    context 'with authentication token' do
      
      it 'responds with a user not found error' do
        user = User.create(first_name: "test_first", last_name: "test_last", username: "test_username", email:"test@test.com", password: "test123")
        post :invite_user, params: { email: user.last_name }
        expect(response).to have_http_status(200)
        json_response = JSON.parse response.body
      end
    end
  end

  describe 'POST /invite_user' do
    before { sign_in_as(user) }
    context 'with authentication token' do

      it 'responds with a unsuccessful invitation' do
        user = User.create(first_name: "test_first", last_name: "test_last", username: "test_username", email:"test@test.com", password: "test123")
        post :invite_user, params: { email: user.email }
        expect(response).to have_http_status(200)
        json_response = JSON.parse response.body
        expect(json_response['type']).to eq('error')
      end
    end
  end

  describe 'POST /invite_user' do
    before { sign_in_as(user) }
    context 'with authentication token' do

      it 'falls back to creating a pending invite' do
        tag = Role.create(id: 1, level: "admin", description: "Admin")
        user.update(current_company: company.id)
        post :invite_user, params: { email: 'new_user@test.com', role_id: tag.id }

        json_response = JSON.parse response.body
        expect(json_response['type']).to eq('error')
        expect(json_response['message']).to eq('This user does not have an Orderbly account. We have sent them an invite to join your company.')
      end

      it 'responds with a success invitation' do
        new_user = User.create(first_name: "test_first", last_name: "test_last", username: "test_username", email:"test@test.com", password: "test123", current_company: company.id)
        role = Role.create(id: 1, level: "admin", description: "Admin")
        user.update(current_company: company.id)
        post :invite_user, params: { email: new_user.email, role_id: role.id }

        json_response = JSON.parse response.body
        expect(response).to have_http_status(200)
        json_response = JSON.parse response.body
        expect(json_response['type']).to eq('success')
      end
    end
  end

end
