require 'rails_helper'

RSpec.describe CustomDeliveriesController, type: :controller do
  let(:headers) { {} }
  let(:supplier) { create(:company) }
  let(:address) { create(:address) }
  let!(:customer) { create(:company) }
  let(:member) { create(:member, company: supplier) }
  let(:member_two) { create(:member, company: supplier) }
  let(:user) { member.user }
  let(:user_two) { member_two.user }
  let(:custom_delivery) { create(:custom_delivery, delivery_driver: user, supplier: supplier, customer: customer, address: address) }

  describe "POST /custom_deliveries" do
    before { sign_in_as(user) }

    it "creates a new custom delivert" do
      post :create, params: { 
        custom_delivery: { 
          supplier_id: supplier.id, 
          customer_id: customer.id,
          address_id: address.id,
          due_date: DateTime.now.end_of_day,
          delivery_driver_id: user.id,
          order_number: "1000"
        }
      }
      expect(response).to have_http_status(201)
      expect(CustomDelivery.last.supplier_id).to eq(supplier.id)
      expect(CustomDelivery.last.customer_id).to eq(customer.id)
      expect(CustomDelivery.last.due_date.month).to eq(DateTime.now.end_of_day.month)
      expect(CustomDelivery.last.due_date.day).to eq(DateTime.now.end_of_day.day)
      expect(CustomDelivery.last.due_date.year).to eq(DateTime.now.end_of_day.year)
      expect(CustomDelivery.last.order_number).to eq("1000")
      expect(CustomDelivery.last.address.address_one).to eq(address.address_one)
      expect(CustomDelivery.last.address.city).to eq(address.city)
      expect(CustomDelivery.last.address.country).to eq(address.country)

    end
  end

  describe "POST /custom_clients" do
    it "fails to create a new custom client due to unauthorised user" do
      post :create, params: { 
        custom_delivery: { 
          supplier_id: supplier.id, 
          customer_id: customer.id,
          address_id: address.id,
          due_date: DateTime.now.end_of_day,
          delivery_driver_id: user.id,
          order_number: "1000"
        }
      }
      expect(response).to have_http_status(401)
    end
  end

  describe "POST /update_custom_delivery_status" do
    before { sign_in_as(user) }

    it "updates a custom delivery statusto - delivery pending" do
      post :update_custom_delivery_status, params: {id: custom_delivery.id, delivery_status: "2"}

      expect(response).to have_http_status(200)
      expect(custom_delivery.reload.delivery_status).to eq("delivery_pending")
    end

    it "updates a custom delivery statusto - delivery in progress" do
      post :update_custom_delivery_status, params: {id: custom_delivery.id, delivery_status: "3"}

      expect(response).to have_http_status(200)
      expect(custom_delivery.reload.delivery_status).to eq("delivery_in_progress")
    end


    it "updates a custom delivery statusto - delivery in progress" do
      post :update_custom_delivery_status, params: {id: custom_delivery.id, delivery_status: "4"}

      expect(response).to have_http_status(200)
      expect(custom_delivery.reload.delivery_status).to eq("delivered")
    end
  end


  describe "DELETE /custom_delivery" do
    before { sign_in_as(user) }

    it "deletes a custom client" do
      delete :destroy, params: {id: custom_delivery.id}

      expect(response).to have_http_status(204)
    end
  end

end