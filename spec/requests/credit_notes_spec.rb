# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CreditNotesController, type: :controller do
  let!(:headers) { {} }
  let!(:company) { create(:company) }
  let!(:supplier) { create(:company) }
  let!(:customer) { create(:company) }
  let!(:member) { create(:member, company: supplier) }
  let!(:user) { member.user }
  let!(:tax) { create(:tax, company: supplier) }
  let!(:product) { create(:product, company: supplier, tax: tax) }
  let!(:product_variant) { create(:product_variant, :with_stock, product: product) }
  let!(:address) { create(:address, company: supplier) }

  describe "POST general create credit note" do
    before { sign_in_as(user) }
    let!(:order_less) { create(:order, :value_smaller_than_thousand, :balance_smaller_than_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:paid_order) { create(:order, :paid_order, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:order_greater) { create(:order, :value_greater_than_thousand, :balance_greater_than_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:order_thousand) { create(:order, :value_thousand, :balance_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:undelivered_order) { create(:order, :value_thousand, :balance_thousand, customer: customer, supplier: supplier, delivery_address: address) }
    let(:order_item) { create(:order_item, order: order_less, product: product, product_variant: product_variant) }
    it 'should create credit note for the case where order value is less than credit note value, and update order balance to 0' do
      post :general_credit_note, params: {:credit_note => {:customer_id => customer.id, :supplier_id => supplier.id, :order_id => order_less.id, :balance => 1200, 
      :order_items => [{product_id: product.id, product_variant_id: product_variant.id, gross_price: 1200, 
      net_price: 1100, vat_price: 100, company_id: supplier.id, credit_amount: "1"}]
      }}
      assert_response :success
      # Testing Response 
      expect(JSON.parse(response.body)['message']['order_id']).to eq(order_less.id)
      expect(JSON.parse(response.body)['message']['customer_id']).to eq(customer.id)
      expect(JSON.parse(response.body)['message']['supplier_id']).to eq(supplier.id)
      expect(JSON.parse(response.body)['message']['balance']).to eq(1200.0)
      expect(JSON.parse(response.body)['message']['remaining_balance']).to eq(0)
      # Checking the most recent credit note
      expect(CreditNote.last.order_id).to eq(order_less.id)
      expect(CreditNote.last.customer_id).to eq(customer.id)
      expect(CreditNote.last.supplier_id).to eq(supplier.id)
      expect(CreditNote.last.balance).to eq(1200.0)
      expect(CreditNote.last.remaining_balance).to eq(0.0)
      expect(CreditNoteClaim.last.value).to eq(CreditNote.last.balance - order_less.balance)
      # Checking the credit note items of the most recent credit note
      expect(CreditNote.last.credit_note_items[0].company_id).to eq(supplier.id)
      expect(CreditNote.last.credit_note_items[0].product_variant_id).to eq(product_variant.id)
      expect(CreditNote.last.credit_note_items[0].quantity).to eq(1.0)
      expect(CreditNote.last.credit_note_items[0].gross_price).to eq(CreditNote.last.balance)
      order_greater_balance_before_reload = order_greater.balance
      expect(order_greater.reload.balance).to eq(order_greater_balance_before_reload - (CreditNote.last.balance - order_less.balance))
      expect(order_less.reload.balance).to eq(0)
      expect(order_less.reload.is_paid).to eq('true')
      expect(CreditNoteClaim.last.order_id).to eq(order_greater.id)
      expect(CreditNoteClaim.last.credit_note_id).to eq(JSON.parse(response.body)['message']['id'])
    end
    
    it 'should create credit note for case where order value is greater than credit note value, and update credit note balance to 0' do
      post :general_credit_note, params: {:credit_note => {:customer_id => customer.id, :supplier_id => supplier.id, :order_id => order_greater.id, :balance => 800, 
      :order_items => [{product_id: product.id, product_variant_id: product_variant.id, gross_price: 800, 
      net_price: 700, vat_price: 100, company_id: supplier.id, credit_amount: "1"}]
      }}
      assert_response :success
      expect(JSON.parse(response.body)['message']['order_id']).to eq(order_greater.id)
      expect(JSON.parse(response.body)['message']['customer_id']).to eq(customer.id)
      expect(JSON.parse(response.body)['message']['supplier_id']).to eq(supplier.id)
      expect(JSON.parse(response.body)['message']['balance']).to eq(800.0)
      expect(JSON.parse(response.body)['message']['remaining_balance']).to eq(0.0)
      expect(JSON.parse(response.body)['message']['order_id']).to eq(order_greater.id)
      expect(CreditNote.last.customer_id).to eq(customer.id)
      expect(CreditNote.last.supplier_id).to eq(supplier.id)
      expect(CreditNote.last.balance).to eq(800.0)
      expect(CreditNote.last.remaining_balance).to eq(0.0)
      paid_order_balance_before_reload = paid_order.balance
      order_less_balance_before_reload = order_less.balance
      order_thousand_balance_before_reload = order_thousand.balance
      expect(paid_order.reload.balance).to eq(paid_order_balance_before_reload)
      expect(order_less.reload.balance).to eq(order_less_balance_before_reload)
      expect(order_thousand.reload.balance).to eq(order_thousand_balance_before_reload)
      expect(CreditNote.last.credit_note_items[0].company_id).to eq(supplier.id)
      expect(CreditNote.last.credit_note_items[0].product_variant_id).to eq(product_variant.id)
      expect(CreditNote.last.credit_note_items[0].quantity).to eq(1.0)
      expect(CreditNote.last.credit_note_items[0].gross_price).to eq(CreditNote.last.balance)
      order_greater_balance_before_reload = order_greater.balance
      expect(order_greater.reload.balance).to eq(order_greater_balance_before_reload - JSON.parse(response.body)['message']['balance'])
      expect(order_greater.reload.is_paid).to eq('partial')
      expect(CreditNoteClaim.last.order_id).to eq(order_greater.id)
      expect(CreditNoteClaim.last.credit_note_id).to eq(JSON.parse(response.body)['message']['id'])
      paid_order_balance_before_reload = paid_order.balance
      expect(paid_order.reload.balance).to eq(paid_order_balance_before_reload)
      expect(CreditNoteClaim.last.value).to eq(JSON.parse(response.body)['message']['balance'] - JSON.parse(response.body)['message']['remaining_balance'])
    end
    
    it 'should create credit note for case where order balance = credit note remaining balance, and update both credit note remaining balance and order balance to 0' do
      post :general_credit_note, params: {:credit_note => {:customer_id => customer.id, :supplier_id => supplier.id, :order_id => order_thousand.id, :balance => 1000, 
      :order_items => [{product_id: product.id, product_variant_id: product_variant.id, gross_price: 1000, 
      net_price: 900, vat_price: 100, company_id: supplier.id, credit_amount: "1"}]
      }}
      assert_response :success
      expect(JSON.parse(response.body)['message']['order_id']).to eq(order_thousand.id)
      expect(JSON.parse(response.body)['message']['customer_id']).to eq(customer.id)
      expect(JSON.parse(response.body)['message']['supplier_id']).to eq(supplier.id)
      expect(JSON.parse(response.body)['message']['balance']).to eq(1000.0)
      expect(JSON.parse(response.body)['message']['remaining_balance']).to eq(0.0)
      expect(CreditNote.last.customer_id).to eq(customer.id)
      expect(CreditNote.last.supplier_id).to eq(supplier.id)
      expect(CreditNote.last.balance).to eq(1000.0)
      expect(CreditNote.last.remaining_balance).to eq(0.0)
      expect(CreditNote.last.credit_note_items[0].company_id).to eq(supplier.id)
      expect(CreditNote.last.credit_note_items[0].product_variant_id).to eq(product_variant.id)
      expect(CreditNote.last.credit_note_items[0].quantity).to eq(1.0)
      expect(CreditNote.last.credit_note_items[0].gross_price).to eq(CreditNote.last.balance)
      expect(order_thousand.reload.is_paid).to eq('true')
      expect(order_thousand.reload.balance).to eq(0.0)
      paid_order_balance_before_reload = paid_order.balance
      order_less_balance_before_reload = order_less.balance
      order_greater_balance_before_reload = order_greater.balance
      expect(paid_order.reload.balance).to eq(paid_order_balance_before_reload)
      expect(order_less.reload.balance).to eq(order_less_balance_before_reload)
      expect(order_greater.reload.balance).to eq(order_greater_balance_before_reload)
      expect(CreditNoteClaim.last.order_id).to eq(order_thousand.id)
      expect(CreditNoteClaim.last.credit_note_id).to eq(JSON.parse(response.body)['message']['id'])
      expect(order_thousand.reload.is_paid).to eq('true')
      expect(CreditNoteClaim.last.value).to eq(JSON.parse(response.body)['message']['balance'] - JSON.parse(response.body)['message']['remaining_balance'])
    end

  it 'should fail to create credit note because order has not been delivered' do
    post :general_credit_note, params: {:credit_note => {:customer_id => customer.id, :supplier_id => supplier.id, :order_id => undelivered_order.id, :balance => 1000, 
    :order_items => [{product_id: product.id, product_variant_id: product_variant.id, gross_price: 1000, 
    net_price: 900, vat_price: 100, company_id: supplier.id, credit_amount: "1"}]
    }}
    expect(JSON.parse(response.body)['type']).to eq('success')
    end
  end 

   
  describe "POST general credit note where multiple orders are affected by the credit note" do
    before { sign_in_as(user) }
    let!(:order_one) { create(:order, :partially_paid, :value_thousand, :balance_smaller_than_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:order_two) { create(:order, :value_thousand, :balance_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:order_three) { create(:order, :partially_paid, :value_thousand, :balance_smaller_than_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:order_item_one) { create(:order_item, order: order_one, product: product, product_variant: product_variant) }
    let!(:order_item_two) { create(:order_item, order: order_two, product: product, product_variant: product_variant) }
    let!(:order_item_three) { create(:order_item, order: order_three, product: product, product_variant: product_variant) }
    it "creates a credit note and changes the balance of several orders that were partially paid" do
      post :general_credit_note, params: {:credit_note => {:customer_id => customer.id, :supplier_id => supplier.id, :order_id => order_one.id, :balance => 3000, 
      :order_items => [{product_id: product.id, product_variant_id: product_variant.id, gross_price: 3000, 
      net_price: 2700, vat_price: 300, company_id: supplier.id, credit_amount: "1"}]
      }}
      order_one_balance_before_reload = order_one.balance
      order_two_balance_before_reload = order_two.balance
      order_three_balance_before_reload = order_three.balance
      expect(order_one.reload.balance).to eq(0.0)
      expect(order_two.reload.balance).to eq(0.0)
      expect(order_three.reload.balance).to eq(0.0)
      expect(CreditNote.last.remaining_balance).to eq(3000.0 - order_one_balance_before_reload - order_two_balance_before_reload - order_three_balance_before_reload)
    end
  end

  describe "GET /current_credit_note" do
    before { sign_in_as(user) }
    let(:order_less) { create(:order, :value_smaller_than_thousand, :balance_smaller_than_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let(:paid_order) { create(:order, :paid_order, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let(:order_greater) { create(:order, :value_greater_than_thousand, :balance_greater_than_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let(:order_thousand) { create(:order, :value_thousand, :balance_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let(:undelivered_order) { create(:order, :value_thousand, :balance_thousand, customer: customer, supplier: supplier, delivery_address: address) }
    let(:order_item) { create(:order_item, order: order_less, product: product, product_variant: product_variant) }
    let(:credit_note) {create(:credit_note, :with_balance_zero, order: order_less, customer: customer, supplier: supplier) }
    let(:credit_note_item) { create(:credit_note_item, credit_note: credit_note, product: product, product_variant: product_variant)}
    it "should return one credit note" do
      get :current_credit_note, params: {id: credit_note.id}
      expect(JSON.parse(response.body)['id']).to eq(credit_note.id)
      expect(JSON.parse(response.body)['customer']['id']).to eq(credit_note.customer.id)
      expect(JSON.parse(response.body)['credit_note_number']).to eq(credit_note.credit_note_number)
      expect(JSON.parse(response.body)['supplier']['id']).to eq(credit_note.supplier.id)
    end
  end
  
  describe "POST create credit note" do
    let!(:order_two) { create(:order, :value_thousand, :balance_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:order_one) { create(:order, :partially_paid, :value_thousand, :balance_smaller_than_thousand, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:order_three) { create(:order, :large_balance, :large_value, :partially_paid, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:paid_order) { create(:order, :paid_order, :delivered, customer: customer, supplier: supplier, delivery_address: address) }
    
    let!(:order_item_one) { create(:order_item, order: order_one, product: product, product_variant: product_variant) }
    let!(:order_item_two) { create(:order_item, order: order_two, product: product, product_variant: product_variant) }
    before {sign_in_as(user)}

    it "should create a credit note and use it to pay off order_one and a portion of order_two" do
      post :create, params: {:credit_note => {:customer_id => customer.id, :supplier_id => supplier.id, :order_id => order_one.id, :balance => 3000, 
      :order_items => [{product_id: product.id, product_variant_id: product_variant.id, gross_price: 3000, vat_price: 300, net_price: 2700, company_id: supplier.id, quantity: 4, received: 3}]
      }}
      # checking response.
      expect(JSON.parse(response.body)['message']['order_id']).to eq(order_one.id)
      expect(JSON.parse(response.body)['message']['customer_id']).to eq(customer.id)
      expect(JSON.parse(response.body)['message']['supplier_id']).to eq(supplier.id)
      expect(JSON.parse(response.body)['message']['balance']).to eq(3000.0)
      order_one_balance_before_reload = order_one.balance
      order_two_balance_before_reload = order_two.balance
      order_three_balance_before_reload = order_three.balance
      expect(JSON.parse(response.body)['message']['remaining_balance']).to eq(0.0)
      # checking most recent credit note
      expect(CreditNote.last.order_id).to eq(order_one.id)
      expect(CreditNote.last.customer_id).to eq(customer.id)
      expect(CreditNote.last.supplier_id).to eq(supplier.id)
      expect(CreditNote.last.balance).to eq(3000.0)
      expect(CreditNote.last.remaining_balance).to eq(0.0)
      # checking order 
      expect(order_one.reload.balance).to eq(0.0)
      expect(order_two.reload.balance).to eq(0.0)
      expect(order_three.reload.balance).to eq(order_three_balance_before_reload - CreditNoteClaim.last.value)
      paid_order_balance_before_reload = paid_order.balance
      expect(paid_order.reload.balance).to eq(paid_order_balance_before_reload)
    end
  end

end
