require 'rails_helper'

RSpec.describe DiscountBooksController, type: :controller do
  let!(:headers) { {} }
  let!(:supplier) { create(:company) }
  let!(:user) { create(:user) }
  let!(:member) { create(:member, company: supplier, user: user) }
  let!(:discount_book) { create(:discount_book, supplier: supplier)}
  let!(:company_two) { create(:company) }

  describe 'GET /discount_book' do
    before { sign_in_as(user) }
    it "retrieves all discount books relating to current company" do
      get :index
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to eq([])
    end
  end

  describe "POST create" do
    before { sign_in_as(user) }
    it 'should create a new discount book' do
      post :create, params: { discount_book: {supplier_id: supplier.id, description: "Test Discount Book", title: "T1"} }
      
      expect(response).to have_http_status(201)
      expect(JSON.parse(response.body)["data"]["title"]).to eq("T1")
      expect(JSON.parse(response.body)['data']["description"]).to eq("Test Discount Book")
    end

    it 'should fail create a new discount book due to duplicate name' do
      post :create, params: { discount_book: {supplier_id: supplier.id, description: "Test Discount Book", title: discount_book.title} }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)["status"]).to eq(false)
      expect(JSON.parse(response.body)["message"]).to eq("There is already a price book with this title.")
    end

  end
  
  describe "GET /discount_book/id" do
    before {sign_in_as(user)}
    it 'should return the discount book with the specified id' do
      get :show, params: {id: discount_book.id}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)['id']).to eq(discount_book.id)
      expect(JSON.parse(response.body)['supplier_id']).to eq(discount_book.supplier_id)
      expect(JSON.parse(response.body)['title']).to eq(discount_book.title)
      expect(JSON.parse(response.body)['description']).to eq(discount_book.description)
    end
  end

  describe "GET /list_all_companies/:discount_book_id" do
    before {sign_in_as(user)}
    it 'should return a list containing just one company, which is company_two' do
      get :list_all_companies, params: {id: discount_book.id}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).find{|c| c['id'] == company_two.id}).should_not be_nil
      expect(JSON.parse(response.body).find{|c| c['id'] == supplier.id}.present?).to eq(false)
    end
  end
  
  describe "PATCH /discount_books/:discount_book_id" do
    before {sign_in_as(user)}
    it 'should return the updated discount book in json format.' do
      patch :update, params: {id: discount_book.id, discount_book: {title: "test_title", description: "test_description"}}
      returned_discount_book = discount_book.reload
      expect(response).to have_http_status(200)
      expect(returned_discount_book.title).to eq('test_title')
      expect(returned_discount_book.description).to eq('test_description')
    end
  end

  describe "DELETE /delete_discount_book_membership/:discount_book_id/:customer_id" do
    before {sign_in_as(user)}
    it 'should delete the specified discount book from existence.' do
      delete :destroy, params: {id: discount_book.id}
      expect(JSON.parse(response.body)['status']).to eq(true)
    end
  end

end