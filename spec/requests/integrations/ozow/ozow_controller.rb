require 'rails_helper'

RSpec.describe Integrations::Ozow::OzowController, type: :controller do


  describe "POST ozow paid" do
    let(:headers) { {} }
    let(:company) { create(:company) }
    let(:supplier) { create(:company) }
    let(:customer) { create(:company) }
    let(:member) { create(:member, company: supplier) }
    let(:user) { member.user }
    let(:tax) { create(:tax, company: supplier) }
    let(:product) { create(:product, company: supplier, tax: tax, company: supplier) }
    let(:product) { create(:product, company: supplier, tax: tax, company: supplier) }
    let(:product_variant) { create(:product_variant, :with_stock, product: product) }
    let(:product_variant_no_stock) { create(:product_variant, :with_no_stock, product: product) }
    let!(:address) { create(:address, company: supplier) }
    let!(:order) { create(:order, :is_accepted, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:order_two) { create(:order, :is_accepted, customer: customer, supplier: supplier) }
    let(:order_item) { create(:order_item, order: order, company: supplier, product: product, product_variant: product_variant) }

    context 'successful payments' do

      it 'should successfully create and allocate payment' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Complete",
          "Optional1"=> order.id,
          "Optional2"=> supplier.id,
          "Optional3"=>"order", 
          "Optional4"=>customer.id,
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        order_balance = order.balance - 100

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("success")
        expect(JSON.parse(response.body)["message"]).to eq("Payment successfully created")
        expect(order.reload.balance).to eq(order_balance)
        assert_response :success
      end

      it 'should successfully create an on account payment' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Complete",
          "Optional1"=> 'none',
          "Optional2"=> supplier.id,
          "Optional3"=>"account_sale", 
          "Optional4"=>customer.id,
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        order_balance = order.balance - 100
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("success")
        expect(JSON.parse(response.body)["message"]).to eq("Account Payment successfully created and allocated")
        expect(order.reload.balance).to eq(order_balance)
        assert_response :success
      end

      it 'should create a general payment due to order id not being valid' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"1100",
          "Status"=>"Complete",
          "Optional1"=> 23321,
          "Optional2"=> supplier.id,
          "Optional3"=>"order", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(order.reload.balance).to eq(0)
        expect(order_two.reload.balance).to eq(900)
        expect(JSON.parse(response.body)["status"]).to eq("success")
        expect(JSON.parse(response.body)["message"]).to eq("Account Payment successfully created and allocated")
        assert_response :success
      end
    end

    context 'unsuccessful payments' do

      it 'should fail to create a payment due to incorrect supplier id' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Complete",
          "Optional1"=> order.id,
          "Optional2"=> 'kldnflsdnf',
          "Optional3"=>"order", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("error")
        expect(JSON.parse(response.body)["message"]).to eq("Payment could not be created. Please contact support")
        expect(order.reload.balance).to eq(1000)
        assert_response :success
      end

      it 'should fail to create a general payment due to incorrect supplier id' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Complete",
          "Optional1"=> order.id,
          "Optional2"=> 'kldnflsdnf',
          "Optional3"=>"account_sale", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("error")
        expect(JSON.parse(response.body)["message"]).to eq("Account Payment allocation could not be created. Please contact support")
        expect(order.reload.balance).to eq(1000)
        assert_response :success
      end

      it 'should fail to create a payment due to incorrect customer id' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Complete",
          "Optional1"=> order.id,
          "Optional2"=> 'kldnflsdnf',
          "Optional3"=>"order", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("error")
        expect(JSON.parse(response.body)["message"]).to eq("Payment could not be created. Please contact support")
        expect(order.reload.balance).to eq(1000)
        assert_response :success
      end

      it 'should fail to create a general payment due to incorrect customer id' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Complete",
          "Optional1"=> order.id,
          "Optional2"=> 'kldnflsdnf',
          "Optional3"=>"account_sale", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("error")
        expect(JSON.parse(response.body)["message"]).to eq("Account Payment allocation could not be created. Please contact support")
        expect(order.reload.balance).to eq(1000)
        assert_response :success
      end
    end

    context 'handling unsuccessful repsonses from ozow' do

      it 'should return a cancelled payment attempt' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Cancelled",
          "Optional1"=> order.id,
          "Optional2"=> supplier.id,
          "Optional3"=>"account_sale", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("cancelled")
        expect(JSON.parse(response.body)["message"]).to eq("Order has been cancelled")
        expect(order.reload.balance).to eq(1000)
        assert_response :success
      end
      
      it 'should return a cancelled payment attempt' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Cancelled",
          "Optional1"=> order.id,
          "Optional2"=> supplier.id,
          "Optional3"=>"account_sale", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("cancelled")
        expect(JSON.parse(response.body)["message"]).to eq("Order has been cancelled")
        expect(order.reload.balance).to eq(1000)
        assert_response :success
      end

      it 'should return a abandoned payment attempt' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Abandoned",
          "Optional1"=> order.id,
          "Optional2"=> supplier.id,
          "Optional3"=>"account_sale", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("abandoned")
        expect(JSON.parse(response.body)["message"]).to eq("Order has been abandoned")
        expect(order.reload.balance).to eq(1000)
        assert_response :success
      end


      it 'should return a pending investigation order payment attempt' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"PendingInvestigation",
          "Optional1"=> order.id,
          "Optional2"=> supplier.id,
          "Optional3"=>"account_sale", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("pending investigation")
        expect(JSON.parse(response.body)["message"]).to eq("Order is pending investigation")
        expect(order.reload.balance).to eq(1000)
        assert_response :success
      end

      it 'should return a pending investigation order payment attempt' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Error",
          "Optional1"=> order.id,
          "Optional2"=> supplier.id,
          "Optional3"=>"account_sale", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("error")
        expect(JSON.parse(response.body)["message"]).to eq("An error has occured")
        expect(order.reload.balance).to eq(1000)
        assert_response :success
      end

      it 'should return a pending investigation order payment attempt' do
        post :paid, params: { 
          "SiteCode"=>"ORD-ORD-002",
          "TransactionId"=>"3519c8cc-9d20-43e5-819b-b71e947da0f0",
          "TransactionReference"=>"TheDairyCompany1593775025654",
          "Amount"=>"100",
          "Status"=>"Pending",
          "Optional1"=> order.id,
          "Optional2"=> supplier.id,
          "Optional3"=>"account_sale", 
          "Optional4"=>customer.id,
          "Optional5"=>"",
          "CurrencyCode"=>"ZAR",
          "IsTest"=>"True",
          "IsTest"=>"True",
          "StatusMessage"=>"Test transaction completed",
          "Hash"=>"275bccedfebb56f011a6caa36d0c8fddfd43a53d594e417ed03f88485edd2babe9ae71dac5a1a0d7348cc5f37e501ae24f1f98f7f867d8b55e3de077ddecae72",
          "controller"=>"integrations/ozow/ozow",
          "action"=>"paid"
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["status"]).to eq("pending")
        expect(JSON.parse(response.body)["message"]).to eq("Order is pending")
        expect(order.reload.balance).to eq(1000)
        assert_response :success
      end
    end
    
  end
end