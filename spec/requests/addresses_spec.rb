require 'rails_helper'

RSpec.describe AddressesController, type: :controller do
  let(:headers) { {} }
  let(:company) { create(:company) }
  let(:member) { create(:member, company: company) }
  let(:user) { member.user }

  describe 'GET /addresses' do
    before { sign_in_as(user) }
    context 'with authentication token' do

      it 'responds with 200 and an (empty) array of addresses' do
        get :index
        expect(response).to have_http_status(200)
        expect(response.body).to eq "[]"
      end
    end
  end

end