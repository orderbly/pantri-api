# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProductVariantsController, type: :controller do
  let(:headers) { {} }
  let(:company) { create(:company) }
  let(:member) { create(:member, company: company) }
  let(:user) { member.user }

  describe 'GET /product_variants' do
    before { sign_in_as(user) }
    context 'with authentication token' do

      it 'responds with 200 and an (empty) array of products', redis: true do
        get :index
        
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'without authentication token' do
    it 'responds with a 401' do
      get :index
      expect(response).to have_http_status(401)
    end
  end

  describe "POST create" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      let(:company) { create(:company) }
      let(:product) { create(:product, company: company, tax: tax) }
      let(:tax) { create(:tax, company: company) }
      let(:product_variant) { create(:product_variant, product: product) }

      it 'should create a new product variant' do
        post :create, params: { product_variant: FactoryBot.attributes_for(:product_variant, product_id: product.id) }
        assert_response :success
      end
    end
  end

  describe "PUT update" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      let(:company) { create(:company) }
      let(:tax) { create(:tax, company: company) }
      let(:product) { create(:product, company: company, tax: tax) }
      let(:product_variant) { create(:product_variant, product: product) }

      it 'should update an existing product variant status' do
        put :update, params: { id: product_variant.id, product_variant: { id: product_variant.id, status: 'Blah'} }
        assert_response :success
        expect(ProductVariant.find(product_variant.id).status).to eq('Blah')
      end
    end
  end

  describe "DELETE destroy" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      let(:company) { create(:company) }
      let(:tax) { create(:tax, company: company) }
      let(:product) { create(:product, company: company, tax: tax) }
      let(:product_variant) { create(:product_variant, product: product) }

      it 'should delete an existing product variant' do
        delete :destroy, params: {id: product_variant.id, product_variant: { id: product_variant.id } }
        assert_response :success
        expect(ProductVariant.where(id: product_variant.id).count).to eq(0)
      end
    end
  end

end
