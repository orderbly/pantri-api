require 'rails_helper'


RSpec.describe ProductVariantDiscountsController, type: :controller do
    let(:user) { create(:user) }
    let(:member) { create(:member, company: supplier, user: user) }
    let(:supplier) { create(:company) }
    let(:customer) { create(:company) }
    let(:tax) { create(:tax, company: supplier) }
    let(:product) { create(:product, company: supplier, tax: tax, company: supplier) }
    let(:product_variant) { create(:product_variant, :with_stock, product: product) }
    let(:product_variant_two) { create(:product_variant, :with_stock, product: product) }
    let(:product_variant_three) { create(:product_variant, :with_stock, product: product) }
    let(:discount_book) { create(:discount_book, supplier: supplier)}
    let!(:company_two) { create(:company, title: "c2") }
    let!(:membership) {create(:discount_book_membership, customer: company_two, discount_book: discount_book)}
    
    describe "GET /product_variant_discounts" do
        let!(:product_variant_discount) { create(:product_variant_discount, product_variant: product_variant, discount_book: discount_book)}
        before { sign_in_as(user) }
        it "should return only one product variant discount" do
            get :index, params: {id: discount_book.id}
            expect(JSON.parse(response.body)[0]['discount_book_id']).to eq(discount_book.id)
            expect(JSON.parse(response.body)[0]['product_variant_id']).to eq(product_variant.id)
            expect(JSON.parse(response.body)[0]['discount_percentage']).to eq(product_variant_discount.discount_percentage)
        end
    end
    
    describe "POST /product_variant_discount" do
        before { sign_in_as(user) }
        it "should create a product variant discount" do
            post :create, params: {discount_list: [
                {id: 0,
                product_variant_id: product_variant_two.id, 
                discount_book_id: discount_book.id, 
                discount_percentage: 15}],
                discount_book_id: discount_book.id, 
                supplier_id: supplier.id}
                expect(JSON.parse(response.body)['all_added']).to eq(true)
                expect(JSON.parse(response.body)['product_variant_discounts'][0]['product_variant_discount']['discount_percentage']).to eq(15.0)
                expect(JSON.parse(response.body)['product_variant_discounts'][0]['product_variant_discount']['product_variant_id']).to eq(product_variant_two.id)
            end
            
            it "should create two product variant discounts" do
                post :create, params: {discount_list: [
                    {id: 0,
                    product_variant_id: product_variant_two.id, 
                    discount_percentage: 20,
                    discount_book_id: discount_book.id}, 
                    {id: 0,
                    product_variant_id: product_variant_three.id, 
                    discount_percentage: 21,
                    discount_book_id: discount_book.id}],
                    discount_book_id: discount_book.id, 
                    supplier_id: supplier.id}
                    expect(JSON.parse(response.body)['all_added']).to eq(true)
                    expect(JSON.parse(response.body)['product_variant_discounts'][0]['product_variant_discount']['discount_percentage']).to eq(20.0)
                    expect(JSON.parse(response.body)['product_variant_discounts'][0]['product_variant_discount']['product_variant_id']).to eq(product_variant_two.id)
                    expect(JSON.parse(response.body)['product_variant_discounts'][1]['product_variant_discount']['discount_percentage']).to eq(21.0)
                    expect(JSON.parse(response.body)['product_variant_discounts'][1]['product_variant_discount']['product_variant_id']).to eq(product_variant_three.id)
                end
                
                let(:product_variant_discount) { create(:product_variant_discount, product_variant: product_variant, discount_book: discount_book)}
                it "should update the product variant discount with id = product_variant.id" do
                    post :create, params: {discount_list: [
                        {id: product_variant_discount.id,
                        product_variant_id: product_variant.id,
                        discount_percentage: 60,
                        discount_book_id: discount_book.id}],
                        discount_book_id: discount_book.id, 
                        supplier_id: supplier.id}
                        expect(JSON.parse(response.body)['all_added']).to eq(true)
                        expect(JSON.parse(response.body)['product_variant_discounts'][0]['product_variant_discount']['discount_percentage']).to eq(60.0)
                        expect(JSON.parse(response.body)['product_variant_discounts'][0]['product_variant_discount']['product_variant_id']).to eq(product_variant.id)
                        expect(JSON.parse(response.body)['product_variant_discounts'][0]['product_variant_discount']['discount_book_id'].to_i).to eq(discount_book.id)
                    end
                end
            end
            