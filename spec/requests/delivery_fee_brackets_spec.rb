require 'rails_helper'

RSpec.describe DeliveryFeeBracketsController, type: :controller do
  let(:headers) { {} }
  let(:company) { create(:company) }
  let(:supplier) { create(:company) }
  let(:member) { create(:member, company: supplier) }
  let(:user) { member.user }


  describe 'Post delivery_fee_brackets' do
    before { sign_in_as(user) }
    context 'successful creation' do
        it "successfully creates a new delivery fee bracket" do
        post :create, params: {delivery_fee_bracket: {company_id: supplier.id, max_distance: "120", min_distance: "0", price: "20"}}

        expect(response).to have_http_status(201)
        expect(JSON.parse(response.body)[0]['max_distance']).to eq("120.0")
        expect(JSON.parse(response.body)[0]['min_distance']).to eq("0.0")
        expect(JSON.parse(response.body)[0]['price']).to eq("20.0")
      end
    end
    
    context 'unsuccessful creation' do
      let!(:delivery_fee_bracket) {create(:delivery_fee_bracket, company: supplier)}
      it "should fail to creates a new delivery fee bracket due to overlapping distances" do
        post :create, params: {delivery_fee_bracket: {company_id: supplier.id, max_distance: "20", min_distance: "0", price: "20"}}

        expect(response).to have_http_status(422)
      end
    end
  end
  
  describe 'Delete delivery_fee_brackets' do
    before { sign_in_as(user) }

    context 'deletes a delivery fee bracket' do
      let!(:delivery_fee_bracket) {create(:delivery_fee_bracket, company: supplier)}
      it "successful deletes a delivery fee bracket" do
        delete :destroy, params: {id: delivery_fee_bracket.id }
        expect(response).to have_http_status(200)
        expect(DeliveryFeeBracket.find_by(id: delivery_fee_bracket)).to be(nil)
      end
    end

    context 'deletes a delivery fee bracket' do
      let!(:delivery_fee_bracket) {create(:delivery_fee_bracket, company: company)}
      it "fails to delete a delivery fee bracket due to incorrect company permission" do
        delete :destroy, params: {id: delivery_fee_bracket.id }
        
        expect(response).to have_http_status(422)
      end
    end

  end



end