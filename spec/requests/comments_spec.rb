require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  let(:headers) { {} }
  let(:company) { create(:company) }
  let(:user) { create(:user) }
  let(:member) { create(:member, company: company, user: user) }

  describe 'GET /discount_book_memberships' do
    before { sign_in_as(user) }
    it "works! (now write some real specs)" do
      get :index
      expect(response).to have_http_status(200)
    end
  end

end
