# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TaxesController, type: :controller do
  let(:headers) { {} }
  let(:company) { create(:company) }
  let(:member) { create(:member, company: company) }
  let(:user) { member.user }

  describe 'GET /taxes' do
    before { sign_in_as(user) }
    context 'with authentication token' do

      it 'responds with 200' do
        get :index
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'without authentication token' do
    it 'responds with a 401' do
      get :index
      expect(response).to have_http_status(401)
    end
  end
  

  describe "POST create" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      let(:company) { create(:company) }
      let(:tax) { create(:tax, company: company) }

    it 'should create a new tax' do
        post :create, params: { tax: FactoryBot.attributes_for(:tax, company_id: company.id) }
        assert_response :success
      end
    end
  end
end

