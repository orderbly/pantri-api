require 'rails_helper'

RSpec.describe CustomClientsController, type: :controller do
  let(:headers) { {} }
  let(:supplier) { create(:company) }
  let(:member) { create(:member, company: supplier) }
  let(:user) { member.user }
  let!(:custom_client) { create(:custom_client, supplier: supplier) }

  describe "POST /custom_clients" do
    before { sign_in_as(user) }

    it "creates a new custom client" do
      post :create, params: { 
        custom_client: { 
          supplier_id: supplier.id, 
          address_one: "83 Castle street",
          suburb: "Cape Town",
          province: "WC",
          country: "South Africa",
          city: "Cape Town",
          postal_code: "8001",
          latitude: "-33.2411",
          longitude: "18.2342",
          email: "test@test.com",
          contact: "0844381188"
        }
      }
      expect(response).to have_http_status(201)
      expect(CustomClient.last.address_one).to eq("83 Castle street")
      expect(CustomClient.last.suburb).to eq("Cape Town")
      expect(CustomClient.last.latitude).to eq("-33.2411")
      expect(CustomClient.last.longitude).to eq("18.2342")
      expect(CustomClient.last.email).to eq("test@test.com")
    end
  end

  describe "POST /custom_clients" do
    it "fails to create a new custom client due to unauthorised user" do
      post :create, params: { 
        custom_client: { 
          supplier_id: supplier.id, 
          address_one: "83 Castle street",
          email: "test@test.com",
          contact: "0844381188"
        }
      }
      expect(response).to have_http_status(401)
    end
  end

  describe "GET /custom_clients" do
    before { sign_in_as(user) }

    it "gets all custom clients for a speicif company" do
      get :company_custom_clients, params: {supplier_id: supplier.id}

      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body).map{|cc| cc["supplier_id"]}.uniq.length).to eq(1)
      expect(JSON.parse(response.body)[0]["supplier_id"]).to eq(supplier.id)
    end
  end

  describe "Update /custom_clients" do
    before { sign_in_as(user) }

    it "updates an existing custom client" do
      post :update, params: { 
        id: custom_client.id,
        custom_client: { 
          address_one: "22 Jump Street",
          suburb: "Holywood",
          province: "Cali",
          country: "USA",
          city: "Los Angeles",
          postal_code: "8001",
          latitude: "-34.2411",
          longitude: "28.2342",
          email: "test@test.com",
          contact: "0725958360"
        }
      }

      expect(response).to have_http_status(200)
      expect(custom_client.reload.address_one).to eq("22 Jump Street")
      expect(custom_client.reload.suburb).to eq("Holywood")
      expect(custom_client.reload.latitude).to eq("-34.2411")
      expect(custom_client.reload.longitude).to eq("28.2342")
      expect(custom_client.reload.contact).to eq("0725958360")
    end
  end


  describe "DELETE /custom_clients" do
    before { sign_in_as(user) }

    it "deletes a custom client" do
      delete :destroy, params: {id: custom_client.id}

      expect(response).to have_http_status(204)
    end
  end

end