require 'rails_helper'

RSpec.describe CreditLimitsController, type: :controller do
  let!(:headers) { {} }
  let!(:company) { create(:company) }
  let!(:supplier) { create(:company) }
  let!(:supplier_two) { create(:company) }
  let!(:customer) { create(:company) }
  let!(:customer_two) { create(:company) }
  let!(:member) { create(:member, company: supplier) }
  let!(:member_two) { create(:member, company: supplier) }
  let!(:user) { member.user }
  let!(:user_two) { member_two.user }
  let!(:credit_limit) {create(:credit_limit, supplier: supplier, customer: customer)}

  describe "credit_limit actions" do
    before { sign_in_as(user) }

    it "gets all credit notes" do
      get :company_credit_limits, params: {supplier_id: supplier.id}

      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)[0]["supplier_id"]).to eq(supplier.id)
      expect(JSON.parse(response.body)[0]["customer_id"]).to eq(customer.id)
      expect(JSON.parse(response.body)[0]["customer_name"]).to eq(customer.title)
    end
  end

  describe "POST /credit_limits" do
    before { sign_in_as(user_two) }

    it "creates a new credit limit" do
      post :create, params: {credit_limit: { supplier_id: supplier_two.id, customer_id: customer.id, credit_limit: 2000 }}
      expect(response).to have_http_status(403)
    end
  

    it "creates a new credit limit" do
      post :create, params: {credit_limit: { supplier_id: supplier.id, customer_id: customer_two.id, credit_limit: 2000 }}
      
      expect(JSON.parse(response.body)["supplier_id"]).to eq(supplier.id)
      expect(JSON.parse(response.body)["customer_id"]).to eq(customer_two.id)
      expect(JSON.parse(response.body)["customer_name"]).to eq(customer_two.title)
    end
    
    it "fails to create a new credit limit due to existing limit" do
      post :create, params: {credit_limit: { supplier_id: supplier.id, customer_id: customer.id, credit_limit: 2000 }}
      
      expect(response).to have_http_status(422)
    end
    
    it "creates a new credit limit" do
      post :create, params: {credit_limit: { supplier_id: supplier.id, customer_id: customer_two.id, credit_limit: 2000 }}
      
      expect(JSON.parse(response.body)["supplier_id"]).to eq(supplier.id)
      expect(JSON.parse(response.body)["customer_id"]).to eq(customer_two.id)
      expect(JSON.parse(response.body)["customer_name"]).to eq(customer_two.title)
      expect(JSON.parse(response.body)["credit_limit"]).to eq(2000)
    end

    it "updates an existing credit limit" do
      patch :update, params: {id: credit_limit.id, credit_limit: {supplier_id: supplier.id, customer_id: customer_two.id, credit_limit: 3000 }}
      expect(JSON.parse(response.body)["credit_limit"]).to eq(3000)
    end
    
    it "deletes a credit note" do
      expect{ delete :destroy, params: { id: credit_limit.id} }.to change(CreditLimit, :count).by(-1)
    end

  end
end
