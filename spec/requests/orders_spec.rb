# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrdersController, type: :controller do
  let(:headers) { {} }
  let!(:company) { create(:company) }
  let!(:supplier) { create(:company) }
  let!(:supplier_two) { create(:company, :low_credit_limit) }
  let!(:customer) { create(:company) }
  let!(:member_two) { create(:member, company: supplier) }
  let!(:member) { create(:member, company: supplier) }
  let!(:member) { create(:member, company: customer) }
  let!(:user) { member.user }
  let!(:tax) { create(:tax, company: supplier) }
  let(:address) { create(:address, company: customer)}
  let(:address_two) { create(:address, company: supplier)}
  let(:address_three) { create(:address, company: supplier_two)}
  # let(:product) { create(:product, company: supplier, tax: tax, company: supplier) }
  let(:product) { create(:product, company: supplier, tax: tax, company: supplier) }
  let(:product_variant) { create(:product_variant, :with_stock, product: product) }
  let(:product_variant_no_stock) { create(:product_variant, :with_no_stock, product: product) }
  let!(:order) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }
  let!(:order_item) { create(:order_item, order: order, product: product, product_variant: product_variant) }
  
  describe 'GET /orders' do
    before { sign_in_as(user) }
    context 'with authentication token' do

      it 'responds with 200 and an (empty) array of orders', redis: true do
        get :index
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'without authentication token' do
    it 'responds with a 401' do
      get :index
      expect(response).to have_http_status(401)
    end
  end

  describe "Create orders" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      it 'should create a new order' do
        supplier.update(default_address_id: address_two.id)
        quantity = 5
        vat_price = 10
        net_price = 20
        gross_price = 30
        post :create, params: { order: {
          delivery_date: Date.tomorrow,
          supplier_id: supplier.id,
          customer_id: customer.id,
          delivery_address_id: address.id,
          order_items_attributes: [{
            product_id: product.id, 
            company_id: supplier.id,
            product_variant_id: product_variant.id,
            quantity: quantity,
            gross_price: gross_price,
            vat_price: vat_price,
            net_price: net_price
          }]
        }}

        assert_response :success
        expect(JSON.parse(response.body)['customer']['id']).to eq(customer.id)
        expect(JSON.parse(response.body)['supplier']['id']).to eq(supplier.id)
        expect(JSON.parse(response.body)['gross_amount']).to eq(gross_price * quantity)
        expect(JSON.parse(response.body)['net_amount']).to eq(net_price * quantity)
        expect(JSON.parse(response.body)['vat_amount']).to eq(vat_price * quantity)
      end

      context 'insufficient credit balance' do

        it 'should fail to create a new order due to insuffecient account balance with the supplier' do
          supplier_two.update(default_address_id: address_two.id)
          quantity = 5
          vat_price = 10
          net_price = 20
          gross_price = 30
          post :create, params: { order: {
            delivery_date: Date.tomorrow,
            supplier_id: supplier_two.id,
            customer_id: customer.id,
            delivery_address_id: address.id,
            order_items_attributes: [{
              product_id: product.id, 
              company_id: supplier_two.id,
              product_variant_id: product_variant.id,
              quantity: quantity,
              gross_price: gross_price,
              vat_price: vat_price,
              net_price: net_price
            }]
          }}
          expect(response).to have_http_status(422)
          expect(JSON.parse(response.body)).to eq({})
        end
      end

      context 'sufficient credit balance' do
        let!(:credit_limit) {create(:credit_limit, :with_high_value, supplier: supplier_two, customer: customer)}
        it 'should create a new order with a custom credit limit provided by the supplier' do
          supplier_two.update(default_address_id: address_two.id)
          quantity = 50
          vat_price = 100
          net_price = 200
          gross_price = 330
          post :create, params: { order: {
            delivery_date: Date.tomorrow,
            supplier_id: supplier_two.id,
            customer_id: customer.id,
            delivery_address_id: address.id,
            order_items_attributes: [{
              product_id: product.id, 
              company_id: supplier_two.id,
              product_variant_id: product_variant.id,
              quantity: quantity,
              gross_price: gross_price,
              vat_price: vat_price,
              net_price: net_price
            }]
          }}

          expect(response).to have_http_status(201)
          expect(Order.last.supplier_id).to eq(supplier_two.id)
          expect(Order.last.customer_id).to eq(customer.id)
        end
      end
    end
  end

  describe "PUT update" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      it 'should update an existing order' do
        patch :update, params: { id: order.id, order: {notes: "hello there", delivery_date: Date.today.end_of_month} }
        
        expect(order.reload.notes).to eq("hello there")
        expect(order.reload.delivery_date).to eq(Date.today.end_of_month)
        assert_response :success
      end
    end
  end

  describe "Action orders" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      it 'should accept the order' do
        get :accept_order, params: {order_uuid: order.uuid }
        
        assert_response :success
        expect(order.reload.status).to eq("accepted")
        expect(order.reload.delivery_status).to eq("in_preparation")
      end

      it 'should decline the order' do
        get :decline_order, params: {order_uuid: order.uuid }

        assert_response :success
        expect(order.reload.status).to eq("declined")
      end

      it 'should cancel the order' do
        get :cancel_order, params: {order_uuid: order.uuid }
        assert_response :success

        expect(order.reload.status).to eq("cancelled")
        expect(order.reload.delivery_status).to eq("order_cancelled")
      end

    end
  end

  describe "Update delivery order status" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      it 'should set the delivery status to preparation' do
        post :update_order_delivery_status, params: {order_uuid: order.uuid, order: {delivery_status: "1" }}

        assert_response :success
        expect(order.reload.delivery_status).to eq("in_preparation")
      end

      it 'should set the delivery status to delivery_pending' do
        post :update_order_delivery_status, params: {order_uuid: order.uuid, order: {delivery_status: "2" }}

        assert_response :success
        expect(order.reload.delivery_status).to eq("delivery_pending")
      end

      it 'should set the delivery status to delivery_in_progress' do
        post :update_order_delivery_status, params: {order_uuid: order.uuid, order: {delivery_status: "3" }}

        assert_response :success
        expect(order.reload.delivery_status).to eq("delivery_in_progress")
      end

      it 'should set the delivery status to delivered' do
        post :update_order_delivery_status, params: {order_uuid: order.uuid, order: {delivery_status: "4" }}

        assert_response :success
        expect(order.reload.delivery_status).to eq("delivered")
      end
    end
  end

  describe "Update single order status" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      it 'should set the delivery status to preparation' do
        post :update_single_order_delivery_status, params: {order_uuid: order.uuid, order: {delivery_status: "1" }}

        assert_response :success
        expect(order.reload.delivery_status).to eq("in_preparation")
      end

      it 'should set the delivery status to delivery_pending' do
        post :update_single_order_delivery_status, params: {order_uuid: order.uuid, order: {delivery_status: "2" }}

        assert_response :success
        expect(order.reload.delivery_status).to eq("delivery_pending")
      end

      it 'should set the delivery status to delivery_in_progress' do
        post :update_single_order_delivery_status, params: {order_uuid: order.uuid, order: {delivery_status: "3" }}

        assert_response :success
        expect(order.reload.delivery_status).to eq("delivery_in_progress")
      end

      it 'should set the delivery status to delivered' do
        post :update_single_order_delivery_status, params: {order_uuid: order.uuid, order: {delivery_status: "4" }}

        assert_response :success
        expect(order.reload.delivery_status).to eq("delivered")
      end
    end
  end

  describe "Receive order" do
    before { sign_in_as(user) }

    it 'should return the order object' do
      get :receive_order, params: {order_uuid: order.uuid}

      assert_response :success
      expect(response.status).to eq(200)
    end
  end

  describe "Create order with credit note" do
    before { sign_in_as(user) }
    subject { Order.all }
    context 'successfully creates credit note with order' do
      it 'create a new order with a credit note' do
        order.update(value: order.order_items.map{|oi| oi.quantity  * oi.gross_price}.sum)
        order.update(status: 2)
        user.update(current_company: order.customer_id)
        quantity = 10
        gross_price = 100
        post :order_outstanding_items, params: { 
          order_uuid: order.uuid
        }

        expect(CreditNote.last.balance).to eq(order.order_items.map{|oi| (oi.quantity - oi.received_amount) * oi.gross_price}.sum)
        expect(order.value).to eq(order.order_items.map{|oi| (oi.quantity - oi.received_amount) * oi.gross_price}.sum)
        expect(order.is_paid).to eq("false")
        expect(Order.last.balance).to eq(0)
        expect(Order.last.is_paid).to eq("true")
        expect(Order.last.value).to eq(gross_price * quantity)
        expect(CreditNoteClaim.last.order_id).to eq(Order.last.id)
        expect(CreditNoteClaim.last.credit_note_id).to eq(CreditNote.last.id)
        expect(CreditNoteClaim.last.value).to eq(gross_price * quantity)
        expect(CreditNote.last.balance).to eq(CreditNote.last.credit_note_items.map{|cni| (cni.quantity * cni.gross_price) }.sum)

        expect(CreditNote.last.remaining_balance).to eq(0)
        expect(JSON.parse(response.body)["status"]).to eq(true)
        expect(Order.last.delivery_date.day ).to eq((DateTime.now + 1.day).day)
        expect(Order.last.delivery_date.month ).to eq((DateTime.now + 1.day).month)
        assert_response :success
      end

      describe "Claim credit note with order parent_id" do
        before { sign_in_as(user) }
        subject { Order.all }
        context 'successfully creates credit note with order' do
          it 'create a new order with a credit note' do
            order.update(value: order.order_items.map{|oi| oi.quantity  * oi.gross_price}.sum)
            order.update(status: 2)
            quantity = 10
            gross_price = 100
            post :claim_credit, params: { 
              order_uuid: order.uuid,
            }
            expect(CreditNote.last.balance).to eq(order.order_items.map{|oi| (oi.quantity - oi.received_amount) * oi.gross_price}.sum)
            expect(order.value).to eq(order.order_items.map{|oi| (oi.quantity - oi.received_amount) * oi.gross_price}.sum)
            expect(CreditNoteClaim.last.order_id).to eq(Order.last.id)
            expect(CreditNoteClaim.last.credit_note_id).to eq(CreditNote.last.id)
            expect(CreditNoteClaim.last.value).to eq(gross_price * quantity)
            expect(CreditNote.last.balance).to eq(CreditNote.last.credit_note_items.map{|cni| (cni.quantity * cni.gross_price) }.sum)
            expect(CreditNote.last.remaining_balance).to eq(0)
            expect(CreditNote.last.order.id).to eq(order.id)
            assert_response :success
          end
        end
      end

      context 'unsuccessfully creates credit note with order' do
        it 'tries to create a new order and a credit note but fails to create an order due to insufficient stock & allocates credit to parent (original) order' do
          order.update(value: order.order_items.map{|oi| oi.quantity  * oi.gross_price}.sum)
          order.update(status: 2)
          order.order_items.first.product_variant.update(available_stock: 0)
          quantity = 10
          gross_price = 100
          post :order_with_credit_note, params: { 
            order: {
              customer_id: customer.id,
              delivery_date: Date.tomorrow,
              notes: "New Order with Credit Note",
              delivery_address_id: address.id,
              order_items_attributes: [
                { 
                  company_id: supplier.id,
                  gross_price: gross_price,
                  net_price: order.order_items.first.product_variant.unit_amount + order.order_items.first.product_variant.tax_amount,
                  product_id: order.order_items.first.product_variant.product.id,
                  product_variant_id: order.order_items.first.product_variant.id,
                  quantity: quantity,
                  received_amount: "10",
                }
              ],
              parent_order: order.uuid,
              supplier_id: supplier.id
            }
          }

          # check response
          expect(JSON.parse(response.body)["message"]).to eq("Insufficient stock was available, for the reorder. A credit note has been created for the outstanding products")
          expect(JSON.parse(response.body)["type"]).to eq(true)

          # check the last credit note fields | should match orders balance and credit note items
          expect(CreditNote.last.balance).to eq(order.order_items.map{|oi| (oi.quantity - oi.received_amount) * oi.gross_price}.sum)
          expect(CreditNote.last.balance).to eq(CreditNote.last.credit_note_items.map{|cni| (cni.quantity * cni.gross_price) }.sum)
          expect(CreditNote.last.remaining_balance).to eq(0)

          # check original order
          expect(order.reload.value).to eq(order.order_items.map{|oi| (oi.quantity - oi.received_amount) * oi.gross_price}.sum)
          expect(order.reload.is_paid).to eq("true")
          expect(Order.last.id).to eq(order.id)
          expect(Order.last.balance).to eq(0)
          expect(Order.last.is_paid).to eq("true")
          expect(Order.last.value).to eq(gross_price * quantity)
          expect(Order.last.notes ).to eq(order.notes)
          expect(Order.last.delivery_date ).to eq(order.delivery_date)

          #check credit note claims
          expect(CreditNoteClaim.last.order_id).to eq(order.id)
          expect(CreditNoteClaim.last.credit_note_id).to eq(CreditNote.last.id)
          expect(CreditNoteClaim.last.value).to eq(gross_price * quantity)

          assert_response :success
        end
      end

      context 'unsuccessfully creates credit note with order' do
        let!(:order_two) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }
        let!(:order_item_two) { create(:order_item, order: order, product: product, product_variant: product_variant) }
        let!(:product_variant_two) { create(:product_variant, :with_stock, product: product) }
        let!(:order_item_two) { create(:order_item, order: order, product: product, product_variant: product_variant_two) }

        it 'tries to create a new order and a credit note but fails to create an order due to insufficient stock & allocates to prior invoices due
            to parent order being paid. The created credit note only gets partially distrubutes due to insuffecient payable orders & as a result has a remaining balance' do

          order.update(value: order.order_items.map{|oi| oi.quantity  * oi.gross_price}.sum)
          order.update(status: 2)
          order.update(is_paid: "true")

          order.order_items.first.product_variant.update(available_stock: 0)
          quantity = 10
          gross_price = 100
          post :order_with_credit_note, params: { 
            order: {
              customer_id: customer.id,
              delivery_date: Date.tomorrow,
              notes: "New Order with Credit Note",
              delivery_address_id: address.id,
              order_items_attributes: [
                { 
                  company_id: supplier.id,
                  gross_price: gross_price,
                  net_price: order.order_items.first.product_variant.unit_amount + order.order_items.first.product_variant.tax_amount,
                  product_id: order.order_items.first.product_variant.product.id,
                  product_variant_id: order.order_items.first.product_variant.id,
                  quantity: quantity,
                  received_amount: "10",
                },
                { 
                  company_id: supplier.id,
                  gross_price: gross_price,
                  net_price: product_variant_two.unit_amount + product_variant_two.tax_amount,
                  product_id: product_variant_two.product.id,
                  product_variant_id: product_variant_two.id,
                  quantity: quantity,
                  received_amount: "10",
                }
              ],
              parent_order: order.uuid,
              supplier_id: supplier.id
            }
          }

          # check response
          expect(JSON.parse(response.body)["message"]).to eq("Insufficient stock was available, for the reorder. A credit note has been created for the outstanding products")
          expect(JSON.parse(response.body)["type"]).to eq(true)

          # check the last credit note fields | should match orders balance and credit note items
          expect(CreditNote.last.balance).to eq((quantity * gross_price) * 2) # should be 2000
          expect(CreditNote.last.balance).to eq(CreditNote.last.credit_note_items.map{|cni| (cni.quantity * cni.gross_price) }.sum)
          expect(CreditNote.last.remaining_balance).to eq(1000)

          # check original order
          expect(order.reload.value).to eq(order.order_items.map{|oi| (oi.quantity - oi.received_amount) * oi.gross_price}.sum)
          expect(order.reload.is_paid).to eq("true")
          expect(order).to eq(Order.first)

          expect(Order.last.id).to eq(order_two.id)
          expect(order_two.reload.balance).to eq(0)
          expect(order_two.reload.is_paid).to eq("true")
          expect(order_two.reload.value).to eq(1000)
          expect(Order.last.balance).to eq(0)
          expect(Order.last.is_paid).to eq("true")
          expect(Order.last.value).to eq(gross_price * quantity) # 1000 is the default order value in the factory
          expect(Order.last.notes ).to eq(order_two.notes)
          expect(Order.last.delivery_date ).to eq(order_two.delivery_date)

          #check credit note claims
          expect(CreditNoteClaim.last.order_id).to eq(order_two.id)
          expect(CreditNoteClaim.last.credit_note_id).to eq(CreditNote.last.id)
          expect(CreditNoteClaim.last.value).to eq(gross_price * quantity)

          assert_response :success
        end
      end

      context 'unsuccessfully creates credit note with order' do
        let!(:product_variant_two) { create(:product_variant, :with_stock, product: product) }
        let!(:product_variant_three) { create(:product_variant, :with_stock, product: product) }
        let!(:order_two) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }
        let!(:order_three) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }
        let!(:order_item_two) { create(:order_item, order: order, product: product, product_variant: product_variant_two) }
        let!(:order_item_three) { create(:order_item, order: order, product: product, product_variant: product_variant) }
      
        it 'tries to create a new order and a credit note but fails to create an order due to insufficient stock & allocates to prior invoices due
            to parent order being paid. The created credit note gets fully distributed with no left over credit' do
      
          order.update(value: order.order_items.map{|oi| oi.quantity  * oi.gross_price}.sum)
          order.update(status: 2)
          order.update(is_paid: "true")
          order_two.update(value: 2000, balance: 2000)
      
          order.order_items.first.product_variant.update(available_stock: 0)
          quantity = 10
          gross_price = 100
          post :order_with_credit_note, params: { 
            order: {
              customer_id: customer.id,
              delivery_date: Date.tomorrow,
              notes: "New Order with Credit Note",
              delivery_address_id: address.id,
              order_items_attributes: [
                { 
                  company_id: supplier.id,
                  gross_price: gross_price,
                  net_price: order.order_items.first.product_variant.unit_amount + order.order_items.first.product_variant.tax_amount,
                  product_id: order.order_items.first.product_variant.product.id,
                  product_variant_id: order.order_items.first.product_variant.id,
                  quantity: quantity,
                  received_amount: "10",
                },
                { 
                  company_id: supplier.id,
                  gross_price: gross_price,
                  net_price: product_variant_two.unit_amount + product_variant_two.tax_amount,
                  product_id: product_variant_two.product.id,
                  product_variant_id: product_variant_two.id,
                  quantity: quantity,
                  received_amount: "10",
                },
                { 
                  company_id: supplier.id,
                  gross_price: gross_price,
                  net_price: product_variant_three.unit_amount + product_variant_three.tax_amount,
                  product_id: product_variant_three.product.id,
                  product_variant_id: product_variant_three.id,
                  quantity: quantity,
                  received_amount: "10",
                }
              ],
              parent_order: order.uuid,
              supplier_id: supplier.id
            }
          }
          
          # check response
          expect(JSON.parse(response.body)["message"]).to eq("Insufficient stock was available, for the reorder. A credit note has been created for the outstanding products")
          expect(JSON.parse(response.body)["type"]).to eq(true)
      
          # check the last credit note fields | should match orders balance and credit note items
          expect(CreditNote.last.balance).to eq((quantity * gross_price) * 3) # should be 2000
          expect(CreditNote.last.balance).to eq(CreditNote.last.credit_note_items.map{|cni| (cni.quantity * cni.gross_price) }.sum)
          expect(CreditNote.last.remaining_balance).to eq(0)
      
          # check original order
          expect(order.reload.value).to eq(order.order_items.map{|oi| (oi.quantity - oi.received_amount) * oi.gross_price}.sum)
          expect(order.reload.is_paid).to eq("true")
          expect(order).to eq(Order.first)
      
          expect(Order.last.id).to eq(order_three.id)

          expect(order_two.reload.balance).to eq(0)
          expect(order_two.reload.is_paid).to eq("true")
          expect(order_two.reload.value).to eq(2000)

          expect(order_three.reload.balance).to eq(0)
          expect(order_three.reload.is_paid).to eq("true")
          expect(order_three.reload.value).to eq(1000)

          expect(Order.last.balance).to eq(0)
          expect(Order.last.is_paid).to eq("true")
          expect(Order.last.value).to eq(gross_price * quantity) # 1000 is the default order value in the factory
          expect(Order.last.notes ).to eq(order_three.notes)
          expect(Order.last.delivery_date ).to eq(order_three.delivery_date)
          
      
          #check credit note claims
          expect(CreditNote.first.orders.first.id).to eq(order_two.id)
          expect(CreditNoteClaim.first.value).to eq(2000)
          expect(CreditNoteClaim.first.credit_note_id).to eq(CreditNote.last.id)
          expect(CreditNoteClaim.first.value).to eq((gross_price * quantity) * 2)
          
          expect(CreditNoteClaim.second.value).to eq(1000)
          expect(CreditNoteClaim.last.order_id).to eq(order_three.id)
          expect(CreditNoteClaim.last.credit_note_id).to eq(CreditNote.last.id)
          expect(CreditNoteClaim.last.value).to eq(gross_price * quantity)
      
          assert_response :success
        end
      end

      context 'unsuccessfully creates credit note with order' do
        let!(:product_variant_two) { create(:product_variant, :with_stock, product: product) }
        let!(:product_variant_three) { create(:product_variant, :with_stock, product: product) }
        let!(:order_two) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }
        let!(:order_three) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }
        let!(:order_four) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }
        let!(:order_five) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }

        let!(:order_item_two) { create(:order_item, order: order, product: product, product_variant: product_variant_two) }
        let!(:order_item_three) { create(:order_item, order: order, product: product, product_variant: product_variant) }
      
        it 'tries to create a new order and a credit note but fails to create an order due to insufficient stock & allocates to prior invoices due
            to parent order being paid. The credit note gets fully distrubted with the second last order being partially paid and last order unaffected' do
      
          order.update(value: order.order_items.map{|oi| oi.quantity  * oi.gross_price}.sum)
          order.update(balance: 0)
          order.update(status: 2)
          order.update(is_paid: "true")
          order_four.update(value: 1500, balance: 1500)
      
          order.order_items.first.product_variant.update(available_stock: 0)
          quantity = 10
          gross_price = 100
          post :order_with_credit_note, params: { 
            order: {
              customer_id: customer.id,
              delivery_date: Date.tomorrow,
              notes: "New Order with Credit Note",
              delivery_address_id: address.id,
              order_items_attributes: [
                { 
                  company_id: supplier.id,
                  gross_price: gross_price,
                  net_price: order.order_items.first.product_variant.unit_amount + order.order_items.first.product_variant.tax_amount,
                  product_id: order.order_items.first.product_variant.product.id,
                  product_variant_id: order.order_items.first.product_variant.id,
                  quantity: quantity,
                  received_amount: "10",
                },
                { 
                  company_id: supplier.id,
                  gross_price: gross_price,
                  net_price: product_variant_two.unit_amount + product_variant_two.tax_amount,
                  product_id: product_variant_two.product.id,
                  product_variant_id: product_variant_two.id,
                  quantity: quantity,
                  received_amount: "10",
                },
                { 
                  company_id: supplier.id,
                  gross_price: gross_price,
                  net_price: product_variant_three.unit_amount + product_variant_three.tax_amount,
                  product_id: product_variant_three.product.id,
                  product_variant_id: product_variant_three.id,
                  quantity: quantity,
                  received_amount: "10",
                }
              ],
              parent_order: order.uuid,
              supplier_id: supplier.id
            }
          }
        
          # check response
          expect(JSON.parse(response.body)["message"]).to eq("Insufficient stock was available, for the reorder. A credit note has been created for the outstanding products")
          expect(JSON.parse(response.body)["type"]).to eq(true)
      
          # check the last credit note fields | should match orders balance and credit note items
          expect(CreditNote.last.balance).to eq((quantity * gross_price) * 3) # should be 2000
          expect(CreditNote.last.balance).to eq(CreditNote.last.credit_note_items.map{|cni| (cni.quantity * cni.gross_price) }.sum)
          expect(CreditNote.last.remaining_balance).to eq(0)
      
          # check original order
          expect(order.reload.value).to eq(order.order_items.map{|oi| (oi.quantity - oi.received_amount) * oi.gross_price}.sum)
          expect(order.reload.is_paid).to eq("true")
          expect(order).to eq(Order.first)
          expect(order.reload.balance).to eq(0)
          expect(Order.last.id).to eq(order_five.id)
      

          expect(order_two.reload.balance).to eq(0)
          expect(order_two.reload.is_paid).to eq("true")
          expect(order_two.reload.value).to eq(1000)

          expect(order_three.reload.balance).to eq(0)
          expect(order_three.reload.is_paid).to eq("true")
          expect(order_three.reload.value).to eq(1000)

          expect(order_four.reload.balance).to eq(500)
          expect(order_four.reload.is_paid).to eq("partial")
          expect(order_four.reload.value).to eq(1500)

          expect(order_five.reload.balance).to eq(1000)
          expect(order_five.reload.is_paid).to eq("false")
          expect(order_five.reload.value).to eq(1000)

          expect(Order.last.balance).to eq(1000)
          expect(Order.last.is_paid).to eq("false")
          expect(Order.last.value).to eq(gross_price * quantity) # 1000 is the default order value in the factory
          expect(Order.last.notes ).to eq(order_five.notes)
          expect(Order.last.delivery_date ).to eq(order_five.delivery_date)
          
      
          #check credit note claims
          expect(CreditNote.last.orders.first.id).to eq(order_two.id)
          expect(CreditNoteClaim.first.value).to eq(1000)
          expect(CreditNoteClaim.first.credit_note_id).to eq(CreditNote.last.id)
          expect(CreditNoteClaim.first.order_id).to eq(order_two.id)


          expect(CreditNote.last.orders.second.id).to eq(order_three.id)
          expect(CreditNoteClaim.second.value).to eq(1000)
          expect(CreditNoteClaim.second.order_id).to eq(order_three.id)
          expect(CreditNoteClaim.second.credit_note_id).to eq(CreditNote.first.id)


          expect(CreditNote.last.orders.third.id).to eq(order_four.id)
          expect(CreditNoteClaim.third.value).to eq(1000)
          expect(CreditNoteClaim.third.order_id).to eq(order_four.id)
          expect(CreditNoteClaim.third.credit_note_id).to eq(CreditNote.first.id)

          assert_response :success
        end
      end


    end
  end

  describe "Fail Create order and only create credit note" do
    before { sign_in_as(user) }
    it 'create a new order with a credit note' do
      post :order_with_credit_note, params: { 
        order: {
          customer_id: customer.id,
          delivery_date: Date.tomorrow,
          notes: "New Order with Credit Note",
          order_items_attributes: [
            { 
              company_id: supplier.id,
              gross_price: 100,
              net_price: 100,
              product_id: product.id,
              product_title: "Beef Sirloin Roast (200g)",
              product_variant_id: product_variant_no_stock.id,
              quantity: 10,
              received: "10",
              vat_price: 0
            }
          ],
          parent_order: order.uuid,
          delivery_address_id: address.id,
          status: "paid",
          supplier_id: supplier.id
        }
      }

      expect(JSON.parse(response.body)["type"]).to eq(true)
      expect(JSON.parse(response.body)["message"]).to eq("Insufficient stock was available, for the reorder. A credit note has been created for the outstanding products")
      expect(CreditNote.last.order_id ).to eq(order.id)
      expect(CreditNote.last.supplier_id ).to eq(supplier.id)
      expect(CreditNote.last.balance ).to eq(1000)
    end
  end


  describe "Create a weekly schedule" do
    before { sign_in_as(user) }
    it 'create weekly monday order' do
      post :create_order_schedule, params: { 
        order_uuid: order.uuid, frequency: { end_date: Date.today + 1.month, frequency: "weekly", order_id: order.id, start_date: Date.today, value: "Monday" }
      }

      expect(OrderSchedule.last.next_date).to eq(Date.today.next_occurring(:monday))
      expect(OrderSchedule.last.frequency).to eq("weekly")
    end
    it 'create weekly tuesday order' do
      post :create_order_schedule, params: { 
        order_uuid: order.uuid, frequency: { end_date: Date.today + 1.month, frequency: "weekly", order_id: order.id, start_date: Date.today, value: "Saturday" }
      }

      expect(OrderSchedule.last.next_date).to eq(Date.today.next_occurring(:saturday))
      expect(OrderSchedule.last.frequency).to eq("weekly")
    end
    it 'create weekly wednesday order' do
      post :create_order_schedule, params: { 
        order_uuid: order.uuid, frequency: { end_date: Date.today + 1.month, frequency: "weekly", order_id: order.id, start_date: Date.today, value: "Thursday" }
      }

      expect(OrderSchedule.last.next_date).to eq(Date.today.next_occurring(:thursday))
      expect(OrderSchedule.last.frequency).to eq("weekly")
    end
  end

  describe "Create a daily schedule" do
    before { sign_in_as(user) }
    it 'create daily order' do
      post :create_order_schedule, params: { 
        order_uuid: order.uuid, frequency: { end_date: Date.today + 1.month, frequency: "daily", order_id: order.id, start_date: Date.tomorrow, value: nil }
      }

      expect(OrderSchedule.last.next_date).to eq(Date.tomorrow)
      expect(OrderSchedule.last.frequency).to eq("daily")
    end
  end

  describe "Create a monthly schedule" do
    before { sign_in_as(user) }
    it 'creates a schedule on the 2nd of the month' do
      day_of_month = 2
      
      post :create_order_schedule, params: { 
        order_uuid: order.uuid, frequency: { end_date: Date.today + 1.month, frequency: "monthly", order_id: order.id, start_date: Date.tomorrow, value: day_of_month }
      }

      if Date.today.day >= day_of_month
        expect(OrderSchedule.last.next_date.month).to eq((Date.today.end_of_month + 2.days).month)
        expect(OrderSchedule.last.next_date.day).to eq((Date.today.end_of_month + 2.days).day)
      else
        expect(OrderSchedule.last.next_date.month).to eq((Date.today.beginning_of_month + 1.days).month)
        expect(OrderSchedule.last.next_date.day).to eq((Date.today.beginning_of_month + 1.days).day)
      end
      expect(OrderSchedule.last.frequency).to eq("monthly")
    end
  end

  describe "Create a duplicate order" do
    before { sign_in_as(user) }
    it 'create duplicate order' do
      post :duplicate_order, params: { id: order.id }

      expect(Order.last.customer_id).to eq(order.customer_id)
      expect(Order.last.supplier_id).to eq(order.supplier_id)
      expect(Order.last.status).to eq("pending")
      expect(Order.last.is_paid).to eq("false")
    end
  end

  describe "POST / update_delivery_driver_and_date" do
    let!(:order_temp) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }
    let!(:delivery_allocation) { create(:delivery_allocation, order: order_temp, user: user) }
    let!(:customer) { create(:company) }
    let!(:address) { create(:address) }
    let!(:custom_delivery_two) { create(:custom_delivery, :delivered, delivery_driver: user, supplier: supplier, customer: company, address: address) }
    let!(:custom_delivery) { create(:custom_delivery, delivery_driver: user, supplier: supplier, customer: company, address: address) }
    let(:user_two) { member_two.user }
    let!(:member) { create(:member, company: supplier) }
    
    before { sign_in_as(user) }
    it 'updates an orders delivery_driver_and_date' do
      post :update_delivery_driver_and_date, params: {
        order_id: order_temp.id,
        type: 'orderbly', 
        route: '/upcoming_deliveries',
        date: DateTime.now.end_of_day,
        driver_id: user_two.id,
        supplier_id: supplier.id
      }
      
      expect(response.status).to eq(200)
      expect(order_temp.reload.delivery_date.day).to eq(DateTime.now.end_of_day.day)
      expect(order_temp.reload.delivery_date.month).to eq(DateTime.now.end_of_day.month)
      expect(order_temp.reload.delivery_date.year).to eq(DateTime.now.end_of_day.year)
      expect(order_temp.reload.delivery_assignees.first.id).to eq(user_two.id)

      expect(JSON.parse(response.body).keys.include? "orders").to eq(true)
      expect(JSON.parse(response.body).keys.include? "customers").to eq(true)
      expect(JSON.parse(response.body).keys.include? "order_dates").to eq(true)
      expect(JSON.parse(response.body).keys.include? "order_statuses").to eq(true)

    end

    it 'updates an orders delivery_driver_and_date' do
      post :update_delivery_driver_and_date, params: {
        order_id: custom_delivery.id,
        type: '', 
        route: '/upcoming_deliveries',
        date: DateTime.now.end_of_week,
        driver_id: user_two.id,
        supplier_id: supplier.id
      }

      expect(response.status).to eq(200)
      expect(custom_delivery.reload.due_date.day).to eq(DateTime.now.end_of_week.day)
      expect(custom_delivery.reload.due_date.month).to eq(DateTime.now.end_of_week.month)
      expect(custom_delivery.reload.due_date.year).to eq(DateTime.now.end_of_week.year)
      expect(custom_delivery.reload.delivery_driver.id).to eq(user_two.id)

      expect(JSON.parse(response.body).keys.include? "orders").to eq(true)
      expect(JSON.parse(response.body).keys.include? "customers").to eq(true)
      expect(JSON.parse(response.body).keys.include? "order_dates").to eq(true)
      expect(JSON.parse(response.body).keys.include? "order_statuses").to eq(true)

    end
  end



end


