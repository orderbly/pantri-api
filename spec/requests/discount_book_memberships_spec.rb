require 'rails_helper'

RSpec.describe DiscountBookMembershipsController, type: :controller do
  let(:headers) { {} }
  let(:supplier) { create(:company) }
  let(:user) { create(:user) }
  let(:member) { create(:member, company: supplier, user: user) }
  let(:discount_book) { create(:discount_book, supplier: supplier)}
  let!(:company_two) { create(:company, title: "c2") }
  let!(:membership) {create(:discount_book_membership, customer: company_two, discount_book: discount_book)}
  let!(:company_three) { create(:company, title: "c3") }
  let!(:company_four) { create(:company, title: "c4") }

  describe 'GET memberships_by_discount_book' do
    before { sign_in_as(user) }
    it "retrieves all discount book memberships relating to current company" do
      get :memberships_by_discount_book, params: {discount_book_id: discount_book.id}
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)[0]['customer']).to eq(company_two.title)
      expect(JSON.parse(response.body)[0]['discount_book_id']).to eq(discount_book.id)
    end
  end

  describe 'POST create' do
    before { sign_in_as(user) }
    it "creates a discount book membership where company two is a member of a discount book owned by supplier." do
        post :create, params: {customer_ids: [company_three.id], discount_book_id: discount_book.id}
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)['all_added']).to eq(true)
        expect(JSON.parse(response.body)['memberships'][0]['customer']).to eq(company_three.title)
        expect(JSON.parse(response.body)['memberships'][0]['discount_book_id'].to_i).to eq(discount_book.id)
    end

    it "creates a discount book memberships where company three and company four are members of a discount book owned by supplier." do
        post :create, params: {customer_ids: [company_three.id, company_four.id], discount_book_id: discount_book.id}
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)['all_added']).to eq(true)
        expect(JSON.parse(response.body)['memberships'][0]['customer']).to eq(company_three.title)
        expect(JSON.parse(response.body)['memberships'][0]['discount_book_id'].to_i).to eq(discount_book.id)
        expect(JSON.parse(response.body)['memberships'][1]['customer']).to eq(company_four.title)
        expect(JSON.parse(response.body)['memberships'][1]['discount_book_id'].to_i).to eq(discount_book.id)
    end

    it "creates one discount book membership even though two company ids are passed through." do
        post :create, params: {customer_ids: [company_two.id, company_three.id], discount_book_id: discount_book.id}
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)['all_added']).to eq(false)
        expect(JSON.parse(response.body)['memberships'][0]['customer']).to eq(company_three.title)
        expect(JSON.parse(response.body)['memberships'][0]['discount_book_id'].to_i).to eq(discount_book.id)
    end
  end

  describe 'DELETE destroy' do
    before { sign_in_as(user) }
    it "deletes a discount book membership" do
        delete :destroy, params: {id: membership.id}
        expect(response).to have_http_status(204)
    end

    it "deletes a discount book membership" do
        delete :destroy, params: {id: (membership.id + 100)}
        expect(response).to have_http_status(404)
        expect(JSON.parse(response.body)['error']).to eq("Not found")
    end
  end

end
