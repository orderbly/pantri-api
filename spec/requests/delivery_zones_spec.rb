require 'rails_helper'

RSpec.describe DeliveryZonesController, type: :controller do
  let(:headers) { {} }
  # company setup
  let!(:company) { create(:company) }
  let!(:customer) { create(:company) }
  let(:member) { create(:member, company: company) }
  let(:user) { member.user }
  
  # address setup
  let(:cape_town_address) { create(:address, :cape_town, company: company) }
  let(:johannesburg_address) { create(:address, :johannesburg_address, company: customer) }

  let!(:delivery_zone) { create(:delivery_zone, :same_day_cutoff, company: company) }
  let!(:delivery_zone_two) { create(:delivery_zone, :same_day_cutoff, company: company) }
  let!(:delivery_zone_fee) { create(:delivery_zone_fee, delivery_zone: delivery_zone_two ) }
  let!(:area) { create(:area) }

  # Create a delivery zone
  describe 'Post delivery_zones' do
    before { sign_in_as(user) }
    it "successfully creates a new delivery zone bracket with same day cut off" do
      post :create, params: {
        delivery_zone: 
        { name:
          "Zone G",
          company_id: company.id,
          delivery_days: ["Tuesday","Wednesday"],
          cut_off_time: "08:00",
          cut_off_day: 0
        }
      }

      expect(response).to have_http_status(201)
      expect(JSON.parse(response.body)["company_id"]).to eq(company.id)
      expect(JSON.parse(response.body)["cut_off_time"]).to eq("08:00")
      expect(JSON.parse(response.body)["delivery_days"]).to eq(["Tuesday","Wednesday"])
      expect(JSON.parse(response.body)["zone_cut_off_day"]).to eq('Same day')
    end

    it "successfully creates a new delivery zone bracket previous day cut off" do
      post :create, params: {
        delivery_zone: 
        { name:
          "Zone G",
          company_id: company.id,
          delivery_days: ["Tuesday","Wednesday"],
          cut_off_time: "08:00",
          cut_off_day: 1
        }
      }
      
      expect(response).to have_http_status(201)
      expect(JSON.parse(response.body)["company_id"]).to eq(company.id)
      expect(JSON.parse(response.body)["cut_off_time"]).to eq("08:00")
      expect(JSON.parse(response.body)["delivery_days"]).to eq(["Tuesday","Wednesday"])
      expect(JSON.parse(response.body)["zone_cut_off_day"]).to eq('Previous day')
    end

    it "successfully creates a new delivery zone bracket 2 days prior cut off" do
      post :create, params: {
        delivery_zone: 
        { name:
          "Zone G",
          company_id: company.id,
          delivery_days: ["Tuesday","Wednesday"],
          cut_off_time: "08:00",
          cut_off_day: 2
        }
      }
      
      expect(response).to have_http_status(201)
      expect(JSON.parse(response.body)["company_id"]).to eq(company.id)
      expect(JSON.parse(response.body)["cut_off_time"]).to eq("08:00")
      expect(JSON.parse(response.body)["delivery_days"]).to eq(["Tuesday","Wednesday"])
      expect(JSON.parse(response.body)["zone_cut_off_day"]).to eq('Two days prior')
    end
    
    it "successfully creates a new delivery zone bracket 2 days prior cut off" do
      post :create, params: {
        delivery_zone: 
        { name:
          "Zone G",
          company_id: company.id,
          delivery_days: ["Tuesday","Wednesday"],
          cut_off_time: "08:00",
          cut_off_day: 3
        }
      }
      
      expect(response).to have_http_status(201)
      expect(JSON.parse(response.body)["company_id"]).to eq(company.id)
      expect(JSON.parse(response.body)["cut_off_time"]).to eq("08:00")
      expect(JSON.parse(response.body)["delivery_days"]).to eq(["Tuesday","Wednesday"])
      expect(JSON.parse(response.body)["zone_cut_off_day"]).to eq('Three days prior')
    end
  end

  # Create a delivery zone area fee

  describe 'Post add_delivery_zone_fees' do
    before { sign_in_as(user) }
    it "successfully creates a new delivery zone fee" do
      post :add_delivery_zone_fees, params: {
        delivery_zone_uuid: delivery_zone.uuid,
        name: "Super Buyers",
        price: "100",
        value_max: "100",
        value_min: "0"
      }

      expect(response).to have_http_status(200)
      # test delivery zone params
      expect(JSON.parse(response.body)["delivery_zone"]["id"]).to eq(delivery_zone.id)
      expect(JSON.parse(response.body)["delivery_zone"]["delivery_days"]).to eq(delivery_zone.delivery_days)
      expect(JSON.parse(response.body)["delivery_zone"]["cut_off_day"]).to eq(delivery_zone.cut_off_day)
      expect(JSON.parse(response.body)["delivery_zone"]["cut_off_time"]).to eq(delivery_zone.cut_off_time)
      
      # test delivery zone fee params
      expect(JSON.parse(response.body)["delivery_zone_fees"][0]["name"]).to eq("Super Buyers")
      expect(JSON.parse(response.body)["delivery_zone_fees"][0]["price"]).to eq("100.0")
      expect(JSON.parse(response.body)["delivery_zone_fees"][0]["value_max"]).to eq("100.0")
      expect(JSON.parse(response.body)["delivery_zone_fees"][0]["value_min"]).to eq("0.0")
    end

    it "fails to create a new delivery zone fee based on existing brackets" do
      # DeliveryZoneFee.create(delivery_zone_id: delivery_zone_two.id, name: "Super Buyers", price: "100", value_max: "100", value_min: "0")
      post :add_delivery_zone_fees, params: {
        delivery_zone_uuid: delivery_zone_two.uuid,
        name: "Super Buyers",
        price: "100",
        value_max: "100",
        value_min: "0"
      }

      expect(response).to have_http_status(422)
      expect(JSON.parse(response.body)["table"]["obj"]["message"]).to eq("The fee bracket could not be created. Please ensure there are no overlaps in minimum and maximum values.")
    end
  end


  describe 'Post add_areas_to_delivery_zone' do
    before { sign_in_as(user) }
    it "successfully creates a new delivery zone area" do
      post :add_areas_to_delivery_zone, params: {
        delivery_zone_uuid: delivery_zone.uuid,
        delivery_areas: [area.id]
      }

      # test delivery zone params
      expect(JSON.parse(response.body)["delivery_zone"]["id"]).to eq(delivery_zone.id)
      expect(JSON.parse(response.body)["delivery_zone"]["delivery_days"]).to eq(delivery_zone.delivery_days)
      expect(JSON.parse(response.body)["delivery_zone"]["cut_off_day"]).to eq(delivery_zone.cut_off_day)
      expect(JSON.parse(response.body)["delivery_zone"]["cut_off_time"]).to eq(delivery_zone.cut_off_time)
      
      # test delivery zone areas
      expect(JSON.parse(response.body)["delivery_zone_areas"][0]["delivery_zone_id"]).to eq(delivery_zone.id)
      expect(JSON.parse(response.body)["delivery_zone_areas"][0]["name"]).to eq(area.name)
      expect(JSON.parse(response.body)["delivery_zone_areas"][0]["street_code"]).to eq(area.street_code)
      expect(JSON.parse(response.body)["delivery_zone_areas"][0]["postal_code"]).to eq(area.postal_code)
      expect(JSON.parse(response.body)["delivery_zone_areas"][0]["company_id"]).to eq(delivery_zone.company_id)
      expect(JSON.parse(response.body)["delivery_zone_areas"][0]["province"]).to eq(area.province)
    end

    it "fails to create a new delivery zone area based on existing area" do
      DeliveryZoneArea.create(delivery_zone_id: delivery_zone_two.id, company_id: delivery_zone_two.company_id, name: area.name, street_code: area.street_code, postal_code: area.postal_code, province: area.postal_code)
      post :add_areas_to_delivery_zone, params: {
        delivery_zone_uuid: delivery_zone.uuid,
        delivery_areas: [area.id]
      }

      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)["delivery_zone_areas"]).to eq([])
    end
  end

  #delivery_fee/:address_id/:quantity/:price/:supplier_id
  describe "Get delivery_fee/" do
    before { sign_in_as(user) }
    context 'Delivery Fee Based on Value' do

      let!(:supplier) { create(:company) }
      let!(:delivery_zone) { create(:delivery_zone, :same_day_cutoff, company: supplier) }
      let!(:zero_to_hundred_fee) { create(:delivery_zone_fee, :zero_to_hundred_value, :two_hundred_rand_fee, delivery_zone: delivery_zone) }
      let!(:delivery_zone_area) { create(:delivery_zone_area, :cape_town, delivery_zone: delivery_zone, company: supplier ) }
      
      let!(:supplier_with_quantity_pricing) { create(:company) }
      let!(:delivery_zone) { create(:delivery_zone, :same_day_cutoff, company: supplier) }
      let!(:delivery_zone_area) { create(:delivery_zone_area, :cape_town, delivery_zone: delivery_zone, company: supplier ) }
      let!(:zero_to_ten_quantity) { create(:delivery_zone_fee, :zero_to_ten_quantity, :two_hundred_rand_fee, delivery_zone: delivery_zone) }

      it "returns a delivery fee based on value and area (Cape Town) and the value of 100" do
        get :delivery_fee, params: {
          address_id: cape_town_address.id,
          quantity: 0,
          price: 50,
          supplier_id: supplier.id
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["delivery_fee"]).to eq(200)
      end

      it "returns a delivery fee based on the area (Cape Town) and the value of 0" do
        get :delivery_fee, params: {
          address_id: cape_town_address.id,
          quantity: 0,
          price: 2000,
          supplier_id: supplier.id
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["delivery_fee"]).to eq(0)
      end

      it "returns a delivery fee based on the area (Cape Town) and gets free shipping due to value" do
        get :delivery_fee, params: {
          address_id: cape_town_address.id,
          quantity: 0,
          price: 2000,
          supplier_id: supplier.id
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["delivery_fee"]).to eq(0)
      end
    end

    context 'Delivery Fee Based on Quantity' do
      let!(:supplier_with_quantity_pricing) { create(:company) }
      let!(:delivery_zone) { create(:delivery_zone, :same_day_cutoff, :quantity_based, company: supplier_with_quantity_pricing) }
      let!(:delivery_zone_area) { create(:delivery_zone_area, :cape_town, delivery_zone: delivery_zone, company: supplier_with_quantity_pricing ) }
      let!(:zero_to_ten_quantity) { create(:delivery_zone_fee, :zero_to_ten_quantity, :two_hundred_rand_fee, delivery_zone: delivery_zone) }

      it "returns a delivery fee based on quantity and area (Cape Town) and the value of 0" do
        get :delivery_fee, params: {
          address_id: cape_town_address.id,
          quantity: 10,
          price: 50,
          supplier_id: supplier_with_quantity_pricing.id
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["delivery_fee"]).to eq(200)
      end

      it "returns a delivery fee based on quantity and area (Cape Town) and expects on the value of 100" do
        get :delivery_fee, params: {
          address_id: cape_town_address.id,
          quantity: 21,
          price: 50,
          supplier_id: supplier_with_quantity_pricing.id
        }

        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["delivery_fee"]).to eq(0)
      end

    end
    context 'Get Disabled Delivery Dates' do
      let!(:supplier) { create(:company) }
      let!(:delivery_zone) { create(:delivery_zone, :same_day_cutoff, :weekday_delivery_days, :early_cut_off, :quantity_based, company: supplier) }
      let!(:delivery_zone_area) { create(:delivery_zone_area, :cape_town, delivery_zone: delivery_zone, company: supplier ) }
  
      it "returns a list of disabled dates based on weekday deliveries" do
        get :supplier_delivery_days, params: {
          delivery_address_id: cape_town_address.id,
          supplier_id: supplier.id
        }

        expect(response).to have_http_status(200)

        dates = JSON.parse(response.body)["dates"].map {|d| DateTime.parse(d).strftime("%A") }
        cutoff_time = DateTime.now > delivery_zone.cut_off_time.to_time

        if cutoff_time
          expect( dates.count("Saturday") ).to eq(2)
          expect(dates.count("Sunday")).to eq(2)
          expect( dates.count(DateTime.now.strftime("%A")) ).to eq(3)
        else 
          expect( dates.count("Saturday") ).to eq(2)
          expect( dates.count("Sunday") ).to eq(2)
        end
      end

      let!(:supplier) { create(:company) }
      let!(:delivery_zone) { create(:delivery_zone, :same_day_cutoff, :tuesday_and_thursday_delivery_days, :early_cut_off, :quantity_based, company: supplier) }
      let!(:delivery_zone_area) { create(:delivery_zone_area, :cape_town, delivery_zone: delivery_zone, company: supplier ) }
  
        it "returns a list of disabled dates based on tuesday and thursday deliveries with a 06:00 cutoff" do
          get :supplier_delivery_days, params: {
            delivery_address_id: cape_town_address.id,
            supplier_id: supplier.id
          }

          expect(response).to have_http_status(200)

          dates = JSON.parse(response.body)["dates"].map {|d| DateTime.parse(d).strftime("%A") }
          cutoff_time = DateTime.now > delivery_zone.cut_off_time.to_time

          if cutoff_time
            expect( dates.count("Saturday") ).to eq(2)
            expect(dates.count("Sunday")).to eq(2)
            expect( dates.count(DateTime.now.strftime("%A")) ).to eq(3)
          else 
            expect( dates.count("Saturday") ).to eq(2)
            expect( dates.count("Sunday") ).to eq(2)
            expect( dates.count(DateTime.now.strftime("%A")) ).to eq(1)
          end
        end
      end
  end


  # Test disabled dates & fee

  # describe "POST /delivery_zones" do
  #   it "works! (now write some real specs)" do
  #     get delivery_zones_path
  #     expect(response).to have_http_status(200)
  #   end
  # end

end
