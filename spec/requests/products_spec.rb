require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  let(:headers) { {} }
  let(:company) { create(:company) }
  let(:member) { create(:member, company: company) }
  let(:user) { member.user }

  describe 'GET /products' do
    before { sign_in_as(user) }
    context 'with authentication token' do

      it 'responds with 200 and an (empty) array of products' do
        get :index
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'without authentication token' do
    it 'responds with a 401' do
      get :index
      expect(response).to have_http_status(401)
    end
  end

  describe "POST create" do
    before { sign_in_as(user) }
    context 'with authentication token' do

      let!(:company) { create(:company) }
      let!(:tax) { create(:tax, company: company) }
      let!(:product) { create(:product, company: company, tax: tax) }
      let!(:product_variant) { create(:product_variant, product: product) }
      
      it 'should create a new product with product ' do
        new_company =  create(:company)
        post :create, params: {  product: { description: "High marbling Wagyu Beef Rump Steak. Marbling score of +6.", title: "Wagyu Beef Rump Steak", company_id: new_company.id, tax_id: tax.id, got_variants: true,
            product_variants_attributes: [{ description: "200g", unit_amount: 180.90, available_stock: 10 }]
          }
        }
        assert_response :success
      end
    end
  end

  describe "PUT update" do
    before { sign_in_as(user) }
    context 'with authentication token' do

      let!(:company) { create(:company) }
      let!(:tax) { create(:tax, company: company) }
      let!(:product) { create(:product, company: company, tax: tax) }
      let!(:product_variant) { create(:product_variant, product: product) }

      it 'should update an existing product title' do
        put :update, params: { id: product.id, 
          product: { id: product.id, title: 'My New Title', tax_id: tax.id, 
            product_variants_attributes: [
              {id: product_variant.id, product_id: product.id, description: "300g", available_stock: 1010, track_stock: false, unit_amount: 173.91, tax_amount: 26.09, gross_amount: 200}
            ]
          }
        }
        assert_response :success
        expect(Product.find(product.id).title).to eq('My New Title')
        expect(ProductVariant.find(product_variant.id).unit_amount).to eq(173.91)
        expect(ProductVariant.find(product_variant.id).tax_amount).to eq(26.09)
        expect(ProductVariant.find(product_variant.id).gross_amount).to eq(200)
      end
    end
  end

  describe "DELETE destroy" do
    before { sign_in_as(user) }
    context 'with authentication token' do
      let(:company) { create(:company) }
      let(:tax) { create(:tax, company: company) }
      let(:product) { create(:product, company: company, tax: tax) }
      let(:product_dup) { create(:product, company: company, tax: tax) }
      let(:product_variant) { create(:product_variant, product: product_dup) }

      it 'should delete an existing product' do
        delete :destroy, params: {id: product.id, product: { id: product.id } }
        assert_response :success
        expect(Product.where(id: product.id).count).to eq(0)
      end

      it 'should delete an existing product variant' do
        delete :destroy, params: {id: product.id, product: { product_variants_attributes: { id: product_variant.id, _destroy: true } } }
        assert_response :success
        expect(ProductVariant.where(id: product_variant.id).count).to eq(1)
      end

    end
  end

  describe 'Product tags' do
    before { sign_in_as(user) }
    context 'with authentication token' do
      let(:company) { create(:company) }
      let(:tax) { create(:tax, company: company) }
      let(:tag) { create(:tag, company: company) }
      let(:product) { create(:product, company: company, tax: tax) }

      it 'should assign a tag to a product' do
        put :product_add_tag, params: { id: product.id, product: { id: product.id, tags: [tag.id] } }
        assert_response :success
        expect(Product.find(product.id).tags.count).to eq(1)
      end

      it 'should remove a tag from a product' do
        put :product_add_tag, params: {id: product.id, product: { id: product.id, tags: [tag.id] } }

        delete :product_delete_tag, params: { id: product.id, tag_id: tag.id,  product: { tag_id: tag.id } }
        assert_response :success
        expect(Product.find(product.id).tags.count).to eq(0)
      end

    end
  end

  describe 'Get exclusive products' do
    context "creating exlusive products" do
      let!(:supplier) { create(:company) }
      let!(:customer) { create(:company) }
      let!(:customer_two) { create(:company) }
      let!(:tax) { create(:tax, company: supplier) }
      let!(:product) { create(:product, company: supplier, tax: tax, company: supplier) }
      let!(:product_two) { create(:product, company: supplier, tax: tax, company: supplier) }
      let!(:product_variant) { create(:product_variant, :with_stock, product: product) }
      let!(:product_variant_two) { create(:product_variant, :with_stock, product: product_two) }
      before { sign_in_as(user) }
      
      it 'should get two products, one with an exclusive relation' do
        product.update(exclusive_companies: [customer.uuid])
        user.update(current_company: customer.id)
        get :company_products, params: {title: supplier.title}

        expect(JSON.parse(response.body).length).to eq(2)
        expect(JSON.parse(response.body).first['product']['id']).to eq(product_two.id)
        expect(JSON.parse(response.body).second['product']['id']).to eq(product.id)
      end
      
      it 'should get a single product without an exclusive relation' do
        product.update(exclusive_companies: [customer.uuid])
        user.update(current_company: customer_two.id)
        get :company_products, params: {title: supplier.title}

        expect(JSON.parse(response.body).length).to eq(1)
        expect(JSON.parse(response.body).first['product']['id']).to eq(product_two.id)
        expect(JSON.parse(response.body).length).to eq(1)
      end
    end
  end

end
