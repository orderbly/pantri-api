require 'rails_helper'

RSpec.describe CompaniesController, type: :controller do
  let(:headers) { {} }
  let(:company) { create(:company) }
  let(:member) { create(:member, company: company) }
  let(:user) { member.user }

  describe 'POST / add_emails' do
    before { sign_in_as(user) }
    context 'with authentication token' do

      it 'adds a new email to the company emailing list', redis: true do
        post :add_emails, params: { company_id: company.id, email: "test@test.com" }
        
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["obj"]["email_list"]).to include('test@test.com')
        expect(company.reload.email_list).to include('test@test.com')
      end
      
      it 'adds fails to create an existing email', redis: true do
        post :add_emails, params: { company_id: company.id, email: company.email_list[0] }
        
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["message"]).to eq("This email is already on this company's email list")
        expect(company.reload.email_list.length).to eq(3)
      end

      it 'deletes an existing email', redis: true do
        post :delete_email, params: { company_id: company.id, email: company.email_list[0] }
        
        expect(response).to have_http_status(200)
        expect(JSON.parse(response.body)["obj"]["email_list"].length).to eq(2)
        expect(company.reload.email_list.length).to eq(2)
      end
    end
  end

  describe 'GET / minimum order value' do
    before { sign_in_as(user) }
    it 'should be able to retrieve minimum order value' do
        get :fetch_minimum_order_value, params: { supplier_id: company.id }
        expect(JSON.parse(response.body)["title"]).to eq(company.title)
        expect(JSON.parse(response.body)["minimum_order_value"]).to eq(company.minimum_order_value.to_s)
    end
    it 'should not be able to retrieve minimum order value due to incorrect supplier ID' do
      get :fetch_minimum_order_value, params: { supplier_id: 'XYZ' }
      expect(response.status).to eq(422)
    end
  end



end