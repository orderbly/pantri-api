require 'rails_helper'


RSpec.describe OrdersService::CreateScheduledOrder, type: :service do
let!(:company) { create(:company) }
let!(:supplier) { create(:company) }
let!(:supplier_two) { create(:company, :low_credit_limit) }
let!(:customer) { create(:company) }
let!(:member) { create(:member, company: supplier) }
let!(:user) { member.user }
let!(:tax) { create(:tax, company: supplier) }
let(:address) { create(:address, company: customer)}
let(:product) { create(:product, company: supplier, tax: tax, company: supplier) }
let(:product_variant) { create(:product_variant, :with_stock, product: product) }
let(:product_variant_no_stock) { create(:product_variant, :with_no_stock, product: product) }
let!(:order) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }
let!(:order_two) { create(:order, customer: customer, supplier: supplier, delivery_address: address) }
let!(:order_item) { create(:order_item, order: order, product: product, product_variant: product_variant) }
let!(:order_item_two) { create(:order_item, order: order_two, product: product, product_variant: product_variant_no_stock) }


  it "should create a new order" do
    order_schedule = OrdersService::CreateScheduledOrder.new(order.id).call

    new_order = order_schedule[:obj]
    expect(new_order.supplier_id).to eq(order.supplier_id)
    expect(new_order.customer_id).to eq(order.customer_id)
    expect(new_order.delivery_address_id).to eq(order.delivery_address_id)
    expect(new_order.status).to eq("pending")
    expect(new_order.delivery_status).to eq("order_pending")
    expect(new_order.delivery_status).to eq("order_pending")
    expect(new_order.order_items.count).to eq(order.order_items.count)
    expect(new_order.order_items.sum(:quantity)).to eq(new_order.order_items.sum(:quantity))
    expect(new_order.order_items.map{|oi| oi.product_variant_id}).to eq(order.order_items.map{|oi| oi.product_variant_id})

  end

  it "should not be able to create a new order due to insufficient stock" do
    order_schedule = OrdersService::CreateScheduledOrder.new(order_two.id).call

    expect(order_schedule[:status]).to eq(false)
    expect(order_schedule[:error]).to eq("Stock not available.")
  end

  

end