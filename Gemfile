source 'https://rubygems.org'

ruby '2.6.1'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0'
gem 'pg'
# Use Puma as the app server
gem 'puma', '~> 3.11'
gem 'rack-cors'
gem 'active_model_serializers'
gem 'pry', '~> 0.12.2'
gem 'jwt_sessions', github: 'tuwukee/jwt_sessions', ref: '1a6449dd700c40f9de16525268dcf5949288a6c5'
gem 'redis'
gem 'redis-namespace'
gem "pgcrypto"
gem 'redis-rails'
gem 'redis-rack-cache'
gem 'sendgrid-ruby'
gem "aws-sdk-s3", require: false
gem 'mock_redis'
gem 'sidekiq', '~> 5.0', '>= 5.0.5'
gem 'sidekiq-status'
gem 'pusher'
gem 'twilio-ruby', '~> 5.6'
gem 'money'
gem 'offsite_payments'
gem 'omniauth-google-oauth2'
gem 'httparty', '~> 0.18.0'
gem 'whenever', require: false
gem 'date_format'
gem 'money-rails', '~>1.12'
gem 'gecko-ruby'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

gem 'thin'

gem 'faraday'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'dotenv-rails'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails', '~> 3.8'
  gem 'factory_bot_rails'
  gem 'shoulda-matchers'
  gem 'faker'
  gem 'pry-rails'
  gem 'simplecov', require: false, group: :test
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'annotate'
end


# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
