This is the API for Orderbly. 

# Config

 Ensure that you have:
 -  Ruby 2.6.1
 -  Rails 6.0

Redis is used for authentication purposes and acts as a caching layer


# Specs

This project makes use of Rspec which contains both Integration and Unit tests for all areas of the application. It is important to maintain this and to try keep a high level of test coverage. 

# Environment Variables

With extensive API's it is important to have access to third party services to run this application as it is intended to run. Please reach out to fellow devs on this project to get the Env variables.

# Database

This project makes use of Postgresql

