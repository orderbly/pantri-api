# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_11_30_092321) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "account_limit_memberships", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "supplier_id"
    t.float "account_limit"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "account_sales", force: :cascade do |t|
    t.integer "supplier_id"
    t.datetime "due_date"
    t.integer "customer_id"
    t.integer "order_id"
    t.float "credit_note_value"
    t.integer "status"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "value"
  end

  create_table "accounting_stores", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "company_id", null: false
    t.bigint "parent_id"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.string "name", null: false
    t.string "category"
    t.jsonb "blob", default: "{}", null: false
    t.boolean "active", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_accounting_stores_on_company_id"
    t.index ["parent_id"], name: "index_accounting_stores_on_parent_id"
    t.index ["user_id"], name: "index_accounting_stores_on_user_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "addresses", force: :cascade do |t|
    t.string "address_one"
    t.string "suburb"
    t.string "city"
    t.string "province"
    t.string "country"
    t.string "postal_code"
    t.integer "company_id"
    t.decimal "latitude"
    t.decimal "longitude"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "custom_client_id"
    t.string "name"
    t.string "additional_info"
  end

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.string "postal_code"
    t.string "street_code"
    t.string "city"
    t.string "province"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "brands", force: :cascade do |t|
    t.string "title"
    t.integer "company_id"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "comments", force: :cascade do |t|
    t.datetime "date"
    t.integer "order_id"
    t.integer "company_id"
    t.integer "user_id"
    t.text "message"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "company_image"
    t.integer "comment_type", default: 0
  end

  create_table "companies", force: :cascade do |t|
    t.string "title"
    t.string "address_one"
    t.string "country"
    t.string "city"
    t.string "postal_code"
    t.string "contact"
    t.string "website"
    t.string "description"
    t.string "image"
    t.string "colour"
    t.string "category"
    t.boolean "supplier", default: false
    t.boolean "is_active", default: false
    t.string "email"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "default_account_limit", default: 10000.0
    t.string "vat_number"
    t.integer "default_address_id"
    t.jsonb "email_list", default: [], null: false
    t.jsonb "buyers_directly_payable", default: []
    t.jsonb "suppliers_directly_payable", default: []
    t.boolean "advance_delivery", default: false
    t.boolean "is_vat_registered", default: false
    t.decimal "base_delivery_fee", precision: 8, scale: 2, default: "0.0"
    t.string "logo"
    t.boolean "default_delivery_reconciliation", default: false
    t.boolean "is_private", default: false
    t.jsonb "inventory_blob", default: {}
    t.boolean "is_custom_client", default: false
    t.integer "supplier_id"
    t.string "contact_person"
    t.decimal "minimum_order_value", precision: 8, scale: 2, default: "0.0"
    t.boolean "is_delivery_signature_required", default: true
    t.string "trading_as"
    t.jsonb "category_list", default: []
  end

  create_table "company_memberships", force: :cascade do |t|
    t.integer "supplier_id"
    t.integer "customer_id"
    t.string "key"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "credit_limits", force: :cascade do |t|
    t.integer "supplier_id"
    t.integer "customer_id"
    t.float "credit_limit"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "credit_note_claims", force: :cascade do |t|
    t.integer "credit_note_id"
    t.integer "order_id"
    t.float "value"
    t.string "description"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "credit_note_items", force: :cascade do |t|
    t.integer "company_id"
    t.integer "product_id"
    t.integer "order_id"
    t.float "quantity"
    t.float "gross_price"
    t.float "net_price"
    t.float "vat_price"
    t.integer "product_variant_id"
    t.float "received_amount"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "credit_note_id"
    t.string "notes", default: ""
    t.string "description"
  end

  create_table "credit_notes", force: :cascade do |t|
    t.integer "order_id"
    t.integer "customer_id"
    t.integer "supplier_id"
    t.float "balance", default: 0.0
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "remaining_balance"
    t.string "credit_note_number"
    t.jsonb "accounting", default: {}
    t.string "created_by", default: ""
  end

  create_table "custom_clients", force: :cascade do |t|
    t.string "title"
    t.integer "supplier_id"
    t.integer "address_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "address_one"
    t.string "suburb"
    t.string "province"
    t.string "country"
    t.string "city"
    t.string "postal_code"
    t.string "latitude"
    t.string "longitude"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.string "contact"
    t.string "email"
  end

  create_table "custom_deliveries", force: :cascade do |t|
    t.string "title"
    t.integer "supplier_id"
    t.integer "address_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "delivery_driver_id"
    t.datetime "due_date"
    t.integer "custom_client_id"
    t.integer "delivery_status", default: 0
    t.string "order_number"
    t.integer "customer_id"
  end

  create_table "custom_orders", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "supplier_id"
    t.datetime "delivery_date"
    t.text "notes"
    t.string "order_number"
    t.integer "status"
    t.float "balance"
    t.integer "is_paid"
    t.integer "delivery_status"
    t.float "value"
    t.jsonb "accounting"
    t.integer "order_schedule_id"
    t.boolean "is_recurring"
    t.integer "schedule_id"
    t.string "signature_image"
    t.integer "delivery_user_id"
    t.integer "parent_credit_note_id"
    t.string "received_by"
    t.uuid "uuid"
    t.integer "delivery_address_id"
    t.jsonb "delivery_fee"
    t.boolean "order_reconciliation"
    t.boolean "is_delivery_buyer_actionable"
    t.jsonb "inventory"
    t.decimal "reorder_balance"
    t.jsonb "delivery"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "delivery_allocations", force: :cascade do |t|
    t.integer "order_id"
    t.integer "user_id"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "delivery_fee_brackets", force: :cascade do |t|
    t.integer "company_id"
    t.decimal "min_distance"
    t.decimal "max_distance"
    t.decimal "price", precision: 8, scale: 2
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "delivery_zone_areas", force: :cascade do |t|
    t.integer "delivery_zone_id"
    t.string "name"
    t.string "street_code"
    t.string "postal_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "company_id"
    t.integer "cut_off_time"
    t.integer "cut_off_day"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.string "province"
  end

  create_table "delivery_zone_fees", force: :cascade do |t|
    t.integer "delivery_zone_id"
    t.decimal "value_min"
    t.decimal "value_max"
    t.decimal "price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.string "name"
    t.integer "fee_based_on"
  end

  create_table "delivery_zones", force: :cascade do |t|
    t.string "name"
    t.integer "company_id"
    t.jsonb "delivery_days"
    t.string "cut_off_time"
    t.integer "cut_off_day"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.integer "fee_based_on", default: 0
  end

  create_table "discount_book_memberships", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "discount_book_id"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "discount_books", force: :cascade do |t|
    t.bigint "supplier_id"
    t.bigint "customer_id"
    t.string "description"
    t.string "title"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "integrations", force: :cascade do |t|
    t.bigint "company_id", null: false
    t.string "name", null: false
    t.string "access_token"
    t.string "refresh_token"
    t.string "expires"
    t.jsonb "extras", default: "{}", null: false
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_integrations_on_company_id"
  end

  create_table "inventory_stores", force: :cascade do |t|
    t.bigint "company_id", null: false
    t.bigint "parent_id"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.string "name", null: false
    t.string "category"
    t.jsonb "blob", default: "{}", null: false
    t.boolean "active", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_inventory_stores_on_company_id"
    t.index ["parent_id"], name: "index_inventory_stores_on_parent_id"
  end

  create_table "members", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "company_id"
    t.bigint "role_id"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "user_permissions", default: {"tax_settings"=>{"can_add_tax"=>true, "can_edit_tax"=>true, "can_view_tax_settings"=>true}, "user_settings"=>{"can_edit_user"=>true, "can_create_user"=>true, "can_delete_user"=>true}, "sales_settings"=>{"can_edit_sale"=>true, "can_accept_sale"=>true, "can_decline_sale"=>true, "can_edit_quantity"=>true, "can_create_payment"=>true, "can_view_all_sales"=>true, "can_edit_gross_price"=>true, "can_allocate_delivery"=>true, "can_create_credit_note"=>true, "can_delete_order_items"=>true, "can_create_custom_sales"=>true, "can_view_own_custom_sales"=>true, "can_update_delivery_status"=>true}, "company_settings"=>{"can_add_address"=>true, "can_edit_address"=>true, "can_view_dashboard"=>true, "can_add_company_emails"=>true, "can_edit_company_emails"=>true, "can_update_company_images"=>true, "can_view_company_settings"=>true, "can_change_company_details"=>true}, "pricing_settings"=>{"can_edit_pricebook"=>true, "can_view_pricebooks"=>true, "can_create_pricebook"=>true, "can_create_pricebook_membership"=>true}, "product_settings"=>{"can_edit_product"=>true, "can_create_product"=>true}, "delivery_settings"=>{"can_edit_delivery_bracket"=>true, "can_view_delivery_settings"=>true, "can_create_delivery_bracket"=>true, "can_toggle_delivery_reconciliation"=>true}, "purchases_settings"=>{"can_edit_purchases"=>true, "can_cancel_purchases"=>true, "can_view_all_purchases"=>true, "can_integrate_purchases"=>true}, "fulfilment_settings"=>{"can_view_waybills"=>true, "can_view_own_deliveries"=>true, "can_create_custom_delivery"=>true, "can_view_all_daily_deliveries"=>true, "can_view_all_upcoming_deliveries"=>true, "can_view_all_completed_deliveries"=>true}, "statements_settings"=>{"can_view_sales_statement"=>true, "can_view_purchase_statement"=>true}, "integration_settings"=>{"can_add_integration"=>true, "can_view_integrations"=>true, "can_disable_integration"=>true}, "marketplace_settings"=>{"can_create_orders"=>true, "can_integrate_sales"=>true, "can_view_marketplace"=>true, "can_view_supplier_profiles"=>true}, "credit_limit_settings"=>{"can_edit_credit_limits"=>true, "can_view_credit_limits"=>true, "can_create_credit_limits"=>true}}
    t.index ["company_id"], name: "index_members_on_company_id"
    t.index ["role_id"], name: "index_members_on_role_id"
    t.index ["user_id"], name: "index_members_on_user_id"
  end

  create_table "order_items", force: :cascade do |t|
    t.bigint "company_id"
    t.bigint "product_id"
    t.bigint "order_id"
    t.float "quantity", default: 0.0
    t.float "gross_price", default: 0.0
    t.float "net_price", default: 0.0
    t.float "vat_price", default: 0.0
    t.integer "product_variant_id"
    t.float "received_amount", default: 0.0
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "description"
    t.string "item_note"
    t.index ["company_id"], name: "index_order_items_on_company_id"
    t.index ["order_id"], name: "index_order_items_on_order_id"
    t.index ["product_id"], name: "index_order_items_on_product_id"
  end

  create_table "order_schedules", force: :cascade do |t|
    t.integer "order_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.boolean "active"
    t.datetime "next_date"
    t.integer "frequency"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "value"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "supplier_id"
    t.datetime "delivery_date"
    t.text "notes"
    t.string "order_number"
    t.integer "status", default: 0
    t.float "balance"
    t.integer "is_paid", default: 0
    t.integer "delivery_status", default: 0
    t.float "value", default: 0.0
    t.jsonb "accounting", default: {}, null: false
    t.integer "order_schedule_id"
    t.boolean "is_recurring", default: false
    t.integer "schedule_id"
    t.string "signature_image"
    t.integer "delivery_user_id"
    t.integer "parent_credit_note_id"
    t.string "received_by"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "delivery_address_id"
    t.jsonb "delivery_fee", default: {"net_price"=>0.0, "vat_price"=>0.0, "gross_price"=>0.0}
    t.boolean "order_reconciliation", default: false
    t.boolean "is_delivery_buyer_actionable", default: false
    t.jsonb "inventory", default: {}
    t.decimal "reorder_balance", precision: 8, scale: 2, default: "0.0"
    t.jsonb "delivery", default: {}
    t.boolean "is_custom_client", default: false
    t.boolean "is_custom_order"
    t.boolean "is_delivery_signature_required", default: true
    t.integer "delivery_number", default: 0
  end

  create_table "payment_allocations", force: :cascade do |t|
    t.integer "order_id"
    t.integer "payment_id"
    t.float "value"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "payments", force: :cascade do |t|
    t.integer "supplier_id"
    t.integer "customer_id"
    t.integer "order_id"
    t.float "value"
    t.integer "account_sale_id"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "balance"
    t.string "transaction_id"
    t.integer "payment_type"
    t.datetime "payment_date"
    t.string "payment_reference"
    t.integer "user_id"
  end

  create_table "pending_integrations", force: :cascade do |t|
    t.integer "company_id"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "pending_invites", force: :cascade do |t|
    t.string "email_address"
    t.integer "role"
    t.integer "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "user_permissions", default: {"tax_settings"=>{"can_add_tax"=>true, "can_edit_tax"=>true, "can_view_tax_settings"=>true}, "user_settings"=>{"can_edit_user"=>true, "can_create_user"=>true, "can_delete_user"=>true}, "sales_settings"=>{"can_edit_sale"=>true, "can_accept_sale"=>true, "can_decline_sale"=>true, "can_edit_quantity"=>true, "can_create_payment"=>true, "can_view_all_sales"=>true, "can_edit_gross_price"=>true, "can_allocate_delivery"=>true, "can_create_credit_note"=>true, "can_delete_order_items"=>true, "can_create_custom_sales"=>true, "can_view_own_custom_sales"=>true, "can_update_delivery_status"=>true}, "company_settings"=>{"can_add_address"=>true, "can_edit_address"=>true, "can_view_dashboard"=>true, "can_add_company_emails"=>true, "can_edit_company_emails"=>true, "can_update_company_images"=>true, "can_view_company_settings"=>true, "can_change_company_details"=>true}, "pricing_settings"=>{"can_edit_pricebook"=>true, "can_view_pricebooks"=>true, "can_create_pricebook"=>true, "can_create_pricebook_membership"=>true}, "product_settings"=>{"can_edit_product"=>true, "can_create_product"=>true}, "delivery_settings"=>{"can_edit_delivery_bracket"=>true, "can_view_delivery_settings"=>true, "can_create_delivery_bracket"=>true, "can_toggle_delivery_reconciliation"=>true}, "purchases_settings"=>{"can_edit_purchases"=>true, "can_cancel_purchases"=>true, "can_view_all_purchases"=>true, "can_integrate_purchases"=>true}, "fulfilment_settings"=>{"can_view_waybills"=>true, "can_view_own_deliveries"=>true, "can_create_custom_delivery"=>true, "can_view_all_daily_deliveries"=>true, "can_view_all_upcoming_deliveries"=>true, "can_view_all_completed_deliveries"=>true}, "statements_settings"=>{"can_view_sales_statement"=>true, "can_view_purchase_statement"=>true}, "integration_settings"=>{"can_add_integration"=>true, "can_view_integrations"=>true, "can_disable_integration"=>true}, "marketplace_settings"=>{"can_create_orders"=>true, "can_integrate_sales"=>true, "can_view_marketplace"=>true, "can_view_supplier_profiles"=>true}, "credit_limit_settings"=>{"can_edit_credit_limits"=>true, "can_view_credit_limits"=>true, "can_create_credit_limits"=>true}}
  end

  create_table "product_variant_discounts", force: :cascade do |t|
    t.bigint "discount_book_id"
    t.bigint "product_variant_id"
    t.float "discount_percentage"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "product_variants", force: :cascade do |t|
    t.bigint "product_id"
    t.string "description"
    t.string "status"
    t.float "available_stock", default: 0.0
    t.float "stock", default: 0.0
    t.decimal "unit_amount", precision: 8, scale: 2, default: "0.0"
    t.decimal "tax_amount", precision: 8, scale: 2, default: "0.0"
    t.float "discount_percentage", default: 0.0
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "track_stock", default: false
    t.decimal "gross_amount", precision: 8, scale: 2
    t.jsonb "accounting_item", default: {}
    t.string "sku"
    t.decimal "cost_price", precision: 8, scale: 2, default: "0.0"
    t.decimal "margin", precision: 8, scale: 2, default: "0.0"
    t.decimal "retail_amount", precision: 8, scale: 2, default: "0.0"
    t.decimal "base_amount", precision: 8, scale: 2, default: "0.0"
    t.jsonb "inventory_item", default: {}
    t.boolean "is_active", default: true
    t.index ["product_id"], name: "index_product_variants_on_product_id"
  end

  create_table "products", force: :cascade do |t|
    t.bigint "company_id"
    t.bigint "tax_id"
    t.string "title"
    t.string "description"
    t.string "image"
    t.boolean "got_variants"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.money "price", scale: 2
    t.boolean "is_active", default: true
    t.jsonb "accounting_item", default: {}
    t.boolean "is_exclusive", default: false
    t.jsonb "exclusive_companies", default: []
    t.boolean "marketplace_active", default: true
    t.boolean "is_delivery_product", default: false
    t.jsonb "inventory_item", default: {}
    t.boolean "is_archived", default: false
    t.index ["company_id"], name: "index_products_on_company_id"
    t.index ["tax_id"], name: "index_products_on_tax_id"
  end

  create_table "products_tags", id: false, force: :cascade do |t|
    t.bigint "product_id"
    t.bigint "tag_id"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.index ["product_id"], name: "index_products_tags_on_product_id"
    t.index ["tag_id"], name: "index_products_tags_on_tag_id"
  end

  create_table "roles", force: :cascade do |t|
    t.integer "level", default: 1
    t.string "description"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "tags", force: :cascade do |t|
    t.string "description"
    t.bigint "company_id"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_tags_on_company_id"
  end

  create_table "taxes", force: :cascade do |t|
    t.bigint "company_id"
    t.integer "category", default: 0
    t.string "description"
    t.float "percentage", default: 0.0
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_taxes_on_company_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.string "first_name"
    t.string "last_name"
    t.string "username"
    t.string "contact_number"
    t.uuid "uuid", default: -> { "gen_random_uuid()" }
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "current_company"
    t.integer "role", default: 0, null: false
    t.string "reset_password_token"
    t.datetime "reset_password_token_expires_at"
    t.string "avatar"
    t.string "default_colour", default: "#2dce89"
    t.jsonb "delivery_payload", default: []
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
