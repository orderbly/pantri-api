class CreateCompanyMemberships < ActiveRecord::Migration[6.0]
  def change
    create_table :company_memberships do |t|
      t.integer :supplier_id
      t.integer :customer_id
      t.string :key

      t.timestamps
    end
  end
end
