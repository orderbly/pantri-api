class AddIsVatRegisteredToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :is_vat_registered, :boolean, default: false
  end
end
