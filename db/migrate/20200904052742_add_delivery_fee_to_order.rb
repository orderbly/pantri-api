class AddDeliveryFeeToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :delivery_fee, :jsonb, default: { gross_price: 0.0, net_price: 0.0, vat_price: 0.0 }
  end
end
