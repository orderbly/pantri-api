class CreatePendingInvites < ActiveRecord::Migration[6.0]
  def change
    create_table :pending_invites do |t|
      t.string :email_address
      t.integer :role
      t.integer :company_id

      t.timestamps
    end
  end
end
