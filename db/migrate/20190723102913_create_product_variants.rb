class CreateProductVariants < ActiveRecord::Migration[6.0]
  def change
    create_table :product_variants do |t|
      t.references :product

      t.string :description
      t.string :status
      t.float :available_stock, default: 0.0
      t.float :stock, default: 0.0
      t.float :unit_amount, default: 0.0
      t.float :tax_amount, default: 0.0
      t.float :discount_percentage, default: 0.0
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end

  end
end
