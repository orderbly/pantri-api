class CreateCreditNotes < ActiveRecord::Migration[6.0]
  def change
    create_table :credit_notes do |t|
      t.integer :order_id
      t.integer :customer_id
      t.integer :supplier_id
      t.float :balance, :default => 0.0
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
