class CreateDiscountBookMemberships < ActiveRecord::Migration[6.0]
  def change
    create_table :discount_book_memberships do |t|
      t.integer :customer_id
      t.integer :discount_book_id
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
