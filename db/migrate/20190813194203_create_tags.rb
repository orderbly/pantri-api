class CreateTags < ActiveRecord::Migration[6.0]
  def change
    create_table :tags do |t|
      t.string :description
      t.references :company
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }
      t.timestamps
    end
  end
end
