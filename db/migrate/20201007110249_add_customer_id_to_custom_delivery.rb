class AddCustomerIdToCustomDelivery < ActiveRecord::Migration[6.0]
  def change
    add_column :custom_deliveries, :customer_id, :integer
  end
end
