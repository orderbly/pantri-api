class CreateDeliveryZoneFees < ActiveRecord::Migration[6.0]
  def change
    create_table :delivery_zone_fees do |t|
      t.integer :delivery_zone_id
      t.decimal :value_min
      t.decimal :value_max
      t.decimal :price

      t.timestamps
    end
  end
end
