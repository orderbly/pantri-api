class AddIsDeliveryBuyerActionableToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :is_delivery_buyer_actionable, :boolean, default: false
  end
end
