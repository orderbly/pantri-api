class UpdatePricingForProductVariants < ActiveRecord::Migration[6.0]
  def change
    add_column :product_variants, :gross_amount, :decimal, :precision => 8, :scale => 2
    change_column :product_variants, :unit_amount, :decimal, :precision => 8, :scale => 2
    change_column :product_variants, :tax_amount, :decimal, :precision => 8, :scale => 2
  end

end
