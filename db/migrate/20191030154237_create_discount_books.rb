class CreateDiscountBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :discount_books do |t|
      t.bigint :supplier_id
      t.bigint :customer_id
      t.string :description
      t.string :title
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
