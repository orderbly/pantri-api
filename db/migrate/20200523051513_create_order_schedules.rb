class CreateOrderSchedules < ActiveRecord::Migration[6.0]
  def change
    create_table :order_schedules do |t|
      t.integer :order_id
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :active
      t.datetime :next_date
      t.integer :frequency
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
