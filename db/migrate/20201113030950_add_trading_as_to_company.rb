class AddTradingAsToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :trading_as, :string
  end
end
