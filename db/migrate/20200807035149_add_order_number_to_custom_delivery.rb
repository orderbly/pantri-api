class AddOrderNumberToCustomDelivery < ActiveRecord::Migration[6.0]
  def change
    add_column :custom_deliveries, :order_number, :string
  end
end
