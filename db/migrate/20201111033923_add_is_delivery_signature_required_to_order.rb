class AddIsDeliverySignatureRequiredToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :is_delivery_signature_required, :boolean, default: true
    add_column :companies, :is_delivery_signature_required, :boolean, default: true
  end
end
