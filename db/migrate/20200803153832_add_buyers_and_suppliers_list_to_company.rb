class AddBuyersAndSuppliersListToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :buyers_directly_payable, :jsonb, default: []
    add_column :companies, :suppliers_directly_payable, :jsonb, default: []
  end
end
