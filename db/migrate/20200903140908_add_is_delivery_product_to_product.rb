class AddIsDeliveryProductToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :is_delivery_product, :boolean, default: false
  end
end
