class AddDeliveryPayloadToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :delivery_payload, :jsonb, default: []
  end
end
