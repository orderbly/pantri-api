class AddTrackStockToProductVariant < ActiveRecord::Migration[6.0]
  def change
    add_column :product_variants, :track_stock, :boolean, :default => false
  end
end
