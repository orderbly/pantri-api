class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :title
      t.string :address_one
      t.string :country
      t.string :city
      t.string :postal_code
      t.string :contact
      t.string :website
      t.string :description
      t.string :image
      t.string :colour
      t.string :category
      t.boolean :supplier, default: false
      t.boolean :is_active, default: false
      t.string :email
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
