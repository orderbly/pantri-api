class CreateDeliveryZones < ActiveRecord::Migration[6.0]
  def change
    create_table :delivery_zones do |t|
      t.string :name
      t.integer :company_id
      t.jsonb :delivery_days
      t.string :cut_off_time
      t.integer :cut_off_day

      t.timestamps
    end
  end
end
