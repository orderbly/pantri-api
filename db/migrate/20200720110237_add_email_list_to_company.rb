class AddEmailListToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :email_list, :jsonb, default: [], null: false
  end
end
