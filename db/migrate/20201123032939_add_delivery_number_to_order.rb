class AddDeliveryNumberToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :delivery_number, :integer, default: 0
  end
end
