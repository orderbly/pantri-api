class AddBaseDeliveryFeeToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :base_delivery_fee, :decimal, default: 0, :precision => 8, :scale => 2
  end
end
