class CreateRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :roles do |t|
      t.integer :level, default: 1
      t.string :description
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
