class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.string :address_one
      t.string :suburb
      t.string :city
      t.string :province
      t.string :country
      t.string :postal_code
      t.integer :company_id
      t.decimal :latitude
      t.decimal :longitude
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
