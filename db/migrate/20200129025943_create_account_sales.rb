class CreateAccountSales < ActiveRecord::Migration[6.0]
  def change
    create_table :account_sales do |t|
      t.integer :supplier_id
      t.datetime :due_date
      t.integer :customer_id
      t.integer :order_id
      t.float :credit_note_value
      t.integer :status
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
