class AddCustomClientToCustomDelivery < ActiveRecord::Migration[6.0]
  def change
    add_column :custom_deliveries, :custom_client_id, :integer
  end
end
