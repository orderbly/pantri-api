class AddAddressFieldsToCustomClient < ActiveRecord::Migration[6.0]
  def change
    add_column :custom_clients, :address_one, :string
    add_column :custom_clients, :suburb, :string
    add_column :custom_clients, :province, :string
    add_column :custom_clients, :country, :string
    add_column :custom_clients, :city, :string
    add_column :custom_clients, :postal_code, :string
    add_column :custom_clients, :latitude, :string
    add_column :custom_clients, :longitude, :string
    add_column :custom_clients, :uuid, :uuid, default: -> { 'gen_random_uuid()' }
  end
end
