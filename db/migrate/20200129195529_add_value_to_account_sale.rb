class AddValueToAccountSale < ActiveRecord::Migration[6.0]
  def change
    add_column :account_sales, :value, :float
  end
end
