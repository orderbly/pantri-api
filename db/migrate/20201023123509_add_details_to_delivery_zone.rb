class AddDetailsToDeliveryZone < ActiveRecord::Migration[6.0]
  def change
    add_column :delivery_zones, :uuid, :uuid, default: -> { 'gen_random_uuid()' }
    add_column :delivery_zone_areas, :uuid, :uuid, default: -> { 'gen_random_uuid()' }
    add_column :delivery_zone_fees, :uuid, :uuid, default: -> { 'gen_random_uuid()' }
  end
end
