class AddIsActiveToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :is_active, :bool, default: true
  end
end
