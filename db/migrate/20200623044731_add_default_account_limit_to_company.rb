class AddDefaultAccountLimitToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :default_account_limit, :float, default: 10000.00
  end
end
