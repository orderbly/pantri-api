class AddInventoryToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :inventory, :jsonb, default: {}
  end
end
