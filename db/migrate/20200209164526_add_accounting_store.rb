class AddAccountingStore < ActiveRecord::Migration[6.0]
  def change
    create_table :accounting_stores do |t|
      t.references :user, null: false
      t.references :company, null: false
      t.references :parent
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.string :name, null: false
      t.string :category
      t.jsonb :blob, null: false, default: '{}'
      t.boolean :active, default: false

      t.timestamps
    end
  end
end
