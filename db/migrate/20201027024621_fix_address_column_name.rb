class FixAddressColumnName < ActiveRecord::Migration[6.0]
  def change
    rename_column :addresses, :alias, :name
  end
end
