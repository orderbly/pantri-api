class AddDueDateToCustomDelivery < ActiveRecord::Migration[6.0]
  def change
    add_column :custom_deliveries, :due_date, :datetime
  end
end
