class CreateOrderItems < ActiveRecord::Migration[6.0]
  def change
    create_table :order_items do |t|
      t.references :company
      t.references :product
      t.references :order
      t.float :quantity, default: 0.0
      t.float :gross_price, default: 0.0
      t.float :net_price, default: 0.0
      t.float :vat_price, default: 0.0
      t.integer :product_variant_id
      t.float :received_amount, default: 0.0
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }



      t.timestamps
    end
  end
end
