class AddCreatedByToCreditNote < ActiveRecord::Migration[6.0]
  def change
    add_column :credit_notes, :created_by, :string, default: ""
    add_column :credit_note_items, :notes, :string, default: ""
  end
end
