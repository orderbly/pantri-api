class AddCurrentCompanyToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :current_company, :integer
  end
end
