class AddCreditNoteNumberToCreditNote < ActiveRecord::Migration[6.0]
  def change
    add_column :credit_notes, :credit_note_number, :string
  end
end
