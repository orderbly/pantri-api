class AddDefaultAddressIdToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :default_address_id, :integer
  end
end
