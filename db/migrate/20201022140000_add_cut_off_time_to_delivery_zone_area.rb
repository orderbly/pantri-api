class AddCutOffTimeToDeliveryZoneArea < ActiveRecord::Migration[6.0]
  def change
    add_column :delivery_zone_areas, :cut_off_time, :integer
    add_column :delivery_zone_areas, :cut_off_day, :integer
  end
end
