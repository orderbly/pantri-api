class AddMinimumOrderValueToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :minimum_order_value, :decimal, default: 0, :precision => 8, :scale => 2
  end
end
