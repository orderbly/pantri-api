class CreateDeliveryAllocations < ActiveRecord::Migration[6.0]
  def change
    create_table :delivery_allocations do |t|
      t.integer :order_id
      t.integer :user_id
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
