class AddAddressIdToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :delivery_address_id, :integer
  end
end
