class AddFeeBasedOnToDeliveryZoneFee < ActiveRecord::Migration[6.0]
  def change
    add_column :delivery_zone_fees, :fee_based_on, :integer
  end
end
