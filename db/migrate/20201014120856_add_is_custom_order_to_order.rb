class AddIsCustomOrderToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :is_custom_order, :boolean
  end
end
