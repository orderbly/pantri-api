class AddPaymenDetailsToPayment < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :payment_type, :integer
    add_column :payments, :payment_date, :datetime
    add_column :payments, :payment_reference, :string
  end
end
