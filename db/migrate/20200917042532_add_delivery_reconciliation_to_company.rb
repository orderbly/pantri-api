class AddDeliveryReconciliationToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :default_delivery_reconciliation, :boolean, default: false
  end
end
