class CreateCreditNoteItems < ActiveRecord::Migration[6.0]
  def change
    create_table :credit_note_items do |t|
      t.integer :company_id
      t.integer :product_id
      t.integer :order_id
      t.float :quantity
      t.float :gross_price
      t.float :net_price
      t.float :vat_price
      t.integer :product_variant_id
      t.float :received_amount
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
