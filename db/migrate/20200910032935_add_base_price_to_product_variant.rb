class AddBasePriceToProductVariant < ActiveRecord::Migration[6.0]
  def change
    add_column :product_variants, :base_amount, :decimal, default: 0, :precision => 8, :scale => 2
  end
end
