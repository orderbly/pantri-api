class AddCompanyIdToDeliveryZoneArea < ActiveRecord::Migration[6.0]
  def change
    add_column :delivery_zone_areas, :company_id, :integer
  end
end
