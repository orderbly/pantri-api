class CreateProductsTags < ActiveRecord::Migration[6.0]
  def change
    create_table :products_tags, id: false do |t|
      t.references :product
      t.references :tag
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }
    end
  end
end
