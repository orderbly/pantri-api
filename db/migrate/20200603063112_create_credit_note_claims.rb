class CreateCreditNoteClaims < ActiveRecord::Migration[6.0]
  def change
    create_table :credit_note_claims do |t|
      t.integer :credit_note_id
      t.integer :order_id
      t.float :value
      t.string :description
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
