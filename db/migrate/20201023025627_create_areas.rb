class CreateAreas < ActiveRecord::Migration[6.0]
  def change
    create_table :areas do |t|
      t.string :name
      t.string :postal_code
      t.string :street_code
      t.string :city
      t.string :province

      t.timestamps
    end
  end
end
