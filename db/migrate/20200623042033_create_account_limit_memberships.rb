class CreateAccountLimitMemberships < ActiveRecord::Migration[6.0]
  def change
    create_table :account_limit_memberships do |t|
      t.integer :customer_id
      t.integer :supplier_id
      t.float :account_limit
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
