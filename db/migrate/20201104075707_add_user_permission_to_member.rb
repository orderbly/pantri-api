class AddUserPermissionToMember < ActiveRecord::Migration[6.0]
  def change
    add_column :pending_invites, :user_permissions, :jsonb, default: user_permissions
    add_column :members, :user_permissions, :jsonb, default: user_permissions
  end

  def user_permissions
    {
      user_settings: {
        can_create_user: true,
        can_edit_user: true,
        can_delete_user: true
      },
    
      company_settings: {
        can_view_dashboard: true,
        can_view_company_settings: true,
        can_change_company_details: true,
        can_update_company_images: true,
        can_add_address: true,
        can_edit_address: true,
        can_edit_company_emails: true,
        can_add_company_emails: true
      },
    
      tax_settings: {
        can_view_tax_settings: true,
        can_add_tax: true,
        can_edit_tax: true
      },
    
      delivery_settings: {
        can_view_delivery_settings: true,
        can_edit_delivery_bracket: true,
        can_create_delivery_bracket: true,
        can_toggle_delivery_reconciliation: true
      },
    
      integration_settings: {
        can_view_integrations: true,
        can_add_integration: true,
        can_disable_integration: true
      },
    
      marketplace_settings: {
        can_view_marketplace: true,
        can_view_supplier_profiles: true,
        can_create_orders: true,
        can_integrate_sales: true
      }, 
    
      purchases_settings: {
        can_view_all_purchases: true,
        can_cancel_purchases: true,
        can_edit_purchases: true,
        can_integrate_purchases: true
      },
    
      sales_settings: {
        can_view_all_sales: true,
        can_view_own_custom_sales: true,
        can_create_payment: true,
        can_accept_sale: true,
        can_decline_sale: true,
        can_delete_order_items: true,
        can_edit_sale: true,
        can_edit_quantity: true,
        can_edit_gross_price: true,
        can_allocate_delivery: true,
        can_update_delivery_status: true,
        can_create_custom_sales: true,
        can_create_credit_note: true
      },

      product_settings: {
        can_edit_product: true,
        can_create_product: true
      },
    
      pricing_settings: {
        can_view_pricebooks: true, #
        can_create_pricebook: true, #
        can_edit_pricebook: true, #
        can_create_pricebook_membership: true
      },
    
      credit_limit_settings: {
        can_view_credit_limits: true, #
        can_edit_credit_limits: true, #
        can_create_credit_limits: true #
      },
    
      statements_settings: {
        can_view_sales_statement: true, #
        can_view_purchase_statement: true #
      },
    
      fulfilment_settings: {
        can_view_own_deliveries: true, #
        can_view_waybills: true, #
        can_view_all_daily_deliveries: true, #
        can_view_all_upcoming_deliveries: true, #
        can_view_all_completed_deliveries: true, #
        can_create_custom_delivery: true #
      },
    }
  end
end
