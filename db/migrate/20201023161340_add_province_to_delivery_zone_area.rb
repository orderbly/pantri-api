class AddProvinceToDeliveryZoneArea < ActiveRecord::Migration[6.0]
  def change
    add_column :delivery_zone_areas, :province, :string
  end
end
