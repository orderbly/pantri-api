class AddXeroItemToProductVariant < ActiveRecord::Migration[6.0]
  def change
    add_column :product_variants, :accounting_item, :jsonb, default: {}
  end
end
