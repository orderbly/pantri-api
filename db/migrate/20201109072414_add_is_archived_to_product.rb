class AddIsArchivedToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :is_archived, :boolean, default: false
  end
end
