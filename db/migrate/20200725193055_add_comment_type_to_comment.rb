class AddCommentTypeToComment < ActiveRecord::Migration[6.0]
  def change
    add_column :comments, :comment_type, :integer, default: 0
  end
end
