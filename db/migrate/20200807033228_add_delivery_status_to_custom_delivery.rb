class AddDeliveryStatusToCustomDelivery < ActiveRecord::Migration[6.0]
  def change
    add_column :custom_deliveries, :delivery_status, :integer, default: 0
  end
end
