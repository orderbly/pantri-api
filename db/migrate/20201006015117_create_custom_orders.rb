class CreateCustomOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :custom_orders do |t|
      t.integer :customer_id
      t.integer :supplier_id
      t.datetime :delivery_date
      t.text :notes
      t.string :order_number
      t.integer :status
      t.float :balance
      t.integer :is_paid
      t.integer :delivery_status
      t.float :value
      t.jsonb :accounting
      t.integer :order_schedule_id
      t.boolean :is_recurring
      t.integer :schedule_id
      t.string :signature_image
      t.integer :delivery_user_id
      t.integer :parent_credit_note_id
      t.string :received_by
      t.uuid :uuid
      t.integer :delivery_address_id
      t.jsonb :delivery_fee
      t.boolean :order_reconciliation
      t.boolean :is_delivery_buyer_actionable
      t.jsonb :inventory
      t.decimal :reorder_balance
      t.jsonb :delivery

      t.timestamps
    end
  end
end
