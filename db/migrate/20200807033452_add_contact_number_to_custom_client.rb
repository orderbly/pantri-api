class AddContactNumberToCustomClient < ActiveRecord::Migration[6.0]
  def change
    add_column :custom_clients, :contact, :string
    add_column :custom_clients, :email, :string
  end
end
