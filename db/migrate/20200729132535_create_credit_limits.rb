class CreateCreditLimits < ActiveRecord::Migration[6.0]
  def change
    create_table :credit_limits do |t|
      t.integer :supplier_id
      t.integer :customer_id
      t.float :credit_limit
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
