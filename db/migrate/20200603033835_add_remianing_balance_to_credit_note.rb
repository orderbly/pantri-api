class AddRemianingBalanceToCreditNote < ActiveRecord::Migration[6.0]
  def change
    add_column :credit_notes, :remaining_balance, :float
  end
end
