class AddXeroItemToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :accounting_item, :jsonb, default: {}
  end
end
