class AddReorderBalanceToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :reorder_balance, :decimal, default: 0, :precision => 8, :scale => 2
  end
end
