class AddVatNumberToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :vat_number, :string
  end
end
