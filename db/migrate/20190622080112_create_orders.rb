class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.integer :customer_id
      t.integer :supplier_id
      t.datetime :delivery_date
      t.text :notes
      t.string :order_number
      t.integer :status, default: 0
      t.float :balance
      t.integer :is_paid, default: false
      t.integer :delivery_status, default: 0
      t.float :value, default: 0.0
      t.jsonb :accounting, default: {}, null: false
      t.integer :order_schedule_id
      t.boolean :is_recurring, default: false
      t.integer :schedule_id
      t.string :signature_image
      t.integer :delivery_user_id
      t.integer :parent_credit_note_id
      t.string :received_by
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
