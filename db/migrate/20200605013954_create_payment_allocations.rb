class CreatePaymentAllocations < ActiveRecord::Migration[6.0]
  def change
    create_table :payment_allocations do |t|
      t.integer :order_id
      t.integer :payment_id
      t.float :value
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
