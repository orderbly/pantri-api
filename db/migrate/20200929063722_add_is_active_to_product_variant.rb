class AddIsActiveToProductVariant < ActiveRecord::Migration[6.0]
  def change
    add_column :product_variants, :is_active, :boolean, default: true
  end
end
