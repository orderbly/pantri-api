class AddOrderReconciliationToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :order_reconciliation, :boolean, default: false
  end
end
