class AddIsPrivateToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :is_private, :boolean, default: false
  end
end
