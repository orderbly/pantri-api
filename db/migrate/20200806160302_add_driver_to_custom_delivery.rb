class AddDriverToCustomDelivery < ActiveRecord::Migration[6.0]
  def change
    add_column :custom_deliveries, :delivery_driver_id, :integer
  end
end
