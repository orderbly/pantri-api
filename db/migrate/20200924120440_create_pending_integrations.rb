class CreatePendingIntegrations < ActiveRecord::Migration[6.0]
  def change
    create_table :pending_integrations do |t|
      t.integer :company_id
      t.string :name

      t.timestamps
    end
  end
end
