class AddExclusivityToProduct < ActiveRecord::Migration[6.0]
  def change
    add_column :products, :exclusive_companies, :jsonb, default: []
  end
end
