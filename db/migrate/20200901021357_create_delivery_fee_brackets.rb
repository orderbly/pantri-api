class CreateDeliveryFeeBrackets < ActiveRecord::Migration[6.0]
  def change
    create_table :delivery_fee_brackets do |t|
      t.integer :company_id
      t.decimal :min_distance
      t.decimal :max_distance
      t.decimal :price, :precision => 8, :scale => 2

      t.timestamps
    end
  end
end
