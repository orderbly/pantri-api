class AddCompanyImageToComment < ActiveRecord::Migration[6.0]
  def change
    add_column :comments, :company_image, :string
  end
end
