class CreateCustomDeliveries < ActiveRecord::Migration[6.0]
  def change
    create_table :custom_deliveries do |t|
      t.string :title
      t.integer :supplier_id
      t.integer :address_id

      t.timestamps
    end
  end
end
