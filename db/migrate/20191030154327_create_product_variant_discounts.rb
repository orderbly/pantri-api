class CreateProductVariantDiscounts < ActiveRecord::Migration[6.0]
  def change
    create_table :product_variant_discounts do |t|
      t.bigint :discount_book_id
      t.bigint :product_variant_id
      t.float :discount_percentage
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
