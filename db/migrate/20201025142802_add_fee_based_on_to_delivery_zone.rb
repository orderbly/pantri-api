class AddFeeBasedOnToDeliveryZone < ActiveRecord::Migration[6.0]
  def change
    add_column :delivery_zones, :fee_based_on, :integer, default: 0
  end
end
