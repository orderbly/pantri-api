class AddAdditionalFieldsToAddress < ActiveRecord::Migration[6.0]
  def change
    add_column :addresses, :alias, :string
    add_column :addresses, :additional_info, :string
  end
end
