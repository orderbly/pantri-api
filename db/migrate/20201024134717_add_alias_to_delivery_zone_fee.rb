class AddAliasToDeliveryZoneFee < ActiveRecord::Migration[6.0]
  def change
    add_column :delivery_zone_fees, :name, :string
  end
end
