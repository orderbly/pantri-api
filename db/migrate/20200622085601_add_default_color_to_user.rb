class AddDefaultColorToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :default_colour, :string, default: "#2dce89"
  end
end
