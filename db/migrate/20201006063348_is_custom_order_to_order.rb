class IsCustomOrderToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :is_custom_client, :boolean, default: false
  end
end
