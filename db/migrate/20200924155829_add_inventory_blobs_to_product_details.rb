class AddInventoryBlobsToProductDetails < ActiveRecord::Migration[6.0]
  def change
    add_column :product_variants, :inventory_item, :jsonb, default: {}
    add_column :products, :inventory_item, :jsonb, default: {}
  end
end
