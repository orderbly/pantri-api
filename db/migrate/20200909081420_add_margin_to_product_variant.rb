class AddMarginToProductVariant < ActiveRecord::Migration[6.0]
  def change
    add_column :product_variants, :margin, :decimal, default: 0, :precision => 8, :scale => 2
  end
end
