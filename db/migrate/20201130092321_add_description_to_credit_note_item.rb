class AddDescriptionToCreditNoteItem < ActiveRecord::Migration[6.0]
  def change
    add_column :credit_note_items, :description, :string
  end
end
