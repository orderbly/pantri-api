class AddInventoryBlobToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :inventory_blob, :jsonb, default: {}
  end
end
