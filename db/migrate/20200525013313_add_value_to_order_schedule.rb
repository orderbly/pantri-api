class AddValueToOrderSchedule < ActiveRecord::Migration[6.0]
  def change
    add_column :order_schedules, :value, :string
  end
end
