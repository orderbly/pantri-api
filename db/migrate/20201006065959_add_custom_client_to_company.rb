class AddCustomClientToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :is_custom_client, :boolean, default: false
    add_column :companies, :supplier_id, :integer
  end
end
