class AddCostingToProductVariant < ActiveRecord::Migration[6.0]
  def change
    add_column :product_variants, :sku, :string
    add_column :product_variants, :cost_price, :decimal, precision: 8, scale: 2, default: "0.0"
  end
end

