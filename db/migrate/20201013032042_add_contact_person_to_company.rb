class AddContactPersonToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :contact_person, :string
  end
end
