class AddBalanceToPayment < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :balance, :float
  end
end
