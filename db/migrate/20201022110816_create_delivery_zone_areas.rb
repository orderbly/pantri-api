class CreateDeliveryZoneAreas < ActiveRecord::Migration[6.0]
  def change
    create_table :delivery_zone_areas do |t|
      t.integer :delivery_zone_id
      t.string :name
      t.string :street_code
      t.string :postal_code

      t.timestamps
    end
  end
end
