class AddAdvanceDeliveryToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :advance_delivery, :boolean, default: false
  end
end
