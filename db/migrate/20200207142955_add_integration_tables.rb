class AddIntegrationTables < ActiveRecord::Migration[6.0]
  def change

    create_table :integrations do |t|
      t.references :company, null: false
      t.string :name, null: false
      t.string :access_token
      t.string :refresh_token
      t.string :expires
      t.jsonb :extras, null: false, default: '{}'
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end

  end
end
