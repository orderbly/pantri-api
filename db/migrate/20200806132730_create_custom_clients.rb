class CreateCustomClients < ActiveRecord::Migration[6.0]
  def change
    create_table :custom_clients do |t|
      t.string :title
      t.integer :supplier_id
      t.integer :address_id

      t.timestamps
    end
  end
end
