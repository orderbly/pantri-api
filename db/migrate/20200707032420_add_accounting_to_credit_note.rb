class AddAccountingToCreditNote < ActiveRecord::Migration[6.0]
  def change
    add_column :credit_notes, :accounting, :jsonb, default: {}
  end
end
