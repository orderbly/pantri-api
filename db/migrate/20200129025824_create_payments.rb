class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.integer :supplier_id
      t.integer :customer_id
      t.integer :order_id
      t.float :value
      t.integer :account_sale_id
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end
  end
end
