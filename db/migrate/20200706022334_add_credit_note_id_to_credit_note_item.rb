class AddCreditNoteIdToCreditNoteItem < ActiveRecord::Migration[6.0]
  def change
    add_column :credit_note_items, :credit_note_id, :integer
  end
end
