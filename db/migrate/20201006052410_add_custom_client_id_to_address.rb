class AddCustomClientIdToAddress < ActiveRecord::Migration[6.0]
  def change
    add_column :addresses, :custom_client_id, :integer
  end
end
