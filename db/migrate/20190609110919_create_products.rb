class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.references :company
      t.references :tax
      t.string :title
      t.string :description
      t.string :image
      t.boolean :got_variants
      t.uuid :uuid, default: -> { 'gen_random_uuid()' }

      t.timestamps
    end

  end
end
