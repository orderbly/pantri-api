require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)



module PantryApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    config.active_job.queue_adapter = :sidekiq
    # config.cache_store = :redis_store, ENV['REDISTOGO_URL'], { expires_in: 90.minutes }
    config.cache_store = :redis_store, ENV.fetch('REDISTOGO_URL'), { namespace: 'Raidis' }
    config.api_only = true
    config.session_store :cookie_store, key: '_interslice_session'
    config.middleware.use ActionDispatch::Cookies # Required for all session management
    config.middleware.use ActionDispatch::Session::CookieStore, config.session_options
    JWTSessions.token_store = :redis, { redis_url: ENV.fetch('REDISTOGO_URL') + '/0' }
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins [ 'https://www.orderbly.co', 'https://www.thepantry.co', 'https://www.thepantry.co/', 'https://www.orderbly.co/', 'http://www.thepantry.co', 'http://www.orderbly.co', ' http://www.thepantry.co/', ' http://www.orderbly.co/', 'http://localhost:8080', 'https://jsfiddle.net/iamjeanmalan/k53jawdv/2/']
        origins '*'
        resource '*',
          headers: :any,
          methods: [:get, :post, :put, :patch, :delete, :options, :head],
          credentials: false
      end
    end

    config.comission_fee = 0.02
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
  end
end
