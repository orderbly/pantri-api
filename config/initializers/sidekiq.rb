require 'sidekiq'
require 'sidekiq-status'

Sidekiq.configure_client do |config|
  config.redis = { url: ENV.fetch('REDISTOGO_URL'), size: 2 }
end

Sidekiq.configure_server do |config|
  config.redis = { url: ENV.fetch('REDISTOGO_URL') }
end