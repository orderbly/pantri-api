Rails.application.routes.draw do
  
  scope 'api/' do
    root :to => "products#index"
  
    resources :custom_orders


    resources :brands
    resources :custom_deliveries do
      collection do
        post 'update_custom_delivery_status/:id/:delivery_status', action: :update_custom_delivery_status
      end
    end
    
    resources :delivery_fee_brackets do
      collection do 
        get 'company_delivery_brackets/:company_uuid', action: :company_delivery_brackets
        get 'calculate_delivery_fee/:customer_address_id/:supplier_id', action: :calculate_delivery_fee
      end
    end

    resources :delivery_zones do 
      collection do 
        get 'company_delivery_zones/:company_uuid', action: :company_delivery_zones, controller: :delivery_zones
        get 'single_delivery_zone/:delivery_zone_uuid', action: :single_delivery_zone, controller: :delivery_zones
        post 'add_areas_to_delivery_zone/:delivery_zone_uuid', action: :add_areas_to_delivery_zone, controller: :delivery_zones
        post 'add_delivery_zone_fees/:delivery_zone_uuid', action: :add_delivery_zone_fees, controller: :delivery_zones
        patch 'update_delivery_zone_fees/:delivery_zone_fee_id', action: :update_delivery_zone_fees, controller: :delivery_zones
        get 'supplier_delivery_days/:delivery_address_id/:supplier_id', action: :supplier_delivery_days, controller: :delivery_zones
        get 'all_available_suburbs/:company_uuid/:city', action: :all_available_suburbs, controller: :delivery_zones
        get 'all_available_areas/:company_uuid/:search_query', action: :all_available_areas, controller: :delivery_zones
        get 'delivery_fee/:address_id/:quantity/:price/:supplier_id', action: :delivery_fee, controller: :delivery_zones
        patch 'delete_delivery_zone_fee/:delivery_zone_fee_id', action: :delete_delivery_zone_fee, controller: :delivery_zones
        patch 'delete_delivery_zone/:delivery_zone_id', action: :delete_delivery_zone, controller: :delivery_zones
        patch 'delete_delivery_zone_area/:delivery_zone_area_id', action: :delete_delivery_zone_area, controller: :delivery_zones
      end
    end

    resources :custom_clients do
      collection do
        get 'company_custom_clients/:supplier_id', action: :company_custom_clients
      end
    end
    resources :credit_note_items
    resources :account_limit_memberships
    resources :addresses

    resources :credit_limits do 
      collection do
        get 'company_credit_limits/:supplier_id', action: :company_credit_limits
        get 'available_companies/:supplier_id', action: :available_companies
        get 'account/:supplier_id/:customer_id', action: :account
      end
    end
    
    #Credit Notes
    resources :credit_notes do
      collection do
        get 'balance/:supplier_id', action: :balance
        get 'account/:supplier_id', action: :account
        get 'account/:supplier_id', action: :account
        get 'received_credit_notes', action: :received_credit_notes
        get 'current_credit_note/:id', action: :current_credit_note
        post 'general_credit_note', action: :general_credit_note
      end
    end
    
    #members
    resources :members do
      collection do
        get 'get_member/:uuid/:company_id', action: :get_member
        patch 'update_member', action: :update_member
      end
    end

    #payments
    resources :payments do
      collection do
        post 'pay_order_directly/:order_uuid', action: :pay_order_directly
        post 'create_payment/:order_uuid', action: :create_payment
      end
    end
    
    # Comments
    resources :comments
    
    # Companies Routes
    resources :companies do
      collection do
        post 'update_default_address/:id/:address_id', action: :update_default_address, controller: :companies
        post 'create_custom_customer/:supplier_uuid', action: :create_custom_customer, controller: :companies
        post 'delete_invite', action: :delete_invite, controller: :companies
        post 'add_emails', action: :add_emails, controller: :companies
        post 'delete_email', action: :delete_email, controller: :companies
        post 'update_company_logo/:company_uuid', action: :update_company_logo, controller: :companies
        post 'delete_memberships', action: :delete_memberships, controller: :companies
        post 'update_company_banner/:company_uuid', action: :update_company_banner, controller: :companies
        post 'update_delivery_reconciliation/:company_uuid', action: :update_delivery_reconciliation, controller: :companies
       
        get 'current_company', action: :current_company, controller: :companies
        get 'company_memberships', action: :company_memberships, controller: :companies
        get 'company_invites/:company_uuid', action: :company_invites, controller: :companies
        get 'purchases_dashboard', action: :purchase_dashboard, controller: :companies
        get 'sales_dashboard', action: :sales_dashboard, controller: :companies
        get 'company_by_title/:title', action: :company_by_title, controller: :companies
        get 'list_all_companies/:id', action: :list_all_companies, controller: :discount_books
        get 'slim_company_list/:company_id', action: :slim_company_list, controller: :companies
        get 'delivery_reconciliation/:company_uuid', action: :delivery_reconciliation, controller: :companies
        get 'delivery_signature/:company_uuid', action: :delivery_signature, controller: :companies
        post 'update_delivery_signature/:company_uuid', action: :update_delivery_signature, controller: :companies
        get 'find_company_by_title/:company_uuid/:query', action: :find_company_by_title, controller: :companies
        get 'fetch_minimum_order_value/:supplier_id', action: :fetch_minimum_order_value, controller: :companies
        get 'all_provinces/', action: :all_provinces, controller: :companies
        get 'all_cities/:province', action: :all_cities, controller: :companies
        get 'all_suburbs/:city', action: :all_suburbs, controller: :companies
        get 'all_areas/:search_query', action: :all_areas, controller: :companies
        get 'all_areas_by_postal_code_or_name/:search_query', action: :all_areas_by_postal_code_or_name, controller: :companies
        get 'area_by_province/:province', action: :area_by_province, controller: :companies
        get 'find_all_custom_companies/:company_uuid', action: :find_all_custom_companies, controller: :companies
        get 'all_customers/:company_uuid', action: :all_customers, controller: :companies
        get 'fetch_my_customer/:company_uuid/:customer_uuid', action: :fetch_my_customer, controller: :companies
      end
    end
    
    
    resources :delivery_allocations
    post '/delete_delivery_allocation', action: :delete_delivery_allocation, controller: :delivery_allocations

    # Orders Routes
    resources :order_items
    resources :orders
    get 'fetch_all_todays_deliveries/:supplier_id', action: :fetch_all_todays_deliveries, controller: :orders
    get 'fetch_all_completed_deliveries/:supplier_id/:date', action: :fetch_all_completed_deliveries, controller: :orders
    get 'fetch_all_upcoming_orders/:supplier_id', action: :fetch_all_upcoming_orders, controller: :orders
    get 'user_daily_deliverable_orders/:user_id/:supplier_id', action: :user_daily_deliverable_orders, controller: :orders
    get '/accept_order/:order_uuid', action: :accept_order, controller: :orders
    get '/cancel_order/:order_uuid', action: :cancel_order, controller: :orders
    get '/decline_order/:order_uuid', action: :decline_order, controller: :orders
    get '/receive_order/:order_uuid', action: :receive_order, controller: :orders
    get '/filter_sales_orders/:start_date/:end_date', action: :filter_sales_orders, controller: :orders
    get '/filter_purchase_orders/:start_date/:end_date', action: :filter_purchase_orders, controller: :orders
    get '/supplier_statement/:supplier_id', action: :supplier_statement, controller: :orders
    get '/order_breakdown/:order_uuid', action: :order_breakdown, controller: :orders
    get '/customer_statement/:customer_id', action: :customer_statement, controller: :orders
    get '/purchase_order_summary/:status', action: :purchase_order_summary, controller: :orders
    get '/sales_summary/:status', action: :sales_summary, controller: :orders
    get '/all_purchases_summary', action: :all_purchases_summary, controller: :orders
    get '/all_sales_summary', action: :all_sales_summary, controller: :orders

    get '/customer_age_analysis', action: :customer_age_analysis, controller: :orders
    get '/customer_analysis', action: :customer_analysis, controller: :orders
    get '/calendar_summary', action: :calendar_summary, controller: :orders
    get '/deliverable_orders', action: :deliverable_orders, controller: :orders
    get '/delivered_orders', action: :delivered_orders, controller: :orders
    get '/order_delivery_history', action: :order_delivery_history, controller: :orders
    get '/order_waybills', action: :waybills, controller: :orders
    get '/sales_order_summary', action: :sales_order_summary, controller: :orders
    get '/summary', action: :summary, controller: :orders
    get '/supplier_analysis', action: :supplier_analysis, controller: :orders
    get '/supplier_age_analysis', action: :supplier_age_analysis, controller: :orders
    get '/user_deliverable_orders', action: :user_deliverable_orders, controller: :orders
    
    post 'add_item_to_order',  action: :add_item_to_order, controller: :orders
    post 'update_delivery_driver_and_date', action: :update_delivery_driver_and_date, controller: :orders
    post '/create_order_schedule', action: :create_order_schedule, controller: :orders
    post '/delete_scheduled_order/:id', action: :delete_scheduled_order, controller: :orders
    post '/update_address/:id', action: :update_address, controller: :orders
    post '/duplicate_order/:id', action: :duplicate_order, controller: :orders
    put '/edit_order/:order_uuid', action: :edit_order, controller: :orders
    post '/order_signature/:order_uuid', action: :order_signature, controller: :orders
    post '/order_with_credit_note', action: :order_with_credit_note, controller: :orders
    post '/update_order_received_values/:order_uuid', action: :update_order_received_values, controller: :orders
    post '/update_order_delivery_status/:order_uuid', action: :update_order_delivery_status, controller: :orders
    post '/update_single_order_delivery_status/:order_uuid', action: :update_single_order_delivery_status, controller: :orders
    post '/update_delivery_fee/:order_uuid', action: :update_delivery_fee, controller: :orders
    post '/update_delivery_reconcilation/:order_uuid',  action: :update_delivery_reconcilation, controller: :orders
    post '/update_order_delivery_signature/:order_uuid',  action: :update_order_delivery_signature, controller: :orders
    post '/order_outstanding_items/:order_uuid',  action: :order_outstanding_items, controller: :orders
    post '/claim_credit/:order_uuid',  action: :claim_credit, controller: :orders
    post '/create_custom_order',  action: :create_custom_order, controller: :orders
    post '/order_items_breakdown/:company_uuid', action: :order_items_breakdown, controller: :orders
    post 'update_delivery_order/:company_uuid/:user_id', action: :update_delivery_order, controller: :orders

    # PriceBooks Routes
    resources :discount_books
    resources :discount_book_memberships
    get '/price_book_memberships/:discount_book_id', action: :memberships_by_discount_book, controller: :discount_book_memberships
    get '/price_book_discounts/:discount_book_id', action: :discounts_by_discount_book, controller: :product_variant_discounts
    
    # Product Routes
    resources :products
    resources :product_variants do
      collection do
        patch '/disable_product_variant/:id',  action: :disable_product_variant, controller: :product_variants
      end
    end

    resources :product_variant_discounts
    get '/get_company_products/:company_id', action: :get_company_products, controller: :products
    get '/company_products/:title', action: :company_products, controller: :products
    get '/company_products_for_order/:title/:customer_id', action: :company_products_for_order, controller: :products
    get '/products_by_company/:id', action: :products_by_company, controller: :products
    get '/variants_by_product/:id', action: :variants_by_product, controller: :product_variants
    get '/products_by_tag', action: :products_by_tag, controller: :products
    put '/product_add_tag/:id', action: :product_add_tag, controller: :products
    post '/toggle_active_product/:id', action: :toggle_active_product, controller: :products
    delete '/product_delete_tag/:id/:tag_id', action: :product_delete_tag, controller: :products
    
    # Tax Routes
    resources :taxes
    
    #Tag Routes
    resources :tags
    patch '/tags', action: :update, controller: :users

    #Account Sales Routes
    resources :account_sales

    resources :emails do
      collection do
        post 'send_order_email/:order_uuid', action: :send_order_email
        post 'send_delivery_email/:order_uuid', action: :send_delivery_email
      end
    end
    
    # User Routes
    resources :users
    get '/user_memberships', action: :user_memberships, controller: :users
    post '/user_exists', action: :user_exists, controller: :users
    post '/remove_user_avatar', action: :remove_user_avatar, controller: :users
    post '/signup', action: :signup, controller: :users
    post '/get_google_authorization', action: :get_google_authorization, controller: :users
    post '/get_google_authorization_with_company', action: :get_google_authorization_with_company, controller: :users
    post '/mobile_google_auth', action: :mobile_google_auth, controller: :users
    post '/refresh', action: :refresh, controller: :users
    post '/signin', action: :signin, controller: :users
    post '/signout', action: :signout, controller: :users
    post '/invite_user', action: :invite_user, controller: :users
    post '/user_details', action: :user_details, controller: :users
    patch '/select_current_company', action: :select_current_company, controller: :users

    resources :password_resets, only: [:create] do
      collection do
        get ':token', action: :edit, as: :edit
        patch ':token', action: :update
      end
    end

    resources :statements do
      collection do
        get 'customers_index_statement/:supplier_uuid', action: :customers_index_statement
        get 'suppliers_index_statement/:customer_uuid', action: :suppliers_index_statement
        get 'trade_statement/:supplier_uuid/:customer_uuid/:start_date/:end_date', action: :trade_statement
      end
    end

    namespace :integrations do
      get '/installed', action: :installed, controller: :integrations

      namespace :ozow do 
        post 'paid', action: :paid, controller: :ozow
        get 'success', action: :success, controller: :ozow
        get 'fail', action: :success, controller: :ozow
        get 'details', action: :details, controller: :ozow
      end

      namespace :paygate do
        post 'pay_fast/paid', action: :paid, controller: :pay_fast
        get 'pay_fast/success', action: :success, controller: :pay_fast
        get 'pay_fast/fail', action: :success, controller: :pay_fast
        get 'pay_fast/details', action: :details, controller: :pay_fast
      end

      namespace :twilio do
        post 'twilio/receive_new_message', action: :receive_new_message, controller: :twilio
      end

      namespace :tradegecko do
        get 'auth/authorize_uri/:company_id', action: :authorize_uri, controller: :auth
        get 'auth/create', action: :create, controller: :auth
        get 'products/retrieve_products/:company_uuid', action: :retrieve_products, controller: :products
        get 'contacts/all_contacts/:company_uuid', action: :all_contacts, controller: :contacts
        
        post 'contacts/create_company/:order_uuid/:company_uuid', action: :create_company, controller: :contacts
        post 'orders/sales_order/:company_uuid/:order_uuid/:tradegecko_company_id/:tradegecko_address_id', action: :sales_order, controller: :orders
        post 'disconnect/:company_uuid', action: :disconnect, controller: :configurations
        # post 'webhooks/payments', action: :payments, controller: :webhooks
        post 'webhooks/payments', action: :payments, controller: :webhooks
        post 'webhooks/orders', action: :payments, controller: :webhooks
      end

      namespace :sage do 
        post 'orders/purchase_order/:order_uuid/:sage_one_supplier_id', action: :purchase_order, controller: :orders
        post 'orders/purchase_return/:credit_note_uuid/:sage_one_supplier_id', action: :purchase_return, controller: :orders
        post 'orders/sales_order/:order_uuid/:sage_one_customer_id', action: :sales_order, controller: :orders
        post 'orders/sales_return/:credit_note_uuid/:sage_one_customer_id', action: :sales_return, controller: :orders
        
        post 'create', action: :create, controller: :auth
        post 'disconnect/:company_uuid', action: :disconnect, controller: :auth
        post 'accounts/savedefaults/:company_uuid', action: :savedefaults, controller: :accounts

        post 'sync/:company_uuid', action: :sync, controller: :configurations

        get 'configurations/:company_uuid', action: :index, controller: :configurations
        get 'contacts/suppliers/:company_uuid', action: :suppliers, controller: :contacts
        get 'contacts/customers/:company_uuid', action: :customers, controller: :contacts
        get 'accounts/:company_uuid', action: :index, controller: :accounts
        get 'items/:company_uuid', action: :index, controller: :items
      end

      namespace :optimo do
        post ':company_uuid', action: :create, controller: :optimo
        delete ':company_uuid', action: :disconnect, controller: :optimo
        post 'create_delivery/:order_uuid/:company_uuid', action: :create_delivery, controller: :optimo
      end

      namespace :xero do
          get 'auth/callback', action: :create, controller: :auth
          post 'disconnect/:company_uuid', action: :disconnect, controller: :auth
          get 'auth/authorize_uri/:company_id', action: :authorize_uri, controller: :auth
          get 'configurations/', action: :index, controller: :configurations
          get 'configurations/sync', action: :sync, controller: :configurations
          get 'accounts/', action: :index, controller: :accounts
          post 'accounts/savedefaults', action: :savedefaults, controller: :accounts
          get 'contacts/:company_uuid', action: :index, controller: :contacts
          post 'contacts/savecontact', action: :savecontact, controller: :contacts
          post 'contacts/save_credit_note_contact', action: :save_credit_note_contact, controller: :contacts
          get 'items/', action: :index, controller: :items
      end
    end
  end
end
