class UserMailer < ApplicationMailer

  def reset_password(user)
    @user = user
    mail(to: @user.email, subject: 'Reset your password')
  end

  def email_signup(user)
   
  end

  def welcome(user)

  end

end
