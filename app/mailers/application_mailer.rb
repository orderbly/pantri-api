class ApplicationMailer < ActionMailer::Base
  default from: 'jean@orderbly.co'
  layout 'mailer'
end
