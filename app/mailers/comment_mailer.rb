class CommentMailer < ApplicationMailer

  def new_comment_email(comment)
    @comment = comment

    mail(to: (@comment.order.supplier.email || "developers@orderbly.co"), subject: 'New Message')
  end

end
