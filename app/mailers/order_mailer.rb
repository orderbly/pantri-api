class OrderMailer < ApplicationMailer

  def send_order_email(order)
    @order = order
    @customer = Company.find(order['customer']['id'])
    
    mail(to: (@customer.email || "developers@orderbly.co"), subject: 'Order Sent')
  end

  def send_supplier_order(order)
    @order = order
    @supplier = Company.find(order['supplier']['id'])
    
    mail(to: (@supplier.email || "developers@orderbly.co"), subject: 'Order Sent')
  end

  def send_cancelled_order(order)
    @order = JSON.parse(order)
    @supplier = Company.find(JSON.parse(order)['supplier']['id'])

    mail(to: (@supplier.email || "developers@orderbly.co"), subject: 'Order Cancelled')
  end

  def send_declined_order(order)
    @order = JSON.parse(order)
    @customer = Company.find(JSON.parse(order)['customer']['id'])

    mail(to: (@customer.email || "developers@orderbly.co"), subject: 'Order Declined')
    
  end

  def send_accepted_order(order)
    @order = JSON.parse(order)
    @customer = Company.find(JSON.parse(order)['customer']['id'])
    
    mail(to: (@customer.email || "developers@orderbly.co"), subject: 'Order Declined')
  end

  def send_delivered_order(order)
    @order = JSON.parse(order)
    @customer = Company.find(JSON.parse(order)['customer']['id'])

    mail(to: (@customer.email || "developers@orderbly.co"), subject: 'Order Delivered')
  end

  def send_out_of_stock(order)
    @order = order
    @customer = Company.find(order['customer']['id'])

    mail(to: (@customer.email || "developers@orderbly.co"), subject: 'Order Declined')
  end

  def payment_received(customer, supplier, payment)
    @payment = payment
    @supplier = supplier

    mail(to: (@supplier.email || "developers@orderbly.co"), subject: 'Order Declined')
  end

end
