class CustomOrderSerializer < ActiveModel::Serializer
  attributes :id, :customer_id, :supplier_id, :delivery_date, :notes, :order_number, :status, :balance, :is_paid, :delivery_status, :value, :accounting, :order_schedule_id, :is_recurring, :schedule_id, :signature_image, :delivery_user_id, :parent_credit_note_id, :received_by, :uuid, :delivery_address_id, :delivery_fee, :order_reconciliation, :is_delivery_buyer_actionable, :inventory, :reorder_balance, :delivery
end
