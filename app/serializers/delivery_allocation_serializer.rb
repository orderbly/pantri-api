class DeliveryAllocationSerializer < ActiveModel::Serializer
  attributes :id, :user, :order
end
