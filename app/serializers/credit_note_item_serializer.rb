class CreditNoteItemSerializer < ActiveModel::Serializer
  attributes :id, :company_id, :product_id, :order_id, :quantity, :gross_price, :net_price, :vat_price, :product_variant_id, :received_amount
end
