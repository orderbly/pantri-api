# == Schema Information
#
# Table name: taxes
#
#  id          :bigint           not null, primary key
#  category    :integer          default("standard_tax_rate")
#  description :string
#  company_id  :bigint
#  percentage  :float            default(0.0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class TaxSerializer < ActiveModel::Serializer
  attributes :id, :category, :description, :percentage
  
end
