class DeliveryFeeBracketSerializer < ActiveModel::Serializer
  attributes :id, :min_distance, :max_distance, :price
end
