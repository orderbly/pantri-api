class AccountLimitMembershipSerializer < ActiveModel::Serializer
  attributes :id, :customer_id, :supplier_id, :account_limit
end
