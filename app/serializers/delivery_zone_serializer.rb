class DeliveryZoneSerializer < ActiveModel::Serializer
  attributes :id, :name, :company_id, :delivery_days, :cut_off_time, :zone_cut_off_day, :uuid, :zone_fee_based_on

  def zone_cut_off_day
    object.cut_off_day.humanize || ""
  end

  def zone_fee_based_on
    object.fee_based_on.humanize || ""
  end
end
