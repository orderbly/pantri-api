class CreditLimitSerializer < ActiveModel::Serializer
  attributes :id, :supplier_id, :customer_id, :credit_limit, :customer_name

  def customer_name
    begin
      self.object.customer.title ? self.object.customer.title : ""
    rescue
      ''
    end
  end
end
