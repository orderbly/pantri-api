class DiscountBookSerializer < ActiveModel::Serializer
  attributes :id, :supplier_id, :customer_id, :title, :description
end
