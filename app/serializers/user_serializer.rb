# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  company_id      :bigint
#  email           :string
#  password_digest :string
#  first_name      :string
#  last_name       :string
#  username        :string
#  contact_number  :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class UserSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :username, :contact_number, :email, :role
  
  has_many :members
  has_many :companies, through: :members

end
