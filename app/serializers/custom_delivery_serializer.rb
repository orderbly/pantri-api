class CustomDeliverySerializer < ActiveModel::Serializer
  attributes :id, :title, :supplier_id
end
