class CreditNoteSerializer < ActiveModel::Serializer
  attributes :id, :order_id, :customer_id, :balance 
  has_many :credit_note_items
end
