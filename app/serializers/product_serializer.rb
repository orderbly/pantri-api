# == Schema Information
#
# Table name: products
#
#  id           :bigint           not null, primary key
#  company_id   :bigint
#  tax_id       :bigint
#  title        :string
#  description  :string
#  image        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  got_variants :boolean
#

class ProductSerializer < ActiveModel::Serializer
  attributes :id, :description, :title, :image, :company_id, :got_variants, :tax_id, :is_active, :accounting_item, :uuid, :exclusive_companies

  belongs_to :company
  has_many :tags
  has_many :product_variants

  def product_variants
    object.product_variants.where(is_active: true)
  end
end
