# == Schema Information
#
# Table name: companies
#
#  id          :bigint           not null, primary key
#  title       :string
#  address_one :string
#  country     :string
#  city        :string
#  postal_code :string
#  contact     :string
#  website     :string
#  description :string
#  image       :string
#  colour      :string
#  category    :string
#  supplier    :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class CompanySerializer < ActiveModel::Serializer
  attributes :id, :title, :address_one, :uuid, :country, 
    :city, :postal_code, :email_list ,:contact, :website,
    :description, :image, :colour, :category, :supplier, :is_active,
    :first_address, :product_count, :accounting_integration, :default_address_id,
    :is_custom_client, :minimum_order_value, :default_account_limit, :vat_number, :logo,
    :contact_person, :email, :is_vat_registered, :trading_as, :category_list

  has_many :addresses
  
  def first_address
    self.object.addresses.first ? self.object.addresses.first : []
  end

  def product_count
    self.object.products.count
  end

  def accounting_integration
    self.object.integrations.index{|i| i.name == "xero"} != nil
  end
  
end


