# == Schema Information
#
# Table name: members
#
#  id                  :bigint           not null, primary key
#  company_id          :bigint
#  user_id             :string
#  role_id             :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class MemberSerializer < ActiveModel::Serializer
  # belongs_to :product
  attributes :id, :uuid, :permissions

  def permissions
    Base64.encode64(object.user_permissions.to_json)
  end

  belongs_to :company
  belongs_to :user
  belongs_to :role



end
