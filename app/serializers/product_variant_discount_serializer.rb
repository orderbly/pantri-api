class ProductVariantDiscountSerializer < ActiveModel::Serializer
  belongs_to :product_variant
  attributes :id, :discount_book_id, :product_variant_id, :discount_percentage
end
