class PaymentSerializer < ActiveModel::Serializer
  attributes :id, :supplier_id, :customer_id, :order_id, :value, :account_sale_id
end
