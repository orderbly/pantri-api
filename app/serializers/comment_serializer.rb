class CommentSerializer < ActiveModel::Serializer
  attributes :id, :date, :order_id, :company_id, :user_id, :message, :company_image
end
