class DiscountBookMembershipSerializer < ActiveModel::Serializer
  attributes :id, :customer_id, :discount_book_id
  belongs_to :customer_id
end
