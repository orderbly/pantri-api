class AddressSerializer < ActiveModel::Serializer
  attributes :id, :address_one, :suburb, :city, :province, :country, :postal_code, :company_id, :latitude, :longitude, :name, :additional_info
end
