class CustomClientSerializer < ActiveModel::Serializer
  attributes :id, :title, :supplier_id, :address_id, :address_one, :city, :postal_code
end
