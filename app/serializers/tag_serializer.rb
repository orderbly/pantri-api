# == Schema Information
#
# Table name: tags
#
#  id          :bigint           not null, primary key
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class TagSerializer < ActiveModel::Serializer
  attributes :id, :description
  
  belongs_to :product
  belongs_to :company
end
