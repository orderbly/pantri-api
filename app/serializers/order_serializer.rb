# == Schema Information
#
# Table name: orders
#
#  id            :bigint           not null, primary key
#  customer_id   :integer
#  supplier_id   :integer
#  delivery_date :datetime
#  notes         :text
#  order_number  :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class OrderSerializer < ActiveModel::Serializer
  attributes :id, :customer_id, :delivery_date, :notes, :order_number, :supplier_id, :status, :delivery_status, :value, :invoice, :recurring_details

  has_many :order_items

  def invoice
    object.accounting['purchases'] || object.accounting['sales']
  end

  def recurring_details
    object.order_schedule
  end
  
end
