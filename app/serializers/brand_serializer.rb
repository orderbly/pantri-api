class BrandSerializer < ActiveModel::Serializer
  attributes :id, :title, :company_id
end
