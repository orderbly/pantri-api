# == Schema Information
#
# Table name: product_variants
#
#  id                  :bigint           not null, primary key
#  product_id          :bigint
#  description         :string
#  status              :string
#  available_stock     :float            default(0.0)
#  stock               :float            default(0.0)
#  unit_amount         :float            default(0.0)
#  tax_amount          :float            default(0.0)
#  discount_percentage :float            default(0.0)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class ProductVariantSerializer < ActiveModel::Serializer
  # belongs_to :product
  attributes :id, :product_id, :description, :status, :available_stock, :track_stock, :stock, :unit_amount, :tax_amount, :discount_percentage, :_destroy, :gross_amount, :sku, :margin, :cost_price, :base_amount

end
