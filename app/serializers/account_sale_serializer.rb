class AccountSaleSerializer < ActiveModel::Serializer
  attributes :id, :supplier_id, :due_date, :customer_id, :order_id, :credit_note_value, :status
end
