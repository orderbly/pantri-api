module CompanyService
  class SaleDashboardDetailService

    def initialize(company_id:)
      @company_id = company_id
    end

    def call
      # get current month orders and subsequent values
      current_month_orders = Order.by_period(Date.today.beginning_of_month, Date.today.end_of_month).where(supplier_id: @company_id).order( 'id DESC' )
      total_current_month_orders = current_month_orders_count = 0
      current_month_customers = []
      current_month_orders.each do |o|
        current_month_orders_count += 1
        if !current_month_customers.include? o.customer.title 
          current_month_customers.push(o.customer.title)
        end
        o.order_items.each do |oi|
          if (oi.gross_price && oi.quantity)
            total_current_month_orders += (oi.gross_price * oi.quantity)
          else
            total_current_month_orders = 0
          end
        end
      end
  
      # get last month orders and subsequent values
      last_month_orders = Order.by_period(Date.today.beginning_of_month - 1.month, Date.today.beginning_of_month - 1.day).where(supplier_id: @company_id).order( 'id DESC' )
      total_last_month_orders = last_month_orders_count = 0
      last_month_customers = []
      last_month_orders.each do |o|
        if !last_month_customers.include? o.customer.title 
          last_month_customers.push(o.customer.title)
        end
        last_month_orders_count += 1
        o.order_items.each do |oi|
          if (oi.gross_price && oi.quantity)
            total_last_month_orders += (oi.gross_price * oi.quantity)
          end
        end
      end
      {
        order_totals: {
          current_month: 
            total_current_month_orders.round(2),
          last_month: 
            total_last_month_orders.round(2),
          order_totals_ratio: 
            (total_current_month_orders - total_last_month_orders).round(2)
        }, 
        average_order_values: {
          current_month: 
            current_month_orders_count > 0 ? (total_current_month_orders / current_month_orders_count).round(2) : 0,
          last_month: 
            last_month_orders_count > 0 ? (total_last_month_orders / last_month_orders_count).round(2) : 0,
          average_order_totals_ratio: 
          current_month_orders_count > 0 && last_month_orders_count > 0 ? ((total_current_month_orders / current_month_orders_count) - (total_last_month_orders / last_month_orders_count)).round(2) : 0
        },
        number_of_orders: {
          current_month: 
            current_month_orders_count.round(2),
          last_month_orders_count: 
            last_month_orders_count.round(2),
          average_number_of_orders_ratio: 
            last_month_orders_count > 0 ? (current_month_orders_count - last_month_orders_count).round(2) : 0
        },
        number_of_suppliers: {
          current_month: 
            current_month_customers.length,
          last_month_orders_count: 
            last_month_customers.length,
          number_of_suppliers_ratio: 
            last_month_customers.length > 0 ? (current_month_customers.length - last_month_customers.length).round(2) : 0
  
        },
        dashboard_sales: dashboard_sales
      }
  
    end

    def dashboard_sales
      OrdersService::SummaryBreakdown.new(Order.where(supplier_id: @company_id).last(10), 10).call
    end

  end
end