module CompanyService 
  class AddCompanyEmail
    def initialize(company_id:, email:)
      @email = email
      @company = Company.find(company_id)
    end

    def call
      add_email
    end

    def add_email
      if !@company.email_list.include? @email
        @company.email_list << @email
        if @company.save
          {:status => true}
        else
          {:status => false, message: "Something went wrong. Please try again or contact support should the problem persist"}
        end
      else
        return {:status => false, message: "This email is already on this company's email list"}
      end

    end

  end
end