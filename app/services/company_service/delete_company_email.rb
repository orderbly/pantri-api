module CompanyService 
  class DeleteCompanyEmail
    def initialize(company_id:, email:)
      @email = email
      @company = Company.find(company_id)
    end

    def call
      add_email
    end

    def add_email
      
      if @company.email_list.length > 1 
        @company.email_list.delete(@email)
        if !@company.email_list.include? @email
          if @company.save
            {:status => true, message: "The email has been successfully deleted"}
          else
            {:status => false, message: "Something went wrong. Please try again or contact support should the problem persist"}
          end
        end
      else
        return {:status => false, message: "There must be at least one company associated with this company"}
      end
    end

  end
end