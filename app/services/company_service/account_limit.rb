module CompanyService
  class AccountLimit

    def initialize(supplier_id:, customer_id:)
      @supplier_id = supplier_id
      @customer_id = customer_id
      @account_limit = 0
    end

    def call
      get_account_limit
    end

    def get_account_limit
      account_limit_memberhship = AccountLimitMembership.find_by(supplier_id: @supplier_id, customer_id: @customer_id)
      @account_limit = account_limit_memberhship&.account_limit || Company.find(@supplier_id).default_account_limit
    end

  end
end