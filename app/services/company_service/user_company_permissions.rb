module CompanyService
  class UserCompanyPermissions

    def initialize(user:, company_ids:)
      @user = user
      @company_ids = company_ids
    end

    def call
      user_memberships = @user.members.map{|m| m.company_id}
      return (@company_ids.map(&:to_s) & user_memberships.map(&:to_s)).length > 0
    end

  end
end