module StatementsService
  module Summaries
    class CreditNotesSummary

      def initialize(credit_notes)
        @credit_notes = credit_notes
      end

      def call
        credit_notes_summary
      end

      def credit_notes_summary
        @credit_notes.map do |c|
          {
            customer: c.customer.title,
            supplier: {
              title: c.supplier.title,
              id: c.supplier.id
            },
            order_number: c.order_id,
            uuid: c.uuid,
            id: c.id,
            type: "Credit Note #{c.order_id}",
            date:  DateFormat.change_to(c.created_at, "MEDIUM_DATE"),
            gross_amount: c.balance * -1,
            balance: c.balance * -1
          }
        end
      end
    
    end
  end
end