module StatementsService
  module Summaries
    class OrdersSummary

      def initialize(orders)
        @orders = orders
      end

      def call
        orders_summary
      end

      def orders_summary
        @orders.map do |o|
          {
            customer: {
              title: o.customer.title,
              id: o.customer.id
            },
            supplier: {
              title: o.supplier.title,
              id: o.supplier.id
            },
            uuid: o.uuid,
            id: o.id,
            order_number: o.id,
            type: "Order (#{o.id})",
            date:  DateFormat.change_to(o.created_at, "MEDIUM_DATE"),
            gross_amount: o.value,
            balance: o.balance 
          }
        end
      end

    end
  end
end