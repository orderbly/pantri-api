module StatementsService
  module Summaries
    class PaymentsSummary

      def initialize(payments)
        @payments = payments
      end

      def call
        payments_summary
      end

      def payments_summary
        @payments.map do |p|
          {
            customer: p.customer.title,
            supplier: {
              title: p.supplier.title,
              id: p.supplier.id
            },
            type: p.order_id ? "Payment (#{p.order_id})" : "Payment General",
            order_number: p.order_id,
            date:  DateFormat.change_to(p.created_at, "MEDIUM_DATE"),
            gross_amount: p.value * -1,
            balance: p.value * -1,
            reference: p.payment_reference,
            payment_type: p.payment_type.humanize,
            orders: p.payment_allocations.map{|p| {order: OrdersService::OrderBreakdown.new(p.order.id).call, value: p.value}}
          }
        end
      end

    end
  end
end