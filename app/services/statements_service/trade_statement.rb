module StatementsService
  class TradeStatement
    
    def initialize(supplier_uuid:, customer_uuid:, end_date:, start_date:)
      @supplier = Company.find_by(uuid: supplier_uuid)
      @customer = Company.find_by(uuid: customer_uuid)
      @end_date = begin DateTime.parse(end_date) rescue DateTime.now.end_of_month end
      @start_date = begin DateTime.parse(start_date) rescue DateTime.now.beginning_of_month end
    end

    def call
      breakdown = [ orders, payments, credit_notes ].flatten
      total_transaction_value = breakdown.map{|b| b[:gross_amount]}.sum
      sorted_breakdown = breakdown.sort {|a,b| a[:date] <=> b[:date]}
      { 
        statement: sorted_breakdown,
        opening_balance:  Money.from_amount(opening_balance).format,
        transaction_value: Money.from_amount(total_transaction_value).format,
        closing_balance: Money.from_amount(opening_balance + total_transaction_value).format,
        customer: { title: @customer.title, uuid: @customer.uuid, id: @customer.id },
        supplier: { title: @supplier.title, uuid: @supplier.uuid, id: @supplier.id }
      }
    end

    def opening_balance
      order_opening_balance - (credit_note_opening_balance + payment_opening_balance)
    end

    def orders
      StatementsService::Summaries::OrdersSummary.new(
        Order
        .where(customer_id: @customer.id, supplier_id: @supplier.id)
        .where(status: ['accepted', 'paid', 'partially_paid'])
        .where(:created_at => @start_date...@end_date+1.day)
        .order( 'id DESC' )
      ).call

    end

    def payments
      StatementsService::Summaries::PaymentsSummary.new(
        Payment
        .where(customer_id: @customer.id, supplier_id: @supplier.id)
        .where(:created_at => @start_date...@end_date+1.day)
        .order( 'id DESC' )
      ).call
    end

    def credit_notes
      StatementsService::Summaries::CreditNotesSummary.new(
        CreditNote
        .where(customer_id: @customer.id, supplier_id: @supplier.id)
        .where(:created_at => @start_date..@end_date+1.day)
        .order( 'id DESC' )
      ).call
    end

    def order_opening_balance
      Order
      .where(customer_id: @customer.id, supplier_id: @supplier.id, status: ['accepted', 'paid', 'partially_paid'])
      .where('created_at < ?', @start_date)
      .sum(:value)
    end

    def credit_note_opening_balance
      CreditNote
      .where(customer_id: @customer.id, supplier_id: @supplier.id)
      .where('created_at < ?', @start_date)
      .sum(:balance)
    end

    def payment_opening_balance
      Payment
      .where(customer_id: @customer.id, supplier_id: @supplier.id)
      .where('created_at < ?', @start_date)
      .sum(:value)
    end

  end
end