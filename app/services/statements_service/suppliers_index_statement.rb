module StatementsService
  class SuppliersIndexStatement 
    
    def initialize(customer_id)
      @customer = Company.find_by(uuid: customer_id)
      @customer_id = @customer.id
    end

    def call
      get_customer_balances
    end
    
    def get_customer_balances
      customer_ids = Order.where(customer_id: @customer.id).pluck(:supplier_id).uniq
      customers = Company.where(id: customer_ids).map{|c| OpenStruct.new(id: c.id, uuid: c.uuid, title: c.title, balance: 0)}
      customers.map do |c|
        total = purchases_total(c.id)
        total -= credit_notes_total(c.id)
        total -= payment_total(c.id)
        {id: c.id, uuid: c.uuid, title: c.title, balance: Money.from_amount(total).format}
      end
    end

    def purchases_total(supplier_id)
      Order.where(customer_id: @customer.id, supplier_id: supplier_id).where(status: ['accepted', 'paid', 'partially_paid']).pluck(:value).sum
    end

    def credit_notes_total(supplier_id)
      CreditNote.where(customer_id: @customer.id, supplier_id: supplier_id).pluck(:balance).sum
    end

    def payment_total(supplier_id)
      Payment.where(customer_id: @customer.id, supplier_id: supplier_id).pluck(:value).sum
    end

  end
end