module DeliveryService
  class ValidateFeeBrackets

    def initialize(min_value, max_value, delivery_zone, delivery_zone_fee) 
      @min_value = min_value.to_i
      @max_value = max_value.to_i
      @delivery_zone = delivery_zone
      @delivery_zone_fee = delivery_zone_fee
    end

    def call
      add_delivery_fee_bracket
    end

    def add_delivery_fee_bracket
      validator = true
      @delivery_zone.delivery_zone_fees.where(fee_based_on: @delivery_zone.fee_based_on).where.not(id: @delivery_zone_fee.id).each do |d|
        if !( validate_middle_fee(d) || validate_end_fees(d)  || validate_start_fees(d))
          validator = false
          break
        end
      end
      return validator
    end
    
    def validate_middle_fee(existing_fee)
      (existing_fee[:value_min].to_i < @max_value) && (existing_fee[:value_max].to_i <  @min_value)
    end
    
    def validate_start_fees(existing_fee)
      (( @min_value < existing_fee[:value_max].to_i) && ( @min_value < existing_fee[:value_min].to_i) && (@max_value < existing_fee[:value_min].to_i))
    end
    
    def validate_end_fees(existing_fee)
      ((@max_value > existing_fee[:value_max].to_i) && ( @min_value > existing_fee[:value_max].to_i))
    end

  end
end
