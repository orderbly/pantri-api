module DeliveryService
  module Calculations
    class DeliveryDisabledDates

      def initialize(delivery_address, supplier_id)
        @delivery_address = Address.find(delivery_address)
        @supplier_id = supplier_id
        @disabled_dates = []
      end

      def call
        if find_delivery_bracket.present?
          disable_by_cutoff_date
          { dates: @disabled_dates }
        else 
          { dates: [] }
        end
      end

      def find_delivery_bracket
        @delivery_zone_area = DeliveryZoneArea.find_by(company_id: @supplier_id, street_code: @delivery_address.postal_code, name:  @delivery_address.suburb )
        @delivery_zone = @delivery_zone_area.delivery_zone
        @delivery_zone ? @delivery_zone : false
      end

      def delivery_days
        (DateTime.now..DateTime.now+2.weeks).each do |date| 
          if !@delivery_zone.delivery_days.include? date.strftime("%A")
            @disabled_dates.push(date.to_date.to_s)
          end
        end
      end

      def disable_by_cutoff_date
        delivery_days
        case @delivery_zone.cut_off_day
        when 'previous_day'
          cut_off_previous_day
          return
        when 'same_day'
          cut_off_same_day_delivery
          return
        when 'two_days_prior'
          cut_off_two_days_prior
          return
        when 'three_days_prior'
          cut_off_three_days_prior
          return
        else
        return
        end
      end

      def cut_off_previous_day
        @disabled_dates.push(today) if !@disabled_dates.include? today
        @disabled_dates.push(today_plus_day(1)) if (!@disabled_dates.include? (today_plus_day(1))) && (cut_off_time_has_passed?)
      end

      def cut_off_two_days_prior
        @disabled_dates.push(today) if !@disabled_dates.include? today 
        @disabled_dates.push(today_plus_day(1)) if !@disabled_dates.include? today_plus_day(1)
        @disabled_dates.push(today_plus_day(2)) if (!@disabled_dates.include? today_plus_day(2)) && (cut_off_time_has_passed?)
      end

      def cut_off_three_days_prior
        @disabled_dates.push(today) if !@disabled_dates.include? today 
        @disabled_dates.push(today_plus_day(1)) if !@disabled_dates.include? today_plus_day(1)
        @disabled_dates.push(today_plus_day(2)) if !@disabled_dates.include? today_plus_day(2)
        @disabled_dates.push(today_plus_day(3)) if (!@disabled_dates.include? today_plus_day(3)) && (cut_off_time_has_passed?)
      end

      def cut_off_same_day_delivery
        @disabled_dates.push(today) if (!@disabled_dates.include? today) && (cut_off_time_has_passed?)
      end

      def cut_off_time_has_passed?
        @delivery_zone.cut_off_time ? (DateTime.now+2.hours) > @delivery_zone.cut_off_time.to_time : false
      end

      def today
        (DateTime.current).strftime("%Y-%m-%d")
      end

      def today_plus_day(number_of_day)
        (DateTime.current + number_of_day.day).strftime("%Y-%m-%d")
      end
      
    end
  end
end
