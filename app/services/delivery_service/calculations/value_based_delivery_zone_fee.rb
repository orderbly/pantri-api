module DeliveryService
  module Calculations
    class ValueBasedDeliveryZoneFee

      def initialize(delivery_address_id, value)
        @delivery_address = Address.find(delivery_address_id)
        @supplier_id = nil
      end

      def call
        if @delivery_zone
          if zone_fees.present? 
            zone_fees
          else
            default_delivery_fees
          end
        end
      end
      
      def calculate
        postal_code = @delivery_address.postal_code
        @delivery_zone_area = DeliveryZoneArea.find_by(company_id: @supplier_id, street_code: postal_code) 
        @delivery_zone = @delivery_zone_area.delivery_zone
      end
      
      def zone_fees
        @delivery_zone.delivery_zone_fees.find_by("value_min < ? AND value_max > ?", @value, @value)
      end

      def default_delivery_fees
        DeliveryZone
      end

    end
  end
end