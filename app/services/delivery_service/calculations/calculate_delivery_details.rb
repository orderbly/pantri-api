module DeliveryService
  module Calculations
    class CalculateDeliveryDetails

      def initialize(order)
        @order = order
        @disabled_dates = []
        @delivery_fee = []
      end

      def call
        if find_delivery_bracket
          calculate_delivery_fee
          disable_by_cutoff_date
          return {delivery_dates: @disabled_dates, delivery_fee: @delivery_fee.to_s}
        else 
          false
        end
      end

      def find_delivery_bracket
        postal_code = @order.delivery_address.postal_code
        @delivery_zone_area = DeliveryZoneArea.find_by(company_id: @order.supplier.id, postal_code: @order.delivery_address.postal_code) || DeliveryZoneArea.find_by(company_id: @order.supplier.id, street_code: @order.delivery_address.postal_code) 
        @delivery_zone = @delivery_zone_area.delivery_zone
        @delivery_zone ? @delivery_zone : false
      end

      def calculate_delivery_fee
        @delivery_fee = @delivery_zone.delivery_zone_fees.find_by("value_min < ? AND value_max > ?", @order.value, @order.value)
        if @delivery_fee.present?
          @delivery_fee.price
        end
      end

      def disabled_dates

      end

      def delivery_days
        (DateTime.now..DateTime.now+2.weeks).each do |date| 
          if !@delivery_zone.delivery_days.include? date.strftime("%A")
            @disabled_dates.push(date.to_date.to_s)
          end
        end
      end

      def disable_by_cutoff_date
        delivery_days
        case @delivery_zone.cut_off_day
        when 'previous_day'
          cut_off_previous_day
          return
        when 'same_day'
          cut_off_same_day_delivery
          return
        when 'two_days_prior'
          cut_off_two_days_prior
          return
        else
        return
        end
      end

      def cut_off_previous_day
        @disabled_dates.push(DateTime.current.to_date.to_s) if !@disabled_dates.include? DateTime.current.to_date.to_s && cut_off_time_has_passed?
      end

      def cut_off_two_days_prior
        tomorrow = DateTime.current.to_date.to_s
        today = (DateTime.current+1.day).to_date.to_s
        @disabled_dates.push(tomorrow) if !@disabled_dates.include? tomorrow && cut_off_time_has_passed?
        @disabled_dates.push(today) if !@disabled_dates.include? today 
      end

      def cut_off_same_day_delivery
        @disabled_dates.push(DateTime.current.to_date.to_s) if (!@disabled_dates.include? DateTime.current.to_date.to_s) && (cut_off_time_has_passed?)
      end

      def cut_off_time_has_passed?
        @delivery_zone.cut_off_time ? DateTime.now > @delivery_zone.cut_off_time.to_time : false
      end

    end
  end
end
