module DeliveryService
  module Calculations
    class DeliveryZoneFeeValue

      def initialize(delivery_address_id:, quantity:, price:, supplier_id:)
        @delivery_address = Address.find(delivery_address_id)
        @quantity = quantity
        @price = price
        @supplier_id = supplier_id
      end

      def call
        begin
          if delivery_zone.present?
            delivery_zone.fee_based_on == 'quantity' ? zone_fees_based_on_quantity : zone_fees_based_on_value
          else
            default_delivery_fees
          end
        rescue
          0
        end
      end
      
      def delivery_zone
        @delivery_zone_area = DeliveryZoneArea.find_by(company_id: @supplier_id, street_code: @delivery_address.postal_code, name:  @delivery_address.suburb )
        @delivery_zone = @delivery_zone_area.delivery_zone
      end
      
      def zone_fees_based_on_quantity
        fee_bracket = @delivery_zone.delivery_zone_fees.find_by("value_min <= ? AND value_max >= ?", @quantity, @quantity)
        fee_bracket.present? ? fee_bracket.price.to_i : default_delivery_fees
      end

      def zone_fees_based_on_value
        fee_bracket = @delivery_zone.delivery_zone_fees.find_by("value_min <= ? AND value_max >= ?", @price, @price)
        fee_bracket.present? ? fee_bracket.price.to_i : default_delivery_fees
      end

      def default_delivery_fees
        0
      end

    end
  end
end