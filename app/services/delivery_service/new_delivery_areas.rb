module DeliveryService
  class NewDeliveryAreas

    def initialize(areas, delivery_zone)
      @areas = areas
      @delivery_zone = delivery_zone
      @track = []
    end

    def call
      @areas.each do |a|
        area = Area.find(a)
        next if DeliveryZoneArea.find_by(company_id: @delivery_zone.company_id, postal_code: area.postal_code, name: area.name ).present?

        delivery_area = DeliveryZoneArea.new(
          delivery_zone_id: @delivery_zone.id,
          name: area.name,
          postal_code: area.postal_code,
          province: area.province,
          street_code: area.street_code,
          company_id: @delivery_zone.company_id
        )
        delivery_area.save ? @track.push(true) : @track.push(false)
      end
      !@track.include? false
    end

  end
end