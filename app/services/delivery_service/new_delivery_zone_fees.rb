module DeliveryService
  class NewDeliveryZoneFees

    def initialize(new_distance_fee, delivery_zone)
      @new_distance_fee = new_distance_fee
      @delivery_zone = delivery_zone
    end

    def call
      if add_delivery_fee_bracket(@new_distance_fee)
        delivery_fee = DeliveryZoneFee.new(
          delivery_zone_id: @delivery_zone.id,
          value_max: @new_distance_fee[:value_max],
          value_min: @new_distance_fee[:value_min],
          name: @new_distance_fee[:name],
          price: @new_distance_fee[:price],
          fee_based_on: @delivery_zone.fee_based_on
        )
        delivery_fee.save ? OpenStruct.new(status: true, obj: delivery_fee) : OpenStruct.new(status: false, obj: {})
      else
        OpenStruct.new(status: false, obj: {message: "The fee bracket could not be created. Please ensure there are no overlaps in minimum and maximum values."})
      end
    end

    def add_delivery_fee_bracket(new_distance_fee)
      validator = true
      @delivery_zone.delivery_zone_fees.where(fee_based_on: @delivery_zone.fee_based_on).each do |d|
        if !( validate_middle_fee(d, new_distance_fee ) || validate_end_fees(d, new_distance_fee)  || validate_start_fees(d, new_distance_fee))
          validator = false
          break
        end
      end
      return validator
    end
    
    def validate_middle_fee(existing_fee, delivery_area_fee)
        (existing_fee[:value_min].to_i < delivery_area_fee[:value_max].to_i) && (existing_fee[:value_max].to_i < delivery_area_fee[:value_min].to_i)
    end
    
    def validate_start_fees(existing_fee, delivery_area_fee)
        ((delivery_area_fee[:value_min].to_i < existing_fee[:value_max].to_i) && (delivery_area_fee[:value_min].to_i < existing_fee[:value_min].to_i) && (delivery_area_fee[:value_max].to_i < existing_fee[:value_min].to_i))
    end
    
    def validate_end_fees(existing_fee, delivery_area_fee)
        ((delivery_area_fee[:value_max].to_i > existing_fee[:value_max].to_i) && (delivery_area_fee[:value_min].to_i > existing_fee[:value_max].to_i))
    end

  end
end