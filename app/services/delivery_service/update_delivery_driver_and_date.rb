module DeliveryService
  class UpdateDeliveryDriverAndDate

    def initialize(params)
      @date = params[:date]
      @driver_id = params[:driver_id]
      @type = params[:type]
      @route = params[:route]
      @order_id = params[:order_id]
      @supplier_id = nil
    end

    def call
      @type == "orderbly" ? update_order : update_custom_delivery
      return_deliveries
    end
    
    def return_deliveries
      if @route == "/upcoming_deliveries"
        OrdersService::Summaries::FetchAllUpcomingOrders.new(@supplier_id).call
      elsif @route == "/todays_deliveries"
        OrdersService::Summaries::FetchAllTodaysOrders.new(@supplier_id).call
      else
        OrdersService::Summaries::FetchAllDeliveredOrders.new(@supplier_id, DateTime.now.strftime("%FT%T")).call
      end
    end

    def update_order
      order = Order.find(@order_id)
      order.update(delivery_date: @date) if @date
      if @driver_id 
        DeliveryAllocation.where(order_id: order.id).delete_all
        DeliveryAllocation.create!(user_id: @driver_id, order_id: order.id) if order.delivery_status !=  "delivered" || "order_cancelled" ? true : false
      end
      @supplier_id = order.supplier_id
    end

    def update_custom_delivery
      custom_delivery = CustomDelivery.find(@order_id)
      custom_delivery.update(delivery_driver_id: @driver_id) if @driver_id
      custom_delivery.update(due_date: @date) if @date
      @supplier_id = custom_delivery.supplier_id
    end 

  end
end