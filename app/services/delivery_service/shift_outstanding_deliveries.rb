module DeliveryService
  class ShiftOutstandingDeliveries

    def call
      Order.all.where('delivery_date < ?',  DateTime.now.beginning_of_day).where.not(delivery_status: 'delivered').update_all(delivery_date: DateTime.now.beginning_of_day +  6.hours)
      CustomDelivery.all.where('due_date < ?',  DateTime.now.beginning_of_day).where.not(delivery_status: 'delivered').update_all(due_date: DateTime.now.beginning_of_day +  6.hours)
    end

  end
end