module DiscountBookService
    class DiscountBookEmptyCheck
        
        def initialize(discount_book)
            @discount_book = discount_book
        end

        def call
            check
        end

        def check
            !@discount_book.discount_book_memberships.any?
        end
    end 
end