module DiscountBookService
  class ListCompanies

    def initialize(current_company_id)
      @current_company_id = current_company_id
    end

    def call 
      list_companies
    end

    def list_companies
      @companies = []
      @all_companies = Company.where.not(id: @current_company_id).each do |c|
        if !DiscountBook.joins(:discount_book_memberships).where(discount_book_memberships: { customer_id: c.id }, supplier_id: @current_company_id).any?
          @companies.push({title: c.title, id: c.id})
        end
      end
      @companies
    end
  end
end