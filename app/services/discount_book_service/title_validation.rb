module DiscountBookService
  class TitleValidation

    def initialize (company_id:, title:)
      @company_id = company_id
      @title = title
    end

    def call
      validate
    end

    def validate
      title = DiscountBook.find_by(supplier_id: @company_id, title: @title)
      if title
        { status: false, message: "There is already a price book with this title." }
      else
        {status: true}
      end
    end

  end
end