module DiscountBookService
    class DiscountBookMembershipsCreate
        def initialize(customer_ids, discount_book_id)
            @customer_ids = customer_ids
            @discount_book_id = discount_book_id
            @supplier_id = DiscountBook.find(discount_book_id).supplier_id
        end

        def call
            check
        end

        def check
            @all_added = true
            @to_return = []
            @customer_ids.each do |cid|
                if !DiscountBookMembership.joins(:discount_book).where(customer_id: cid).where(:discount_books => {supplier_id: @supplier_id}).any?
                    @discount_book_membership = DiscountBookMembership.new({
                        customer_id: cid,
                        discount_book_id: @discount_book_id
                    });
                    if @discount_book_membership.save
                        @to_return.push({
                            id: @discount_book_membership.id,
                            discount_book_id: @discount_book_id,
                            customer: @discount_book_membership.customer.title
                        })
                    end
                else
                    @all_added = false
                end
            end
            {all_added: @all_added, memberships: @to_return}
        end
    end
end