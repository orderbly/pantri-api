module DiscountBookService
  class BuildDiscountBookMembershipsService
      def initialize(discount_book_id)
          @discount_book_id = discount_book_id
      end

      def call
          build_memberships
      end

      def build_memberships
          @discount_book_membership = DiscountBookMembership.where(discount_book_id: @discount_book_id)
          payload = []
          @discount_book_membership.each do |dbm|
              payload.push({
              id: dbm.id,
              customer: dbm.customer.title,
              discount_book_id: dbm.discount_book_id
              })
          end
          payload
      end
  end
end