
module CommentsService
  class CreateComment

    def initialize(user, message, order, type=0)
      @user = user
      @message = message
      @order = order
      @type = type
    end

    def call 
      comment = Comment.create(
        order_id: @order.id,
        date: Date.new,
        message: @message,
        user_id: @user.id,
        comment_type: @type,
        company_id: @user.current_company,
        company_image:  Company.find(@user.current_company).image
      )
    end


  end
end
