module PaymentService
  class PayOrder

    def initialize(order_id:, value:, payment_reference:, supplier_id:,customer_id:, user:, payment_date:, payment_type:)
      @order_id = order_id
      @value = value
      @payment_reference = payment_reference
      @payment_type = payment_type
      @payment_date = payment_date
      @supplier_id = supplier_id
      @customer_id = customer_id
      @user = user
    end

    def call 
      pay_order
    end

    def pay_order
      @order = Order.find_by(id: @order_id)
      if @order
        payment = Payment.new(
          supplier_id: @supplier_id,
          customer_id: @customer_id,
          value: @value,
          order_id: @order.id,
          payment_reference: @payment_reference,
          payment_date: @payment_date,
          user_id: @user.id,
          payment_type: @payment_type
        )
        if payment.save
          payment_allocation = PaymentAllocation.create(order_id: @order.id, payment_id: payment.id, value: @value)
          if payment_allocation.save
            new_balance = @order.balance - @value.to_f
            new_balance = 0 if new_balance < 0.01
            @order.balance.to_f > @value.to_f ? @order.update(status: 2, balance: new_balance, is_paid: 2) : @order.update(status: 2, balance: new_balance, is_paid: 0)
          else 
            # trigger
            return { type: 'error', message: 'Payment allocation could not be created. Please contact support' }
          end
          # trigger
          return { type: 'success', message: 'Payment successfully created'}
        else
          # trigger
          return { type: 'error', message: 'Payment could not be created. Please contact support' }
        end
      else

        PaymentService::GeneralCompanyPayment.new(@customer_id, @supplier_id, @value, @payment_reference, @order_id).call
      end
    end

  end
end