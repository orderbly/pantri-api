module PaymentService
  class GeneralCompanyPayment

    def initialize(customer_id, supplier_id, value, transaction_id )
      @customer_id = customer_id
      @supplier_id = supplier_id
      @value = value
      @transaction_id = transaction_id
    end

    def call 
      create_and_offet_general_payment
    end

    def create_and_offet_general_payment
      payable_orders = Order.all.where(customer_id: @customer_id, supplier_id: @supplier_id, is_paid: ["false", "partial"], status: ["accepted"]).order("id ASC")
      payment = Payment.new(supplier_id: @supplier_id, customer_id: @customer_id, value: @value, balance: @value, transaction_id: @transaction_id)
      if payment.save
        payable_orders.each do |payable_order|
          break if payment.balance <= 0
          if payable_order.balance > payment.balance
            payment_allocation = PaymentAllocation.new(order_id: payable_order.id, payment_id: payment.id, value: payment.balance)
            if payment_allocation.save
              payable_order_balance = payable_order.balance - payment.balance
              payment.update(balance: 0)
              payable_order.update(is_paid: 2, balance: payable_order_balance)
              return {type: 'success', message: 'Account Payment successfully created and allocated'}
            else
              return {type: 'error', message: 'Account Payment allocation could not be created. Please contact support'}
              # trigger
            end
          elsif payable_order.balance <= payment.balance
            payment_allocation = PaymentAllocation.new(order_id: payable_order.id, payment_id: payment.id, value: payable_order.balance)
            if payment_allocation.save
              payment_balance = payment.balance - payable_order.balance
              payable_order.update(balance: 0, is_paid: 0)
              payment.update(balance: payment_balance)
            else
              return {type: 'error', message: 'Account Payment allocation could not be created. Please contact support'}
              # trigger
            end
          end
        end
        # trigger
        return {type: 'success', message: 'Account Payment successfully created'}
      else 
        return {type: 'error', message: 'Account Payment allocation could not be created. Please contact support'}
        # trigger
        # trigger
        # trigger
      end
    end

  end
end