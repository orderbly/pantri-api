module FulfilmentService
  class RouteMapping

    def initialize(user, supplier)
      @user = user
      @supplier = supplier
    end

    def call 
      route = route4me_api_post
    end

    def route4me_api_key
      route4me_api_key = ENV['ROUTE4ME_API_KEY']
    end
  
    def route_url
      "https://api.route4me.com/api.v4/optimization_problem.php?api_key=#{route4me_api_key}"
    end

    def route4me_api_post
      resp = Faraday.post(route_url, addresses.to_json) do |req|
        req.headers['Accept'] = 'application/json'
        req.headers['Content-Type'] = 'application/json'
      end
      if JSON.parse(resp.body)['parameters']['member_id'].present? && JSON.parse(resp.body)['optimization_problem_id'].present?
        delivery_optimization_id = JSON.parse(resp.body)['optimization_problem_id']
        member_id = JSON.parse(resp.body)['parameters']['member_id']
      else
        return FulfilmentService::RegularDeliveryRoute.new(@user, @supplier).call
      end
      optimization_list_url = "https://api.route4me.com/api.v4/optimization_problem.php?optimization_problem_id=#{delivery_optimization_id}&api_key=#{route4me_api_key}&member_id=#{member_id}"
      optimization_list = Faraday.get(optimization_list_url) do |req|
        req.headers['Accept'] = 'application/json'
        req.headers['Content-Type'] = 'application/json'
      end
      address_new = new_address_list(JSON.parse(optimization_list.body)['addresses'])
      address_new
    end

    def new_address_list(addresses)
      address_list = []
      addresses.drop(1).each do |address|
        if Order.find_by(id: address['name'].gsub("order:", "" )).present?
          address_list.push(FulfilmentService::Breakdowns::CustomOrderBreakdown.new.custom_order_response(Order.find_by(id: address['name'].gsub("order: ", "" ))))
        elsif CustomDelivery.find_by(id: address['name'].gsub("custom: ", "" )).present?
          address_list.push(FulfilmentService::Breakdowns::CustomDeliveryBreakdown.new.custom_delivery_response(CustomDelivery.find_by(id: address['name'].gsub("custom: ", "" ))))
        end
      end
      address_list
    end


    def deliverable_orders
      @user.delivery_orders.where(:delivery_date => DateTime.now.beginning_of_day ... DateTime.now.end_of_day).where(supplier_id: @supplier.id).map do |order| 
        if order.delivery_address&.longitude.present? && order.delivery_address&.latitude.present?
          {
            "lng": order.delivery_address.longitude,
            "lat": order.delivery_address.latitude,
            "name": "order: #{order.id}",
            "is_depot": false,
            "time": 3000,
            "address": "#{order.delivery_address.address_one} #{order.delivery_address.city} #{order.delivery_address.postal_code}"
          }
        end
      end
    end

    def custom_deliveries
      @user.custom_deliveries.where(:due_date => DateTime.now.beginning_of_day ... DateTime.now.end_of_day).where(supplier_id: @supplier.id).map do |custom_delivery| 
        if custom_delivery.address.longitude.present? && custom_delivery.address.latitude.present?
          {
            "lng": custom_delivery.address.longitude,
            "lat": custom_delivery.address.latitude,
            "name": "custom: #{custom_delivery.id}",
            "is_depot": false,
            "time": 3000,
            "address": "#{custom_delivery.address.address_one} #{custom_delivery.address.city} #{custom_delivery.address.postal_code}"
          }
        end
      end
    end

    def all_deliverable_orders
      deliverable_orders.compact + custom_deliveries.compact
    end


    def addresses
      address_payload = { 
        parameters: {
          algorithm_type: "1",
          device_type: "web",
          distance_unit: "km",
          optimize: "Distance",
          route_max_duration: "86400",
          route_time: "0",
          store_route: "true",
          travel_mode: "Driving",
          vehicle_capacity: "1",
          vehicle_max_distance_mi: "10000"
        },
        addresses: all_deliverable_orders
      }

      address_payload[:addresses].unshift({
        "lng": @supplier.default_address.longitude,
        "lat": @supplier.default_address.latitude,
        "name": @supplier.title,
        "is_depot": true,
        "time": 3000,
        "address": "#{@supplier.default_address.address_one} #{@supplier.default_address.city} #{@supplier.default_address.postal_code}"
      })
      address_payload
    end


  end
end