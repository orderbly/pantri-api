module FulfilmentService
  module Breakdowns
    class CustomOrderBreakdown

      def custom_order_response(order)
        {
          address: {
            address_one: order&.delivery_address&.address_one,
            city: order&.delivery_address&.city,
            country: order&.delivery_address&.country,
            latitude: order&.delivery_address&.latitude,
            longitude: order&.delivery_address&.longitude,
            postal_code: order&.delivery_address&.postal_code,
            suburb:order&.delivery_address&.suburb,
          },
          driver: {
            id: order.delivery_assignees&.first&.id || "",
            name: order.delivery_assignees&.first ? "#{order.delivery_assignees&.first&.first_name} #{order.delivery_assignees&.first&.last_name}" : "Unassigned",
          },
          id: order.id,
          type: "orderbly",
          balance: order.balance,
          customer: order&.customer&.title,
          delivery_status: order.delivery_status.humanize,
          order_date: DateFormat.change_to(order.delivery_date, "LONG_DATE"),
          order_number: order.order_number,
          order_uuid: order.uuid,
          order_status: order.status.humanize,
          contact: order&.customer&.contact,
          delivery_number: order.delivery_number
        }
      end

    end
  end
end
