module FulfilmentService
  module Breakdowns
    class CustomDeliveryBreakdown

      def custom_delivery_response(custom_delivery) 
        {
          address: {
            address_one: custom_delivery.address.address_one,
            city: custom_delivery.address.city,
            country: custom_delivery.address.country,
            latitude: custom_delivery.address.latitude,
            longitude: custom_delivery.address.longitude,
            postal_code: custom_delivery.address.postal_code,
            suburb:custom_delivery.address.suburb,
          },
          type: "custom",
          driver: {
            id: custom_delivery.delivery_driver.id || "",
            name: "#{custom_delivery.delivery_driver.first_name} #{custom_delivery.delivery_driver.last_name}" || "",
          },
          balance: 0,
          id: custom_delivery.id,
          customer: custom_delivery.customer.title,
          delivery_status: custom_delivery.delivery_status.humanize,
          order_date: DateFormat.change_to(custom_delivery.due_date, "LONG_DATE"),
          order_number: custom_delivery.order_number,
          order_uuid: "",
          order_status: "N/A",
          contact: custom_delivery.customer.contact || "",
          email: custom_delivery.customer.email || "",
        }
      end

    end
  end
end
