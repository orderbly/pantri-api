module FulfilmentService
  class RegularDeliveryRoute
    def initialize(user, supplier)
      @user = user
      @supplier = supplier
    end

    def call 
      new_address_list
    end

    def new_address_list
      deliverable_orders + custom_deliveries
    end


    def deliverable_orders
      @user.delivery_orders.where(:delivery_date => DateTime.now.beginning_of_day ... DateTime.now.end_of_day).where(supplier_id: @supplier.id).where.not(status: ['cancelled', 'declined']).map do |order| 
        FulfilmentService::Breakdowns::CustomOrderBreakdown.new.custom_order_response(order)
      end
    end

    def custom_deliveries
      @user.custom_deliveries.where(:due_date => DateTime.now.beginning_of_day ... DateTime.now.end_of_day).where(supplier_id: @supplier.id).map do |custom_delivery|
        FulfilmentService::Breakdowns::CustomDeliveryBreakdown.new.custom_delivery_response(custom_delivery)
      end
    end

    def all_deliverable_orders
      deliverable_orders + custom_deliveries
    end


  end
end
