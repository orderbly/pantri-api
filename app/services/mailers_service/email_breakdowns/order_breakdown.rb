module MailersService
  module EmailBreakdowns
    class OrderBreakdown
      def initialize(order_id)
        @order_id = order_id
      end
  
      def call
        order_breakdown
      end
  
      def order_breakdown
        order =  Order.find(@order_id)
        breakdown = {
          id: order.id,
          customer: {
            title: order.customer.title,
            id: order.customer.id,
            customer_address: order.customer.default_address,
            vat_number: order.customer.vat_number || ""
          },
          order_number: order.order_number,
          order_uuid: order.uuid,
          balance: order.balance,
          invoice_number: order.order_number,
          order_status: order.status,
          delivery_status: order.delivery_status,
          order_date: DateFormat.change_to(order.delivery_date, "LONG_DATE"),
          supplier: {
            id: order.supplier.id,
            title: order.supplier.title,
            address_one: !order.supplier.addresses.empty? ? order.supplier.addresses.first.address_one : "",
            suburb: !order.supplier.addresses.empty? ? order.supplier.addresses.first.suburb : "",
            city: !order.supplier.addresses.empty? ? order.supplier.addresses.first.city : "",
            country: !order.supplier.addresses.empty? ? order.supplier.addresses.first.country : "",
            postal_code: !order.supplier.addresses.empty? ? order.supplier.addresses.first.postal_code : "",
            contact: !order.supplier.addresses.empty? ? order.supplier.contact : "",
            vat_number: order.supplier.vat_number || ""
          },
          net_amount: Money.from_amount(OrdersService::CalculateDerivedValues.new(order.order_items).call[1]).format,
          vat_amount: Money.from_amount(OrdersService::CalculateDerivedValues.new(order.order_items).call[2]).format,
          gross_amount: Money.from_amount(OrdersService::CalculateDerivedValues.new(order.order_items).call[0]).format,
          order_items: derived_order_items(order.order_items),
          delivery_user: "#{order.delivery_user&.first_name} #{order.delivery_user&.last_name}",
          delivery_address: order.delivery_address,
          received_by: order.received_by,
          order_note: order.notes,
          order_reconciliation: order.order_reconciliation,
          delivery_note: Comment.find_by(order_id: order.id, comment_type: "delivery").present? ? Comment.find_by(order_id: order.id, comment_type: "delivery").message : ""
        }
  
        breakdown
      end
  
      def derived_order_items(order_items)
        items = []
        order_items.each{|o| items.push({  
          id: o.id,
          product_id: o.product_id,
          product_variant_id: o.product_variant_id,
          received_amount: o.received_amount ? o.received_amount : 0,
          quantity: o.quantity,
          order_id: o.order_id,
          gross_price: Money.from_amount(o.gross_price).format,
          net_price: Money.from_amount(o.net_price).format,
          vat_price: Money.from_amount(o.vat_price).format,
          company_id: o.company_id,
          image: o.product_variant.product.image,
          product_title: o.product_variant.product.title + " (" + o.product_variant.description + ")"
        })}
        items
      end

    end
  end
end
