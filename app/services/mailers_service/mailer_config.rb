module MailersService
  class MailerConfig

    def initialize(to:, from:, data:, template:)
      @to_list = to
      @from = from
      @data = data
      @template = template
    end

    def call
      resp = Faraday.post('https://api.sendgrid.com/v3/mail/send') do |req|
        req.headers['Authorization'] = 'Bearer ' + ENV['SENDGRID_API_KEY']
        req.headers['Content-Type'] = 'application/json'
        req.body = body.to_json
      end
    end

    def body 
        return {
          "from" => {
            "email" => @from
          },
          "personalizations" => [
            {
              "to" => @to_list.map{|e| {email: e}},
              "dynamic_template_data": @data
            }
          ],
          "template_id": @template
      }
    end

  end
end

