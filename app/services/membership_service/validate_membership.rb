module MembershipService
  class ValidateMembership

    def initialize(user_id:, company_id:)
      @company_id = company_id
      @user_id = user_id
    end

    def validate
      Member.where(user_id: @user_id, company_id: @company_id).count > 0 ? true : false
    end

  end
end