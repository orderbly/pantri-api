module MembershipService
  class CompanyMemberships

    def initialize(company_id)
      @company_id = company_id
    end

    def call
      memberhip_details
    end

    def memberhip_details
      memberships = Company.find(@company_id).members
      membership_breakdown(memberships)
    end

    def membership_breakdown(memberships)
      payload = []
      memberships.each do |m|
        if m.user
          payload.push({
            id: m.id,
            user_id: m.user_id,
            company_id: m.company.id,
            uuid: m.uuid,
            user_email: m&.user&.email,
            user_name: "#{m&.user&.first_name} #{m&.user&.last_name}",
            role: m.role.description
          })
        end
      end
      payload
    end

  end
end