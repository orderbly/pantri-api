module MembershipService
  class ValidateMembershipPermissions

    def initialize(permissions)
      @permissions = permissions
    end

    def call
      sorted_permissions = get_em(@permissions).sort { |a, b| a <=> b }
      required_permissions = validate_membership_keys
      
      sorted_permissions === validate_membership_keys
    end

    def get_em(h)
      h.each_with_object([]) do |(k,v),keys|      
        keys << k
        keys.concat(get_em(v)) if v.is_a? Hash
      end
    end

    def validate_membership_keys
      [ 
        "user_settings",
        "can_create_user",
        "can_edit_user",
        "can_delete_user",
        "can_delete_order_items",
        "company_settings",
        "can_view_dashboard",
        "can_view_company_settings",
        "can_change_company_details",
        "can_update_company_images",
        "can_add_address",
        "can_edit_address",
        "can_edit_company_emails",
        "can_add_company_emails",
        "tax_settings",
        "can_view_tax_settings",
        "can_add_tax",
        "can_edit_tax",
        "delivery_settings",
        "can_view_delivery_settings",
        "can_edit_delivery_bracket",
        "can_create_delivery_bracket",
        "can_toggle_delivery_reconciliation",
        "integration_settings",
        "can_view_integrations",
        "can_add_integration",
        "can_disable_integration",
        "marketplace_settings",
        "can_view_marketplace",
        "can_view_supplier_profiles",
        "can_create_orders",
        "can_integrate_sales",
        "purchases_settings",
        "can_view_all_purchases",
        "can_cancel_purchases",
        "can_edit_purchases",
        "can_integrate_purchases",
        "sales_settings",
        "can_view_all_sales",
        "can_view_own_custom_sales",
        "can_create_payment",
        "can_accept_sale",
        "can_decline_sale",
        "can_edit_sale",
        'can_update_credit_limits',
        "can_edit_quantity",
        "can_edit_gross_price",
        "can_allocate_delivery",
        "can_update_delivery_status",
        "can_create_custom_sales",
        "can_create_credit_note",
        "product_settings",
        "can_edit_product",
        "can_create_product",
        "pricing_settings",
        "can_view_pricebooks",
        "can_create_pricebook",
        "can_edit_pricebook",
        "can_create_pricebook_membership",
        "credit_limit_settings",
        "can_view_credit_limits",
        "can_edit_credit_limits",
        "can_create_credit_limits",
        "statements_settings",
        "can_view_sales_statement",
        "can_view_purchase_statement",
        "fulfilment_settings",
        "can_view_own_deliveries",
        "can_view_waybills",
        "can_view_all_daily_deliveries",
        "can_view_all_upcoming_deliveries",
        "can_view_all_completed_deliveries",
        "can_create_custom_delivery"
      ].sort { |a, b| a <=> b }
    end

  end
end
