
module ProductVariantsService
  class UpdateStock

    def initialize(product_variant_id:, quantity:)
      @product_variant_id = product_variant_id
      @quantity = quantity
    end

    def call
      update_stock
    end

    def update_stock
      product_variant = ProductVariant.find(@product_variant_id.to_i)
      if product_variant.track_stock && product_variant.available_stock >= @quantity.to_i
        product_variant.available_stock -= @quantity.to_i
      end
      product_variant.save
    end

  end
end
