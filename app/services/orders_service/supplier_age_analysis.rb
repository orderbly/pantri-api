module OrdersService
  class SupplierAgeAnalysis

    def initialize(id)
      @id = id
    end

    def call
      supplier_age_analysis
    end

    def supplier_age_analysis
      orders = []
  
      Order.all.where(customer_id: @id, status: 2).where.not(is_paid: 0).each{|as| orders.push({
        supplier_id: as.supplier_id,
        customer_id: as.customer_id,
        supplier_title: as.supplier.title,
        customer_title: as.customer.title,
        status:  as.status,
        order_id: as.id,
        created_at: as.created_at,
        value: as.value
      })}
  
      orders.group_by{|h| h[:supplier_title]}
    end

  end
end