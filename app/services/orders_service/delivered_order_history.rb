module OrdersService
  class DeliveredOrderHistory

    def initialize(id)
      @id = id
    end

    def call
      delivered_orders
    end

    def delivered_orders
      orders = Order.where(supplier_id: @id).where(delivery_status: 'delivered').order( 'id DESC' )

      OrdersService::Summaries::OrderDeliverySummaryBreakdown.new(orders).call
    end

  end
end