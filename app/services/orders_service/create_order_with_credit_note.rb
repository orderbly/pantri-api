module OrdersService
  class CreateOrderWithCreditNote

    def initialize(order_params:, current_user:)
      @order_params = order_params
      @current_user = current_user
    end

    def call
      create_create_note
      create
    end

    def parent_order
      Order.find_by(uuid: @order_params[:parent_order])
    end

    def create_create_note
      credit_note = CreditNote.create(order_id: parent_order.id, customer_id: parent_order.customer_id, supplier_id: parent_order.supplier_id)
      parent_order.order_items.each do |oi|
        if oi.received_amount < oi.quantity
          CreditNoteItem.create(
            product_id: oi[:product_id],
            product_variant_id: oi[:product_variant_id],
            quantity: oi[:quantity] - oi[:received_amount].to_f,
            gross_price: oi[:gross_price],
            net_price: oi[:net_price],
            vat_price: oi[:vat_price],
            company_id: oi[:company_id],
            credit_note_id: credit_note.id
          )
        end
      end
      credit_note.balance = credit_note.remaining_balance = credit_note.credit_note_items.map{|cn| cn.quantity * cn.gross_price}.sum
      credit_note.save!
    end

    def create
      @order = Order.new(@order_params.except(:parent_order))
      @order.is_paid = 1
      @order.balance = 0.0
      @order.order_number = (Order.all.where(supplier: @order_params[:supplier_id]).length)
      @order.order_items.each do |oi|
        @order.balance += oi.quantity * oi.gross_price
        @order.value += oi.quantity * oi.gross_price
      end
      @order_params[:customer_id] ? @order.customer_id = @order_params[:customer_id] : @order.customer_id = @current_user.current_company
      @order.delivery_address_id = parent_order.delivery_address_id
      if check_adequate_stock_levels(@order).include?(false)
        claim_credit_on_all_unpaid_orders(@order.supplier_id, @order.customer_id)
        return {:obj => @order, :status => true, :reason => 'Insufficient stock was available, for the reorder. A credit note has been created for the outstanding products'}
      end

      if @order.save
        claim_credit_on_created_order(@order)
        create_comment_order()
        update_stock_values(@order)
        send_mails(@order)
        return {:obj => @order, :status => true, :reason => 'A new Order has been created'}
      else 
        claim_credit_on_all_unpaid_orders(@order.supplier_id, @order.customer_id)
        return {:obj => {}, :status => false, :reason => 'New order could not be created'}
      end
    end

    def claim_credit_on_created_order(order)
      CreditNoteService::ClaimCreditNote.new(order).call
    end

    def claim_credit_on_all_unpaid_orders(supplier_id, customer_id)
      CreditNoteService::ClaimCreditNote.new(parent_order).call
      unpaid_order = Order.where(supplier_id: supplier_id, customer_id: customer_id).where.not(is_paid: 'true', id: parent_order.id).order("id ASC")
      if !unpaid_order.empty?
        unpaid_order.each do |order|
          CreditNoteService::ClaimCreditNote.new(order).call
        end
      end
    end

    def check_adequate_stock_levels(order)
      adequate_values = []
      order.order_items.each do |order_item|
        adequate_values.push(ProductVariant.verify_stock(order_item.product_variant_id, order_item.quantity))
      end
      return adequate_values
    end

    def update_stock_values(order)

      order.order_items.each do |order_item|
        ProductVariantsService::UpdateStock.new(product_variant_id: order_item.product_variant_id, quantity: order_item.quantity).call
      end
    end

    def create_comment_order
      order_bot = User.find_by(username: "OrderBot")
      comment_user = order_bot ? order_bot : @current_user
      if Company.find_by(id: comment_user.current_company)
        Comment.create(order_id: @order.id, comment_type: 1, date: Date.new, message: "#{@current_user.first_name} #{@current_user.last_name} created a new order", user_id: comment_user.id, company_id: comment_user.current_company, company_image:  Company.find(comment_user.current_company).image)
      end
    end

    def send_mails(order)
      order_breakdown = OrdersService::OrderBreakdown.new(order.id).call
      Pusher.trigger('orders', "company_#{order.supplier.id}", {customer: {title: order_breakdown[:customer][:title]}, order_number: order_breakdown[:order_number]})
      NewOrderJob.perform_later(order_breakdown)
    end

  end
end

