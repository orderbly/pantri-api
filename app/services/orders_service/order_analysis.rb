module OrdersService
  class OrderAnalysis

    def customer_analysis(supplier_id)
      cids = Order.where(supplier_id: supplier_id).pluck(:customer_id).uniq
      companies = Company.where(id: cids).map{|c| OpenStruct.new(id: c.id, uuid: c.uuid, title: c.title, balance: 0)}
      companies.map do |c|
        total = Order.where(customer_id: c.id, supplier_id: supplier_id).where(status: ['accepted', 'paid', 'partially_paid']).pluck(:value).sum
        total -= Payment.where(customer_id: c.id, supplier_id: supplier_id).pluck(:value).sum
        total -= CreditNote.where(customer_id: c.id, supplier_id: supplier_id).pluck(:balance).sum
        {id: c.id, uuid: c.uuid, title: c.title, balance: total}
      end
    end
  
    def supplier_analysis(customer_id)
      cids = Order.where(customer_id: customer_id).pluck(:supplier_id).uniq
      companies = Company.where(id: cids).map{|c| OpenStruct.new(id: c.id, uuid: c.uuid, title: c.title, balance: 0)}
      companies.map do |c|
        total = Order.where(supplier_id: c.id, customer_id: customer_id).where(status: ['accepted', 'paid', 'partially_paid']).pluck(:value).sum
        total -= Payment.where(supplier_id: c.id, customer_id: customer_id).pluck(:value).sum
        total -= CreditNote.where(supplier_id: c.id, customer_id: customer_id).pluck(:balance).sum
        {id: c.id, uuid: c.uuid, title: c.title, balance: Money.from_amount(total).format}
      end
    end

  end
end
