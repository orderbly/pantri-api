module OrdersService
  class CalculateDerivedValues

    def initialize(order_items)
      @order_items = order_items
    end

    def call
      calculate_derived_values
    end

    def calculate_derived_values
      gross_price = net_price = vat_price = 0
      product = ''

      @order_items.map{|o|
        quantity = o.quantity ? o.quantity : 0
        g_price = o.gross_price ? o.gross_price : 0 
        n_price = o.net_price ? o.net_price : 0 
        v_price = o.vat_price ? o.vat_price : 0 
        gross_price += (g_price * quantity)
        net_price += (n_price  * quantity)
        product = o&.product_variant&.description,
        vat_price += (v_price * quantity)
      }
  
      return [gross_price.round(2), net_price.round(2), vat_price.round(2)]
    end

  end
end