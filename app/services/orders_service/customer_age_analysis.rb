module OrdersService
  class CustomerAgeAnalysis

    def initialize(id)
      @id = id
    end

    def call
      customer_age_analysis
    end

    def customer_age_analysis
      orders = []
      Order.all.where(supplier_id: @id, status: 2).where.not(is_paid: 0).each{|as| orders.push({
        supplier_id: as.supplier_id,
        customer_id: as.customer_id,
        supplier_title: as.supplier.title,
        customer_title: as.customer.title,
        status:  as.status,
        order_id: as.id,
        created_at: as.created_at,
        value: as.value
      })}

      breakdown = orders.group_by{|h| h[:customer_title]}
      breakdown
    end

  end
end