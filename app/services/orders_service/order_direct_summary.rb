module OrdersService
  class OrderDirectSummary

    def initialize(order)
      @order = order
    end

    def call
      order_direct_summary
    end

    def order_direct_summary
      breakdown = {
        customer: @order.customer.title,
        order_uuid: @order.uuid,
        customer_id: @order.customer.id,
        order_number: @order.id,
        order_status: @order.status,
        delivery_status: @order.delivery_status,
        order_date: @order.delivery_date,
        supplier: {
          title: @order.supplier.title,
          contact: @order.supplier.contact,
          email: @order.supplier.email,
          id: @order.supplier.id
        },
        net_amount: OrdersService::CalculateDerivedValues.new(@order.order_items).call[1],
        vat_amount: OrdersService::CalculateDerivedValues.new(@order.order_items).call[2],
        gross_amount: OrdersService::CalculateDerivedValues.new(@order.order_items).call[0],
      }

      breakdown
    end

  end
end