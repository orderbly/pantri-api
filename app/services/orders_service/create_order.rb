module OrdersService
  class CreateOrder

    def initialize(order_params:, current_user:)
      @order_params = order_params
      @current_user = current_user
    end

    def call 
      create
    end

    def create
      @order = Order.new(@order_params)
      @order.is_paid = 1
      @order.balance = 0.0
      @order.is_delivery_signature_required = @order.supplier.is_delivery_signature_required
      @order.order_number = "#{Order.where(supplier_id: @order.supplier_id).length + CustomOrder.where(supplier_id: @order.supplier_id).length + 1}"
      @order_params[:customer_id] ? @order.customer_id = @order_params[:customer_id] : @order.customer_id = @current_user.current_company
      @order.order_reconciliation = @order.supplier.default_delivery_reconciliation.present? ? @order.supplier.default_delivery_reconciliation : false
      @order.order_items.each do |oi|
        @order.balance += oi.quantity * oi.gross_price
        @order.value += oi.quantity * oi.gross_price
      end
      begin
        # delivery_fee = DeliveryFeeBracketService::CalculateDeliveryFee.new(@order.delivery_address_id, @order.supplier_id).call
        delivery_fee = DeliveryService::Calculations::DeliveryZoneFeeValue.new(
          delivery_address_id: @order.delivery_address.id, 
          quantity: @order_params[:order_items_attributes].map{|oi| oi["quantity"]}.sum,
          price: @order.value,
          supplier_id: @order.supplier_id
        ).call
        @order.delivery_fee = calculate_delivery_fee(@order.supplier.is_vat_registered, delivery_fee)
        @order.balance += delivery_fee
        @order.value += delivery_fee
      rescue
        delivery_fee = {"net_price"=>0, "vat_price"=> 0, "gross_price"=>0}
        @order.delivery_fee = delivery_fee
      end
     
      
      # Check if there is adequate stock levels for each item. Throw error if not. 
      @order_params[:order_items_attributes].each do |order_item|
        if !ProductVariant.verify_stock(order_item[:product_variant_id], order_item[:quantity])
          return {'obj': @order, 'status': false}
        end
      end

      # ensure order balance is not higher than the credit limit
      if @order.value > CreditLimitsService::CalculateCreditBalance.new(@order.supplier_id, @order.customer_id).available_balance
        return {'obj': {}, 'status': false}
      end

      @order.delivery_address_id = @order.customer.default_address_id if @order.delivery_address_id == nil
      if @order.save!
        order_bot = User.find_by(username: "OrderBot")
        comment_user = order_bot ? order_bot : @current_user
        if Company.find_by(id: comment_user.current_company)
          Comment.create(order_id: @order.id, comment_type: 1, date: Date.new, message: "#{@current_user.first_name} #{@current_user.last_name} created a new order", user_id: comment_user.id, company_id: comment_user.current_company, company_image:  Company.find(comment_user.current_company).image)
        end
        @order.update(order_number: (Order.all.where(supplier: @order_params[:supplier_id]).length))
        @order_params[:order_items_attributes].each do |order_item|
          ProductVariantsService::UpdateStock.new(product_variant_id: order_item[:product_variant_id], quantity: order_item[:quantity]).call
        end
        return {'obj': @order, 'status': true}
      else 
        return {'obj': {}, 'status': false}
      end
    end

    def calculate_delivery_fee(has_vat, delivery_total)
      vat_price = has_vat ? (((delivery_total / 1.15) - delivery_total) * -1).round(2) : 0
      net_price = delivery_total - vat_price
      gross_price = delivery_total
      {"net_price"=>net_price, "vat_price"=> vat_price, "gross_price"=>gross_price}
    end

  end
end

