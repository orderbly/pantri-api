module OrdersService
  class DerivedOrderItems

    def initialize(order_id)
      @order_id = order_id
    end

    def call
      order_breakdown
    end

    def order_breakdown
      order =  Order.find(@order_id)
      breakdown = {
        customer: {
          title: order.customer.title,
          id: order.customer.id
        },
        order_number: order.id,
        order_status: order.status,
        delivery_status: order.delivery_status,
        order_date: order.delivery_date,
        order_comments: comment_breakdown(order),
        supplier: {
          id: order.supplier.id,
          title: order.supplier.title,
          address_one: order.addresses.first.address_one,
          suburb: order.addresses.first.suburb,
          city: order.addresses.first.city,
          country: order.addresses.first.country,
          postal_code: order.addresses.first.postal_code,
          contact: order.supplier.contact
        },
        net_amount: OrdersService::CalculateDerivedValues.new(order.order_items).call[1],
        vat_amount: OrdersService::CalculateDerivedValues.new(order.order_items).call[2],
        gross_amount: OrdersService::CalculateDerivedValues.new(order.order_items).call[0],
        order_items: derived_order_items(order.order_items)
      }

      breakdown
    end
  end
end