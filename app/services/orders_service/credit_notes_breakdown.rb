module OrdersService
  class CreditNotesBreakdown

    def initialize(credit_notes)
      @credit_notes = credit_notes
    end

    def call
      credit_notes_breakdown
    end

    def credit_notes_breakdown
      breakdown = []
      @credit_notes.map{|c| breakdown.push(
        customer: c.customer.title,
        supplier: {
          title: c.supplier.title,
          id: c.supplier.id
        },
        order_number: c.order_id,
        type: "Credit Note #{c.order_id}",
        date: c.created_at,
        gross_amount: c.balance * -1,
        balance: c.balance * -1
      )}

      breakdown
    end
    
  end
end

