module OrdersService
  class UpdateOrder

    def initialize(order_id, values, current_user)
      @order_id = order_id
      @values = values
      @current_user = current_user
      @supplier_id = OrderItem.find(values.first["id"]).order.supplier_id
      @customer_id = OrderItem.find(values.first["id"]).order.customer_id
    end

    def call 
      if validate_credit_limit(@values)
        update_order
      else
        { status: false, message: "The order could not be saved due to insifficient credit." }
      end
    end

    def update_order
      insuffecient_stock_count = 0
      order = Order.find(@order_id)
      CommentsService::CreateComment.new(@current_user, "#{@current_user.first_name} #{@current_user.last_name} updated the order", order, 1).call
      @values.each do |item|
        order_item = OrderItem.find(item["id"])
        item_product_variant = order_item.product_variant
        # if the editing of an order item reduces in quantity - we need to add the stock back
        if order_item.quantity >= item["quantity"].to_i
          stock_increase_difference = order_item.quantity - item["quantity"].to_i
          order_item.product_variant.update(available_stock: item_product_variant.available_stock + stock_increase_difference )
          order_item.item_note = item["item_note"] if item["item_note"].present?
          order_item.quantity = item["quantity"].to_f
          order_item.net_price = item["net_price"].to_f
          order_item.vat_price = item["vat_price"].to_f
          order_item.gross_price = item["gross_price"].to_f
          order_item.save
        end
        # if the editing of an order item increases in quantity - we need to check available stock and update the order and actual stock values
        if order_item.quantity < item["quantity"].to_f
          stock_deacrease_difference = item["quantity"].to_i - order_item.quantity
          available_stock = ( order_item.product_variant.available_stock > stock_deacrease_difference ) || !order_item.product_variant.track_stock
          if !available_stock
            insuffecient_stock_count += 1 
          end
          if available_stock
            order_item.product_variant.update(available_stock: item_product_variant.available_stock - stock_deacrease_difference ) if order_item.product_variant.track_stock
            order_item.item_note = item["item_note"] if item["item_note"].present?
            order_item.quantity = item["quantity"].to_f
            order_item.net_price = item["net_price"].to_f
            order_item.vat_price = item["vat_price"].to_f
            order_item.gross_price = item["gross_price"].to_f
            order_item.save!
          end
        end
      end
      delivery_fee = 0
      begin
        delivery_fee += DeliveryFeeBracketService::CalculateDeliveryFee.new(order.delivery_address_id, order.supplier_id).call
        order.update(delivery_fee: calculate_delivery_fee(@order.supplier.is_vat_registered, delivery_fee))
      rescue
        order.update(delivery_fee: {"net_price"=>0, "vat_price"=> 0, "gross_price"=>0})
      end
      order_balance = order.order_items.map{|oi| oi.gross_price * oi.quantity}.sum

      order.update(
        balance: order_balance + delivery_fee,
        value: order_balance + delivery_fee,
      )
      insuffecient_stock_count > 0 ? {status: false, } : { status: true }
    end

    def validate_credit_limit(order_items)
      projected_updated_order_value - Order.find(@order_id).balance > avaialble_credit ? false : true
    end

    def calculate_delivery_fee(has_vat, delivery_total)
      vat_price = has_vat ? (((delivery_total / 1.15) - delivery_total) * -1).round(2) : 0
      net_price = delivery_total - vat_price
      gross_price = delivery_total
      {"net_price"=>net_price, "vat_price"=> vat_price, "gross_price"=>gross_price}
    end

    def projected_updated_order_value
      @values.map{|oi| oi["gross_price"].to_f * oi["quantity"].to_f }.sum()
    end

    def original_order_value
      OrderItem.find(@values.first["id"]).order.value
    end

    def avaialble_credit
      CreditLimitsService::CalculateCreditBalance.new(@supplier_id, @customer_id).available_balance + original_order_value
    end

  end
end