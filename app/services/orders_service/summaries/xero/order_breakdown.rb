module OrdersService
  module Summaries
    module Xero
      class OrderBreakdown
    
        def initialize(order_uuids)
          @orders = Order.where(uuid: order_uuids)
        end
  
        def call
          order_item_breakdown
        end
  
        def order_item_breakdown
          items = @orders.map{|o|o.order_items}.flatten
          breakdown = []
          items.each do |item|
            breakdown.push({
              ContactName: "",
              OrderblyContactName: item.order.customer.title,
              EmailAddress: item.order.customer.email,
              POAddressLine1: item.order.delivery_address.address_one,
              POCity: item.order.delivery_address.city,
              PORegion: item.order.delivery_address.suburb,
              POPostalCode: item.order.delivery_address.postal_code,
              POCountry: item.order.delivery_address.country,
              InvoiceNumber: "",
              Reference: "Orderbly Ref: #{item.order.order_number}",
              InvoiceDate: item.order.created_at.strftime("%d/%m/%Y"),
              DueDate: (item.order.created_at+2.weeks).strftime("%d/%m/%Y"),
              Total: "",
              InventoryItemCode: item.product["accounting_item"]["code"].present? ? item.product["accounting_item"]["code"] : "",
              Description: "#{item.product_variant.product.title} #{item.product_variant.description}",
              Quantity: item.quantity,
              UnitAmount: item.vat_price > 0 ? (item.gross_price / 115 * 100).round(10) : item.gross_price,
              AccountCode: "",
              TaxType: item.product.tax.category ? "Standard Rate Sales" : "No VAT",
              # Check TAX types for categories and what they represent
              TaxAmount: item.vat_price > 0 ? (item.gross_price / 115 * 15).round(10) : item.vat_price,
              Currency: "ZAR"
            })
          end
          breakdown
        end
      end
    end
  end
end