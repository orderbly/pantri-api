module OrdersService
  module Summaries
    class OrderItemBreakdown

      def initialize(order_items)
        @order_items = order_items
      end

      def call
        order_item_breakdown
      end

      def order_item_breakdown
        breakdown = []
        @order_items.each do |oi|
          breakdown.push({
            gross_price: oi.gross_price,
            net_price: oi.net_price,
            vat_price: oi.vat_price,
            quantity: oi.quantity,
            image: oi.product.image,
            title: oi.product.title,
            sub_title: oi.product_variant&.description || ""
          })
        end
        breakdown
      end
    end
  end
end