module OrdersService
  module Summaries
    class OrderDeliverySummaryBreakdown

      def initialize(orders, limit = 500)
        @orders = orders
        @limit = limit
      end
  
      def call
        summary_breakdown
      end
  
      def summary_breakdown
        breakdown = [] 
        @orders.first(@limit).map{|o| breakdown.push(
          address: {
            address_one: o.delivery_address.address_one,
            suburb: o.delivery_address.suburb,
            city: o.delivery_address.city,
            country: o.delivery_address.country,
            postal_code: o.delivery_address.postal_code,
            company_id: o.delivery_address.company_id,
            latitude: o.delivery_address.latitude,
            longitude: o.delivery_address.longitude
          },
          customer: o.customer.title,
          customer_id: o.customer.id,
          order_number: o.id,
          order_uuid: o.uuid,
          order_status: o.status,
          delivery_status: o.delivery_status.humanize,
          order_date: o.delivery_date,
          order_items: OrdersService::Summaries::OrderItemBreakdown.new(o.order_items).call,
          supplier: {
            title: o.supplier.title,
            contact: o.supplier.contact,
            email: o.supplier.email,
            id: o.supplier.id
          },
          net_amount: OrdersService::CalculateDerivedValues.new(o.order_items).call[1],
          vat_amount: OrdersService::CalculateDerivedValues.new(o.order_items).call[2],
          gross_amount: OrdersService::CalculateDerivedValues.new(o.order_items).call[0],
          balance: o.balance  && o.balance > 1 ? o.balance : 0,
          invoice: o.accounting.key?('invoice') ? o.accounting['invoice'] : {}
        )}
        breakdown
      end

    end
  end
end