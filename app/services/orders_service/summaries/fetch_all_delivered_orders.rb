module OrdersService
  module Summaries
    class FetchAllDeliveredOrders

      def initialize(supplier_id, date)
        @supplier_id = supplier_id
        @date = Date.parse(date)
      end
    
      def call
        fetch_all_completed_orders
      end

      def fetch_all_completed_orders
        orders = Order.where(supplier_id: @supplier_id, delivery_status: 'delivered').where(:delivery_date => @date.beginning_of_day...@date.end_of_day)
        custom_deliveries = CustomDelivery.where(supplier_id: @supplier_id, delivery_status: 'delivered').where(:due_date => @date.beginning_of_day...@date.end_of_day)

        order_breakdown = orders.map{|order| FulfilmentService::Breakdowns::CustomOrderBreakdown.new.custom_order_response(order) }
        custom_deliveries_breakdown = custom_deliveries.map{|cd| FulfilmentService::Breakdowns::CustomDeliveryBreakdown.new.custom_delivery_response(cd) }
        total_orders = order_breakdown + custom_deliveries_breakdown

        {
          orders: total_orders,
          customers: [""] + total_orders.map{|order| order[:customer] }.uniq.compact.sort,
          order_dates: [""] + total_orders.map{|order| order[:order_date]}.uniq.compact.sort,
          order_statuses: [""] + total_orders.map{|order| order[:order_status].humanize }.uniq.compact.sort,
          drivers: [""] + total_orders.map{|order| order[:driver][:name].humanize if (order[:driver][:name] != nil && order[:driver][:name] != " ")  }.uniq.compact.sort
        }
      end

    end
  end
end