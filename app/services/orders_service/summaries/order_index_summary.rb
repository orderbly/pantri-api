module OrdersService
  module Summaries
    class OrderIndexSummary

      def initialize(orders, credit_notes = [], limit = 300)
        @orders = orders
        @credit_notes = credit_notes
        @limit = limit
        @breakdown = []
      end

      def call
        credit_note_summary
        orders_summary
        @breakdown.sort! { |a, b|  b[:created_at] <=> a[:created_at] }
      end

      def orders_summary
        @orders.map{|o| @breakdown.push(
          created_at: o.created_at,
          type: 'order',
          id: o.id,
          customer: {
            title: o.customer&.title,
            id: o.customer&.id,
            uuid: o.customer&.uuid
          },
          order_uuid: o.uuid,
          customer_id: o.customer&.id,
          order_number: o.order_number ? o.order_number : o.id,
          order_status: o.status,
          delivery_status: o.delivery_status,
          order_date: o.created_at.strftime("%d-%m-%Y"),
          is_delivery_buyer_actionable: o.is_delivery_buyer_actionable,
          supplier: {
            title: o.supplier&.title,
            contact: o.supplier&.contact,
            email: o.supplier&.email,
            id: o.supplier&.id
          },
          balance: (o.balance != nil && o.balance > 1) ? o.balance : 0,
          value: o.value,
          integration_sales: o.accounting.key?('sales') ? true : false,
          integration_purchases: o.accounting.key?('purchases') ? true : false,
          integration_inventory: o.inventory.key?('id').present? ? true : false,
          integration_delivery: o.inventory.key?('success').present? ? true : false,
          payment_status: o.is_paid
        )}
      end

      def credit_note_summary
        if @credit_notes.present?
          @credit_notes.map{|cn| @breakdown.push(
            created_at: cn.created_at,
            type: 'credit_note',
            id: cn.id,
            uuid: cn.uuid,
            customer: {
              title: cn.customer.title,
              id: cn.customer.id
            },
            order_number: cn.credit_note_number ? cn.credit_note_number : cn.id,
            order_status: 'credit note',
            order_date: cn.created_at.strftime("%d-%m-%Y"),
            delivery_status: "",
            supplier: {
              title: cn.supplier.title,
              contact: cn.supplier.contact,
              email: cn.supplier.email,
              id: cn.supplier.id
            },
            net_amount: -(cn.credit_note_items.sum(:net_price) * cn.credit_note_items.sum(:quantity)),
            vat_amount: -(cn.credit_note_items.sum(:vat_price) * cn.credit_note_items.sum(:quantity)),
            gross_amount: cn.balance,
            balance: (cn.remaining_balance != nil && cn.remaining_balance > 0) ? cn.remaining_balance : 0,
            value: cn.balance,
            integration_sales: cn.accounting.key?('credit_notes_issued') ? true : false,
            integration_purchases: cn.accounting.key?('credit_notes_received') ? true : false,
          )}
        end
      end

    end
  end
end