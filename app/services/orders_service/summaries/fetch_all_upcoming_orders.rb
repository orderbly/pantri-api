module OrdersService
  module Summaries
    class FetchAllUpcomingOrders

      def initialize(supplier_id)
        @supplier_id = supplier_id
      end
    
      def call
        fetch_all_upcoming_orders
      end

      def fetch_all_upcoming_orders
        orders = Order
          .all
          .where(supplier_id: @supplier_id)
          .where('delivery_date >= ?', DateTime.now.beginning_of_day)
          .where(status: ['accepted', 'paid', 'pending'])

        custom_deliveries = CustomDelivery
          .all
          .where(supplier_id: @supplier_id)
          .where('due_date >= ?', DateTime.now.beginning_of_day)
          
        order_breakdown = orders.map{|order| FulfilmentService::Breakdowns::CustomOrderBreakdown.new.custom_order_response(order) }
        custom_deliveries_breakdown = custom_deliveries.map{|cd| FulfilmentService::Breakdowns::CustomDeliveryBreakdown.new.custom_delivery_response(cd) }
        total_orders = order_breakdown + custom_deliveries_breakdown

        {
          orders: total_orders,
          customers: [""] + total_orders.map{|order| order[:customer] }.uniq.compact.sort,
          order_dates: [""] + total_orders.map{|order| order[:order_date]}.uniq.compact.sort,
          order_statuses: [""] + total_orders.map{|order| order[:order_status].humanize }.uniq.compact.sort,
          drivers: [""] + total_orders.map{|order| order[:driver][:name].humanize if (order[:driver][:name] != nil && order[:driver][:name] != " ")  }.uniq.compact.sort
        }
      end

    end
  end
end