module OrdersService
  class UserDeliverableOrders

    def initialize(company_id, user)
      @company_id = company_id
      @user_id = user.id
      @user = user
    end

    def call
      user_deliverable_orders
    end

    def user_deliverable_orders
      # order_list = DeliveryAllocation
      # .all
      # .where(user_id: @user_id)
      # .includes(:order)
      # .where(:orders => { supplier_id: @company_id, delivery_status: [ 'in_preparation', 'delivery_pending', 'delivery_in_progress'] })
      # .map {|delivery_allocation| delivery_allocation.order_id }
      
      orders = @user.delivery_orders

      OrdersService::Summaries::OrderDeliverySummaryBreakdown.new(orders).call
    end

  end
end