module OrdersService
  module Actions
    class CancelOrder

      def initialize(id, current_user)
        @id = id
        @order = Order.find(@id)
        @user = current_user
      end
  
      def call
        if @order.status === "pending" && @order.update(status: 4, delivery_status: 5) 
          order_bot = User.find_by(username: "OrderBot")
          comment_user = order_bot ? order_bot : @user
          if Company.find_by(id: comment_user.current_company)
            Comment.create(
              order_id: @order.id,
              date: Date.new,
              comment_type: 1,
              message: "#{@user.first_name} #{@user.last_name} cancelled the order",
              user_id: comment_user.id,
              company_id: comment_user.current_company,
              company_image: Company.find(comment_user.current_company).image
            )
          end
          {:status => true, :data => @order}
        else
          {:status => false}
        end
      end

    end
  end
end