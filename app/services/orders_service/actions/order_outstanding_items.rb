module OrdersService
  module Actions
    class OrderOutstandingItems

      def initialize(order_id:, current_user:)
        @parent_order = Order.find(order_id)
        @current_user = current_user
      end

      def call
        create_create_note
        create
      end

      def create_create_note
        credit_note = CreditNote.create(order_id: @parent_order.id, customer_id: @parent_order.customer_id, supplier_id: @parent_order.supplier_id)
        @parent_order.order_items.each do |oi|
          if oi.received_amount < oi.quantity
            CreditNoteItem.create(
              product_id: oi[:product_id],
              product_variant_id: oi[:product_variant_id],
              quantity: oi[:quantity] - oi[:received_amount].to_f,
              gross_price: oi[:gross_price],
              net_price: oi[:net_price],
              vat_price: oi[:vat_price],
              company_id: oi[:company_id],
              credit_note_id: credit_note.id
            )
          end
        end
        credit_note.balance = credit_note.remaining_balance = credit_note.credit_note_items.map{|cn| cn.quantity * cn.gross_price}.sum
        credit_note.save!
      end

      def create
        @order = Order.create(
          supplier_id: @parent_order.supplier_id,
          customer_id: @parent_order.customer_id,
          delivery_address_id: @parent_order.delivery_address_id,
          order_number: Order.all.where(supplier: @parent_order.supplier_id).length,
          is_paid: 1,
          balance: 0.0,
          delivery_date: DateTime.now + 1.day
        )
        @parent_order.order_items.each do |oi|
          if oi.received_amount < oi.quantity
            OrderItem.create(
              company_id: oi.company_id,
              product_id: oi.product_id,
              order_id: @order.id,
              quantity: oi[:quantity] - oi[:received_amount].to_f,
              gross_price: oi.gross_price,
              net_price: oi.net_price,
              vat_price: oi.vat_price,
              product_variant_id: oi.product_variant_id,
              received_amount: 0
            )
          end
        end
        @order.order_items.each do |oi|
          @order.balance += oi.quantity * oi.gross_price
          @order.value += oi.quantity * oi.gross_price
        end

        if check_adequate_stock_levels(@order).include?(false)
          claim_credit_on_all_unpaid_orders(@order.supplier_id, @order.customer_id)
          return {:obj => @order, :status => false, :message => 'Insufficient stock was available, for the reorder. A credit note has been created for the outstanding products'}
        end

        if @order.save
          claim_credit_on_created_order(@order)
          create_comment_order()
          update_stock_values(@order)
          send_mails(@order)
          @parent_order.update(is_delivery_buyer_actionable: false)
          return {:obj => @order, :status => true, :message => 'A new order and credit note has been successfully created'}
        else 
          claim_credit_on_all_unpaid_orders(@order.supplier_id, @order.customer_id)
          return {:obj => {}, :status => false, :message => 'A new order could not be created. Please try again or contact support should the problem persist'}
        end
      end

      def claim_credit_on_created_order(order)
        CreditNoteService::ClaimCreditNote.new(order).call
      end

      def claim_credit_on_all_unpaid_orders(supplier_id, customer_id)
        CreditNoteService::ClaimCreditNote.new(@parent_order).call
        unpaid_order = Order.where(supplier_id: supplier_id, customer_id: customer_id).where.not(is_paid: 'true', id: @order.id).order("id ASC")
        if !unpaid_order.empty?
          unpaid_order.each do |order|
            CreditNoteService::ClaimCreditNote.new(order).call
          end
        end
      end

      def check_adequate_stock_levels(order)
        adequate_values = []
        order.order_items.each do |order_item|
          adequate_values.push(ProductVariant.verify_stock(order_item.product_variant_id, order_item.quantity))
        end
        return adequate_values
      end

      def update_stock_values(order)
        order.order_items.each do |order_item|
          ProductVariantsService::UpdateStock.new(product_variant_id: order_item.product_variant_id, quantity: order_item.quantity).call
        end
      end

      def create_comment_order
        order_bot = User.find_by(username: "OrderBot")
        comment_user = order_bot ? order_bot : @current_user
        if Company.find_by(id: comment_user.current_company)
          Comment.create(order_id: @order.id, comment_type: 1, date: Date.new, message: "#{@current_user.first_name} #{@current_user.last_name} created a new order", user_id: comment_user.id, company_id: comment_user.current_company, company_image:  Company.find(comment_user.current_company).image)
        end
      end

      def send_mails(order)
        order_breakdown = OrdersService::OrderBreakdown.new(order.id).call
        Pusher.trigger('orders', "company_#{order.supplier.id}", {customer: {title: order_breakdown[:customer][:title]}, order_number: order_breakdown[:order_number]})
        NewOrderJob.perform_later(order_breakdown)
      end

    end
  end
end

