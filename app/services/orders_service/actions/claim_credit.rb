module OrdersService
  module Actions
    class ClaimCredit

      def initialize(order_id:, current_user:)
        @parent_order = Order.find(order_id)
        @current_user = current_user
      end

      def call
        create_create_note
      end

      def create_create_note
        credit_note = CreditNote.create(order_id: @parent_order.id, customer_id: @parent_order.customer_id, supplier_id: @parent_order.supplier_id)
        @parent_order.order_items.each do |oi|
          if oi.received_amount < oi.quantity
            CreditNoteItem.create(
              product_id: oi[:product_id],
              product_variant_id: oi[:product_variant_id],
              quantity: oi[:quantity] - oi[:received_amount].to_f,
              gross_price: oi[:gross_price],
              net_price: oi[:net_price],
              vat_price: oi[:vat_price],
              company_id: oi[:company_id],
              credit_note_id: credit_note.id
            )
          end
        end
        credit_note.balance = credit_note.remaining_balance = credit_note.credit_note_items.map{|cn| cn.quantity * cn.gross_price}.sum
        if credit_note.save!
          claim_credit_on_all_unpaid_orders(credit_note.supplier_id, credit_note.customer_id)
          return {:obj => @parent_order, :status => true, :message => 'A credit note has successfully been created.'}
        else
          return {:obj => {}, :status => false, :message => 'Something went wrong. Please try again or contact support should the problem persist.'}
        end
      end

      def claim_credit_on_all_unpaid_orders(supplier_id, customer_id)
        CreditNoteService::ClaimCreditNote.new(@parent_order).call

        unpaid_order = Order.where(supplier_id: supplier_id, customer_id: customer_id).where.not(is_paid: 'true', id: @parent_order.id).order("id ASC")
        if !unpaid_order.empty?
          unpaid_order.each do |order|
            CreditNoteService::ClaimCreditNote.new(order).call
          end
        end
      end

    end
  end
end

