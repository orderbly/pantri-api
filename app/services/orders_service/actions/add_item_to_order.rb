module OrdersService
  module Actions
    class AddItemToOrder

      def initialize(params, order)
        @params = params
        @product_variant = ProductVariant.find(params[:product_variant_id])
        @order = order
        @quantity = params[:quantity]
      end
  
      def call
        validate_credit_limit ? add_item : { status: false, message: "Insufficient credit available" }
      end
      
      
      def add_item
        if @product_variant.available_stock < @quantity && @product_variant.track_stock
          return { status: false, message: "Not enough available stock"}
        end

        pricing = calculate_gross_price(@product_variant, @order.supplier_id, @order.customer_id)

        order_item = OrderItem.new(
          company_id: @order.supplier_id,
          order_id: @order.id,
          product_id: @product_variant.product_id,
          quantity: @quantity,
          gross_price: pricing[:gross_price],
          net_price: pricing[:net_price],
          vat_price: pricing[:tax_price],
          product_variant_id: @product_variant.id
        )

        if order_item.save 
          ProductVariantsService::UpdateStock.new(product_variant_id: @product_variant.id, quantity: @quantity).call
          order_balance = @order.order_items.map{|oi| oi.gross_price * oi.quantity}.sum + @order.delivery_fee['gross_price']
          @order.update(balance: order_balance, value: order_balance)
          return  { status: true, obj: @order}
        else
          return  { status: false, message: "Something went wrong. Please try again or contact support should the issue occur again"}
        end
      end

      def calculate_gross_price(variant, supplier_id, customer_id)
        discount_book_membership = DiscountBookMembership.joins(:discount_book).where(customer_id: customer_id).where(:discount_books => {supplier_id: supplier_id}).first
        return base_pricing(variant) if discount_book_membership.nil?
        discount = variant.product_variant_discounts.find_by(discount_book_id: discount_book_membership.discount_book_id, product_variant_id: variant.id)
        if discount
          return discount_pricing(variant, discount.discount_percentage)
        else
          return base_pricing(variant)
        end
      end

      def base_pricing(product_variant)
        net_price = tax_price = gross_amount = 0
        net_price = product_variant.unit_amount
        tax_price =  product_variant.tax_amount
        gross_price = (net_price + tax_price).round(2)
        {:net_price => net_price, :tax_price => tax_price, :gross_price => gross_price}
      end

      def discount_pricing(product_variant, discount_percentage)
        return base_pricing(product_variant) if discount_percentage == 0
        net_price = tax_price = gross_amount = 0  
        net_price = product_variant.unit_amount * ((100 - discount_percentage) / 100)
        tax_price = product_variant.tax_amount * ((100 - discount_percentage) / 100)
        gross_price = (net_price + tax_price).round(2)

        {:net_price => net_price, :tax_price => tax_price, :gross_price => gross_price}
      end

      def validate_credit_limit
        credit_limit = CreditLimitsService::CalculateCreditBalance.new(@order.supplier_id, @order.customer_id).available_balance
        product_variant_pricing = calculate_gross_price(@product_variant, @order.supplier_id, @order.customer_id)[:gross_price] * @quantity
        
        product_variant_pricing <= credit_limit ? true : false
      end

    end
  end
end