module OrdersService
  module Actions
    class DuplicateOrder

      def initialize(id, current_user)
        @id = id
        @original_order = Order.find(id)
        @current_user = current_user
      end
  
      def call
        if CreditLimitsService::CalculateCreditBalance.new(@original_order.supplier_id, @original_order.customer_id).available_balance < @original_order.value
          return {'obj': {}, 'status': false, 'error': "Insifficient credit avaialble. Please settle your account to duplicate this order"}
        elsif validate_stock.include? false
          return {'obj': {}, 'status': false, 'error': "Duplicating the order could not be complete due to insifficient stock. "}
        else
          create
        end
      end
  
      def create      
        created_order = new_order(@id)
        new_order = created_order[:order]
  
        if new_order.save! && new_order.order_items.each{|oi| oi.save! } && (!created_order[:order_item_created].include? false)
          new_order.order_items.each do |order_item|
            ProductVariantsService::UpdateStock.new(product_variant_id: order_item[:product_variant_id], quantity: order_item[:quantity]).call
          end
          Comment.create(order_id: new_order.id, comment_type: 1, date: Date.new, message: "#{@current_user.first_name} #{@current_user.last_name} created a new order", user_id: @current_user.id, company_id: new_order.customer_id, company_image:  Company.find(new_order.customer_id).image)
          return {'obj': new_order, 'status': true}
        else 
          return {'obj': new_order, 'status': false}
        end
      end
  
  
      def new_order(id)
        supplier_id = @original_order.supplier_id
        customer_id = @original_order.customer_id
  
        # create the order
        new_order = Order.create(
          customer_id: @original_order.customer_id,
          supplier_id: @original_order.supplier_id,
          delivery_date: DateTime.now(),
          order_number: "#{Order.where(supplier_id: @original_order.supplier_id).length + 1}",
          status: 0,
          delivery_status: 0,
          delivery_address_id: @original_order.delivery_address_id,
          order_schedule_id: @original_order.order_schedule_id,
          is_paid: 1,
          balance: 0.0,
        )
        order_item_created = []
        #create order items 
        @original_order.order_items.each do |oi|
          order_item = OrderItem.new(
            company_id: oi.company_id,
            order_id: new_order.id,
            product_id: oi.product_id,
            quantity: oi.quantity,
            gross_price: calculate_gross_price(oi, supplier_id, customer_id)[:gross_price],
            net_price: calculate_gross_price(oi, supplier_id, customer_id)[:net_price],
            vat_price: calculate_gross_price(oi, supplier_id, customer_id)[:tax_price],
            product_variant_id: oi.product_variant_id
          )
  
          if order_item.save!
            order_item_created.push(true)
            new_order.value += order_item.quantity * order_item.gross_price
            new_order.balance += order_item.quantity * order_item.gross_price
          else
            order_item_created.push(false)
          end
        end
        new_order.save!
        {order: new_order.reload, order_item_created: order_item_created}
      end
  
      def validate_stock 
        @original_order.order_items.map{|order_item| ProductVariant.verify_stock(order_item[:product_variant_id], order_item[:quantity]) }
      end
  
      def calculate_gross_price(order_item, supplier_id, customer_id)
        # find membership and discount book for customer and supplier
        discount_book_membership = DiscountBookMembership.joins(:discount_book).where(customer_id: customer_id).where(:discount_books => {supplier_id: supplier_id}).first
        
        if discount_book_membership.nil?
          return base_pricing(order_item)
        else
          # find discount for product variant based on discount book and product variant
          discount = order_item.product_variant.product_variant_discounts.find_by(discount_book_id: discount_book_membership.discount_book_id, product_variant_id: order_item.product_variant.id)
          if discount
            return discount_pricing(order_item, discount.discount_percentage)
          else
            return base_pricing(order_item)
          end
        end
      end
  
      def base_pricing(order_item)
        net_price = tax_price = gross_amount = 0
        net_price = order_item.product_variant.unit_amount
        tax_price =  order_item.product_variant.tax_amount
        gross_price = (net_price + tax_price).round(2)
        {:net_price => net_price, :tax_price => tax_price, :gross_price => gross_price}
      end
  
      def discount_pricing(order_item, discount_percentage)
        return base_pricing(order_item) if discount_percentage == 0
  
        net_price = tax_price = gross_amount = 0  
        net_price = order_item.product_variant.unit_amount * ((100 - discount_percentage) / 100)
        tax_price = order_item.product_variant.tax_amount * ((100 - discount_percentage) / 100)
        gross_price = (net_price + tax_price).round(2)
  
        {:net_price => net_price, :tax_price => tax_price, :gross_price => gross_price}
      end

    end
  end
end
