module OrdersService
  module Permissions
    class OrderPermission

      def initialize(user, order_id)
        @user = user
        @order_id = order_id
        @order = Order.find(order_id)
      end

      def call
        return ( user_companies.include? @order.supplier_id ) || ( user_companies.include? @order.customer_id )
      end

      def user_companies
        companies = []
        @user.members.each {|m| companies.push(m.company_id)}

        companies
      end

    end
  end
end

