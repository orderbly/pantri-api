module OrdersService
  module Permissions
    class OrderDirectlyPayable

      def initialize(supplier_id:, customer_id:)
        @supplier_id = supplier_id
        @customer_id = customer_id
        
        @supplier = Company.find(supplier_id)
        @customer = Company.find(customer_id)
      end

      def call 
        customer_can_directly_pay || supplier_can_directly_pay
      end

      def customer_can_directly_pay
        ( @customer_id.in?@supplier.buyers_directly_payable ) && ( @supplier_id.in?@customer.suppliers_directly_payable )
      end

      def supplier_can_directly_pay
        ( @customer_id.in?@supplier.suppliers_directly_payable ) && ( @supplier_id.in?@customer.buyers_directly_payable )
      end

    end
  end
end