module OrdersService
  class SlimCustomerSummaryBreakdown

    def initialize(orders)
      @orders = orders
    end

    def call 
      slim_customer_summary_breakdown
    end

    def slim_customer_summary_breakdown
      breakdown = [] 
      @orders.map{|o| breakdown.push(
        customer: o.customer.title,
        supplier: {
          title: o.supplier.title,
          id: o.supplier.id
        },
        customer_id: o.customer.id,
        id: o.id,
        order_number: o.order_number,
        type: "Order (#{o.order_number})",
        order_status: o.status,
        delivery_status: o.delivery_status,
        date: o.created_at,
        gross_amount: OrdersService::CalculateDerivedValues.new(o.order_items).call[0],
        balance: o.balance 
      )}
      breakdown
    end
  end
end
