module OrdersService
  class PaymentSummary

    def initialize(payments)
      @payments = payments
    end

    def call
      payments_summary
    end

    def payments_summary
      breakdown = []
        @payments.map{|p| breakdown.push(
          customer: p.customer.title,
          supplier: {
            title: p.supplier.title,
            id: p.supplier.id
          },
          type: p.order_id ? "Payment (#{p.order_id})" : "Payment General",
          order_number: p.order_id,
          date: p.created_at,
          gross_amount: p.value * -1,
          balance: p.value * -1
        )}
  
        breakdown
    end

  end
end