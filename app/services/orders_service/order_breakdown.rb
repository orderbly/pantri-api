module OrdersService
  class OrderBreakdown

    def initialize(order_id)
      @order_id = order_id
    end

    def call
      order_breakdown
    end

    def order_breakdown
      order =  Order.find(@order_id)
      breakdown = {
        id: order.id,
        customer: {
          title: order.customer&.title,
          id: order.customer&.id,
          customer_address: order.customer&.default_address,
          vat_number: order.customer&.vat_number || ""
        },
        order_reconciliation: order.order_reconciliation,
        order_number: order.order_number,
        order_uuid: order.uuid,
        notes: order.notes,
        is_delivery_signature_required: order.is_delivery_signature_required,
        delivery_date:  DateFormat.change_to(order.delivery_date, "LONG_DATE") || DateFormat.change_to(order.created_at, "LONG_DATE"),
        delivery_fee: order.delivery_fee,
        balance: order.balance,
        invoice_number: order.order_number,
        delivery_zone: DeliveryZoneArea.find_by(company_id: order.supplier_id, street_code: order.delivery_address.postal_code, name: order.delivery_address.suburb),
        order_status: order.status,
        delivery_status: order.delivery_status,
        order_date: DateFormat.change_to(order.created_at, "LONG_DATE"),
        order_schedule: order.order_schedule,
        is_directly_payable: OrdersService::Permissions::OrderDirectlyPayable.new(supplier_id: order.supplier_id, customer_id: order.customer_id).call,
        delivery_assignees: order.delivery_assignees.map {|da| 
          { 
            id: da.id,
            name: "#{da.first_name} #{da.last_name}",
            first_name: da.first_name,
            last_name: da.last_name,
            email: da.email,
            default_colour: da.default_colour,
            avatar: da.avatar
          }
        },
        order_signature: order.signature_image,
        accounting: order.accounting,
        order_comments: comment_breakdown(order),
        is_paid: order.is_paid,
        is_recurring: order.is_recurring,
        supplier: {
          id: order.supplier.id,
          title: order.supplier.title,
          address_one: order.supplier.default_address.present? ? order.supplier.default_address.address_one : "",
          suburb: order.supplier.default_address.present? ? order.supplier.default_address.suburb : "",
          city: order.supplier.default_address.present? ? order.supplier.default_address.city : "",
          country: order.supplier.default_address.present? ? order.supplier.default_address.country : "",
          postal_code: order.supplier.default_address.present? ? order.supplier.default_address.postal_code : "",
          contact: order.supplier.default_address.present? ? order.supplier.contact : "",
          vat_number: order.supplier.vat_number || ""
        },
        net_amount: OrdersService::CalculateDerivedValues.new(order.order_items).call[1],
        vat_amount: OrdersService::CalculateDerivedValues.new(order.order_items).call[2],
        gross_amount: OrdersService::CalculateDerivedValues.new(order.order_items).call[0],
        order_items: derived_order_items(order.order_items),
        credit_note_claims: credit_note_breakdown(order),
        credit_note: order.credit_note,
        payments: payment_breakdown(order),
        delivery_user: "#{order.delivery_user&.first_name} #{order.delivery_user&.last_name}",
        delivery_address: order.delivery_address,
        received_by: order.received_by,
        value: order.value,
        inventory_item: order.inventory,
        is_delivery_buyer_actionable: order.is_delivery_buyer_actionable,
        reorder_balance: order.reorder_balance.to_f
      }
      breakdown
    end

    def derived_order_items(order_items)
      items = []
      order_items.each{|o| items.push({  
        id: o.id,
        item_note: o.item_note,
        product_id: o.product_id,
        product_variant_id: o.product_variant_id,
        received_amount: o.received_amount ? o.received_amount : 0,
        quantity: o.quantity,
        order_id: o.order_id,
        gross_price: o.gross_price.round(2),
        custom_price: o.gross_price.round(2),
        net_price: o.net_price.round(2),
        vat_price: o.vat_price.round(2),
        vat_percentage: o.product_variant&.product&.tax&.percentage.present? ? o.product_variant&.product&.tax&.percentage : 0,
        product_title: o.description.present? ?  o.description : "#{o.product_variant&.product&.title} #{o.product_variant&.description}",
        sku: o.product_variant&.sku,
        available_stock: o.product_variant&.available_stock
      })}
      items.sort {|a, b| a[:id] <=> b[:id]}
    end

    def comment_breakdown(order)
      comments = []
      order.comments.each do |c|
        comments.push({
          order_id: c.order.id,
          company_image: c.company_image,
          commented_at: c.created_at,
          comment_type: c.comment_type,
          message: c.message,
          company: c.company.title,
          image: c.company.image,
          user_id: c.user.id,
          user_name: c.user.first_name + " " +c.user.last_name,
          user_email: c.user.email,
          user: {
            avatar: c.user.avatar,
            id: c.user.id,
            username: c.user.first_name + " " + c.user.last_name,
            first_name: c.user.first_name ,
            email: c.user.email,
            last_name: c.user.last_name,
            default_colour: c.user.default_colour
          }
        })
      end
      comments
    end

    def payment_breakdown(order)
      payments = []
      order.payments.each do |o|
        payments.push({
          id: o.id,
          date: DateFormat.change_to(o.payment_date ? o.payment_date : o.created_at, "SHORT_DATE"),
          type: o.payment_type&.humanize,
          reference: o.payment_reference,
          paid_by: "#{o&.user&.first_name} #{o&.user&.last_name}",
          value: o.value,
        })
      end
      payments
    end

    def credit_note_breakdown(order)
      credit_note_breakdown = []
      order.credit_note_claims.each do |cn|
        credit_note_breakdown.push({
          id: cn&.credit_note&.id,
          date: DateFormat.change_to(cn&.created_at, "SHORT_DATE"),
          type: "Credit Note",
          reference: "Orderbly Credit Note",
          paid_by: "",
          value: cn&.value,
        })
      end
      credit_note_breakdown
    end

  end
end