module OrdersService
  class SummaryBreakdown

    def initialize(orders, limit = 300)
      @orders = orders
      @limit = limit
    end

    def call
      summary_breakdown
    end

    def summary_breakdown
      breakdown = [] 
      @orders.first(@limit).map{|o| breakdown.push(
        id: o.id,
        customer: o.customer.title,
        customer_id: o.customer.id,
        order_uuid: o.uuid,
        order_number: o.order_number ? o.order_number : o.id,
        order_status: o.status,
        delivery_status: o.delivery_status,
        order_date: o.delivery_date.strftime("%d-%m-%Y"),
        supplier: {
          title: o.supplier.title,
          contact: o.supplier.contact,
          email: o.supplier.email,
          id: o.supplier.id
        },
        net_amount: OrdersService::CalculateDerivedValues.new(o.order_items).call[1] || 0,
        vat_amount: OrdersService::CalculateDerivedValues.new(o.order_items).call[2] || 0,
        gross_amount: OrdersService::CalculateDerivedValues.new(o.order_items).call[0] || 0,
        balance: (o.balance != nil && o.balance > 1) ? o.balance : 0,
        integration_sales: o.accounting.key?('sales') ? true : false,
        integration_purchases: o.accounting.key?('purchases') ? true : false,
      )}
      breakdown
    end

  end
end