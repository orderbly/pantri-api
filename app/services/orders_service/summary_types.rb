module OrdersService
  class SummaryTypes

    # all purchases
    def summary(id, status=nil)
      credit_notes = []
      if status.nil?
        end_date = Order.where(customer_id: id).order( 'created_at ASC' ).last&.delivery_date&.end_of_day || DateTime.now
        orders = Order.by_period(Date.today.beginning_of_year, end_date).where(customer_id: id).order( 'created_at ASC' )
        credit_notes = CreditNote.by_period(Date.today.beginning_of_year, end_date).where(customer_id: id).order( 'created_at ASC' )
      else
        orders = Order.where(customer_id: id).where(status: status).order( 'id ASC' )
      end
      OrdersService::Summaries::OrderIndexSummary.new(orders, credit_notes).call
    end

    def all_purchases_summary(id)
      orders = Order.all.where(customer_id: id).order( 'id DESC' )
      credit_notes = CreditNote.where(customer_id: id).order( 'id DESC' )
      OrdersService::Summaries::OrderIndexSummary.new(orders, credit_notes).call
    end

    def all_sales_summary(id)
      orders = Order.all.where(supplier_id: id).order( 'id DESC' )
      credit_notes = CreditNote.where(supplier_id: id).order( 'id DESC' )
      OrdersService::Summaries::OrderIndexSummary.new(orders, credit_notes).call
    end
    

    def summary_by_customer(supplier_id, customer_id)
      orders = Order.where(customer_id: customer_id, supplier_id: supplier_id).where(status: ['accepted', 'paid', 'partially_paid']).order( 'id DESC' )
      payments = Payment.where(customer_id: customer_id, supplier_id: supplier_id).order( 'id DESC' )
      credit_notes = CreditNote.where(customer_id: customer_id, supplier_id: supplier_id).order( 'id DESC' )

      breakdown = [
        OrdersService::SlimSupplierSummaryBreakdown.new(orders).call,
        OrdersService::CreditNotesBreakdown.new(credit_notes).call,
        OrdersService::PaymentSummary.new(payments).call
      ].flatten

      breakdown.sort {|a,b| a[:date] <=> b[:date]}
    end

    def summary_by_supplier(customer_id, supplier_id)
      orders = Order.where(customer_id: customer_id, supplier_id: supplier_id).where(status: ['accepted', 'paid', 'partially_paid']).order( 'id DESC' )
      payments = Payment.where(customer_id: customer_id, supplier_id: supplier_id).order( 'id DESC' )
      credit_notes = CreditNote.where(customer_id: customer_id, supplier_id: supplier_id).order( 'id DESC' )
      
      breakdown = [
        OrdersService::SlimCustomerSummaryBreakdown.new(orders).call,
        OrdersService::CreditNotesBreakdown.new(credit_notes).call,
        OrdersService::PaymentSummary.new(payments).call
      ].flatten
      breakdown.sort {|a,b| a[:date] <=> b[:date]}
    end

    def sales_summary(id, status=nil)
      credit_notes = []
      if status.nil?
        orders = Order.by_period(Date.today.beginning_of_year, Date.today.end_of_month).where(supplier_id: id).order( 'id DESC' )
        credit_notes = CreditNote.by_period(Date.today.beginning_of_year, Date.today.end_of_month).where(supplier_id: id).order( 'id DESC' )
      else
        orders = Order.where(supplier_id: id).where(status: status).order( 'id DESC' )
      end
      OrdersService::Summaries::OrderIndexSummary.new(orders, credit_notes).call
    end
  
    def summary_by_date(id, start_date=Date.today.beginning_of_month, end_date=Date.today)
      if end_date != Date.today
        end_date = end_date.to_date + 1.day  
      end
      orders = Order.where('created_at >= :start AND created_at < :end', :start => start_date, :end => end_date).where(customer_id: id).order( 'id DESC' )
      credit_notes = CreditNote.where('created_at >= :start AND created_at < :end', :start => start_date, :end => end_date).where(customer_id: id).order( 'id DESC' )

      OrdersService::Summaries::OrderIndexSummary.new(orders, credit_notes).call
    end
  
    def sales_summary_by_date(id, start_date=Date.today.beginning_of_month, end_date=Date.today.end_of_month+1.day)
      parsed_end_date = (Date.parse(end_date)+2.day).strftime('%d-%m-%Y')
      orders = Order.by_period(start_date, parsed_end_date).where(supplier_id: id).order( 'id DESC' )
      credit_notes = CreditNote.by_period(start_date, parsed_end_date).where(supplier_id: id).order( 'id DESC' )
      OrdersService::Summaries::OrderIndexSummary.new(orders, credit_notes).call
    end

    def orders_by_date(supplier_id, date)
      Order.where(supplier_id: supplier_id).where(status: ['accepted', 'paid']).for_date(date).order(:order_number).includes(:order_items).map do |order|
        {order_number: order.order_number, title: order.customer.title, quantity: order.order_items.pluck(:quantity).sum, order_uuid: order.uuid}
      end
    end

    def waybill_by_date(supplier_id, date)
      items = Order.where(supplier_id: supplier_id).where(status: ['accepted', 'paid']).for_date(date).order(:order_number).includes(:order_items).collect(&:order_items).flatten.pluck(:quantity, :product_variant_id, :product_id).group_by{|y| [y.second, y.last]}
      items.map do |k,v|
        {description: ProductVariant.find_by(id: k.first)&.description || "", title: Product.find_by(id: k.last)&.title || '', quantity: v.sum{|x| x.first} || 0 }
      end
    end

  end
end
