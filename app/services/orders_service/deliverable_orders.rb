module OrdersService
  class DeliverableOrders

    def initialize(id)
      @id = id
    end

    def call
      deliverable_orders
    end

    def deliverable_orders
      orders = Order.where(supplier_id: @id).where(status: ['accepted', 'paid']).where.not(delivery_status: 'delivered').order( 'id DESC' )

      OrdersService::Summaries::OrderDeliverySummaryBreakdown.new(orders).call
    end

  end
end