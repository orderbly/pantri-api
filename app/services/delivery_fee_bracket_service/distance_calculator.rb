module DeliveryFeeBracketService
  class DistanceCalculator

    def initialize(customer_address_id, supplier)
      @customer_address = Address.find(customer_address_id)
      @customer_latutide = @customer_address.latitude
      @customer_longitude =  @customer_address.longitude

      @supplier_address = Address.find(supplier.default_address.id)
      @supplier_latutide = @supplier_address.latitude
      @supplier_longitude = @supplier_address.longitude
    end

    def call
      calculate(@customer_latutide, @customer_longitude, @supplier_latutide, @supplier_longitude)
    end

    def calculate(lat1, lon1, lat2, lon2, unit="K")
      if (lat1 == lat2) && (lon1 == lon2)
        return 0
      else 
        radlat1 = Math::PI * lat1/180
        radlat2 = Math::PI * lat2/180
        theta = lon1-lon2

        radtheta = Math::PI * theta/180

        dist = Math::sin(radlat1) * Math::sin(radlat2) + Math::cos(radlat1) * Math::cos(radlat2) * Math::cos(radtheta)
        dist = 1 if dist > 1
        dist = Math.acos(dist)
        dist = dist * 180/Math::PI
        dist = dist * 60 * 1.1515

        dist = dist * 1.609344 if unit=="K"
        dist = dist * 0.8684 if unit=="N"
        dist
      end
    end

  end
end