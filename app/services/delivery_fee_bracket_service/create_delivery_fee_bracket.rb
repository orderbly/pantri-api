module DeliveryFeeBracketService
  class CreateDeliveryFeeBracket
      def initialize(new_distance_fee_params)
        @new_distance_fee = new_distance_fee_params
        @distances = DeliveryFeeBracket.where(company_id: new_distance_fee_params[:company_id])
      end

      def call
        if add_delivery_fee_bracket(@new_distance_fee)
          delivery_fee = DeliveryFeeBracket.new(company_id: @new_distance_fee[:company_id], max_distance: @new_distance_fee[:max_distance], min_distance: @new_distance_fee[:min_distance], price: @new_distance_fee[:price])
          delivery_fee.save ? OpenStruct.new(status: true, obj: delivery_fee) : OpenStruct.new(status: false, obj: {})
        else
          OpenStruct.new(status: false, obj: {})
        end
      end

      def add_delivery_fee_bracket(new_distance_fee)
        validator = true
        @distances.each do |d|
          if !( validate_middle_position(d, new_distance_fee ) || validate_end_position(d, new_distance_fee)  || validate_start_position(d, new_distance_fee))
            validator = false
            break
          end
        end
        return validator
      end
      
      def validate_middle_position(existing_distance, new_distance_fee)
          (existing_distance[:min_distance].to_i < new_distance_fee[:max_distance].to_i) && (existing_distance[:max_distance].to_i < new_distance_fee[:min_distance].to_i)
      end
      
      def validate_start_position(existing_distance, new_distance_fee)
          ((new_distance_fee[:min_distance].to_i < existing_distance[:max_distance].to_i) && (new_distance_fee[:min_distance].to_i < existing_distance[:min_distance].to_i) && (new_distance_fee[:max_distance].to_i < existing_distance[:min_distance].to_i))
      end
      
      def validate_end_position(existing_distance, new_distance_fee)
          ((new_distance_fee[:max_distance].to_i > existing_distance[:max_distance].to_i) && (new_distance_fee[:min_distance].to_i > existing_distance[:max_distance].to_i))
      end
    
    
  end
end