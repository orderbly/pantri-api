module DeliveryFeeBracketService
  class CalculateDeliveryFee

    def initialize(address_id, supplier_id)
      @address_id = address_id
      @supplier_id = supplier_id
      @distances = Company.find(@supplier_id).delivery_fee_brackets
      @supplier = Company.find(@supplier_id)
    end

    def call
      calculate
    end

    def calculate
      delivery_fee = 0
      distance_diff = distance_difference
      @distances.each do |distance|
        if distance.min_distance.to_i <= distance_diff && distance.max_distance.to_i >= distance_diff
          delivery_fee = distance.price.to_f
        end
      end
      # @supplier.base_delivery_price
      delivery_fee
    end 

    def distance_difference
      DeliveryFeeBracketService::DistanceCalculator.new(@address_id, Company.find(@supplier_id)).call
    end

  end
end

