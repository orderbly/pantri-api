module Integrations
  module TradegeckoService
		class ConfigureTradegecko

			def initialize(company_id)
				@company_id = company_id
				@company = Company.find(company_id)
				@provider = 'tradegecko'
				@integration = Integration.find_by(company_id: company_id, name: 'tradegecko')
			end

			def call
				@gecko = Gecko::Client.new(client_id, client_secret)
        refresh = @gecko.authorize_from_refresh_token(@integration.refresh_token)
        if refresh.present?
          @integration.update(access_token: refresh.token, refresh_token: refresh.refresh_token)
          @root = Store::Inventory.find_by(company_id: @company_id, name: @provider, category: 'root') 

          @root = Store::Inventory.create(company_id: @company_id, name: @provider, category: 'root')  unless @root.present?
          begin
            create_shipping_product
            order_paid_webook
            order_updated_webhook
            fetch_customers
            return true
          rescue
            @root = Store::Inventory.find_by(company_id: @company_id, name: @provider, category: 'root').destroy_all
            return false
          end
        else
          @root = Store::Inventory.find_by(company_id: @company_id, name: @provider, category: 'root').destroy_all
          return false
        end
      end

      def create_shipping_product
        # if we already have the ids stored in the company - we store it in the parent inventory store
        if @root.company.inventory_blob["delivery_product_id"].present?
          @root.update(blob: {
            delivery_product_id: @root.company.inventory_blob["delivery_product_id"],
            delivery_product_variant_id: @root.company.inventory_blob["delivery_product_variant_id"]
          })
        else
          # create the prodcut and the product variant. If either fail - we delete the integrated record and return false
          product = @gecko.Product.build({ "name": 'Delivery Fee', "status": "active"})
          if product.save
            variant = @gecko.Variant.build({ product_id: product.id, name: 'Standard Delivery Fee'})
            if variant.save
              @root.update(blob: { delivery_product_id: product.id, delivery_product_variant_id: variant.id })
              @root.company.update(inventory_blob: { delivery_product_id: product.id, delivery_product_variant_id: variant.id } )
              return true
            else
              product.destroy
              return false
            end
          else 
            return false
          end
				end
      end

			def fetch_customers
				contact_store = Store::Inventory.find_by(company_id: @company_id, name: @provider, category: 'contacts')
				contact_store.delete if contact_store.present?
				Store::Inventory.create(
					company_id: @company_id,
					name: @provider, category: 'contacts',
					parent_id: @root.id,
					blob: @gecko.Company.where(limit: 250, status: 'active').map{|c| 
						{ 
							id: c.id,
							email: c.email,
							name: c.name,
							notes: c.notes,
							phone_number: c.phone_number,
							description: c.description,
							type: c.company_type,
							website: c.website,
							addresses: @gecko.Address.where(company_id: c["id"]).map{|a|
								{
									id: a.id,
									company_id: a.company_id,
									address1: a.address1,
									label: a.label,
									first_name: a.first_name,
									last_name: a.last_name,
									address2: a.address2,
									city: a.city,
									country: a.country,
									zip_code: a.zip_code,
									phone_number: a.phone_number,
									email: a.email
								}
							}
						}
					}.to_json
				)
      end
      
      def order_paid_webook
        webhook = @gecko.Webhook.build({
					address: ENV['TRADEGECKO_URI'],
					event: "payment.create"
				})
				webhook.save
      end

      def order_updated_webhook
				webhook = @gecko.Webhook.build({
					address: ENV['TRADEGECKO_URI'],
					event: "order.finalized"
				})
				webhook.save
      end
			
			private
			def client_id
				ENV['TRADEGECKO_CLIENT_ID']
			end
		
			def client_secret
				ENV['TRADEGECKO_CLIENT_SECRET']
			end

		end
	end
end