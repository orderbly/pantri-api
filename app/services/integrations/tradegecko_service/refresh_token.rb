module Integrations
	module TradegeckoService
		class RefreshToken

			def client_id
				ENV['TRADEGECKO_CLIENT_ID']
			end
		
			def client_secret
				ENV['TRADEGECKO_CLIENT_SECRET']
			end

			def call(company_id)
				integration = Integration.find_by(company_id: company_id, name: "tradegecko")

				gecko = Gecko::Client.new(client_id, client_secret)
				refresh = gecko.authorize_from_refresh_token(integration.refresh_token)
				integration.update(access_token: refresh.token, refresh_token: refresh.refresh_token)
			end

		end
	end
end

