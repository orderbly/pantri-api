module Integrations
	module TradegeckoService
		class SalesOrder

			def initialize(company_id:, order_uuid:, tradegecko_company_id:, tradegrecko_address_id:)
				@company_id = company_id
				@order = Order.find_by(uuid: order_uuid)
				@tradegecko_company_id = tradegecko_company_id
				@tradegrecko_address_id = tradegrecko_address_id
				@integration = Integration.find_by(company_id: company_id, name: 'tradegecko')
				@status_count = []
			end

			def call
				@gecko = Gecko::Client.new(client_id, client_secret)
				refresh = @gecko.authorize_from_refresh_token(@integration.refresh_token)
				@integration.update(access_token: refresh.token, refresh_token: refresh.refresh_token)
				@tax_type_id = @gecko.TaxType.where(limit: 10, status: 'active')&.second&.id
				begin
					create_sale_order if check_product_integrations	
				rescue
					false
				end
			end
			
			# we need to integrate each product variant if they are not already
			def check_product_integrations
				status_check = true
				@order.order_items.each do |oi|
					begin
						integrate_product_variant(oi) if !oi.product_variant&.inventory_item["id"].present?
					rescue 
						status_check = false
					end
				end
				status_check
			end

			def client_id
				ENV['TRADEGECKO_CLIENT_ID']
			end
		
			def client_secret
				ENV['TRADEGECKO_CLIENT_SECRET']
			end

			# Create the order then the order items - we need the order item id for its items
			def create_sale_order
				@integrated_order = @gecko.Order.build({
					company_id: @tradegecko_company_id,
					reference_number: "Orderbly: #{@order.order_number}",
					tax_treatment: 'inclusive',
					ship_at: @order.delivery_date,
					issued_at: @order.created_at,
					shipping_address_id: @tradegrecko_address_id,
					billing_address_id: @tradegrecko_address_id,
				})
				if @integrated_order.save
					@order.update(
						inventory: {
							id: @integrated_order.id,
							order_number: @integrated_order.order_number,
							document_url: @integrated_order.document_url,
							provider: 'tradegecko',
							provider_name: 'Trade Gecko'
						}
					)
					order_items_status = []
					@order.order_items.each do |oi|
						order_items_status << create_product_variant(oi)
					end

					if order_items_status.include? false
						@integrated_order.destroy
						return false
					else
						# if there is a delivery fee, we will add it to the order as a order item
						create_delivery_fee
						return true
					end

				else
					return false
				end
			end

			def create_product_variant(item)
				begin
					@order_item = @gecko.OrderLineItem.build({
						variant_id: item.product_variant.inventory_item["id"],
						label: item.product_variant.description,
						quantity: item.quantity,
						price: item.gross_price,
						order_id: @integrated_order.id,
						tax_type_id: @tax_type_id
					})
					if @order_item.save
						return true
					else
						@order_item.destroy
						return false
					end
				rescue
					return false
				end
			end


			def create_delivery_fee
				@order_item = @gecko.OrderLineItem.build({
					variant_id: @integration.company.inventory_blob["delivery_product_variant_id"],
					label: 'Standard Delivery Fee',
					quantity: 1,
					price:  @order.delivery_fee["gross_price"],
					order_id: @integrated_order.id,
					tax_type_id: @tax_type_id,
					line_type: 'shipping'
				})
				@order_item.save
			end

			def integrate_product_variant(item)
				# check if variant is not integrated but product is
				if !item&.product_variant&.inventory_item["id"].present? && item&.product_variant&.product&.inventory_item["id"].present? 
					begin
						create_product_variant_integration(item)
					rescue
						false
					end
				# Neither product or product_variant is integrated
				elsif !item&.product_variant&.inventory_item["id"].present? && !item&.product_variant&.product&.inventory_item["id"].present? 
					begin
						if create_product_integration(item)
							create_product_variant_integration(item)
						else
							return false
						end
					rescue
						false
					end
				end
			end

			def create_product_integration(item)
				product = @gecko.Product.build({
					"name": item.product.title,
					"status": "active"
				})
				if product.save
					item.product_variant.product.update(inventory_item: {id: product['id'], name: product['name']})
					return true
				else 
					return false
				end
			end

			def create_product_variant_integration(item)
				variant = @gecko.Variant.build({
					product_id: item.product_variant.product.inventory_item["id"],
					name: item.product_variant.description,
					sku: item.product_variant.sku
				})
				if variant.save
					item.product_variant.update(inventory_item: { id: variant.id, name: variant.name })
					return true
				else
					variant.destroy
					return false
				end
			end

		end
	end
end