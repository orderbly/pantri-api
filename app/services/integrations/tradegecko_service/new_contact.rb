module Integrations
	module TradegeckoService
		class NewContact

			def initialize(order_uuid)
        @order = Order.find_by(uuid: order_uuid)
        @supplier = @order.supplier
				@customer = @order.customer
        @integration = Integration.find_by(company_id: @supplier.id, name: 'tradegecko')
        @provider = 'tradegecko'
			end

      def call
        @root = Store::Inventory.find_by(company_id: @supplier.id, name: @provider, category: 'root') 
				@gecko = Gecko::Client.new(client_id, client_secret)
        refresh = @gecko.authorize_from_refresh_token(@integration.refresh_token)
				@integration.update( access_token: refresh.token, refresh_token: refresh.refresh_token )
        if create_contact
          fetch_customers
          return true
        else
          return false
        end
      end
      
      def create_contact
        company = @gecko.Company.build({
          name: @customer.title,
          company_type: "consumer", 
          email: @customer.email,
          phone_number: @customer.contact,
          tax_number: @customer.vat_number,
          website: @customer.website
        })
        if company.save
          address = @gecko.Address.build({
            company_id: company.id,
            label: "Address",
            address1: @order.delivery_address.address_one,
            state: @order.delivery_address.province,
            suburb: @order.delivery_address.suburb,
            country: @order.delivery_address.country,
            city: @order.delivery_address.city,
            company_name: @order.customer.title
          })
          return address.save
        else
          return false
        end
      end

			def client_id
				ENV['TRADEGECKO_CLIENT_ID']
			end
		
			def client_secret
				ENV['TRADEGECKO_CLIENT_SECRET']
      end

      def fetch_customers
				contact_store = Store::Inventory.find_by(company_id: @supplier.id, name: @provider, category: 'contacts')
				contact_store.delete if contact_store.present?
				Store::Inventory.create(
					company_id: @supplier.id,
					name: @provider, category: 'contacts',
					parent_id: @root.id,
					blob: @gecko.Company.where(limit: 250, status: 'active').map{|c| 
						{ 
							id: c.id,
							email: c.email,
							name: c.name,
							notes: c.notes,
							phone_number: c.phone_number,
							description: c.description,
							type: c.company_type,
							website: c.website,
							addresses: @gecko.Address.where(company_id: c["id"]).map{|a|
								{
									id: a.id,
									company_id: a.company_id,
									address1: a.address1,
									label: a.label,
									first_name: a.first_name,
									last_name: a.last_name,
									address2: a.address2,
									city: a.city,
									country: a.country,
									zip_code: a.zip_code,
									phone_number: a.phone_number,
									email: a.email
								}
							}
						}
					}.to_json
				)
      end

		end
	end
end
