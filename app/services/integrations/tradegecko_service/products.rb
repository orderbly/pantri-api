module Integrations
	module TradegeckoService
		class Products

			def initialize(company_id)
				@company_id = company_id
				@company = Company.find(company_id)
				@provider = 'tradegecko'
				@integration = Integration.find_by(company_id: company_id, name: 'tradegecko')

			end

      def call
        @gecko = Gecko::Client.new(ENV['TRADEGECKO_CLIENT_ID'], ENV['TRADEGECKO_CLIENT_SECRET'])
				refresh = @gecko.authorize_from_refresh_token(@integration.refresh_token)
				@integration.update(access_token: refresh.token, refresh_token: refresh.refresh_token)
        create_products
			end

      def create_products
        begin
          Rails.logger.info "Processing the request..."
          product_url = 'https://api.tradegecko.com/products/'
          products_request = Faraday.get(product_url) do |req|
            req.headers['Authorization'] = "Bearer #{@integration.access_token}" 
            req.headers['Content-Type'] = 'application/json'
          end
          products = JSON.parse(products_request.body)["products"]
          Rails.logger.info "product response"
          Rails.logger.info products_request.body
          build_products(products)
          true
        rescue
          false
        end
			end

      def build_products(products)
        products.each do |p|
          if !Product.find_by(company_id: @company_id, inventory_item: {id: p["id"]}).present?
            next if p["id"] == @company.inventory_blob["delivery_product_id"]
            product = new_product(p)
            Rails.logger.info product
            Rails.logger.info product.save!
            if product.save!
              @gecko.Variant.where(status: 'active', product_id: p["id"]).each do |pv|
                if pv.present?
                  product_variant = new_product_variant(product, pv)
                  if !product_variant
                    ProductVariant.where(product_id: product.id).destroy_all
                    product.delete
                    break
                  end
                end
              end
            end
          else
            product = Product.find_by(company_id: @company_id, inventory_item: {id: p["id"]})
            update_product(product, p)
            @gecko.Variant.where(status: 'active', product_id: p["id"]).each do |pv|
              product_variant = ProductVariant.find_by(inventory_item: {id: pv["id"]})
              if product_variant.present?
                update_product_variant(product_variant, pv)
              else
                new_product_variant(product, pv)
              end
            end
          end
        end
      end


      def new_product_variant(product, pv)
        product_variant = ProductVariant.new(
          product_id: product.id,
          sku: pv["sku"],
          description: pv["name"],
          available_stock: pv["stock_on_hand"].to_f,
          unit_amount: calculate_net(pv["wholesale_price"]).to_f || 0,
          tax_amount: calculate_net(pv["wholesale_price"]).to_f || 0,
          gross_amount: pv["wholesale_price"].to_f || 0,
          base_amount: pv["wholesale_price"].to_f || 0,
          cost_price: pv["last_cost_price"].to_f || 0,
          retail_amount: pv["wholesale_price"].to_f || 0,
          inventory_item: {id: pv["id"]}
        )
        product_variant.save! ? true : false
      end


      def new_product(product)
        image = @gecko.Image.find(product["image_ids"].last) if product["image_ids"].present?
        new_product =  Product.new(
          description: product["description"],
          title: product["name"],
          tax_id:  @company.taxes.last.id,
          image: "#{image&.base_path}/#{image&.file_name}" || "",
          is_active: true,
          company_id: @company_id,
          inventory_item: {id: product["id"]}
        )
        new_product
      end

      def update_product(product, p)
        image = @gecko.Image.find(p["image_ids"].last) if p["image_ids"].present? if !product.image.present?
        image_url = image.present? ? "#{image&.base_path}/#{image&.file_name}" : product.image

        product.update(
          description: p["description"],
          title: p["name"],
          tax_id: @company.taxes.last.id,
          image: image_url
        )
        product
      end

      def update_product_variant(product_variant, pv)
        product_variant.description= pv["name"]
        product_variant.sku= pv["sku"]
        product_variant.available_stock= pv["stock_on_hand"].to_f
        product_variant.unit_amount= calculate_net(pv["wholesale_price"].to_f)
        product_variant.tax_amount= calculate_tax(pv["wholesale_price"].to_f)
        product_variant.gross_amount= pv["wholesale_price"].to_f
        product_variant.base_amount= pv["wholesale_price"].to_f
        product_variant.retail_amount= pv["wholesale_price"].to_f
        product_variant.cost_price= pv["last_cost_price"].to_f
        product_variant.inventory_item= {id: pv["id"]}
        product_variant.save! ? true : false
      end
      
      def calculate_tax(val)
        return 0 if val == nil
        val - calculate_net(val)
      end

      def calculate_net(val)
        return 0 if val == nil
        val / (1+@company.taxes.first.percentage)
      end

		end
	end
end
