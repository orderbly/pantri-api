module Integrations
  module OptimoRoute
    class CreateDelivery

      def initialize(order_uuid)
        @order = Order.find_by(uuid: order_uuid)
        @company = @order.supplier
        @integration = Integration.find_by(company_id: @order.supplier_id, name: 'optimo')
      end

      def call
        url = "https://api.optimoroute.com/v1/create_order?key=#{@integration.access_token}"
        resp = Faraday.post(url, order_details.to_json) do |req|
          req.headers['Content-Type'] = 'application/json'
        end
        if JSON.parse(resp.body)["success"]
          @order.update(delivery: JSON.parse(resp.body))
          return true
        else
          return false
        end
      end

      def order_details
        {
          "operation": "CREATE",
          "orderNo": "#{DateFormat.change_to(@order.delivery_date, "ISO_8601_FORMAT")} #{@order.customer.title} #{@order.order_number}",
          "type": "D",
          "date": DateFormat.change_to(@order.delivery_date, "ISO_8601_FORMAT"),
          "location": {
            "address": "#{@order.delivery_address.address_one}, #{@order.delivery_address.city}, #{@order.delivery_address.country}",
            "locationNo": "#{@order.customer.title}",
            "locationName": "#{@order.delivery_address.address_one}, #{@order.delivery_address.city}",
            "acceptPartialMatch": true
          },
          "duration": 10,
          "notes": "http://localhost:8080/receive_order/9c1b9923-f254-4648-9c52-9f4a5abdb712"
        }
      end
    end
  end
end