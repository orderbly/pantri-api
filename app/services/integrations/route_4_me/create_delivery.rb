module Integrations
    module Route4Me
      class CreateDelivery
  
        def initialize(order_uuid)
          @order = Order.find_by(uuid: order_uuid)
          @company = @order.supplier
          @integration = Integration.find_by(company_id: @order.supplier_id, name: 'optimo')
        end

        def route4me_api_key
          route4me_api_key = ENV['ROUTE4ME_API_KEY']
        end
      
        def route_url
          "https://api.route4me.com/api.v4/order.php?api_key=#{route4me_api_key}"
        end
  
        def call
          resp = Faraday.post(route_url, order_details.to_json) do |req|
            req.headers['Content-Type'] = 'application/json'
          end
          if resp.status == 200
            @order.update(delivery: JSON.parse(resp.body))
            return true
          else
            return false
          end
        end
  
        def order_details
          {
            'address_1': "#{@order.delivery_address.address_one}, #{@order.delivery_address.city}, #{@order.delivery_address.country}",
            'cached_lat': @order.delivery_address.latitude,
            'cached_lng': @order.delivery_address.longitude,
            'address_name': "#{@order.customer.title}: #{@order.delivery_address.address_one}",
            'EXT_FIELD_phone': @order.customer.contact,
            'day_scheduled_for_YYMMDD': DateFormat.change_to(@order.delivery_date, "ISO_8601_FORMAT"),
          }
        end
      end
    end
  end