module Integrations
  require 'twilio-ruby'
  class TwilioService
  
    def send_message
      # Your Account Sid and Auth Token from twilio.com/console
      # DANGER! This is insecure. See http://twil.io/secure
      account_sid = ENV['TWILIO_ACCOUNT_SID']
      auth_token = ENV['TWILIO_AUTH_TOKEN']
      @client = Twilio::REST::Client.new(account_sid, auth_token)
  
      message = @client.messages.create(
        from: 'whatsapp:+14155238886',
        body: 'Your order has been placed',
        to: 'whatsapp:+27844381188'
      )
  
      puts message.sid
    end
  
  end
end