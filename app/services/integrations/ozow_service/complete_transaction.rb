module Integrations
  module OzowService
    class CompleteTransaction

      def initialize( params )
        @params = params
      end
  
      def call 
        complete_transaction
      end

      def complete_transaction
        Rails.logger.info "------- Payment service Parms ----------- #{@params}"
        if @params['Optional3'] != "order" || !Order.find_by(id: @params['Optional1']).present?
          supplier_id = @params['Optional2'].to_i
          customer_id  = @params['Optional4'].to_i
          total = @params["Amount"].to_f
          transaction_ref = @params['TransactionReference']

          payment_service =  PaymentService::GeneralCompanyPayment.new(customer_id, supplier_id, total, transaction_ref).call
          Rails.logger.info "------- Payment service ----------- #{payment_service}"
          if payment_service[:type] == "success"
            Pusher.trigger('payment_update',"#{@params['Optional4']}_order_#{@params['TransactionReference']}", {type: payment_service[:type], status: payment_service[:message]})
            return { status: "success", message: payment_service[:message]}
          else
            Pusher.trigger('payment_update',"#{@params['Optional4']}_order_#{@params['TransactionReference']}", {type: payment_service[:type], status: payment_service[:message]})
            return { status: "error", message: payment_service[:message]}
          end
        else
          pay_order = PaymentService::PayOrder.new(
            order_id: @params['Optional1'],
            value: @params['Amount'],
            transaction_id:  @params['TransactionReference'],
            supplier_id: @params['Optional2'].to_i,
            customer_id: @params['Optional4'].to_i
          ).call
  
          if pay_order[:type] == "success"
            Pusher.trigger('payment_update',"#{@params['Optional4']}_order_#{@params['TransactionReference']}", {type: pay_order[:type], order_id: @params['Optional1'].to_i, status: pay_order[:message]})
            return { status: "success", message: pay_order[:message]}
          else
            Pusher.trigger('payment_update',"#{@params['Optional4']}_order_#{@params['TransactionReference']}", {type: pay_order[:type], order_id: @params['Optional1'].to_i, status: pay_order[:message]})
            return { status: "error", message: pay_order[:message]}
          end
        end
      end


    end
  end
end
