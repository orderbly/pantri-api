module Integrations
  module SageService
    module CreditNotes
      class SalesReturn

        def initialize(credit_note:, sage_one_customer_id:)
          @sage_one_customer_id = sage_one_customer_id
          @credit_note = credit_note
          @customer = @credit_note.customer
          @supplier = @credit_note.supplier
        end

        def call
          create_invoice
        end

        def sage_company_id
          @supplier.integrations.find_by(name: 'sage').extras["company"]
        end

        def basic_token
          @supplier.integrations.find_by(name: 'sage').extras["basic_token"]
        end

        def url
          Integrations::SageService::SageOneUrl.new.call('sales_return', sage_company_id)
        end

        def create_invoice
          resp = Faraday.post(url, invoice_payload) do |req|
            req.headers['Authorization'] = 'Basic ' + basic_token
            req.headers['Content-Type'] = 'application/json'
          end

          @credit_note.accounting[:sales] = { invoice: JSON.parse(resp.body), supplier_id: @supplier.id }
          if @credit_note.save
            OpenStruct.new(type: 'success', message: 'Sage One integration was successfully completed' , obj: @credit_note)
          else
            OpenStruct.new(type: 'error', message: 'Sage One integration could not be completed' , obj: @credit_note)
          end
        end

        def credit_note_breakdown
          CreditNoteService::Breakdowns::CreditNoteBreakdown.new(@credit_note).call
        end

        def default_account
          Store::Accounting.find_by(company_id: @supplier.id, category: 'account_defaults').blob["defaultSalesAccount"]["ID"]
        end

        def selection_type(credit_note_item)
          product = CreditNoteItem.find(credit_note_item[:id]).product
          if product.accounting_item && product.accounting_item["item_id"]
            return OpenStruct.new(line_type: 0, selection_id: product.accounting_item["item_id"], quantity: credit_note_item[:quantity], gross_price: credit_note_item[:gross_price])
          else
            return OpenStruct.new(line_type: 1, selection_id: default_account, quantity: 1, gross_price: credit_note_item[:total_price])
          end
        end 


        def invoice_payload
          {
            'Date': DateTime.now,
            'CustomerId': @sage_one_customer_id,
            'CustomerName': credit_note_breakdown[:customer][:title],
            "Customer": {
              "Name": credit_note_breakdown[:customer][:title],
              "Category": {
                "Description": "Customer",
              },
            },
            'Inclusive': true,
            'Reference': "Orderbly: Return #{credit_note_breakdown[:invoice_number]}",
            'Lines': credit_note_breakdown[:credit_note_items].map{|cni| {
              'LineType': selection_type(cni).line_type,
              'SelectionId': selection_type(cni).selection_id,
              'Description': cni[:product_name],
              'Quantity': selection_type(cni).quantity,
              'UnitPriceInclusive': selection_type(cni).gross_price,
              'Total': selection_type(cni).gross_price,
            }}
          }.to_json
        end

      end
    end
  end
end
