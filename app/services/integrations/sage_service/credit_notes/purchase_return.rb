module Integrations
  module SageService
    module CreditNotes
      class PurchaseReturn

        def initialize(credit_note:, sage_one_supplier_id:)
          @credit_note = credit_note
          @sage_one_supplier_id = sage_one_supplier_id
          @customer = @credit_note.customer
          @supplier = @credit_note.supplier
        end

        def call
          create_invoice
        end

        def sage_company_id
          @customer.integrations.find_by(name: 'sage').extras["company"]
        end

        def basic_token
          @customer.integrations.find_by(name: 'sage').extras["basic_token"]
        end

        def url
          Integrations::SageService::SageOneUrl.new.call('purchase_return', sage_company_id)
        end

        def create_invoice
          resp = Faraday.post(url, invoice_payload) do |req|
            req.headers['Authorization'] = 'Basic ' + basic_token
            req.headers['Content-Type'] = 'application/json'
          end
          @credit_note.accounting[:credit_notes_received] = { invoice: JSON.parse(resp.body), supplier_id: @supplier.id }
          if @credit_note.save
            OpenStruct.new(type: 'success', message: 'Sage One integration was successfully completed' , obj: @credit_note)
          else
            OpenStruct.new(type: 'error', message: 'Sage One integration could not be completed' , obj: @credit_note)
          end
        end

        def credit_note_breakdown
          CreditNoteService::Breakdowns::CreditNoteBreakdown.new(@credit_note).call
        end

        def default_account
          Store::Accounting.find_by(company_id: @customer.id, category: 'account_defaults').blob["defaultPurchaseAccount"]["ID"]
        end

        def tax_amount(order_item)
          OrderItem.find(order_item[:id]).product.tax.percentage
        end

        def invoice_payload
          {
            'Date': DateTime.now,
            'SupplierId': @sage_one_supplier_id,
            'SupplierName': credit_note_breakdown[:supplier][:title],
            "Supplier": {
              "Name": credit_note_breakdown[:supplier][:title],
              "Category": {
                "Description": "Supplier",
              },
            },
            'Inclusive': true,
            'Reference': "Orderbly: Return #{credit_note_breakdown[:invoice_number]}",
            'Lines': credit_note_breakdown[:credit_note_items].map{|oi| {
              'LineType': 1,
              'SelectionId': default_account,
              'Description': oi[:product_name],
              'Quantity': 1,
              'UnitPriceInclusive': oi[:total_price],
              'Total': oi[:total_price]
            }}
          }.to_json
        end

      end
    end
  end
end
