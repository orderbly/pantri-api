module Integrations
  module SageService
    module Orders
      class PurchaseOrder

        def initialize(order_id:, sage_one_supplier_id:)
          @order_id = order_id
          @sage_one_supplier_id = sage_one_supplier_id
          @order = Order.find(order_id)
          @customer = Order.find(order_id).customer
          @supplier = Order.find(order_id).supplier
        end

        def call
          create_invoice
        end

        def sage_company_id
          @customer.integrations.find_by(name: 'sage').extras["company"]
        end

        def basic_token
          @customer.integrations.find_by(name: 'sage').extras["basic_token"]
        end

        def url
          Integrations::SageService::SageOneUrl.new.call('purchase_order', sage_company_id)
        end

        def create_invoice
          resp = Faraday.post(url, invoice_payload) do |req|
            req.headers['Authorization'] = 'Basic ' + basic_token
            req.headers['Content-Type'] = 'application/json'
          end

          @order.accounting[:purchases] = { invoice: JSON.parse(resp.body), customer_id: @customer.id }
          if @order.save
            OpenStruct.new(type: 'success', message: 'Sage One integration was successfully completed' , obj: @order)
          else
            OpenStruct.new(type: 'error', message: 'Sage One integration could not be completed' , obj: @order)
          end
        end

        def order_breakdown
          OrdersService::OrderBreakdown.new(@order_id).call
        end

        def default_account
          Store::Accounting.find_by(company_id: @customer.id, category: 'account_defaults').blob["defaultPurchaseAccount"]["ID"]
        end

        def invoice_line_items
          line_items = order_breakdown[:order_items].map{|oi| {
              'LineType': 1,
              'SelectionId': default_account,
              'Description': oi[:product_title],
              'Quantity': 1,
              'UnitPriceExclusive': oi[:gross_price] * oi[:quantity],
              'UnitPriceInclusive': oi[:gross_price] * oi[:quantity],
              'Total': oi[:gross_price] * oi[:quantity]
            }
          }

          if @order.delivery_fee["gross_price"].present?
            line_items.push({
              'LineType': 1,
              'SelectionId': default_account,
              'Description': 'Delivery Fee',
              'Quantity': 1,
              'UnitPriceInclusive': @order.delivery_fee["gross_price"],
              'Total': @order.delivery_fee["gross_price"]
            })
          end
          line_items
        end

        def invoice_payload
          {
            'DeliveryDate': order_breakdown[:order_date],
            'Date': order_breakdown[:order_date],
            'DueDate': order_breakdown[:order_date],
            'SupplierId': @sage_one_supplier_id,
            'SupplierName': order_breakdown[:supplier][:title],
            "Supplier": {
              "Name": order_breakdown[:supplier][:title],
              "Category": {
                "Description": "Supplier",
              },
            },
            'Inclusive': true,
            'Reference': "Orderbly: Invoice #{order_breakdown[:invoice_number]}",
            'Lines': invoice_line_items
          }.to_json
        end

      end
    end
  end
end