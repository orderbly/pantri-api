module Integrations
  module SageService
    module Orders
      class SalesOrder

        def initialize(order_id:, sage_one_customer_id:)
          @order_id = order_id
          @sage_one_customer_id = sage_one_customer_id
          @order = Order.find(order_id)
          @customer = Order.find(order_id).customer
          @supplier = Order.find(order_id).supplier
        end

        def call
          create_invoice
        end

        def sage_company_id
          @supplier.integrations.find_by(name: 'sage').extras["company"]
        end

        def basic_token
          @supplier.integrations.find_by(name: 'sage').extras["basic_token"]
        end

        def url
          Integrations::SageService::SageOneUrl.new.call('sales_order', sage_company_id)
        end

        def create_invoice
          resp = Faraday.post(url, invoice_payload) do |req|
            req.headers['Authorization'] = 'Basic ' + basic_token
            req.headers['Content-Type'] = 'application/json'
          end

          @order.accounting[:sales] = { invoice: JSON.parse(resp.body), supplier_id: @supplier.id }
          if @order.save
            OpenStruct.new(type: 'success', message: 'Sage One integration was successfully completed' , obj: @order)
          else
            OpenStruct.new(type: 'error', message: 'Sage One integration could not be completed' , obj: @order)
          end
        end

        def order_breakdown
          OrdersService::OrderBreakdown.new(@order_id).call
        end

        def default_account
          Store::Accounting.find_by(company_id: @supplier.id, category: 'account_defaults').blob["defaultSalesAccount"]["ID"]
        end

        def selection_type(order_item)
          product = OrderItem.find(order_item[:id]).product
          if product.accounting_item && product.accounting_item["item_id"]
            return OpenStruct.new(line_type: 0, selection_id: product.accounting_item["item_id"], quantity: order_item[:quantity], gross_price: order_item[:gross_price])
          else
            return OpenStruct.new(line_type: 1, selection_id: default_account, quantity: 1, gross_price: order_item[:gross_price] * order_item[:quantity])
          end
        end

        def invoice_line_items
          line_items = order_breakdown[:order_items].map{|oi| {
              'LineType': selection_type(oi).line_type,
              'SelectionId': selection_type(oi).selection_id,
              'Description': oi[:product_title],
              'Quantity': selection_type(oi).quantity,
              'UnitPriceInclusive': selection_type(oi).gross_price,
              'Total': selection_type(oi).gross_price
            }
          }

          if @order.delivery_fee["gross_price"].present?
            line_items.push({
              'LineType': 1,
              'SelectionId': default_account,
              'Description': 'Delivery Fee',
              'Quantity': 1,
              'UnitPriceInclusive': @order.delivery_fee["gross_price"],
              'Total': @order.delivery_fee["gross_price"]
            })
          end
          line_items
        end


        def invoice_payload
          {
            'DeliveryDate': order_breakdown[:order_date],
            'Date': order_breakdown[:order_date],
            'DueDate': order_breakdown[:order_date],
            'CustomerId': @sage_one_customer_id,
            'CustomerName': order_breakdown[:customer][:title],
            "Customer": {
              "Name": order_breakdown[:customer][:title],
              "Category": {
                "Description": "Customer",
              },
            },
            'Inclusive': true,
            'Reference': "Orderbly: Invoice #{order_breakdown[:invoice_number]}",
            'Lines': invoice_line_items
          }.to_json
        end

      end
    end
  end
end