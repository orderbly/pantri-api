module Integrations
  module SageService
    class SageOneUrl

      def call(path, sage_client)
        sage_api_key = ENV['SAGE_API_KEY']
        sage_endpoint + specific_path(path) + "?apikey={#{sage_api_key}}&companyId=#{sage_client}"
      end

      def sage_endpoint
        'https://resellers.accounting.sageone.co.za/api/2.0.0/'
      end
    
      def specific_path(path_name)
        case path_name
        when 'items'
          'Item/Get'
        when 'customers'
          'Customer/Get'
        when 'suppliers'
          'Supplier/Get'
        when 'accounts'
          'Account/Get'
        when 'companies'
          'Company/Get'
        when 'login'
          'Login/Validate'
        when 'purchase_order'
          'SupplierInvoice/Save'
        when 'sales_order'
          'TaxInvoice/Save'
        when 'purchase_return'
          'SupplierReturn/Save'
        when 'sales_return'
          'CustomerReturn/Save'
        else
          'Login/Validate'
        end
      end

    end
  end
end