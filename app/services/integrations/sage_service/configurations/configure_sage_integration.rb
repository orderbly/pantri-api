module Integrations
  module SageService
    module Configurations
      class ConfigureSageIntegration

        def initialize(company_id, current_user_id)
          @company_id = company_id
          @current_user = User.find(current_user_id)
          @company = Company.find(company_id)
          @provider = 'sage'
          # @root = nil
        end

        def call
          override_store
          fetch_customers
          fetch_suppliers
          fetch_items
          fetch_accounts
        end
        
        def override_store
          Store::Accounting.where(company_id: @company_id, name: @provider).delete_all
          @root = Store::Accounting.create(user_id: @current_user.id, company_id: @company_id, name: @provider, category: 'root')
        end

        def basic_token
          @company.integrations.find_by(name: 'sage').extras["basic_token"]
        end

        def sage_client
          @company.integrations.find_by(name: 'sage').extras["company"]
        end

        def fetch_customers
          resp = Faraday.get(sage_url('customers')) do |req|
            req.headers['Authorization'] = 'Basic ' + basic_token
            req.headers['Content-Type'] = 'application/json'
          end

          Store::Accounting.create(user_id: @current_user.id, company_id: @company_id, name: @provider, parent_id: @root.id, category: 'customers', blob: JSON.parse(resp.body)["Results"]) if resp.status == 200
        end
        
        def fetch_suppliers
          resp = Faraday.get(sage_url('suppliers')) do |req|
            req.headers['Authorization'] = 'Basic ' + basic_token
            req.headers['Content-Type'] = 'application/json'
          end

          Store::Accounting.create(user_id: @current_user.id, company_id: @company_id, name: @provider, parent_id: @root.id, category: 'suppliers', blob: JSON.parse(resp.body)["Results"]) if resp.status == 200
        end
        
        def fetch_items
          resp = Faraday.get(sage_url('items')) do |req|
            req.headers['Authorization'] = 'Basic ' + basic_token
            req.headers['Content-Type'] = 'application/json'
          end

          Store::Accounting.create(user_id: @current_user.id, company_id: @company_id, name: @provider, parent_id: @root.id, category: 'items', blob: JSON.parse(resp.body)["Results"]) if resp.status == 200
        
        end
        
        def fetch_accounts
          resp = Faraday.get(sage_url('accounts')) do |req|
            req.headers['Authorization'] = 'Basic ' + basic_token
            req.headers['Content-Type'] = 'application/json'
          end

          Store::Accounting.create(user_id: @current_user.id, company_id: @company_id, name: @provider, parent_id: @root.id, category: 'accounts', blob: JSON.parse(resp.body)["Results"]) if resp.status == 200
        end

        def sage_endpoint
          'https://resellers.accounting.sageone.co.za/api/2.0.0/'
        end
      
        def sage_url(path)
          sage_api_key = ENV['SAGE_API_KEY']
          sage_endpoint + specific_path(path) + "?apikey={#{sage_api_key}}&companyId=#{sage_client}"
        end
      
        def specific_path(path_name)
          case path_name
          when 'items'
            'Item/Get'
          when 'customers'
            'Customer/Get'
          when 'suppliers'
            'Supplier/Get'
          when 'accounts'
            'Account/Get'
          when 'companies'
            'Company/Get'
          when 'login'
            'Login/Validate'
          else
            'Login/Validate'
          end
        end
        
      end
    end
  end
end