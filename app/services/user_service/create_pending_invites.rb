module UserService
  class CreatePendingInvites

    def initialize(email)
      @email = email
      @user = User.find_by(email: email)
    end

    def call
      PendingInvite.all.where(email_address: @email).each do |pi|
        membership = Member.new(user_id: @user.id, company_id: pi.company_id, role_id: pi.role, user_permissions: pi.user_permissions)
        pi.delete if membership.save!
      end
    end

  end
end
