module DeliveryAllocationService
  class CreateDeliveryAllocation

    def initialize(users:, orders:)
      @user = users
      @orders = orders
    end

    def call
      create_delivery_allocations
    end

    def user
      @user.class == Array ? @user.first : @user
    end

    def create_delivery_allocations
      @orders.each do |order|
        if MembershipService::ValidateMembership.new(user_id: user, company_id: Order.find(order).supplier.id).validate
          DeliveryAllocation.where(order_id: order).delete_all
          DeliveryAllocation.create!(user_id: user.to_i, order_id: order.to_i) if Order.find(order).delivery_status !=  "delivered" || "order_cancelled" ? true : false
        else
          return {:type => false, :status => 'failed', :message => 'User does not have a membership with this company'}
        end  
      end
      return  {:type => true, :status => 'success', :message => 'Delivery allocations have been created'}
    end

  end
end