module CreditNoteService
  class CreateCreditNote

    def initialize(params, credit_note_items)
      @params = params
      @order = Order.find(params[:order_id])
      @credit_note_items = credit_note_items
    end

    def call
      # create the credit note
      credit_note = CreditNote.create(@params)
      credit_note.update(remaining_balance: credit_note.balance)
      credit_note_number = CreditNote.where(supplier_id: credit_note.supplier_id).length + 1
      credit_note.update(credit_note_number: "CN-#{credit_note_number}")
      @credit_note_items.each do |cn|
        if cn[:received].to_f < cn[:quantity].to_f
          CreditNoteItem.create(
            product_id: cn[:product_id],
            product_variant_id: cn[:product_variant_id],
            description: cn[:description],
            quantity: cn[:quantity].to_f - cn[:received].to_f,
            gross_price: cn[:gross_price],
            net_price: cn[:net_price],
            vat_price: cn[:vat_price],
            company_id: cn[:company_id],
            credit_note_id: credit_note.id
          )
        end
      end

      if credit_note.save!
        CreditNoteService::ClaimCreditNote.new(@order).call
        unpaid_order = Order.where(supplier_id: @order.supplier_id, customer_id: @order.customer_id).where.not(is_paid: 'true', id: @order.id).order("id ASC")
        if !unpaid_order.empty?
          unpaid_order.each do |order|
            CreditNoteService::ClaimCreditNote.new(order).call
          end
        end
        return {'obj': CreditNote.find(credit_note.id), 'status': true}
      else
        return {'error': 'err', 'status': false}
      end
    end

  end
end
