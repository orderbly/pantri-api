module CreditNoteService
  module Breakdowns
    class CreditNoteBreakdown

      def initialize(credit_note)
        @credit_note = credit_note
      end

      def call
        credit_note_summary
      end
      
      def credit_note_summary
          {
            id: @credit_note.id,
            customer: {
              id: @credit_note.customer.id,
              title: @credit_note.customer.title,
              address_one: @credit_note.customer.default_address&.address_one || "",
              city: @credit_note.customer.default_address&.city || "",
              postal_code: @credit_note.customer.default_address&.postal_code || ""
            },
            credit_note_number: @credit_note.credit_note_number ? @credit_note.credit_note_number : @credit_note.id,
            date: @credit_note.created_at.strftime("%d-%m-%Y"),
            supplier: {
              title: @credit_note.supplier.title,
              contact: @credit_note.supplier.contact,
              email: @credit_note.supplier.email,
              id: @credit_note.supplier.id,
              address_one: @credit_note.supplier.addresses&.first&.address_one || "",
              city: @credit_note.supplier.addresses&.first&.city || "",
              postal_code: @credit_note.supplier.addresses&.first&.postal_code || "",
            },
            balance: @credit_note.balance,
            balance: @credit_note.balance,
            credit_note_items: @credit_note.credit_note_items.map {|cni| {
              id: cni.id,
              product_id: cni.product_id,
              description: cni.description,
              notes: cni.notes,
              product_name: "#{cni.product.title} #{cni.product_variant.description}",
              quantity: cni.quantity,
              gross_price: cni.gross_price,
              net_price: cni.net_price,
              vat_price: cni.vat_price,
              total_price: (cni.quantity) * (cni.gross_price || 0)
            } if (cni.quantity) * (cni.gross_price || 0) > 0}.compact
          }
      end

    end
  end
end
