module CreditNoteService
  module Breakdowns
    class CreditNoteIndexBreakdown

      def initialize(credit_notes)
        @credit_notes = credit_notes
      end

      def call
        claim_credit_note
      end

      def claim_credit_note
        breakdown = []
          @credit_notes.map{|cn| breakdown.push(
            id: cn.id,
            customer: {
              id: cn.customer.id,
              title: cn.customer.title,
              address_one: cn.customer.addresses.first.address_one || "",
              city: cn.customer.addresses.first.city || "",
              postal_code: cn.customer.addresses.first.postal_code || ""
            },
            credit_note_number: cn.credit_note_number ? cn.credit_note_number : cn.id,
            date: cn.created_at.strftime("%d-%m-%Y"),
            supplier: {
              title: cn.supplier.title,
              contact: cn.supplier.contact,
              email: cn.supplier.email,
              id: cn.supplier.id,
              address_one: cn.supplier.addresses.first.address_one || "",
              city: cn.supplier.addresses.first.city || "",
              postal_code: cn.supplier.addresses.first.postal_code || ""
            },
            balance: cn.balance,
            credit_note_items: cn.credit_note_items.map {|cni| {
              product_name: "#{cni.product.title} #{cni.product_variant.description}",
              quantity: cni.quantity,
              description: cni.description,
              gross_price: cni.gross_price,
              net_price: cni.net_price,
              vat_price: cni.vat_price,
              total_price: cni.quantity * cni.gross_price
            }}
          )}
          breakdown
      end

    end
  end
end
