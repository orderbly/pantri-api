module CreditNoteService
  class ClaimCreditNote

    def initialize(order)
      @order = order
    end

    def call
      claim_credit_note
    end

    def claim_credit_note
      claimable_credit_notes = CreditNote.where(supplier_id: @order.supplier_id, customer_id: @order.customer_id).where('remaining_balance > ?', 0)
      claimable_credit_notes.each do |credit_note|
        break if @order.is_paid == "true"
      if credit_note.remaining_balance >= @order.balance
          CreditNoteClaim.create(credit_note_id: credit_note.id, order_id: @order.id, value: @order.balance)
          credit_note.update(remaining_balance: credit_note.remaining_balance - @order.balance)
          @order.update(balance: 0, is_paid: 0)
        elsif credit_note.remaining_balance < @order.balance
          new_balance = @order.balance - credit_note.remaining_balance
          CreditNoteClaim.create(credit_note_id: credit_note.id, order_id: @order.id, value: credit_note.remaining_balance)
          @order.update(balance: new_balance, is_paid: 2)
          credit_note.update(remaining_balance: 0)
        end
      end
    end

  end
end