module CreditNoteService
  class GeneralCreditNote

    def initialize(params)
      @params = params
      @order = Order.find(params['credit_note']['order_id'])
    end

    def call
      # if @order.delivery_status != "delivered"
      #   return {'error': 'The order needs to be delivered before a credit note is placed', 'status': false}
      # end
      # create the credit note
      credit_note = CreditNote.create(
        order_id: @params['credit_note']['order_id'],
        customer_id: @params['credit_note']['customer_id'],
        supplier_id: @params['credit_note']['supplier_id'],
        balance: @params['credit_note']['balance'],
        remaining_balance: @params['credit_note']['balance']
      )

      credit_note_number = CreditNote.where(supplier_id: credit_note.supplier_id).length + 1
      credit_note.update(credit_note_number: "CN-#{credit_note_number}")

      @params['credit_note']['order_items'].each do |oi|
        if oi["gross_price"].to_f > 0
          CreditNoteItem.create(
            product_id: oi['product_id'],
            description: oi[:description],
            product_variant_id: oi["product_variant_id"],
            quantity: oi["quantity"].to_f,
            gross_price: oi["gross_price"].to_f,
            notes: oi["notes"],
            net_price: oi["net_price"].to_f,
            vat_price: oi["vat_price"].to_f,
            company_id: oi["company_id"],
            credit_note_id: credit_note.id
          )
        end
      end
      if credit_note.save!
        CreditNoteService::ClaimCreditNote.new(@order).call
        unpaid_order = Order.where(supplier_id: @order.supplier_id, customer_id: @order.customer_id).where.not(is_paid: 'true', id: @order.id).order("id ASC")
        if !unpaid_order.empty?
          unpaid_order.each do |order|
            CreditNoteService::ClaimCreditNote.new(order).call
          end
        end
        return {'obj': CreditNote.find(credit_note.id), 'status': true}
      else
        return {'error': 'err', 'status': false}
      end
    end

  end
end
