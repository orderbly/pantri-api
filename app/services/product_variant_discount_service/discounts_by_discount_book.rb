module ProductVariantDiscountService
  class DiscountsByDiscountBook
    def initialize(company_id, discount_book_id)
      @company_id = company_id
      @discount_book_id = discount_book_id
    end
    def call
      discounts_by_discount_book
    end
    def discounts_by_discount_book
      product_variants = ProductVariant.all.includes(:product).where(:products => {company_id: @company_id, is_active: true})
      payload = []
      product_variants.each do |pv| 
        if (pv.product_variant_discounts.where(discount_book_id: @discount_book_id).any?) 
          payload.push(
            product_variant: pv,
            product_variant_discount: {
              product_variant_id: pv.id,
              discount_percentage: pv.product_variant_discounts.where(discount_book_id: @discount_book_id).first.discount_percentage,
              id: pv.product_variant_discounts.where(discount_book_id: @discount_book_id).first.id,
              discount_book_title: pv.product_variant_discounts.where(discount_book_id: @discount_book_id).first.discount_book.title,
              discount_book_id: @discount_book_id
            },
            product_name: pv.product.title
          )
        else 
          payload.push(
            product_variant: pv,
            product_variant_discount: {
              product_variant_id: pv.id,
              discount_percentage: 0,
              id: 0,
              discount_book_title: 0,
              discount_book_id: @discount_book_id
            },
            product_name: pv.product.title
          )
        end
      end
      payload
    end
  end
end