module ProductVariantDiscountService
    class SaveProductVariantDiscountsService
        def initialize (product_variant_discounts)
            @all_added = true
            @product_variant_discounts = product_variant_discounts
            @saved_discounts = []
        end

        def call
            save_discounts
            {:all_added => @all_added, :product_variant_discounts => @saved_discounts}
        end

        def save_discounts
            @product_variant_discounts.each do |pvd|
                if pvd[:id].to_f == 0
                    new_pvd = ProductVariantDiscount.new(
                        product_variant_id: pvd[:product_variant_id], 
                        discount_book_id: pvd[:discount_book_id].to_f, 
                        discount_percentage: pvd[:discount_percentage]
                        )
                        if !new_pvd.save
                            @all_added = false
                        else
                            @saved_discounts.push(pvd)
                        end
                    else
                   updated_pvd = ProductVariantDiscount.find(pvd[:id])
                   updated_pvd.discount_percentage = pvd[:discount_percentage].to_f
                   if !updated_pvd.save
                    @all_added = false
                    else
                     @saved_discounts.push(pvd)
                    end
                end
            end
        end
    end
end