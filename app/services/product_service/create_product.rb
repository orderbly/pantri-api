module ProductService
  class CreateProduct

    def initialize(params:, company_id:)
      @params = params
      @company_id = company_id
    end

    def call
      create_product
    end

    def create_product
      product_params_no_image = {}
      product_params_no_image['product_variants_attributes'] = []
      product_params_no_image['got_variants'] =  @params[:got_variants] == true 
      product_params_no_image['description'] = @params[:description]
      product_params_no_image['title'] = @params[:title]
      product_params_no_image['exclusive_companies'] = @params[:exclusive_companies]
      product_params_no_image['tax_id'] = @params[:tax_id]
      product_params_no_image['accounting_item'] = @params[:accounting_item] ? JSON.parse(@params[:accounting_item]) : ""
      product_params_no_image['company_id'] = @params[:company_id] || @company_id
      @product = Product.create(product_params_no_image)
      @params[:product_variants_attributes].each do |pv|
        ProductVariant.create(description: pv[:description], base_amount: pv[:base_amount], margin: pv[:margin], cost_price: pv[:cost_price], sku: pv[:sku], track_stock: pv[:track_stock], available_stock: pv[:available_stock], gross_amount: pv[:gross_amount], unit_amount: pv[:unit_amount], tax_amount: pv[:tax_amount], product_id: @product.id)
      end

      @product.tags.delete_all
      if  @params[:tags].present?
        @params[:tags].uniq.each do |tag|
          @product.tags << Tag.find(tag)
        end
      end

      if @params[:image]
        decoded_data = Base64.decode64(@params[:image].split(',')[1])
        signature = { io: StringIO.new(decoded_data), filename: @params[:title] }
        @product.product_image.attach(signature)
        @product.image =  @product.product_image.service.send(:object_for, @product.product_image.key).public_url
        @product.save
      end
  
      return @product.save ? {'obj': @product, 'status': true} : {'obj': @product, 'status': false}
    end

  end
end