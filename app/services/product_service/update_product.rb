module ProductService
  class UpdateProduct

    def initialize(params:, product:)
      @params = params
      @product = product
    end

    def call
      update_product
    end

    def update_product
      product_params_no_image = {}
      product_params_no_image['description'] = @params[:description]
      product_params_no_image['title'] = @params[:title]
      product_params_no_image['exclusive_companies'] = @params[:exclusive_companies]
      product_params_no_image['tax_id'] = @params[:tax_id]
      product_params_no_image['accounting_item'] = @params[:accounting_item] ? JSON.parse(@params[:accounting_item]) : ""

      @product.update(product_params_no_image)
      if @params[:product_variants_attributes] 
        @params[:product_variants_attributes].each do |pv| 
          product = ProductVariant.find_by(id: pv[:id])
          if product 
            product.update(description: pv[:description], base_amount: pv[:base_amount], margin: pv[:margin], cost_price: pv[:cost_price], sku: pv[:sku], track_stock: pv[:track_stock], available_stock: pv[:available_stock], gross_amount: pv[:gross_amount], unit_amount: pv[:unit_amount], tax_amount: pv[:tax_amount])
          else
            ProductVariant.create(description: pv[:description], base_amount: pv[:base_amount], margin: pv[:margin], cost_price: pv[:cost_price], sku: pv[:sku], track_stock: pv[:track_stock], available_stock: pv[:available_stock], gross_amount: pv[:gross_amount], unit_amount: pv[:unit_amount], tax_amount: pv[:tax_amount], product_id: @product.id)
          end
        end
      end
      
      if @params[:tags].present?
        @params[:tags].uniq.each do |tag|
          @product.tags << Tag.find(tag)
        end
      end

      if @params[:image]
        decoded_data = Base64.decode64(@params[:image].split(',')[1])
        signature = { io: StringIO.new(decoded_data), filename: @params[:title] }
        @product.product_image.attach(signature)
        @product.image =  @product.product_image.service.send(:object_for, @product.product_image.key).public_url
      end
      
      return @product.update(product_params_no_image) ? {'obj': @product, 'status': true} : {'obj': @product, 'status': false}
    end

  end
end
