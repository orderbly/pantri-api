module ProductService
  class CompanyProducts

    def initialize(current_company:, company_id:)
      @supplier_id = company_id
      @customer_id = current_company
      @customer = Company.find(current_company)
      @discount_book = DiscountBookMembership.joins(:discount_book).where(customer_id: current_company).where(:discount_books => {supplier_id: company_id}).first
      # @comission_fee = Rails.configuration.comission_fee
    end

    def call
      @products = build_products
    end

    def build_products
      if @discount_book
        build_products_with_discount_book
      else
        build_products_no_discount
      end
    end
  
    def build_products_with_discount_book
      payload = []
      products.compact.each do |product|
        product.product_variants.where(is_active: true).each do |pv, index|
          relative_discount = pv.product_variant_discounts.find_by(discount_book_id: @discount_book.discount_book_id, product_variant_id: pv.id)
          if relative_discount.present?
            pv.unit_amount = calculate_discount(pv.unit_amount, relative_discount.discount_percentage).to_f
            pv.tax_amount = calculate_discount(pv.tax_amount, relative_discount.discount_percentage).to_f
            pv.gross_amount = calculate_discount(pv.gross_amount, relative_discount.discount_percentage).to_f
            pv.base_amount = calculate_discount(pv.base_amount, relative_discount.discount_percentage).to_f
          end
        end
        payload.push({ product: product, product_variants: product.product_variants.order('description ASC'), has_stock: has_stock(product), tags: product.tags.map{|t| t.description}  })
      end
      payload
    end

    def has_stock(product)
      product_list = []
      product.product_variants.each do |pv|
        if !pv.track_stock
          product_list.push(true)
        elsif pv.available_stock > 0
          product_list.push(true)
        else
          product_list.push(false)
        end
      end
      product_list.include? true
    end
  
    def build_products_no_discount
      payload = []
      products.compact.each do |product|
        payload.push({ product: product, product_variants: product.product_variants.order('description ASC'), has_stock: has_stock(product), tags: product.tags.map{|t| t.description}  })
      end
      payload
    end
    
    def products
      products = Product.where(company_id: @supplier_id, is_active: true, is_archived: false).map  do |product| 
        if (product.exclusive_companies.present? && (product.exclusive_companies.include? @customer.uuid)) || !product.exclusive_companies.present? 
          product
        end
      end
    end

    # def add_comissionto_to_variants(product_variants)
    #   product_variants.each do |pv|
    #     pv.unit_amount = pv.unit_amount * (@comission_fee + 1)
    #     pv.tax_amount = pv.tax_amount * (@comission_fee + 1)
    #   end
    # end

    # def add_commission_to_single_variant(variant_value)
    #   variant_value * (@comission_fee + 1)
    # end

    def calculate_discount(value, discount)
      value * ((100 - discount) / 100)
    end

  end
end
