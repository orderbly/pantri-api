module CreditLimitsService
  class CalculateCreditBalance

    def initialize(supplier_id, customer_id)
      @supplier_id = supplier_id
      @customer_id = customer_id
    end

    def call
      { balance: balance, credit_limit: credit_limit, available_balance: (credit_limit - balance) }
    end

    def balance
      order_balance - credit_balance
    end

    def available_balance
      credit_limit - balance
    end

    def order_balance
      Order.all.where(supplier_id: @supplier_id, customer_id: @customer_id).where('balance > ?', 0).where.not(status: ['cancelled', 'declined']).sum(:balance)
    end

    def credit_balance
      CreditNote.all.where(supplier_id: @supplier_id, customer_id: @customer_id).where('remaining_balance > ?', 0).sum(:remaining_balance)
    end

    def credit_limit
      CreditLimit.find_by(supplier_id: @supplier_id, customer_id: @customer_id)&.credit_limit ||  Company.find(@supplier_id).default_account_limit
    end


  end
end
