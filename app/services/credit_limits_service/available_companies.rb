module CreditLimitsService
  class AvailableCompanies

    def initialize(supplier_id:)
      @supplier_id = supplier_id
      @supplier = Company.find(supplier_id)
    end

    def call 
      available_companies
    end

    def available_companies
      existing_companies_with_limits = @supplier.credit_limits.pluck(:customer_id)
      existing_companies_with_limits << @supplier_id
      Company.all.where.not(id: existing_companies_with_limits).map do |c|
        {
          id: c.id,
          title: c.title
        }
      end
    end

  end
end