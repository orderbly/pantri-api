module OrderScheduleService
  class EidtOrderSchedule

    def initialize(params)
      @params = params
    end

    def update
      order =  OrderSchedule.new(
        order_id:  @params[:order_id],
        end_date: @params[:end_date],
        active: true,
        frequency: @params[:frequency],
        start_date: DateTime.now(),
        )
        order.next_date = OrderScheduleService::OrderScheduleDate.new(order).call
        
        if order.save!
          return {'obj': order, 'status': true}
        else
          return {'error': 'err', 'status': false}
        end
    end

  end
end