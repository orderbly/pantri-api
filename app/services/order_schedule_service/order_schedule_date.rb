module OrderScheduleService
  class OrderScheduleDate

    def initialize(order_schedule)
      @order_schedule = order_schedule
    end

    def call
      set_next_date
    end

    def set_next_date
      order_schedule = @order_schedule
      case order_schedule.frequency
      when 'daily'
        date = daily_orders(order_schedule)
        setDate(date, order_schedule)
      when 'weekly'
        date = weekly_orders(order_schedule.value)
        setDate(date, order_schedule)
      when 'monthly'
        date = monthly_orders(order_schedule)
        setDate(date, order_schedule)
      else
        "Error: capacity has an invalid value)"
      end
    end
  
    def daily_orders(order_schedule)
      order_schedule.next_date == nil ? order_schedule.start_date : order_schedule.next_date + 1.day
    end

    def weekly_orders(day)
      date  = Date.parse(day)
      delta = date > Date.today ? 0 : 7
      date + delta
    end
  
    def every_two_weeks
      date = Date.today
      date + 2.weeks
    end
  
    def every_1st_of_the_month
      Date.today.at_beginning_of_month.next_month
    end
  
    def every_15th_of_the_month
      date = Date.today
      mid_month  = (date + 1.month).beginning_of_month + 14
    end
  
    def setDate(date, order_schedule) 
      if date < order_schedule.end_date
        order_schedule.next_date = date
        order_schedule.save
        order_schedule
      else
        order_schedule.active = false
      end
      date
    end

    def monthly_orders(order_schedule) 
      day_of_month = (order_schedule.value.to_i - 1) || 0
      date = nil
      if order_schedule.value.to_i >= Date.new.end_of_month.day 
        date = order_schedule.start_date.end_of_month
      else
        start_date_month = order_schedule.start_date.beginning_of_month
        temp_start = start_date_month + day_of_month.day
        if temp_start.beginning_of_day < order_schedule.start_date.beginning_of_day
          date = order_schedule.start_date.end_of_month + order_schedule.value.to_i.days
        else 
          date = start_date_month + day_of_month.day
        end
      end
      date
    end

  end
end