module OrderScheduleService
  class UpdateOrderSchedule
  
    def call
      OrderSchedule.all.where(next_date: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day, active: true).each do |schedule|
        update_order_schedule(schedule)
      end
    end

    def update_order_schedule(schedule)
      OrdersService::CreateScheduledOrder.new(schedule.order_id).call
      OrderScheduleService::OrderScheduleDate.new(schedule).call
    end

  end
end