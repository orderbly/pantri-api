module OrderScheduleService
  class CreateOrderSchedule

    def initialize(params)
      @params = params
    end

    def create
      order_schedule =  OrderSchedule.new(
        order_id:  @params[:order_id],
        end_date: @params[:end_date],
        active: true,
        frequency: @params[:frequency],
        value: @params[:value],
        start_date: @params[:start_date],
        )
      order_schedule.next_date = OrderScheduleService::OrderScheduleDate.new(order_schedule).call
        
      if order_schedule.save!
        Order.find(@params[:order_id]).update(order_schedule_id: order_schedule.id)
        return {'obj': order_schedule, 'status': true}
      else
        return {'error': 'err', 'status': false}
      end
    end

  end
end