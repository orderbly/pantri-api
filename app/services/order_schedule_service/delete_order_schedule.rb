module OrderScheduleService
  class DeleteOrderSchedule

    def initialize(id, current_user)
      @id = id
      @current_user = current_user
    end

    def call
      order_schedule = OrderSchedule.find(@id)
      order = Order.find(order_schedule.order.id)
      order.update(is_recurring: false)
      order_schedule.update(active: false, next_date: nil, value: "Cancelled", end_date: Date.today)
      CommentsService::CreateComment.new(@current_user, "#{@current_user.first_name} #{@current_user.last_name} cancelled recurring orders for this order.", order).call
      return {'obj': order, 'status': true}
    end

  end
end