class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :update, :destroy, :update_default_address]
  before_action :authorize_access_request!

  # GET /companies
  def index
    first_company = Company.find_by(id: 75).present? ? Company.find_by(id: 75) : []
    current_company = Company.find(current_user.current_company)
    companies = Company.where(is_active: true, is_private: false).where.not(id: current_company.id)
    private_companies = current_company.private_suppliers
    if current_company.supplier? 
      all_companies = [first_company] + companies + private_companies + [current_company]
      render json: all_companies.uniq.flatten
    else
      all_companies = [first_company] + companies + private_companies
      render json: all_companies.uniq.flatten
    end
  end

  def all_areas_by_postal_code_or_name
    existing_delivery_zone_areas = DeliveryZoneArea.where(company_id: @company.id).pluck(:name)
    Area.all.where('name=? OR street_code=?', params[:search_query], params[:search_query])

    render json: area_by_code + area_by_name
  end

  def all_areas
    render json: Area.all.where('name ~* ?', params[:search_query])
  end

  def all_provinces
    render json: Area.all.pluck(:province).uniq
  end

  def all_cities
    render json: Area.where(province: params[:province]).pluck(:city).uniq
  end

  def all_suburbs
    render json: Area.where('city ~* ?', params[:city])
  end

  def area_by_province
    render json: Area.where('province ~* ?', params[:province])
  end

  def find_company_by_title
    company = Company.find_by(uuid: params[:company_uuid])
    @companies = Company.all.where("title ~* ?", params[:query]).where.not(uuid: params[:company_uuid],is_custom_client: true).map{|c| { id: c.id, title: c.title, image: c.image, email: c.email, contact: c.contact, addresses: c.addresses, type: 'Orderbly Customer' } }
    @custom_clients = Company.all.where("title ~* ?", params[:query]).where(supplier_id: company.id, is_custom_client: true).map{|c| { id: c.id, email: c.email, image: c.image, contact: c.contact, title: c.title, addresses: c.addresses, type: 'Custom Customer' } }
    
    render json: [@companies,  @custom_clients].flatten
  end

  def slim_company_list
    @companies = Company.all.where.not(id: params[:company_id]).where(is_private: false).where.not(is_custom_client: true).pluck(:uuid, :title)
    @custom_companies = Company.where(is_custom_client: true, supplier_id: params[:company_id]).pluck(:uuid, :title)
    @custom_companies.each {|c| c[1] = c[1] + " (Custom customer)"}
    company_list = @companies +  @custom_companies

    render json: company_list.map {|c| {uuid: c[0], title: c[1]} }
  end

  def fetch_minimum_order_value
    begin
      company = Company.find(params[:supplier_id])
      render json: {minimum_order_value: company.minimum_order_value, title: company.title}
    rescue
      render json: {}, status: :unprocessable_entity
    end
  end

  def add_emails
    new_email = CompanyService::AddCompanyEmail.new(company_id: params[:company_id], email: params[:email]).call
    
    render json: {status: new_email[:status], message: new_email[:message], obj: Company.find(params[:company_id])}
  end

  def delete_email
    new_email = CompanyService::DeleteCompanyEmail.new(company_id: params[:company_id], email: params[:email]).call

    render json: { status: new_email[:status], message: new_email[:message], obj: Company.find(params[:company_id]) }
  end

  def purchase_dashboard
    @company_purchases_dashboard = CompanyService::PurchaseDashboardDetailService.new(company_id: current_user.current_company).call

    render json: @company_purchases_dashboard
  end

  def sales_dashboard 
    @company_sales_dashboard = CompanyService::SaleDashboardDetailService.new(company_id: current_user.current_company).call
    render json: @company_sales_dashboard
  end

  def sale_dashboard 
    company = Company.get_dashboard_details(current_user.current_company) 
  end

  # GET delivery_reconciliation/company_uuid
  def delivery_reconciliation
    company = Company.find_by(uuid: params[:company_uuid])
    if  current_user.companies.find(company.id).present?
      render json: company.default_delivery_reconciliation
    else
      render json: {message: "User does not have permission."}, status: :unprocessable_entity
    end
  end

  # POST update_delivery_reconciliation/company_uuid
  def update_delivery_reconciliation
    company = Company.find_by(uuid: params[:company_uuid])
    if  current_user.companies.find(company.id).present?
      company.update(default_delivery_reconciliation: params[:default_delivery_reconciliation])
      render json: company.default_delivery_reconciliation
    else
      render json: {message: "User does not have permission."}, status: :unprocessable_entity
    end
  end

  # GET delivery_reconciliation/company_uuid
  def delivery_signature
    company = Company.find_by(uuid: params[:company_uuid])
    if  current_user.companies.find(company.id).present?
      render json: company.is_delivery_signature_required
    else
      render json: {message: "User does not have permission."}, status: :unprocessable_entity
    end
  end

  # GET find_all_custom_companies/company_uuid
  def find_all_custom_companies
    company = Company.find_by(uuid: params[:company_uuid])
  if current_user.companies.find(company.id).present?
      render json: Company.where(is_custom_client: true, supplier_id: company.id)
    else
      render json: {message: "User does not have permission."}, status: :unprocessable_entity
    end
  end

  # GET all_customers/company_uuid
  def all_customers
    company = Company.find_by(uuid: params[:company_uuid])
  if current_user.companies.find(company.id).present?
      order_from_sales = Company.where(id: Order.where(supplier_id: company.id).pluck(:customer_id).uniq)
      custom_companies = Company.where(is_custom_client: true, supplier_id: company.id)
      render json: (order_from_sales + custom_companies).uniq
    else
      render json: {message: "User does not have permission."}, status: :unprocessable_entity
    end
  end

  # POST update_delivery_reconciliation/company_uuid
  def update_delivery_signature
    company = Company.find_by(uuid: params[:company_uuid])
    if current_user.companies.find(company.id).present?
      company.update(is_delivery_signature_required: params[:is_delivery_signature_required])
      render json: company.is_delivery_signature_required
    else
      render json: {message: "User does not have permission."}, status: :unprocessable_entity
    end
  end


  def update_default_address
    @company.update(default_address_id: params[:address_id])

    render json: @company
  end

  def delete_memberships 
    Member.find(params[:member_id]).delete
    @memberships = MembershipService::CompanyMemberships.new(current_user.current_company).call
    
    render json: @memberships
  end

  # GET /companies/1
  def show
    render json: @company
  end

  def company_memberships
    @memberships = MembershipService::CompanyMemberships.new(current_user.current_company).call

    render json: @memberships
  end

  def company_invites
    company = Company.find_by(uuid: params[:company_uuid])
    @invites = PendingInvite.all.where(company_id: company.id)

    render json: @invites
  end

  def delete_invite
    invite = PendingInvite.find(params[:invite_id])
    company_id = invite.company_id
    invite.delete

    render json:  PendingInvite.all.where(company_id: company_id)
  end

  def company_by_title
    @company = Company.where(title: params[:title].tr('_', ' ')).first

    render json: @company
  end

  def fetch_my_customer
    @company = Company.find_by(uuid: params[:company_uuid])
    @customer = Company.find_by(uuid: params[:customer_uuid])

    if (@customer.supplier_id.present? && @customer.supplier_id == @company.id) || Order.find_by(supplier_id: @company.id, customer_id: @customer.id).present?
      orders = Order.where(supplier_id: @company.id, customer_id: @customer.id)
      credit_notes = CreditNote.where(supplier_id: @company.id, customer_id: @customer.id)
      render json: {
        company: @customer,
        address_list: @customer.addresses,
        orders: OrdersService::Summaries::OrderIndexSummary.new(orders, credit_notes).call
      }
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  # PUT /update_company_logo
  def update_company_logo
    @company = Company.find_by(uuid: params[:company_uuid])
    if current_user.companies.find_by(id: @company.id).present? && params[:image].present?
      decoded_data = Base64.decode64(params[:image] .split(',')[1])
      signature = { io: StringIO.new(decoded_data), filename: 'company_logo' }
      @company.company_logo.attach(signature)
      @company.logo =  @company.company_logo.service.send(:object_for, @company.company_logo.key).public_url
      @company.save!
      render json: @company
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  # PUT /update_company_banner
  def update_company_banner
    @company = Company.find_by(uuid: params[:company_uuid])
    if current_user.companies.find_by(id: @company.id).present? && params[:image].present?
      decoded_data = Base64.decode64(params[:image] .split(',')[1])
      signature = { io: StringIO.new(decoded_data), filename: 'company_banner' }
      @company.company_image.attach(signature)
      @company.image =  @company.company_image.service.send(:object_for, @company.company_image.key).public_url
      @company.save!
      render json: @company
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  # POST / create_custom_customer
  def create_custom_customer
    @company = Company.new(company_params)
    @company.supplier_id = params[:supplier_id]
    @company.is_custom_client = true
    @company.supplier = false
    @company.image = "https://pantryapp.s3.eu-west-1.amazonaws.com/tjTk4WwaQ8gRpL2vwiDYWqar"

    if @company.save
      @address = Address.create(
        address_one: params[:address_one],
        suburb: params[:suburb],
        province: params[:province],
        country: params[:country],
        city: params[:city],
        postal_code: params[:postal_code],
        latitude: params[:latitude],
        longitude: params[:longitude],
        company_id: @company.id
      )
      @company.update(default_address_id: @address.id)
      render json: @company
    end
  end


  # POST /companies
  def create
    company_params_no_image = {}
    comp_params = ['title', 'supplier', 'email', 'contact', 'website', 'description', 'supplier', 'vat_number']
    comp_params.each do |p|
      company_params_no_image[p] = params[:company][p.to_sym]
    end
    @company = Company.new(company_params_no_image)
    if company_params[:image]
      decoded_data = Base64.decode64(company_params[:image] .split(',')[1])
      signature = { io: StringIO.new(decoded_data), filename: company_params[:title] }
      @company.company_image.attach(signature)
      @company.image =  @company.company_image.service.send(:object_for, @company.company_image.key).public_url
    else
      @company.image = "https://pantryapp.s3.eu-west-1.amazonaws.com/tjTk4WwaQ8gRpL2vwiDYWqar"
    end
    @company.email_list << current_user.email
    if @company.save
      if company_params[:addresses_attributes]
        address = Address.new(company_params[:addresses_attributes])
        address.company_id = @company.id
        address.save
        @company.update(default_address_id: address.id)
      end
      member = Member.create(user_id: current_user.id, role_id: 1, company_id: @company.id)
      @company.save!
      Company.create_tax(@company.id)
      user = User.find(current_user.id).update(current_company: member.company_id)
      render json: @company, status: :created, location: @company
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  def current_company
    user = current_user
    unless user.current_company
      user.current_company = user.members.first.company_id
      user.save
    end
    render json: Company.find(user.current_company)
  end

  # PATCH/PUT /companies/1
  def update
    if @company.update(company_params.except(:image))
      @company.update(email_list: params["company"]["email_list"]) if params["company"]["email_list"].class == Array
      if company_params[:image]
        decoded_data = Base64.decode64(company_params[:image] .split(',')[1])
        signature = { io: StringIO.new(decoded_data), filename: company_params[:title] }
        @company.company_image.attach(signature)
        @company.image =  @company.company_image.service.send(:object_for, @company.company_image.key).public_url
      end
      render json: @company, status: :created
    else
      render json: @company.errors, status: :unprocessable_entity
    end
  end

  # DELETE /companies/1
  def destroy
    @company.destroy
  end

  private

    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def company_params
      params
      .require(:company)
      .permit(:title, :address_one, :vat_number, :contact_person, :email_list, :address, :address_id, :email, :member_id, :supplier, :id, :country,
        :city, :postal_code, :contact, :website, :description, :image, :company_image, :colour, :categories, :category, :default_account_limit, :vat_number,
        :is_vat_registered, :trading_as, :minimum_order_value,
        user_attributes: [:email, :username, :first_name, :last_name, :contact_number ],
        :addresses_attributes => [:id, :address_one, :suburb, :city, :province, :country, :postal_code, :company_id, :latitude, :longitude]
      )
    end
end
