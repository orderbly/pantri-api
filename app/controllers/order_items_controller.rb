class OrderItemsController < ApplicationController
  before_action :set_order_item, only: [:show, :update, :destroy]
  before_action :authorize_access_request!

  # GET /order_items
  def index
    @order_items = OrderItem.all

    render json: @order_items
  end

  # GET /order_items/1
  def show
    render json: @order_item
  end

  # POST /order_items
  def create
    @order_item = OrderItem.new(order_item_params)

    if @order_item.save
      render json: @order_item, status: :created, location: @order_item
    else
      render json: @order_item.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /order_items/1
  def update
    if @order_item.update(order_item_params)
      render json: @order_item
    else
      render json: @order_item.errors, status: :unprocessable_entity
    end
  end

  # DELETE /order_items/1
  def destroy
    order = @order_item.order
    @order_item.destroy
    order_balance = order.order_items.map{|oi| oi.gross_price * oi.quantity}.sum
    order.update(balance: order_balance, value: order_balance)
    order_breakdown = OrdersService::OrderBreakdown.new(order.id).call
    render json: order_breakdown
  end

  def current_user
    begin
      current_user ||= User.find(payload['user_id'])
    rescue
      return false
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_item
      @order_item = OrderItem.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_item_params
      params.require(:order_item).permit(:product_id, :product_variant_id, :quantity, :order_id, :gross_price, :net_price, :vat_price, :company_id)
    end
end
