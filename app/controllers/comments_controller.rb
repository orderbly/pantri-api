class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :update, :destroy]
  before_action :authorize_access_request!

  # GET /comments
  def index
    @comments = Comment.all

    render json: @comments
  end

  # GET /comments/1
  def show
    render json: @comment
  end

  # POST /comments
  def create
    @comment = Comment.new(comment_params)
    @comment.user_id = current_user.id
    @comment.company_id = current_user.current_company
    @comment.company_image = Company.find(current_user.current_company).image

    if @comment.save!
      @comment_breakdown = { 
        id: @comment.id,
        company_image: @comment.company_image,
        comment_type: @comment.comment_type,
        order_id: @comment.order_id,
        order_number: @comment.order.order_number,
        commented_at: @comment.created_at,
        message: @comment.message,
        company: @comment.company.title,
        company_id: @comment.company.id,
        user_name: @comment.user.first_name + " " + @comment.user.last_name,
        user_email: @comment.user.email,
        user_id: @comment.user.id,
        user: {
          avatar: @comment.user.avatar,
          id: @comment.user.id,
          username: @comment.user.first_name + " " + @comment.user.last_name,
          first_name: @comment.user.first_name ,
          email: @comment.user.email,
          last_name: @comment.user.last_name,
          default_colour: @comment.user.default_colour
        }
      }
      Pusher.trigger('comments', "invoice_#{@comment.order.id}", @comment_breakdown)
      NewCommentJob.perform_later(@comment_breakdown)
      render json: @comment_breakdown, status: :created, location: @comment
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /comments/1
  def update
    if @comment.update(comment_params)
      render json: @comment
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /comments/1
  def destroy
    @comment.destroy
  end

  def current_user
    begin
      current_user ||= User.find(payload['user_id'])
    rescue
      return false
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def comment_params
      params.require(:comment).permit(:date, :order_id, :company_id, :user_id, :message)
    end
end
