class CustomClientsController < ApplicationController
  before_action :set_custom_client, only: [:show, :update, :destroy]
  before_action :authorize_access_request!

  def company_custom_clients
    @custom_clients = CustomClient.all.where(supplier_id: params[:supplier_id])

    render json: @custom_clients
  end


  # POST /custom_clients
  def create
    @custom_client = CustomClient.new(custom_client_params)
    if @custom_client.save!
      Address.create(
        address_one: params[:address_one],
        suburb: params[:suburb],
        province: params[:province],
        country: params[:country],
        city: params[:city],
        postal_code: params[:postal_code],
        latitude: params[:latitude],
        longitude: params[:longitude],
        custom_client_id: @custom_client.id
      )
      @custom_clients = CustomClient.all.where(supplier_id: custom_client_params[:supplier_id])
      render json: @custom_clients, status: :created
    else
      render json: @custom_client.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /custom_clients/1
  def update
    if @custom_client.update(custom_client_params)
      render json: @custom_client
    else
      render json: @custom_client.errors, status: :unprocessable_entity
    end
  end

  # DELETE /custom_clients/1
  def destroy
    @custom_client.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_custom_client
      @custom_client = CustomClient.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def custom_client_params
      params.require(:custom_client).permit(:title, :supplier_id, :address_id, :address_one, :suburb, :province, :country, :city, :postal_code, :latitude, :longitude, :current_company, :email, :contact)
    end
end
