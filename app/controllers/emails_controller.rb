class EmailsController < ApplicationController
  before_action :authorize_access_request!
  before_action :set_uuid_order, only: [:send_order_email, :send_delivery_email]

  # emails/send_order_email/order_uuid
  def send_order_email
    if current_user.companies.where(id: @order_from_uuid.supplier_id).present?
      OrderAcceptedJob.perform_later(@order_from_uuid.id)
      Comment.create(
        order_id: @order_from_uuid.id,
        comment_type: 1,
        date: Date.new,
        message: "#{current_user.first_name} #{current_user.last_name} sent the order to #{@order_from_uuid.customer.email_list.join("', '")}",
        user_id: current_user.id,
        company_id: current_user.current_company,
        company_image:  Company.find(current_user.current_company).image
      )
      render json: {status: true}
    else
      render json: {error: "User does not have access."}, status: :unprocessable_entity
    end
  end
  
  # emails/send_order_email/order_uuid
  def send_delivery_email
    if current_user.companies.where(id: @order_from_uuid.supplier_id).present?
      OrderDeliveredJob.perform_later(@order_from_uuid.id)
      Comment.create(
        order_id: @order_from_uuid.id,
        comment_type: 1,
        date: Date.new,
        message: "#{current_user.first_name} #{current_user.last_name} sent the delivery confirmation to #{@order_from_uuid.customer.email_list.join("', '")}",
        user_id: current_user.id,
        company_id: current_user.current_company,
        company_image:  Company.find(current_user.current_company).image
      )
      render json: {status: true}
    else
      render json: {error: "User does not have access."}, status: :unprocessable_entity
    end
  end

  def set_uuid_order
    @order_from_uuid = Order.find_by(uuid: params[:order_uuid])
    if OrdersService::Permissions::OrderPermission.new(current_user, @order_from_uuid.id).call
      true
    else
      no_permission
    end
  end



end