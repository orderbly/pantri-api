class DeliveryZonesController < ApplicationController
  before_action :authorize_access_request!
  before_action :set_delivery_zone, only: [:show, :update, :destroy]
  before_action :set_company, only: [:company_delivery_zones, :company_delivery_fees, :all_available_suburbs, :all_available_areas]

  # GET all_available_suburbs/:company_uuid/:city
  def all_available_suburbs
    existing_delivery_zone_areas = DeliveryZoneArea.where(company_id: @company.id).pluck(:name)
    render json: Area.where(city: params[:city]).where.not(name: [existing_delivery_zone_areas])
  end
  
  
  # GET all_available_areas/:company_uuid/:search_query
  def all_available_areas
    existing_delivery_zone_areas = DeliveryZoneArea.where(company_id: @company.id).pluck(:name)
    area_by_name = Area.all.where('name ~* ?', params[:search_query]).where.not(name: [existing_delivery_zone_areas]) 
    area_by_code = Area.all.where('street_code ~* ?', params[:search_query]).where.not(name: [existing_delivery_zone_areas]) 
    render json: area_by_code + area_by_name
  end

  # GET delivery_fee/:address_id/:quantity/:price/:supplier_id
  def delivery_fee
    begin
      delivery_fee = DeliveryService::Calculations::DeliveryZoneFeeValue.new(
        delivery_address_id: params[:address_id], 
        quantity: params[:quantity],
        price: params[:price],
        supplier_id: params[:supplier_id]
      ).call
      render json: {delivery_fee: delivery_fee}
    rescue
      render json: {delivery_fee: 0}
    end
  end

  # GET supplier_delivery_days/:delivery_address_id/:supplier_id/
  def supplier_delivery_days
    begin
      render json: DeliveryService::Calculations::DeliveryDisabledDates.new(params[:delivery_address_id], params[:supplier_id]).call
    rescue
      render json: []
    end
  end

  # POST add_areas_to_delivery_zone/:delivery_zone_uuid
  def add_areas_to_delivery_zone
    @delivery_zone = DeliveryZone.find_by(uuid: params[:delivery_zone_uuid])
    add_deliveries = DeliveryService::NewDeliveryAreas.new(params[:delivery_areas], @delivery_zone).call

    if add_deliveries
      @delivery_zone = DeliveryZone.find_by(uuid: params[:delivery_zone_uuid])
      payload = { 
        delivery_zone: @delivery_zone,
        delivery_zone_fees: @delivery_zone.delivery_zone_fees,
        delivery_zone_areas: @delivery_zone.delivery_zone_areas
      }
      render json: payload
    end
  end

  # POST add_delivery_zone_fees/:delivery_zone_uuid
  def add_delivery_zone_fees
    @delivery_zone = DeliveryZone.find_by(uuid: params[:delivery_zone_uuid])
    new_fee = {value_min: params[:value_min], value_max: params[:value_max], price: params[:price], name: params[:name]}
    add_delivery_fees = DeliveryService::NewDeliveryZoneFees.new(new_fee, @delivery_zone).call
    if add_delivery_fees.status
      @delivery_zone = DeliveryZone.find_by(uuid: params[:delivery_zone_uuid])
      payload = { 
        delivery_zone: @delivery_zone,
        delivery_zone_fees: @delivery_zone.delivery_zone_fees,
        delivery_zone_areas: @delivery_zone.delivery_zone_areas
      }
      render json: payload
    else
      render json: add_delivery_fees, status: :unprocessable_entity
    end
  end

  # GET company_delivery_zones/:company_uuid
  def company_delivery_zones
    render json: DeliveryZone.all.where(company_id: @company.id)
  end

  # GET single_delivery_zone/:delivery_zone_uuid
  def single_delivery_zone
    @delivery_zone = DeliveryZone.find_by(uuid: params[:delivery_zone_uuid])
    payload = { 
      delivery_zone: @delivery_zone,
      delivery_zone_fees: @delivery_zone.delivery_zone_fees,
      delivery_zone_areas: @delivery_zone.delivery_zone_areas
    }
    render json: payload
  end

  # POST /delivery_zones
  def create
    if current_user.companies.find_by(id: delivery_zone_params[:company_id])
      @delivery_zone = DeliveryZone.new(delivery_zone_params.except(:cut_off_day, :fee_based_on))
      @delivery_zone.cut_off_day = params[:delivery_zone][:cut_off_day].to_i
      @delivery_zone.fee_based_on = params[:delivery_zone][:fee_based_on].to_i
      @delivery_zone.delivery_days = params[:delivery_zone][:delivery_days]
      if @delivery_zone.save
        render json: @delivery_zone, status: :created, location: @delivery_zone
      else
        render json: @delivery_zone.errors, status: :unprocessable_entity
      end
    else
      render json: {}, status: :unprocessable_entity
    end
  end


  #PATCH update_delivery_zone_fees/:delivery_zone_fee_id
  def update_delivery_zone_fees
    @delivery_zone_fee = DeliveryZoneFee.find(params[:delivery_zone_fee_id])
    if @delivery_zone_fee.present? && current_user.companies.find_by(id: @delivery_zone_fee.delivery_zone.company_id).present?
      are_brackets_valid = DeliveryService::ValidateFeeBrackets.new(params[:value_min], params[:value_max], @delivery_zone_fee.delivery_zone, @delivery_zone_fee).call
      if are_brackets_valid
        @delivery_zone_fee.value_min = params[:value_min]
        @delivery_zone_fee.value_max = params[:value_max]
        @delivery_zone_fee.price = params[:price]
        @delivery_zone_fee.name = params[:name]
        if @delivery_zone_fee.save
          payload = { 
          delivery_zone: @delivery_zone_fee.delivery_zone,
          delivery_zone_fees: @delivery_zone_fee.delivery_zone.delivery_zone_fees,
          delivery_zone_areas: @delivery_zone_fee.delivery_zone.delivery_zone_areas
        }
        render json: {status: true, obj: payload}
        else 
          render json: {status: false, message: "Please ensure that your fee bracket values do not overlap."}
        end
      else
        render json: {}, status: :unprocessable_entity
      end
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  
  def delete_delivery_zone_fee
    @delivery_zone_fee = DeliveryZoneFee.find_by(id: params[:delivery_zone_fee_id])

    if @delivery_zone_fee.present? && current_user.companies.find_by(id: @delivery_zone_fee.delivery_zone.company_id).present?
      @delivery_zone = @delivery_zone_fee.delivery_zone
      @delivery_zone_fee.delete
      payload = { 
        delivery_zone: @delivery_zone,
        delivery_zone_fees: @delivery_zone.delivery_zone_fees,
        delivery_zone_areas: @delivery_zone.delivery_zone_areas
      }
      render json: payload
    else 
    render json: {}, status: :unprocessable_entity
   end
  end

  def delete_delivery_zone_area
    @delete_delivery_zone_area = DeliveryZoneArea.find_by(id: params[:delivery_zone_area_id])

    if @delete_delivery_zone_area.present? && current_user.companies.find_by(id: @delete_delivery_zone_area.delivery_zone.company_id).present?
      @delivery_zone = @delete_delivery_zone_area.delivery_zone
      @delete_delivery_zone_area.delete
      payload = { 
        delivery_zone: @delivery_zone,
        delivery_zone_fees: @delivery_zone.delivery_zone_fees,
        delivery_zone_areas: @delivery_zone.delivery_zone_areas
      }
      render json: payload
    else 
    render json: {}, status: :unprocessable_entity
   end
  end
  
  def delete_delivery_zone
    @delivery_zone = DeliveryZone.find_by(id: params[:delivery_zone_id])
    if @delivery_zone.present? && current_user.companies.find_by(id: @delivery_zone.company_id).present?
      DeliveryZoneFee.where(delivery_zone_id: @delivery_zone.id).delete_all
      DeliveryZoneArea.where(delivery_zone_id: @delivery_zone.id).delete_all
      @delivery_zone.delete
      return true
    end
    render json: {}, status: :unprocessable_entity
  end

  # PATCH/PUT /delivery_zones/1
  def update
    if @delivery_zone.update(delivery_zone_params.except(:cut_off_day, :fee_based_on))
      @delivery_zone.cut_off_day = params[:delivery_zone][:cut_off_day].to_i
      @delivery_zone.fee_based_on = params[:delivery_zone][:fee_based_on].to_i
      @delivery_zone.delivery_days = params[:delivery_zone][:delivery_days]
      @delivery_zone.save
      payload = { 
        delivery_zone: @delivery_zone,
        delivery_zone_fees: @delivery_zone.delivery_zone_fees,
        delivery_zone_areas: @delivery_zone.delivery_zone_areas
      }
      render json: payload
    else
      render json: @delivery_zone.errors, status: :unprocessable_entity
    end
  end

  def current_user
    begin
      current_user ||= User.find(payload['user_id'])
    rescue
      return false
    end
  end

  def set_company
    if current_user.companies.where(uuid: params[:company_uuid]).present?
      @company = Company.find_by(uuid: params[:company_uuid]) 
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_delivery_zone
      @delivery_zone = DeliveryZone.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def delivery_zone_params
      params.require(:delivery_zone).permit(:name, :company_id, :delivery_days, :cut_off_time, :cut_off_day, :fee_based_on, :delivery_days)
    end
end
