class DeliveryFeeBracketsController < ApplicationController
  before_action :authorize_access_request!
  before_action :set_delivery_fee_bracket, only: [:show, :update, :destroy]
  before_action :set_company, only: [:company_delivery_brackets]


  # GET /company_delivery_brackets/:company_uuid
  def company_delivery_brackets
    render json: @company.delivery_fee_brackets.order(:min_distance)
  end

  # GET /get_delivery_fee/:customer_address_id/supplier_id
  def calculate_delivery_fee
    delivery_fee = DeliveryFeeBracketService::CalculateDeliveryFee.new(params[:customer_address_id], params[:supplier_id]).call
    render json: delivery_fee
  end

  # POST /delivery_fee_brackets
  def create
    create_delivery_fee_bracket = DeliveryFeeBracketService::CreateDeliveryFeeBracket.new(params[:delivery_fee_bracket]).call
    
    if create_delivery_fee_bracket.status
      render json: Company.find(params[:delivery_fee_bracket][:company_id]).delivery_fee_brackets.order(:min_distance), status: :created
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  # DELETE /delivery_fee_brackets/1
  def destroy
    if current_user.companies.where(id: @delivery_fee_bracket.company_id).present? 
      company = Company.find(@delivery_fee_bracket.company_id)
      @delivery_fee_bracket.destroy
      render json: company.delivery_fee_brackets.order(:min_distance)
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_delivery_fee_bracket
      @delivery_fee_bracket = DeliveryFeeBracket.find(params[:id])
    end

    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    def set_company
      if current_user.companies.where(uuid: params[:company_uuid]).present?
        @company = Company.find_by(uuid: params[:company_uuid]) 
      else
        render json: {}, status: :unprocessable_entity
      end
    end

    # Only allow a trusted parameter "white list" through.
    def delivery_fee_bracket_params
      params.require(:delivery_fee_bracket).permit(:min_distance, :max_distance, :price, :company_id)
    end
end
