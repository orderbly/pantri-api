class ProductVariantsController < ApplicationController
  before_action :set_product_variant, only: [:show, :update, :destroy, :disable_product_variant]
  before_action :authorize_access_request!

  # GET /product_variants
  def index
    @product_variant_variants = ProductVariant.all

    render json: @product_variant_variants
  end

  # GET /product_variants/1
  def show
    render json: @product_variant
  end

  def variants_by_product
    @variants = ProductVariant.where(product_id: params[:id]).order(:id).reverse

    render json: @variants
  end

  # POST /product_variants
  def create
    @product_variant = ProductVariant.new(product_variant_params)

    if @product_variant.save
      render json: @product_variant, status: :created, location: @product_variant
    else
      render json: @product_variant.errors, status: :unprocessable_entity
    end
  end

  #PATCH/disable_product_variant/:id
  def disable_product_variant
    @product_variant.update(is_active: false, track_stock: true, available_stock: 0)
  end

  # PATCH/PUT /product_variants/1
  def update
    if @product_variant.update(product_variant_params)
      render json: @product_variant
    else
      render json: @product_variant.errors, status: :unprocessable_entity
      puts json: @product_variant.errors
    end
  end

  # DELETE /product_variants/1
  def destroy
    @product_variant.destroy
  end

  private
  
    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    def set_product_variant
      @product_variant = ProductVariant.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_variant_params
      params.require(:product_variant).permit(:id, :product_id, :description, :status, :available_stock, :stock, :track_stock, :gross_amount,
          :unit_amount, :tax_amount, :discount_percentage, :_destroy)
    end

end
