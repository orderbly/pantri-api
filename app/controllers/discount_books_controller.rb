class DiscountBooksController < ApplicationController
  before_action :set_discount_book, only: [:show, :update, :destroy, :list_all_companies]
  before_action :authorize_access_request!

  # GET /discount_books
  def index
    @discount_books = DiscountBook.all.where(supplier_id: current_user.current_company)

    render json: @discount_books
  end

  # GET /discount_books/1
  def show
    render json: @discount_book
  end

  def list_all_companies
    @all_companies = DiscountBookService::ListCompanies.new(@discount_book.supplier_id).call
    render json: @all_companies
  end

  # POST /discount_books
  def create
    validation = DiscountBookService::TitleValidation.new(company_id: discount_book_params[:supplier_id], title: discount_book_params[:title]).call
    if validation[:status]
      @discount_book = DiscountBook.new(discount_book_params)
    
      if @discount_book.save!
        render json: { status: true, data: @discount_book}, status: :created
      else
        render json: @discount_book.errors, status: :unprocessable_entity
      end
    else
      render json: validation
    end
  end


  # PATCH/PUT /discount_books/1
  def update
    if @discount_book.update(discount_book_params)
      render json: @discount_book
    else
      render json: @discount_book.errors, status: :unprocessable_entity
    end
  end

  # DELETE /discount_books/1
  def destroy
    if DiscountBookService::DiscountBookEmptyCheck.new(@discount_book).call
      @discount_book.destroy
      render json: {status: true}
    else
      render json: {status: false, message: "Cannot delete price book, as it still contains memberships."} 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_discount_book
      @discount_book = DiscountBook.find(params[:id])
    end

    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    # Only allow a trusted parameter "white list" through.
    def discount_book_params
      params.require(:discount_book).permit(:supplier_id, :customer_id, :description, :title)
    end
end
