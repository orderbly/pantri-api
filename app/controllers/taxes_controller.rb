class TaxesController < ApplicationController
  before_action :set_tax, only: [:show, :update, :destroy]
  before_action :authorize_access_request!

  # GET /taxes
  def index
    @taxes = Tax.all.where(company_id: current_user.current_company)
    render json: @taxes
  end

  # GET /taxes/1
  def show
    render json: @tax
  end

  # POST /taxes
  def create
    @tax = Tax.new(tax_params)
    @tax.company_id = current_user.current_company if Company.find_by(id: current_user.current_company)

    if @tax.save
      render json: @tax, status: :created, location: @tax
    else
      render json: @tax.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /taxes/1
  def update
    if @tax.update(tax_params)
      render json: @tax
    else
      render json: @tax.errors, status: :unprocessable_entity
      puts json: @tax.errors
    end
  end
  
  private
  
    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    def set_tax
      @tax = Tax.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tax_params
      params.require(:tax).permit(:id, :category, :description, :percentage, :company_id)
    end
end
