class ProductVariantDiscountsController < ApplicationController
  before_action :set_product_variant_discount, only: [:show, :update, :destroy]
  before_action :authorize_access_request!

  # GET /product_variant_discounts
  def index
    @product_variant_discounts = ProductVariantDiscount.all.where(discount_book_id: params[:id])

    render json: @product_variant_discounts
  end

  def discounts_by_discount_book
    @product_variant_discounts = ProductVariantDiscountService::DiscountsByDiscountBook.new(current_user.current_company, params[:discount_book_id]).call
    
    render json: @product_variant_discounts
  end

  # POST /product_variant_discounts
  def create
    pvd_result = ProductVariantDiscountService::SaveProductVariantDiscountsService.new(params[:discount_list]).call
    product_variant_discounts = ProductVariantDiscountService::DiscountsByDiscountBook.new(params[:supplier_id], params[:discount_book_id]).call
    render json: {
      all_added: pvd_result[:all_added],
      product_variant_discounts: product_variant_discounts
    }
  end

  private

    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_product_variant_discount
      @product_variant_discount = ProductVariantDiscount.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_variant_discount_params
      params.require(:product_variant_discount).permit(:discount_book_id, :product_variant_id, :discount_percentage)
    end
end
