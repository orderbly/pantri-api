class DiscountBookMembershipsController < ApplicationController
  before_action :set_discount_book_membership, only: [:show, :update, :destroy]
  before_action :authorize_access_request!

  def memberships_by_discount_book
    @discount_book_membership = DiscountBookService::BuildDiscountBookMembershipsService.new(params[:discount_book_id].to_i).call
    render json: @discount_book_membership
  end

  # POST /discount_book_memberships
  def create
    discount_book_create = DiscountBookService::DiscountBookMembershipsCreate.new(params[:customer_ids], params[:discount_book_id]).call
    render json: discount_book_create
  end

  # DELETE /discount_book_memberships/1
  def destroy
    @discount_book_membership.destroy
  end
  
  def current_user
    begin
      current_user ||= User.find(payload['user_id'])
    rescue
      return false
    end
  end
  
  private

  
    # Use callbacks to share common setup or constraints between actions.
    def set_discount_book_membership
      @discount_book_membership = DiscountBookMembership.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def discount_book_membership_params
      params.require(:discount_book_membership).permit(:id, :customer_id, :discount_book_id)
    end
end
