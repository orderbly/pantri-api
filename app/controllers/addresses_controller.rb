class AddressesController < ApplicationController
  before_action :set_address, only: [:show, :update, :destroy]
  before_action :authorize_access_request!

  # GET /addresses
  def index
    @addresses = Address.where(company_id: current_user.current_company)

    render json: @addresses
  end

  # Get /company_addresses/:company_uuid
  def comapny_addresses
    if current_user.companies.find_by(uuid: params[:company_uuid]).present?
      @addresses = Address.where(company_id: params[:company_uuid])
      render json: @addresses
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  # GET /addresses/1
  def show
    render json: @address
  end

  # POST /addresses
  def create
    @address = Address.new(address_params)
    if @address.save
      if @address.company.default_address == nil
        @address.company.update(default_address_id: @address.id)
      end
      render json: @address, status: :created, location: @address
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /addresses/1
  def update
    if @address.update(address_params)
      render json: @address
    else
      render json: @address.errors, status: :unprocessable_entity
    end
  end

  # DELETE /addresses/1
  def destroy
    @address.destroy
  end

  def current_user
    begin
      current_user ||= User.find(payload['user_id'])
    rescue
      return false
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address
      @address = Address.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def address_params
      params.require(:address).permit(:address_one, :suburb, :city, :province, :country, :postal_code, :company_id, :latitude, :longitude, :name, :additional_info)
    end
end
