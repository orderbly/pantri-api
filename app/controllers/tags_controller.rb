class TagsController < ApplicationController
  before_action :set_tag, only: [:show, :update, :destroy]
  before_action :authorize_access_request!

  # GET /tags
  def index
    @tags = Tag.all.where(company_id: current_user.current_company)
    render json: @tags
  end

  # GET /tags/1
  def show
    render json: @tag
  end

  # POST /tags
  def create
    @tag = Tag.new(tag_params)
    @tag.company_id = current_user.current_company if Company.find_by(id: current_user.current_company)

    if @tag.save
      render json: @tag, status: :created, location: @tag
    else
      render json: @tag.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /tags/1
  def update
    @tag = Tag.find(params[:tag][:id])
    if @tag.update(tag_params)
      render json: @tag
    else
      render json: @tag.errors, status: :unprocessable_entity
      puts json: @tag.errors
    end
  end

  # DELETE /tags/1
  def destroy
    @tag.destroy
  end

  private
    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end
    
    def set_tag
      @tag = Tag.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tag_params
      params.require(:tag).permit(:id, :description, :company_id)
    end

    def company
      # TODO: This needs to be passed in with headers
      current_user.companies.first
    end

end
