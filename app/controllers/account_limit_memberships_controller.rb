class AccountLimitMembershipsController < ApplicationController
  before_action :set_account_limit_membership, only: [:show, :update, :destroy]

  # GET /account_limit_memberships
  def index
    @account_limit_memberships = AccountLimitMembership.all

    render json: @account_limit_memberships
  end

  # GET /account_limit_memberships/1
  def show
    render json: @account_limit_membership
  end

  # POST /account_limit_memberships
  def create
    @account_limit_membership = AccountLimitMembership.new(account_limit_membership_params)

    if @account_limit_membership.save
      render json: @account_limit_membership, status: :created, location: @account_limit_membership
    else
      render json: @account_limit_membership.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /account_limit_memberships/1
  def update
    if @account_limit_membership.update(account_limit_membership_params)
      render json: @account_limit_membership
    else
      render json: @account_limit_membership.errors, status: :unprocessable_entity
    end
  end

  # DELETE /account_limit_memberships/1
  def destroy
    @account_limit_membership.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account_limit_membership
      @account_limit_membership = AccountLimitMembership.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def account_limit_membership_params
      params.require(:account_limit_membership).permit(:customer_id, :supplier_id, :account_limit)
    end
end
