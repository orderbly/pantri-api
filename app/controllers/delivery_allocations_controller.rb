class DeliveryAllocationsController < ApplicationController
  before_action :set_delivery_allocation, only: [:show, :update, :destroy]
  before_action :authorize_access_request!

  # GET /delivery_allocations
  def index
    @delivery_allocations = DeliveryAllocation.all

    render json: @delivery_allocations
  end

  # GET /delivery_allocations/1
  def show
    render json: @delivery_allocation
  end

  # POST /delivery_allocations
  def create
    delivery_allocation = DeliveryAllocationService::CreateDeliveryAllocation.new(users: params[:delivery_allocation][:users], orders: params[:delivery_allocation][:orders]).call

    if delivery_allocation[:type]
      if params[:delivery_allocation][:orders].length == 1
        delivery_allocation = OrdersService::OrderBreakdown.new(params[:delivery_allocation][:orders][0]).call
        render json: delivery_allocation, status: :created
      end
    else
      render json: delivery_allocation, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /delivery_allocations/1
  def update
    if @delivery_allocation.update(delivery_allocation_params)
      render json: @delivery_allocation
    else
      render json: @delivery_allocation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /delivery_allocations/1
  def destroy
    @delivery_allocation.destroy
  end

  def delete_delivery_allocation
    delivery_allocation = DeliveryAllocation.find_by(user_id: delivery_allocation_params[:user_id], order_id: delivery_allocation_params[:order_id] )
    delivery_allocation.delete if delivery_allocation

    order_breakdown = OrdersService::OrderBreakdown.new(delivery_allocation_params[:order_id]).call
    render json: order_breakdown
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_delivery_allocation
      @delivery_allocation = DeliveryAllocation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def delivery_allocation_params
      params.require(:delivery_allocation).permit(:users, :order, :orders, :order_id, :user, :user_id)
    end
end