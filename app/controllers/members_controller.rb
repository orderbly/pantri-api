class MembersController < ApplicationController
  before_action :authorize_access_request!

  def get_member
    member = Member.find_by(uuid: params[:uuid])
    company = member.company
    requested_user_membership = Member.find_by(user_id: current_user.id, company_id: company.id)
    if requested_user_membership.user_permissions["user_settings"]["can_edit_user"] == true 
      render json: member
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  def update_member
    member = Member.find_by(uuid: params[:uuid])
    company = member.company
    @user = current_user
    if Member.find_by(user_id: current_user.id, company_id: company.id).present?
      permissions = JSON.parse(params[:permissions].to_json)
      if MembershipService::ValidateMembershipPermissions.new(permissions).call
        member.update(user_permissions: params[:permissions], role_id: params[:role])
        render json: member
      else
        render json: {}, status: :unprocessable_entity
      end
    end
  end



  def current_user
    begin
      current_user ||= User.find(payload['user_id'])
    rescue
      return false
    end
  end

end