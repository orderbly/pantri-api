class StatementsController < ApplicationController
  before_action :authorize_access_request!
  before_action :authorise_user

  def customers_index_statement
    statements = StatementsService::CustomersIndexStatement.new(params[:supplier_uuid]).call
    render json: statements
  end

  def suppliers_index_statement
    statements = StatementsService::SuppliersIndexStatement.new(params[:customer_uuid]).call
    render json: statements
  end

  def trade_statement
    statement = StatementsService::TradeStatement.new( supplier_uuid: params[:supplier_uuid], customer_uuid: params[:customer_uuid], start_date: params[:start_date], end_date: params[:end_date] ).call
    render json: statement
  end

  def current_user
    begin
      current_user ||= User.find(payload['user_id'])
    rescue
      return false
    end
  end

  private

  def authorise_user
    current_user.companies.find_by(uuid: params[:supplier_uuid]).present? || current_user.companies.find_by(uuid: params[:customer_uuid]).present? ? true : no_permission
  end


end