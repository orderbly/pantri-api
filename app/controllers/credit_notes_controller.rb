class CreditNotesController < ApplicationController
  before_action :authorize_access_request!

  include Userable

  def current_credit_note
    credit_note = CreditNote.find(params[:id])
    credit_note_breakdown = CreditNoteService::Breakdowns::CreditNoteBreakdown.new(credit_note).call
    render json: credit_note_breakdown
  end

  def received_credit_notes
    @credit_notes = CreditNote.all.where(customer_id: current_user.current_company)
    breakdown = CreditNoteService::Breakdowns::CreditNoteIndexBreakdown.new(@credit_notes).call
    render json: breakdown
  end

  def balance
    render json: {balance: CreditNote.balance(params[:supplier_id], current_user.current_company)}
  end

  def account
    render json: {
      account: Order.account_balance(params[:supplier_id], current_user.current_company),
      account_limit: CompanyService::AccountLimit.new(supplier_id: params[:supplier_id], customer_id: current_user.current_company).call
    }
  end

  # POST /credit_notes
  def create
    credit_note_items = params[:credit_note][:order_items]
    credit_note = CreditNoteService::CreateCreditNote.new(credit_note_params, credit_note_items).call
    if credit_note[:status]
      render json: {type: 'success', message: credit_note[:obj]}, status: :created
    else
      render json: {type: 'error', message: ''}, status: :unprocessable_entity
    end
  end

  def general_credit_note
    credit_note =  CreditNoteService::GeneralCreditNote.new(params).call
    if credit_note[:status]
      credit_note[:obj].update(created_by: "#{current_user.first_name} #{current_user.last_name}")
      render json: {type: 'success', message: credit_note[:obj]}, status: :created
    else
      render json: {type: 'error', message: credit_note[:error]}, status: :unprocessable_entity
    end
  end

  private
    # Only allow a trusted parameter "white list" through.
    def credit_note_params
      params.require(:credit_note).permit(:order_id, :balance, :supplier_id, :customer_id, :order_items)
    end
end
