class OrdersController < ApplicationController
  before_action :authorize_access_request!
  before_action :set_order, only: [:show, :update, :destroy]
  before_action :set_uuid_order, only: [:order_breakdown, :add_item_to_order, :edit_order, :create_order_schedule, :claim_credit,
                                        :accept_order, :decline_order, :cancel_order, :update_order_delivery_status, :update_single_order_delivery_status,
                                        :receive_order, :update_order_received_values, :order_signature, :update_delivery_fee, :update_delivery_reconcilation,
                                        :order_outstanding_items, :update_order_delivery_signature ]
  
  rescue_from XeroError, with: :xero_error

  include Userable
  include Integrations::Xeroable

  def now_error(exceptions)
  end

  # Crud Actions

  def index
    @orders = Order.all.where(customer_id: current_user.current_company)
    render json: @orders
  end

  # GET /orders/1
  def show
    refresh_invoice(@order) if @order.accounting.key?('invoice')
    render json: @order
  end

  # POST /orders
  def create
    order = OrdersService::CreateOrder.new(order_params: order_params, current_user: current_user).call
    if order[:status]
      supplier = Company.find(order[:obj].supplier.id)
      customer = Company.find(order[:obj].customer.id)
      order_breakdown = OrdersService::OrderBreakdown.new(order[:obj].id).call
      NewOrderJob.perform_later(order[:obj].id)
      OrderReceivedJob.perform_later(order[:obj].id)
      Pusher.trigger('orders', "company_#{supplier.id}", {customer: {title: order_breakdown[:customer][:title]}, order_number: order_breakdown[:order_number]})
      render json: order_breakdown, status: :created
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  # POST /orders
  def create_custom_order
    order = OrdersService::CreateOrder.new(order_params: order_params, current_user: current_user).call
    if order[:status]
      order = Order.find(order[:obj].id)
      order.update(is_custom_order: true)
      order_breakdown = OrdersService::OrderBreakdown.new(order.id).call
      render json: order_breakdown, status: :created
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /orders/1
  def update
    user_permissions = current_user.companies.where(id: [@order.customer_id, @order.supplier_id]).present?
    if user_permissions && @order.status != "delivered"
      if @order.update(order_params)
        render json: OrdersService::OrderBreakdown.new(@order.id).call
      else
        render json: @order.errors, status: :unprocessable_entity
      end
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  # ==================== UUID Compatible ======================

  #uuid compatible
  def add_item_to_order
    added_item = OrdersService::Actions::AddItemToOrder.new(params, @order_from_uuid).call
    if added_item[:status]
      order_breakdown = OrdersService::OrderBreakdown.new(added_item[:obj][:id]).call
      render json: order_breakdown
    else
      render json: added_item, status: :unprocessable_entity
    end
  end


    #uuid compatible
    def edit_order
      updated_orders = OrdersService::UpdateOrder.new(@order_from_uuid.id, params["_json"], current_user).call
      order_breakdown = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
      if updated_orders[:status]
        render json: {status: true, obj: order_breakdown}
      else
        render json: {status: false, obj: order_breakdown }
      end
    end

  
  #uuid compatible
  def create_order_schedule
    order_schedule = OrderScheduleService::CreateOrderSchedule.new(params[:frequency]).create
    if order_schedule[:status]
      @order_from_uuid.update(is_recurring: true)
      @order = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
      render json: @order
    else
      render json: {error: "Order schedule could not be created"}, status: :unprocessable_entity
    end
  end

  #uuid compatible
  def update_delivery_order
    begin
      if current_user.companies.where(uuid: params[:company_uuid]).present?
        params[:orders].each do |o| 
          @order = Order.find_by(uuid: o[:order_uuid])
          if current_user.companies.find_by(id: @order.supplier.id).present?
            @order.update(delivery_number: o[:delivery_number])
          end
        end
      render json: FulfilmentService::RegularDeliveryRoute.new(User.find(params[:user_id]), @order.supplier).call
      else
        render json: {error: "Order schedule could not be created"}, status: :unprocessable_entity
      end
    rescue
      render json: {}, status: :unprocessable_entity
    end
  end

  # uuid compatible
  def accept_order
    accept_order = OrdersService::Actions::AcceptOrder.new(@order_from_uuid.id, current_user.id).call
    if accept_order[:status]
      order_breakdown = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
      Pusher.trigger('orders', "company_#{accept_order[:data].customer_id}_accepted_order_updates",  {customer: {title: order_breakdown[:customer][:title]}, order_number: order_breakdown[:order_number]})
      OrderAcceptedJob.perform_later(order_breakdown[:id])
      render json: order_breakdown
    else
      render json: {type: "error", message: "This User does not not have permission"}, status: :unprocessable_entity
    end
  end

  # uuid compatible
  def decline_order
    decline_order = OrdersService::Actions::DeclineOrder.new(@order_from_uuid.id, current_user.id).call
    if decline_order[:status]
      order_breakdown = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
      Pusher.trigger('orders', "company_#{decline_order[:data].customer_id}_declined_order_updates", {customer: {title: order_breakdown[:customer][:title]}, order_number: order_breakdown[:order_number]})
      OrderDeclinedJob.perform_later(order_breakdown[:id])
      render json: order_breakdown
    else
      render json: {type: "error", message: "This order cannot be accepted. Please contact support"}
    end
  end

  # uuid compatible
  def cancel_order
    cancel_order = OrdersService::Actions::CancelOrder.new(@order_from_uuid.id, current_user).call
    if cancel_order[:status]
      order_breakdown = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
      OrderCancelledJob.perform_later(order_breakdown[:id])
      Pusher.trigger('orders', "company_#{cancel_order[:data].supplier.id}_cancelled_order_updates", {customer: {title: order_breakdown[:customer][:title]}, order_number: order_breakdown[:order_number]})
      render json: order_breakdown
    else 
      render json: {type: "error", message: "This order cannot be cancelled. Please contact support for further information"}
    end
  end

  def order_outstanding_items
    if current_user.companies.where(id: @order_from_uuid.customer_id).present?
      new_order = OrdersService::Actions::OrderOutstandingItems.new(order_id:  @order_from_uuid.id, current_user: current_user).call
      if new_order[:status] == true
        Order.find(new_order[:obj][:id]).update(is_delivery_buyer_actionable: false)
        render json: {
          status: new_order[:status],
          message: new_order[:message],
          obj: OrdersService::OrderBreakdown.new(new_order[:obj][:id]).call
        }
      else 
        render json: {
          status: new_order[:status],
          message: new_order[:message],
          obj: {}
        }
      end
    else
      render json: {error: "User does not have access."}, status: :unprocessable_entity
    end
  end

  def claim_credit
    if current_user.companies.where(id: @order_from_uuid.customer_id).present?
      new_credit_note = OrdersService::Actions::ClaimCredit.new(order_id:  @order_from_uuid.id, current_user: current_user).call
      if new_credit_note[:status] == true
        Order.find(new_credit_note[:obj][:id]).update(is_delivery_buyer_actionable: false)
        render json: {
          status: new_credit_note[:status],
          message: new_credit_note[:message],
          obj: OrdersService::OrderBreakdown.new(new_credit_note[:obj][:id]).call
        }
      else 
        render json: {
          status: new_order[:status],
          message: new_order[:message],
          obj: {}
        }
      end
    else
      render json: {error: "User does not have access."}, status: :unprocessable_entity
    end
  end

  def order_items_breakdown
    @orders = OrdersService::Summaries::Xero::OrderBreakdown.new(params[:order_uuids]).call
    render json: @orders
  end

  def order_signature
    OrderSignatureJob.perform_now(@order_from_uuid, params[:signature].split(',')[1]) if params[:signature] != nil
    @order_from_uuid.update(delivery_status: 4)
    @order_from_uuid.received_by = params[:order][:received_by]
    @order_from_uuid.delivery_user_id = current_user.id
    @order_from_uuid.save!
    if params[:delivery_note].present?
      Comment.create(
        order_id: @order_from_uuid.id,
        date: Date.new,
        comment_type: 2,
        message: params[:delivery_note],
        user_id: current_user.id,
        company_id: @order_from_uuid.supplier.id,
        company_image: @order_from_uuid.supplier.image
      )
      render json: true
    end
  end

  # POST update_delivery_reconcilation/:order_uuid
  def update_delivery_reconcilation
    @order_from_uuid.update(order_reconciliation: !@order_from_uuid.order_reconciliation)

    order_breakdown = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
    render json: order_breakdown, status: :created
  end

  # POST update_order_delivery_signature/:order_uuid
  def update_order_delivery_signature
    if current_user.companies.where(id: @order_from_uuid.supplier_id).present?
      @order_from_uuid.update(is_delivery_signature_required: !@order_from_uuid.is_delivery_signature_required)
      order_breakdown = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
      render json: order_breakdown, status: :created
    else
      render json: {error: "User does not have access."}, status: :unprocessable_entity
    end
  end

  # POST /update_delivery_fee/:order_uuid
  def update_delivery_fee
    delivery_fee = order_params[:delivery_fee].to_f
    delivery_fee_breakdown = @order_from_uuid.calculate_delivery_fee(delivery_fee)
    order_balance =  @order_from_uuid.order_items.map{|oi| oi.gross_price * oi.quantity}.sum

    @order_from_uuid.update(
      balance: order_balance + delivery_fee,
      value: order_balance + delivery_fee,
      delivery_fee: delivery_fee_breakdown
    )
    order_breakdown = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
    render json: order_breakdown, status: :created
  end

  #uuid compatible
  def receive_order
    order_breakdown = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
    render json: order_breakdown
  end

  #uuid compatible
  def order_with_credit_note
    new_order = OrdersService::CreateOrderWithCreditNote.new(order_params: order_params, current_user: current_user).call
    if new_order[:status]
      render json: {type: true, message: new_order[:reason]}, status: :created
    else
      render json: {type: false, message: new_order[:reason]}, status: :unprocessable_entity
    end
  end

  # uuid compatible
  def update_single_order_delivery_status
    @order_from_uuid.delivery_status = order_params["delivery_status"].to_i
    if @order_from_uuid.save
      order_bot = User.find_by(username: "OrderBot")
      comment_user = order_bot ? order_bot : current_user
      if Company.find_by(id: comment_user.current_company)
        Comment.create(
          order_id: @order_from_uuid.id,
          date: Date.new,
          comment_type: 1,
          message: "#{current_user.first_name} #{current_user.last_name} marked this as #{@order_from_uuid.delivery_status.humanize}",
          user_id: comment_user.id,
          company_id: comment_user.current_company,
          company_image:  Company.find(comment_user.current_company).image
        )
      end
      order_breakdown = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
      render json: order_breakdown
    else 
      render json: bad_requests
    end
  end

  # uuid compatible
  def update_order_delivery_status
    @order_from_uuid.delivery_status = order_params["delivery_status"].to_i
    if @order_from_uuid.save
      # render json: OrdersService::UserDeliverableOrders.new(@order_from_uuid.supplier_id, current_user).call
      render json: @order_from_uuid
    else 
      render json: bad_requests
    end
  end

  def update_order_received_values
    balance = 0
    if params[:order][:order_items].present?
      params[:order][:order_items].each do |oi|
        order_item = @order_from_uuid.order_items.find(oi[:id])
        order_item.update(received_amount: oi[:received])
        if order_item.received_amount < order_item.quantity
          balance += (order_item.quantity - order_item.received_amount) * order_item.gross_price
          @order_from_uuid.update(is_delivery_buyer_actionable: true)
        end
        @order_from_uuid.update(reorder_balance: balance)
      end
      @order_from_uuid.update(delivery_status: 'delivered')
      OrderDeliveredJob.perform_later(@order_from_uuid.id)
    end

    render json: @order_from_uuid
  end

  def order_breakdown
    if OrdersService::Permissions::OrderPermission.new(current_user, @order_from_uuid.id).call
      @orders = OrdersService::OrderBreakdown.new(@order_from_uuid.id).call
      render json: @orders
    else
      render json: {type: "error", message: "This User does not not have permission"}, status: :unprocessable_entity
    end
  end

  # ==================== end ======================

  
  def delete_scheduled_order
    schedule_id = params[:id]
    order_schedule = OrderScheduleService::DeleteOrderSchedule.new(schedule_id, current_user).call
    @order = OrdersService::OrderBreakdown.new(order_schedule[:obj].id).call
    render json: @order
  end


  def update_order_schedule
    order_id = params[:payload][:order_id]
    order_schedule = OrderScheduleService::CreateOrderSchedule.new(params[:payload]).create
    if order_schedule[:status]
      Order.find(order_id).update(is_recurring: true)
      @order = OrdersService::OrderBreakdown.new(order_id).call
      render json: order_schedule[:obj]
    else
      render json: {error: "Order schedule could not be created"}, status: :unprocessable_entity
    end
  end

  #partially tested
  def duplicate_order
    duplicated_order = OrdersService::Actions::DuplicateOrder.new(params[:id], current_user).call

    if duplicated_order[:status]
      render json: duplicated_order
    else
      render json: duplicated_order
    end
    
  end

  # Order Breakdowns

  def fetch_all_upcoming_orders
    if current_user.companies.where(id: params[:supplier_id]).present?
      upcoming_orders = OrdersService::Summaries::FetchAllUpcomingOrders.new(params[:supplier_id]).call
      render json: upcoming_orders
    else
      render json: {error: "User does not have access."}, status: :unprocessable_entity
    end
  end

  def fetch_all_completed_deliveries
    if current_user.companies.where(id: params[:supplier_id]).present?
      upcoming_orders = OrdersService::Summaries::FetchAllDeliveredOrders.new(params[:supplier_id], params[:date] ).call
      render json: upcoming_orders
    else
      render json: {error: "User does not have access."}, status: :unprocessable_entity
    end
  end

  def fetch_all_todays_deliveries
    if current_user.companies.where(id: params[:supplier_id]).present?
      upcoming_orders = OrdersService::Summaries::FetchAllTodaysOrders.new(params[:supplier_id]).call
      render json: upcoming_orders
    else
      render json: {error: "User does not have access."}, status: :unprocessable_entity
    end
  end

  def user_daily_deliverable_orders
    if current_user.companies.where(id: params[:supplier_id]).present?
      user = User.find(params[:user_id])
      supplier = Company.find(params[:supplier_id])
      if supplier.advance_delivery
        orders = FulfilmentService::RouteMapping.new(user, supplier).call
      else
        orders = FulfilmentService::RegularDeliveryRoute.new(user, supplier).call
      end
      render json: orders
    else
      render json: {error: "User does not have access."}, status: :unprocessable_entity
    end
  end

  def user_deliverable_orders
    @orders =  OrdersService::UserDeliverableOrders.new(current_user.current_company, current_user).call
    render json: @orders
  end

  def update_delivery_driver_and_date
    if current_user.companies.find_by(id: params[:supplier_id]).present?
      updated_deliveries = DeliveryService::UpdateDeliveryDriverAndDate.new(params).call
      if updated_deliveries
        render json: updated_deliveries
      else
        render json: {error: "Something went wrong please contact support"}, status: :unprocessable_entity
      end
    else
      render json: {error: "User does not have access."}, status: :unprocessable_entity
    end
  end

  def order_delivery_history
    order_history = OrdersService::DeliveredOrderHistory.new(current_user.current_company).call
    render json: order_history
  end

  def summary
    orders =  OrdersService::SummaryTypes.new.summary(current_user.current_company)

    render json: orders
  end

  #mobile
  def all_purchases_summary
    orders = OrdersService::SummaryTypes.new.all_purchases_summary(current_user.current_company)
    render json: orders
  end

  #sales
  def all_sales_summary
    orders = OrdersService::SummaryTypes.new.all_sales_summary(current_user.current_company)
    render json: orders
  end

  def supplier_statement
    sales_summary = OrdersService::SummaryTypes.new.summary_by_supplier(current_user.current_company, params[:supplier_id])
    render json: sales_summary
  end

  def customer_statement
    customer_statement = OrdersService::SummaryTypes.new.summary_by_customer(current_user.current_company, params[:customer_id])
    render json: customer_statement
  end

  def supplier_age_analysis
    supplier_analysis = OrdersService::SupplierAgeAnalysis.new(current_user.current_company).call
    render json: supplier_analysis
  end

  def customer_age_analysis
    customer_age_analysis = OrdersService::CustomerAgeAnalysis.new(current_user.current_company).call
    render json: customer_age_analysis
  end

  def customer_analysis
    customer_analysis = OrdersService::OrderAnalysis.new().customer_analysis(current_user.current_company)
    render json: customer_analysis
  end

  def supplier_analysis
    supplier_analysis = OrdersService::OrderAnalysis.new().supplier_analysis(current_user.current_company)
    render json: supplier_analysis
  end

  def deliverable_orders
    @orders =  OrdersService::DeliverableOrders.new(current_user.current_company).call
    render json: @orders
  end

  def delivered_orders
    orders = OrdersService::DeliveredOrders.new(current_user.current_company).call
    render json: orders
  end

  def filter_sales_orders
    @orders = OrdersService::SummaryTypes.new.sales_summary_by_date(current_user.current_company, params[:start_date], params[:end_date])
    render json: @orders
  end

  def filter_purchase_orders
    @orders = OrdersService::SummaryTypes.new.summary_by_date(current_user.current_company, params[:start_date], params[:end_date])
    render json: @orders
  end

  def sales_summary
    @orders = OrdersService::SummaryTypes.new.sales_summary(current_user.current_company, params[:status])
    render json: @orders
  end

  def purchase_order_summary
    @orders = OrdersService::SummaryTypes.new.summary(current_user.current_company, params[:status])
    render json: @orders
  end

  def calendar_summary
    @calendar_order = []
    Order.where(customer_id: current_user.current_company).map{|o| @calendar_order.push(
      title: "#{o.supplier.title} - Order ##{o.order_number}",
      id: o.uuid,
      start: o.delivery_date,
      end: o.delivery_date,
      className: "bg-orange",
      allDay: true
    )}
    Order.where(supplier_id: current_user.current_company).map{|o| @calendar_order.push(
      title: "#{o.supplier.title} - Order ##{o.order_number}",
      id: o.uuid,
      start: o.delivery_date,
      end: o.delivery_date,
      className: "bg-success",
      allDay: true
    )}

    render json: @calendar_order
  end

  def sales_order_summary
    @orders = OrdersService::SummaryTypes.new.sales_summary(current_user.current_company)

    render json: @orders
  end

  def waybills
    waybill_details = (0..13).to_a.map do |offset|
      new_date = Date.today + offset.day
      {
        id: offset,
        date: new_date.strftime('%e %b %Y'),
        orders: OrdersService::SummaryTypes.new.orders_by_date(current_user.current_company, new_date),
        waybills: OrdersService::SummaryTypes.new.waybill_by_date(current_user.current_company, new_date)
      }
    end
    render json: waybill_details
  end

  
  private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  end
  
  def check_user_permission
    if !CompanyService::UserCompanyPermissions.new(user: current_user, company_ids: [params[:company_id]]).call
      no_permission
    else
      true
    end
  end
  
  def set_uuid_order
    @order_from_uuid = Order.find_by(uuid: params[:order_uuid])
    if OrdersService::Permissions::OrderPermission.new(current_user, @order_from_uuid.id).call
      true
    else
      no_permission
    end
  end

    # Only allow a trusted parameter "white list" through.
    def order_params
      params.require(:order).permit(
        :customer_id, :delivery_address_id, :delivery_fee, :signature, :delivery_status, :received_by, :id, :delivery_date, :parent_order, :status, :notes, :order_number, :unit_amount, :supplier_id, :order_uuid, :tax_amount, :order_id, :end_date, :frequency, :user_id, :start_date, :order_list, :value,
        order_items_attributes: [:product_id, :id, :product_name, :quantity, :order_id, :gross_price, :net_price, :vat_price, :company_id, :product_variant_id, :unit_amount, :tax_amount ]
      )
    end
end
