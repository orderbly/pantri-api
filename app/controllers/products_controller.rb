class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy, :product_add_tag, :product_delete_tag]
  before_action :authorize_access_request!

  # GET /products
  def index
    @products = Product.all.where(company_id: current_user.current_company)
    render json: @products
  end

  def get_company_products
    if current_user.companies.find_by(id: params[:company_id]).present?
      @products = Product.all.where(company_id: params[:company_id]).where.not(is_archived: true)
      render json: @products
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  # Products by ID
  def products_by_company
    company_id = params[:id]
    @products = ProductService::CompanyProducts.new(current_company: current_user.current_company, company_id: company_id).call

    render json: @products
  end

  # Products by title
  def company_products
    company_id = Company.where(title: params[:title].tr('_', ' ')).first.id
    
    @products = ProductService::CompanyProducts.new(current_company: current_user.current_company, company_id: company_id).call

    render json: @products
  end

  def company_products_for_order
    company_id = Company.where(title: params[:title].tr('_', ' ')).first.id
    
    @products = ProductService::CompanyProducts.new(current_company: params[:customer_id], company_id: company_id).call

    render json: @products
  end

  def products_by_tag
    @products = Product.all.where(company_id: company.id).by_tag(product_params[:search_tag])
    render json: @products
  end

  def product_add_tag
    params[:product][:tags].each do |tag|
      @product.tags << Tag.find(tag)
    end
    render json: @product, status: :created, location: @product
  end

  def product_delete_tag
    @product.tags.delete(Tag.find(params[:tag_id]))
  end

  def toggle_active_product
    product = Product.find(params[:id])
    if current_user.companies.find_by(id: product.company_id).present?
      product.is_active = !product.is_active
      product.save!
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  # GET /products/1
  def show
    render json: @product
  end

  # POST /products
  def create
    new_product = ProductService::CreateProduct.new(params: params[:product], company_id: current_user.current_company).call
    if new_product[:status]
      render json: new_product[:obj], status: :created, location: new_product[:obj]
    else
      render json: new_product[:obj].errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  def update
    update_product = ProductService::UpdateProduct.new(params: params[:product], product: @product).call
    if update_product[:status]
      render json: update_product[:obj], status: :created, location: update_product[:obj]
    else
      render json: update_product[:obj].errors, status: :unprocessable_entity
    end
  end


  # DELETE /products/1
  def destroy
    @product.product_variants.each do |p|
      p.delete
      p.save!
    end
    @product.destroy
  end

  protected

    def record_not_found(exception)
      render json: {error: exception.message}.to_json, status: 404
      return
    end

  private

    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(
        :company_id, :accounting_item, :id, :is_exclusive, :exclusive_companies, :vat, :got_variants, :description, :tags, :tax_id, :title, :image, :tag_id, :search_tag, :product_image,
        product_variants_attributes: [ :id, :product_id, :description, :status, :track_stock, :available_stock, :stock, :unit_amount, :tax_amount, :discount_percentage, :_destroy, :gross_amount, :accounting_item ]
      )
    end

    def company
      # TODO: This needs to be passed in with headers
      Company.find(current_user.current_company)
    end
end
