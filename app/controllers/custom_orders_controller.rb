class CustomOrdersController < ApplicationController
  before_action :set_custom_order, only: [:show, :update, :destroy]

  # GET /custom_orders
  def index
    @custom_orders = CustomOrder.all

    render json: @custom_orders
  end

  # GET /custom_orders/1
  def show
    render json: @custom_order
  end

  # POST /custom_orders
  def create
    @custom_order = CustomOrder.new(custom_order_params)

    if @custom_order.save
      render json: @custom_order, status: :created, location: @custom_order
    else
      render json: @custom_order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /custom_orders/1
  def update
    if @custom_order.update(custom_order_params)
      render json: @custom_order
    else
      render json: @custom_order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /custom_orders/1
  def destroy
    @custom_order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_custom_order
      @custom_order = CustomOrder.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def custom_order_params
      params.require(:custom_order).permit(:customer_id, :supplier_id, :delivery_date, :notes, :order_number, :status, :balance, :is_paid, :delivery_status, :value, :accounting, :order_schedule_id, :is_recurring, :schedule_id, :signature_image, :delivery_user_id, :parent_credit_note_id, :received_by, :uuid, :delivery_address_id, :delivery_fee, :order_reconciliation, :is_delivery_buyer_actionable, :inventory, :reorder_balance, :delivery)
    end
end
