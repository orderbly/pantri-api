class PaymentsController < ApplicationController
  before_action :authorize_access_request!
  before_action :validate_user_permission, only: [:pay_order_directly, :validate_user_permission]

  # POST pay_order_directly/:order_uuid
  def pay_order_directly
    order = Order.find_by(uuid: params[:order_uuid])
    if current_user.companies.find_by(id: order.supplier.id).present? && order.status == "accepted"
      @order_from_uuid = order
      if order.is_paid != "true"  && order.status == "accepted"
        payment = PaymentService::PayOrder.new(order_id: order.id, value: order.balance, transaction_id: "#{current_user.email}", supplier_id: order.supplier_id, customer_id: order.customer_id).call
        if payment[:type] == "success"
          order_breakdown = OrdersService::OrderBreakdown.new(order.id).call
          render json: order_breakdown
        else
          render json: {}, status: :unprocessable_entity
        end
      else
        render json: {}, status: :unprocessable_entity
      end
    else
      render json: {}, status: :unprocessable_entity
    end
  end

    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    def create_payment
      begin
        order = Order.find_by(uuid: params[:order_uuid])
        PaymentService::PayOrder.new(
          order_id: order.id,
          value: params[:payment_amount],
          payment_reference: payment_params[:payment_reference],
          payment_date: payment_params[:payment_date],
          payment_type: payment_params[:payment_type],
          supplier_id: order.supplier_id,
          customer_id: order.customer_id,
          user: current_user
        ).call
        order_breakdown = OrdersService::OrderBreakdown.new(order.id).call
        render json: order_breakdown
      rescue
        render json: {}, status: :unprocessable_entity
      end
    end

    # Only allow a trusted parameter "white list" through.

    def validate_user_permission
      order = Order.find_by(uuid: params[:order_uuid])
      if current_user.companies.where(id: [order.supplier_id, order.customer_id]).present?
        return true
      else
        render json: {}, status: :unprocessable_entity
      end
    end

    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    def payment_params
      params.require(:payment).permit(:supplier_id, :customer_id, :order_id, :value, :account_sale_id, :payment_amount, :payment_type, :payment_reference, :payment_date)
    end


end
