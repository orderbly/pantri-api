class CreditNoteItemsController < ApplicationController
  before_action :set_credit_note_item, only: [:show, :update, :destroy]

  # GET /credit_note_items
  def index
    @credit_note_items = CreditNoteItem.all

    render json: @credit_note_items
  end

  # GET /credit_note_items/1
  def show
    render json: @credit_note_item
  end

  # POST /credit_note_items
  def create
    @credit_note_item = CreditNoteItem.new(credit_note_item_params)

    if @credit_note_item.save
      render json: @credit_note_item, status: :created, location: @credit_note_item
    else
      render json: @credit_note_item.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /credit_note_items/1
  def update
    if @credit_note_item.update(credit_note_item_params)
      render json: @credit_note_item
    else
      render json: @credit_note_item.errors, status: :unprocessable_entity
    end
  end

  # DELETE /credit_note_items/1
  def destroy
    @credit_note_item.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_credit_note_item
      @credit_note_item = CreditNoteItem.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def credit_note_item_params
      params.require(:credit_note_item).permit(:company_id, :product_id, :order_id, :quantity, :gross_price, :net_price, :vat_price, :product_variant_id, :received_amount)
    end
end
