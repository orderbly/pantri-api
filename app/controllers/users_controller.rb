class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  before_action :authorize_access_request!, only: [:update, :show, :destroy, :signout, :invite_user, :user_memberships, :select_current_company, :user_details, :remove_user_avatar]
  include HTTParty

  # POST /users
  def signup
    user = User.new(user_params.except(:company, :is_supplier))
    if user.save!
      if params[:company].present?
        company = Company.create!(title: params[:company], contact: params[:user][:contact], supplier: params[:is_supplier], image: "https://pantryapp.s3.eu-west-1.amazonaws.com/tjTk4WwaQ8gRpL2vwiDYWqar")
        member = Member.create!(user_id: user.id, company_id: company.id, role_id: 1)
      end
      UserService::CreatePendingInvites.new(user.email).call
      WelcomeEmailerJob.perform_later({email: user.email, user_name: "#{user.first_name} #{user.last_name}"})
      payload  = { user_id: user.id, usignature: (Time.now.to_f * 10000.0).to_i }
      session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
      render json: session.login.merge({
        usignature: payload[:usignature],
        company: company.present? ? Company.find(company.id) : {},
        user: {
          id: user.id ? user.id : '',
          name: user.first_name ? user.first_name : '',
          last_name: user.last_name ? user.last_name : '',
          username: user.username ? user.username : '',
          contact_number: user.contact_number ? user.contact_number : '',
          uuid: user.uuid,
          user_email: user.email,
          avatar: user.avatar ? user.avatar : ''
        }
        })
    else
      render json: { error: user.errors.full_messages.join(' ') }, status: :unprocessable_entity
    end
  end

  def mobile_google_auth
    client_id = ENV['GOOGLE_CLIENT_ID']
    access_token = params['access_token']     
    profile_url = "https://www.googleapis.com/oauth2/v3/userinfo?access_token=#{access_token}"
    profile = HTTParty.post(profile_url)

    if User.where(email: profile['email']).length > 0 
      user = User.find_by!(email: profile['email'])
      if user.authenticate(profile['sub'])
        payload  = { user_email: user.email, user_name: "#{user.first_name} #{user.last_name}", user_id: user.id, usignature: (Time.now.to_f * 10000.0).to_i }
        session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
        render json: session.login.merge({
          usignature: payload[:usignature],
          user: user.to_json,
          user_name: payload[:user_name],
          id: user.id ? user.id : '',
          name: user.first_name ? user.first_name : '',
          last_name: user.last_name ? user.last_name : '',
          username: user.username ? user.username : '',
          uuid: user.uuid,
          contact_number: user.contact_number ? user.contact_number : '',
          user_email: payload[:user_email],
          avatar: user.avatar ? user.avatar : ''
        })
      end
    else 
      user = User.create_user_for_google(profile)
      if user.save
        UserService::CreatePendingInvites.new(user.email).call
        WelcomeEmailerJob.perform_later({email: user.email, user_name: "#{user.first_name} #{user.last_name}"})
        payload  = { user_email: user.email, user_name: "#{user.first_name} #{user.last_name}", user_id: user.id, usignature: (Time.now.to_f * 10000.0).to_i }
        session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
        render json: session.login.merge({usignature: payload[:usignature]})
      else
        render json: { error: user.errors.full_messages.join(' ') }, status: :unprocessable_entity
      end
    end
  end

  def get_google_authorization_with_company
    client_secret = ENV['GOOGLE_CLIENT_SECRET']
    client_id = ENV['GOOGLE_CLIENT_ID']
    code = params["id_token"]
    url = "https://accounts.google.com/o/oauth2/token?code=#{code}&client_id=#{client_id}&client_secret=#{client_secret}&redirect_uri=postmessage&grant_type=authorization_code"
    tokens = HTTParty.post(url, { headers: {"X-Requested-With" => "XMLHttpRequest"}})
    access_token = tokens['access_token']     
    id_token = tokens['id_token']     
    profile_url = "https://www.googleapis.com/oauth2/v3/userinfo?access_token=#{access_token}"
    profile = HTTParty.post(profile_url)
    if User.where(email: profile['email']).length > 0 
      user = User.find_by(email: profile['email'])
      if user.authenticate(profile['sub'])
        if params[:company].present?
          company = Company.create!(title: params[:company], contact: params[:contact], supplier: params[:is_supplier], image: "https://pantryapp.s3.eu-west-1.amazonaws.com/tjTk4WwaQ8gRpL2vwiDYWqar")
          member = Member.create!(user_id: user.id, company_id: company.id, role_id: 1)
        end
        payload  = { user_email: user.email, user_name: "#{user.first_name} #{user.last_name}", user_id: user.id, usignature: (Time.now.to_f * 10000.0).to_i }
        session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
        render json: session.login.merge({
          company: params[:company].present? ? Company.find(company.id): {},
          user: {
            role: member.present? ? member.role : {},
            uuid: user.uuid,
            id: user.id,
            name: user.first_name,
            last_name: user.last_name,
            username: user.username,
            contact_number: user.contact_number,
            user_email: user.email,
            default_colour: user.default_colour,
            avatar: user.avatar,
          }
        })
      end
    else 
      user = User.create_user_for_google(profile)
      if user.save
        if params[:company] 
          company = Company.create!(title: params[:company], contact: params[:contact], image: "https://pantryapp.s3.eu-west-1.amazonaws.com/tjTk4WwaQ8gRpL2vwiDYWqar")
          member = Member.create!(user_id: user.id, company_id: company.id, role_id: 1)
        end
        UserService::CreatePendingInvites.new(user.email).call
        WelcomeEmailerJob.perform_later({email: user.email, user_name: "#{user.first_name} #{user.last_name}"})
        payload  = { user_id: user.id, usignature: (Time.now.to_f * 10000.0).to_i }
        session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
        render json: session.login.merge({
          company: Company.find(company.id),
          user: {
            role: member.role,
            uuid: user&.uuid,
            id: user&.id,
            name: user&.first_name,
            last_name: user&.last_name,
            username: user&.username,
            contact_number: user&.contact_number,
            user_email: user&.email,
            default_colour: user&.default_colour,
            avatar: user&.avatar,
          }
        })
      else
        render json: { error: user.errors.full_messages.join(' ') }, status: :unprocessable_entity
      end
    end
  end

  def get_google_authorization
    client_secret = ENV['GOOGLE_CLIENT_SECRET']
    client_id = ENV['GOOGLE_CLIENT_ID']
    code = params["id_token"]
    url = "https://accounts.google.com/o/oauth2/token?code=#{code}&client_id=#{client_id}&client_secret=#{client_secret}&redirect_uri=postmessage&grant_type=authorization_code"
    tokens = HTTParty.post(url, { headers: {"X-Requested-With" => "XMLHttpRequest"}})

    access_token = tokens['access_token']     
    id_token = tokens['id_token']     
    profile_url = "https://www.googleapis.com/oauth2/v3/userinfo?access_token=#{access_token}"
    profile = HTTParty.post(profile_url)
    if User.where(email: profile['email']).length > 0 
      user = User.find_by(email: profile['email'])
      if user.authenticate(profile['sub'])
        payload  = { user_email: user.email, user_name: "#{user.first_name} #{user.last_name}", user_id: user.id, usignature: (Time.now.to_f * 10000.0).to_i }
        session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
        render json: session.login.merge({
          usignature: payload[:usignature],
          user: user.to_json,
          user_name: payload[:user_name],
          id: user.id ? user.id : '',
          name: user.first_name ? user.first_name : '',
          last_name: user.last_name ? user.last_name : '',
          username: user.username ? user.username : '',
          contact_number: user.contact_number ? user.contact_number : '',
          uuid: user.uuid,
          user_email: payload[:user_email],
          avatar: user.avatar ? user.avatar : ''
        })
      end
    else 
      user = User.create_user_for_google(profile)
      if user.save
        UserService::CreatePendingInvites.new(user.email).call
        WelcomeEmailerJob.perform_later({email: user.email, user_name: "#{user.first_name} #{user.last_name}"})
        payload  = { user_id: user.id, usignature: (Time.now.to_f * 10000.0).to_i }
        session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
        render json: session.login.merge({
          usignature: payload[:usignature],
          user: user.to_json,
          user_name: payload[:user_name],
          id: user.id ? user.id : '',
          name: user.first_name ? user.first_name : '',
          last_name: user.last_name ? user.last_name : '',
          username: user.username ? user.username : '',
          contact_number: user.contact_number ? user.contact_number : '',
          uuid: user.uuid,
          user_email: payload[:user_email],
          avatar: user.avatar ? user.avatar : ''
        })
      else
        render json: { error: user.errors.full_messages.join(' ') }, status: :unprocessable_entity
      end
    end
  end

  def signin
    user = User.find_by!(email: params[:auth][:email])
    if user.authenticate(params[:auth][:password])
      payload  = { user_email: user.email, user_name: "#{user.first_name} #{user.last_name}", user_id: user.id, usignature: (Time.now.to_f * 10000.0).to_i }
      session = JWTSessions::Session.new(payload: payload, refresh_by_access_allowed: true)
      render json: session.login.merge({
        usignature: payload[:usignature],
        user: user.to_json,
        user_name: payload[:user_name],
        id: user.id ? user.id : '',
        name: user.first_name ? user.first_name : '',
        last_name: user.last_name ? user.last_name : '',
        username: user.username ? user.username : '',
        contact_number: user.contact_number ? user.contact_number : '',
        uuid: user.uuid,
        user_email: payload[:user_email],
        avatar: user.avatar ? user.avatar : ''
      })
    else
      not_authorized
    end
  end

  def refresh
    session = JWTSessions::Session.new(
      payload: claimless_payload,
      refresh_by_access_allowed: true,
      namespace: "user_#{claimless_payload['user_id']}"
    )
    tokens = session.refresh_by_access_payload do
      raise JWTSessions::Errors::Unauthorized, 'Malicious activity detected'
    end

    render json: session.login.merge({usignature: payload[:usignature]})
  end

  def signout
    session = JWTSessions::Session.new(payload: payload)
    session.flush_by_access_payload
    render json: :ok
  end

  # GET /users
  def index
    current_user ||= User.find(payload['user_id'])
    @user = User.find(current_user.id)

    render json: @user
  end

  # GET /users/1
  def show
    render json: @user
  end

  def user_details
    user = User.find(current_user.id)
    render json: user
  end

  def user_exists
    user_present = User.find_by(email: user_params[:email]).present?
    render json: user_present
  end

  def invite_user
    permissions = JSON.parse(params[:permissions].to_json)
    if MembershipService::ValidateMembershipPermissions.new(permissions).call
      current_user ||= User.find(payload['user_id'])
      email = params[:email]
      if User.where('lower(email) = ?', email.downcase).first
        new_membership = User.create_invite_membership(params[:email], current_user.current_company, params[:role_id], params[:permissions])
        render json: new_membership
      else 
        if !PendingInvite.find_by(email_address: email.downcase, company_id: current_user.current_company).present?
          PendingInvite.create(email_address: email.downcase, role: params[:role_id], company_id: current_user.current_company, user_permissions: params[:permissions])
          InviteUserJob.perform_later(email.downcase, current_user.current_company)
          render json: {type: "error", message: "This user does not have an Orderbly account. We have sent them an invite to join your company."}
        else
          render json: {type: "error", message: "This user has already been invited to join your company."}
        end
      end
    else
      render json: {message: "User does not have permission."}, status: :unprocessable_entity
    end
  end

  def select_current_company
    @user = User.find(current_user.id)
    membership_role = Member.find_by!(company_id: params[:company_id], user_id: current_user.id).role.level
    role = 2
    if membership_role ==  "admin"
      role = 0
    elsif membership_role ==  "staff"
      role = 1
    else 
      role = 2
    end

    @user.update(role: role, current_company:  params[:company_id])
    Pusher.trigger('user_auth', "user_#{current_user.id}", {company_id: current_user.current_company, unique_key: params[:unique_key]})

    render json: {
      company: Company.find(@user.current_company),
      user: {
        permissions: Base64.encode64(Member.find_by(user_id: @user.id, company_id: params[:company_id]).user_permissions.to_json),
        role: @user.role,
        uuid: @user.uuid,
        id: @user.id,
        name: @user.first_name,
        last_name: @user.last_name,
        username: @user.username,
        contact_number: @user.contact_number,
        user_email: @user.email,
        default_colour: @user.default_colour,
        avatar: @user.avatar,
      }
    }
  end

  def user_memberships
    current_user ||= User.find(payload['user_id'])
    @user = current_user
    render json: @user.members
  end

  # PATCH/PUT /users/1
  def update
    if user_params[:avatar] && user_params[:avatar] != ""
      decoded_data = Base64.decode64(user_params[:avatar] .split(',')[1])
      signature = { io: StringIO.new(decoded_data), filename: user_params[:first_name] }
      @user.avatar_image.attach(signature)
      @user.avatar =  @user.avatar_image.service.send(:object_for, @user.avatar_image.key).public_url
    end
    if @user.update(user_params.except(:avatar))
      render json: { 
        id: @user.id,
        name: @user.first_name,
        last_name: @user.last_name,
        role: @user.role, 
        username: @user.username,
        contact_number: @user.contact_number,
        default_colour: @user.default_colour,
        user_email: @user.email,
        avatar: @user.avatar
      }
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  def remove_user_avatar
    @user = current_user
    @user.update(avatar: nil)
    render json: { 
        role: @user.role,
        id: @user.id,
        name: @user.first_name,
        last_name: @user.last_name,
        username: @user.username,
        contact_number: @user.contact_number,
        default_colour: @user.default_colour,
        user_email: @user.email,
        avatar: @user.avatar
      }
  end

  # def current_user
  #   @
  # end

  private

    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:id, :email, :company, :contact, :company_id, :password, :password_confirmation, :first_name, :last_name, :username, :contact_number, :id_token, :role_id, :access_token, :avatar, :default_colour, :permissions)
    end
end
