class CustomDeliveriesController < ApplicationController
  before_action :set_custom_delivery, only: [:show, :update, :destroy]
  before_action :authorize_access_request!
  
  def update_custom_delivery_status
    delivery = CustomDelivery.find(params[:id])
    delivery.update(delivery_status: params[:delivery_status].to_i)
    order_breakdown = FulfilmentService::Breakdowns::CustomDeliveryBreakdown.new.custom_delivery_response(delivery) 

    render json: order_breakdown
  end

  # POST /custom_deliveries
  def create
    # need to add user validation
    @custom_delivery = CustomDelivery.new(custom_delivery_params)
    repsonse = nil

    if @custom_delivery.save!
      if params[:route] == "/my_deliveries"
        if Company.find_by(id: custom_delivery_params[:supplier_id])&.advance_delivery
          response = FulfilmentService::RouteMapping.new(User.find(custom_delivery_params[:delivery_driver_id]), Company.find(custom_delivery_params[:supplier_id])).call
        else
          response = FulfilmentService::RegularDeliveryRoute.new(User.find(custom_delivery_params[:delivery_driver_id]), Company.find(custom_delivery_params[:supplier_id])).call
        end
      elsif params[:route] == "/todays_deliveries"
        response = OrdersService::Summaries::FetchAllTodaysOrders.new(custom_delivery_params[:supplier_id]).call
      elsif params[:route] == "/upcoming_deliveries"
        response = OrdersService::Summaries::FetchAllUpcomingOrders.new(custom_delivery_params[:supplier_id]).call
      else
        response = @custom_delivery
      end
      render json: response, status: :created
    else
      render json: @custom_delivery.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /custom_deliveries/1
  def update
    if @custom_delivery.update(custom_delivery_params)
      render json: @custom_delivery
    else
      render json: @custom_delivery.errors, status: :unprocessable_entity
    end
  end

  # DELETE /custom_deliveries/1
  def destroy
    @custom_delivery.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_custom_delivery
      @custom_delivery = CustomDelivery.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def custom_delivery_params
      params.require(:custom_delivery).permit(:title, :supplier_id, :address_id, :due_date, :delivery_driver_id, :custom_client_id, :order_number, :customer_id)
    end
end
