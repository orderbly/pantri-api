class CreditLimitsController < ApplicationController
  before_action :authorize_access_request!
  before_action :set_credit_limit, only: [:show, :update, :destroy]
  before_action :validate_viewing_privilege, only: [:company_credit_limits, :available_companies, :create, :update]

  def company_credit_limits
    credit_limits = CreditLimit.where(supplier_id: params[:supplier_id])

    render json: credit_limits
  end

  def available_companies
    available_companies = CreditLimitsService::AvailableCompanies.new(supplier_id: params[:supplier_id]).call

    render json: available_companies
  end

  def account
    if current_user.companies.find_by(id: params[:supplier_id]).present? || current_user.companies.find_by(id: params[:customer_id]).present?
      account_balance = CreditLimitsService::CalculateCreditBalance.new(params[:supplier_id], params[:customer_id]).call
      render json: account_balance
    else
      forbidden
    end
  end

  # POST /credit_limits
  def create
    if !CreditLimit.find_by(supplier_id: credit_limit_params[:supplier_id], customer_id: credit_limit_params[:customer_id]).present?
      @credit_limit = CreditLimit.new(credit_limit_params)
      if @credit_limit.save
        render json: @credit_limit, status: :created, location: @credit_limit
      else
        render json: @credit_limit.errors, status: :unprocessable_entity
      end
    else
      render status: :unprocessable_entity
    end
  end

  # PATCH/PUT /credit_limits/1
  def update
    if @credit_limit.update(credit_limit_params)
      render json: @credit_limit
    else
      render json: @credit_limit.errors, status: :unprocessable_entity
    end
  end

  # DELETE /credit_limits/1
  def destroy
    if current_user.companies.find_by(id: @credit_limit.supplier_id).present?
      @credit_limit.destroy
      return true
    else
      forbidden
    end
  end

  private

    def current_user
      begin
        current_user ||= User.find(payload['user_id'])
      rescue
        return false
      end
    end

    def validate_viewing_privilege 
      supplier_id = params[:supplier_id] || credit_limit_params[:supplier_id]
      if current_user.companies.find_by(id: supplier_id).present?
        true
      else 
        forbidden
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_credit_limit
      @credit_limit = CreditLimit.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def credit_limit_params
      params.require(:credit_limit).permit(:supplier_id, :customer_id, :credit_limit)
    end
end
