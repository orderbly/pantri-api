module Userable
  extend ActiveSupport::Concern

  def current_user
    begin
      current_user ||= User.find(payload['user_id'])
    rescue
      return false
    end
  end

end