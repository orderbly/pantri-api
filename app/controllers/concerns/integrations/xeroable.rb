module Integrations::Xeroable
  extend ActiveSupport::Concern

  def xero_error(exception)
    render json: {type: 'error', messsage: exception.message}, status: 503
  end

  def xero_api_get(xero_endpoint, company_id, tenant)
    xero = credentials(company_id)

    refresh_token(xero) if token_expired?(xero)

    resp = Faraday.get(xero_endpoint) do |req|
      req.headers['Authorization'] = 'Bearer ' + xero.access_token
      req.headers['Accept'] = 'application/json'
      req.headers['Content-Type'] = 'application/json'
      req.headers['xero-tenant-id'] = tenant
    end
    raise XeroError, JSON.parse(resp.body) if 200 != resp.status
    resp
  end

  def xero_api_post(xero_endpoint, company_id, tenant, data)
    xero = credentials(company_id)

    refresh_token(xero) if token_expired?(xero)
    resp = Faraday.post(xero_endpoint, data) do |req|
      req.headers['Authorization'] = 'Bearer ' + xero.access_token
      req.headers['Accept'] = 'application/json'
      req.headers['Content-Type'] = 'application/json'
      req.headers['xero-tenant-id'] = tenant
    end
    raise XeroError, JSON.parse(resp.body) if 200 != resp.status
    resp
  end

  def provider
    'xero'
  end

  def authorize_url
    'https://login.xero.com/identity/connect/authorize'
  end

  def xero_token_endpoint
    'https://identity.xero.com/connect/token'
  end

  def redirect_url
    ENV.fetch('xero_redirect')
  end

  def scope
    'openid profile email files accounting.transactions accounting.transactions.read accounting.reports.read accounting.journals.read accounting.settings accounting.settings.read accounting.contacts accounting.contacts.read accounting.attachments accounting.attachments.read offline_access'
  end

  def build_authorize_url(user)
    auth_url = URI.parse(authorize_url)
    auth_url.query = URI.encode_www_form(
      response_type: 'code',
      client_id: ENV['xero_api_client_id'],
      scope: scope,
      redirect_uri: redirect_url,
      state: user.id
    )
    auth_url.to_s
  end

  def create_draft_invoice(order_id, contact_id)
    order = Order.find(order_id)
    type = order.customer_id == current_user.current_company ? "ACCPAY" : "ACCREC"
    payload = { Type: type,
      Contact: {
        ContactID: contact_id
      },
      "Date": "\/Date(#{(order.created_at.to_f*1000).to_i})\/",
      "DueDate": "\/Date(#{(order.delivery_date.to_f*1000).to_i})\/",
      "DateString": order.created_at.to_date.strftime('%Y-%m-%dT%H:%M:%S'),
      "Reference": "Orderbly Order: #{order.order_number}",
      "DueDateString": order.delivery_date.to_date.strftime('%Y-%m-%dT%H:%M:%S'),
      "LineAmountTypes": "Inclusive",
      "Status": "DRAFT",
      "LineItems": line_items(order, type)
    }
    resp = xero_api_post("https://api.xero.com/api.xro/2.0/Invoices", current_user.current_company, tenant['tenantId'], payload.to_json)
    resp_hash = JSON.parse(resp.body)
    if order.customer_id == current_user.current_company
      order.accounting[:purchases] = { invoice: resp_hash['Invoices'].first, customer_id: order.customer_id}
    else
      order.accounting[:sales] = { invoice: resp_hash['Invoices'].first, sales_id: order.supplier_id} 
    end
    order.save
    resp
  end


  def refresh_invoice(order)
    isCustomer = order.customer_id == current_user.current_company
    invoice_id = isCustomer ? order.acccounting['purchase']['invoice']['InvoiceId'] : order.acccounting['sales']['invoice']['InvoiceId']

    resp = xero_api_post("https://api.xero.com/api.xro/2.0/Invoices/#{invoice_id}", current_user.current_company, tenant['tenantId'], payload.to_json)
    resp_hash = JSON.parse(resp.body)
    if isCustomer 
      order.accounting[:purchases] = { invoice: resp_hash['Invoices'].first, customer_id: order.customer_id}
    else
      order.accounting[:sales] = { invoice: resp_hash['Invoices'].first, sales_id: order.supplier_id} 
    end
    order.save
  end


  def create_credit_note(credit_note_id, contact_id)
    credit_note = CreditNote.find(credit_note_id)
    type = credit_note.customer_id == current_user.current_company ? "ACCPAYCREDIT" :  "ACCRECCREDIT"
    payload = { 
      Type: type,
      Contact: {
        ContactID: contact_id
      },
      "Date": "\/Date(#{(credit_note.created_at.to_f*1000).to_i})\/",
      "DateString": credit_note.created_at.to_date.strftime('%Y-%m-%dT%H:%M:%S'),
      "Reference": "Orderbly Credit note: #{credit_note.credit_note_number}",
      "DueDateString": credit_note.created_at.to_date.strftime('%Y-%m-%dT%H:%M:%S'),
      "LineAmountTypes": "Inclusive",
      "LineItems": credit_note_items(credit_note, type)
    }

    resp = xero_api_post("https://api.xero.com/api.xro/2.0/CreditNotes", current_user.current_company, tenant['tenantId'], payload.to_json)
    resp_hash = JSON.parse(resp.body)
    if credit_note.customer_id == current_user.current_company
      credit_note.accounting[:credit_notes_received] = { credit_note: resp_hash['CreditNotes'].first, customer_id: credit_note.customer_id}
    else
      credit_note.accounting[:credit_notes_issued] = { credit_note: resp_hash['CreditNotes'].first, sales_id: credit_note.supplier_id} 
    end
    credit_note.save
    resp
  end


  private

  def credentials(company_id)
    Integration.find_by(company_id: company_id, name: provider)
  end

  def tenant
    Store::Accounting.where(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'tenant').pluck(:blob).first
  end

  def token_expired?(xero)
    system_time = Time.now().to_i
    token_expiry_time = xero.expires.to_i

    return (system_time >= token_expiry_time) ? true : false 
  end

  def refresh_token(xero)
    system_time = Time.now().to_i
    refresh_request_body_hash = {
      grant_type: 'refresh_token',
      refresh_token: xero.refresh_token
    }

    resp = Faraday.post(xero_token_endpoint) do |req|
      req.headers['Authorization'] = 'Basic ' + Base64.strict_encode64(ENV['xero_api_client_id'] + ':' + ENV['xero_api_client_secret'])
      req.headers['Content-Type'] = 'application/x-www-form-urlencoded'
      req.body = URI.encode_www_form(refresh_request_body_hash)
    end

    if resp.status == 200
      resp_hash = JSON.parse(resp.body)
      xero.access_token = resp_hash['access_token']
      xero.refresh_token = resp_hash['refresh_token']
      xero.expires = system_time + resp_hash['expires_in']
      xero.extras = resp_hash
      xero.save
    end
  end

  def default_account_code(account_type)
    accounts = Store::Accounting.where(company_id: current_user.current_company, name: provider, category: 'account_defaults').pluck(:blob).first
    account_type == "ACCREC" ? accounts['defaultSalesAccount']['Code'] : accounts['defaultPurchaseAccount']['Code']
  end

  def line_items(order, account_type)
    account_code = default_account_code(account_type)
    items = []
    order.order_items.each do |item|
      prod = Product.find(item.product_id)
      items.push({
        "Description": "#{prod.title} #{item.product_variant.description}",
        "Quantity": item.quantity,
        "UnitAmount": item.gross_price,
        "ItemCode":  account_type == "ACCREC" ? (prod.accounting_item['code'] || "") : "",
        "AccountCode": account_code,
      })
    end
    if order.delivery_fee['gross_price'].present? && order.delivery_fee['gross_price'] > 0
      items.push({
        "Description": "Delivery Fee",
        "Quantity": 1,
        "UnitAmount": order.delivery_fee['gross_price'],
        "ItemCode": "",
        "AccountCode": account_code,
      })
    end
    items
  end

  def credit_note_items(credit_note ,account_type)
    type = account_type == "ACCRECCREDIT" ? "purchases" : "sales"
    account_code = default_account_code(type)
    credit_note.credit_note_items.map do |item|
      prod = Product.find(item.product_id)
      {
        "Description": item.description || "",
        "Quantity": item.quantity,
        "ItemCode": prod.accounting_item['code'] || "",
        "UnitAmount": item.gross_price,
        "AccountCode": account_code,
      }
    end
  end

end
