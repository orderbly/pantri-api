module Integrations::Webable
  extend ActiveSupport::Concern

  def close_html
    '<!DOCTYPE html>'\
    '<html>'\
    '<body onload="closeme()">'\
    '</body>'\
    '<script>'\
    'function closeme() {'\
    'window.close();'\
    '}'\
    '</script>'\
    '</html>'
  end
end
