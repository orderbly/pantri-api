module Integrations::Sageable
  extend ActiveSupport::Concern
  def provider
    'sage'
  end

  def authorize_url(company)
    auth_url = URI.parse(authorize_url)
    auth_url.query = URI.encode_www_form(
      response_type: 'code',
      companyId: company.integrations.find_by(name: 'sage', company_id: company.id).extras.company_id,
    )
    auth_url.to_s
  end

  private

  def credentials(company_id)
    Integration.find_by(company_id: company_id, name: provider)
  end

  def tenant
    Store::Accounting.where(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'tenant').pluck(:blob).first
  end
end
