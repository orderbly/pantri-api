module Integrations::Tradegeckoable
  extend ActiveSupport::Concern

  def tradegrecko_error(exception)
    render json: {type: 'error', messsage: exception.message}, status: 503
  end

  def provider
    'tradegecko'
  end

  def client_id
    ENV['TRADEGECKO_CLIENT_ID']
  end

  def client_secret
    ENV['TRADEGECKO_CLIENT_SECRET']
  end

  def redirect_url
    ENV['TRADEGECKO_URI']
  end

  def authorize_url
    "https://api.tradegecko.com/oauth/authorize?response_type=code&client_id=#{client_id}&redirect_uri=#{redirect_url}"
  end

  def xero_token_endpoint
    'https://identity.xero.com/connect/token'
  end

  def scope
    'openid profile email files accounting.transactions accounting.transactions.read accounting.reports.read accounting.journals.read accounting.settings accounting.settings.read accounting.contacts accounting.contacts.read accounting.attachments accounting.attachments.read offline_access'
  end

  def build_authorize_url
    auth_url = URI.parse(authorize_url)
    auth_url.to_s
  end

end