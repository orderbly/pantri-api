class Integrations::Xero::ContactsController < ApplicationController
  before_action :authorize_access_request!

  rescue_from XeroError, with: :xero_error

  include Userable
  include Integrations::Xeroable

  def index
    company = Company.find_by(uuid: params[:company_uuid])
    contacts = Store::Accounting.where(company_id: company.id, name: provider, category: 'contacts').pluck(:blob)
    render json: contacts.flatten
  end

  def savecontact
    acc = Order.where(id: contact_params[:order_id]).pluck(:accounting).first
    (render json: {errors: "contact already associated with order"}, status: 400 and return) if acc.key?(:contact_id)
    resp = create_draft_invoice(contact_params[:order_id], contact_params[:contact_id])
    render json: :ok
  end

  def save_credit_note_contact
    acc = CreditNote.where(id: contact_params[:credit_note_id]).pluck(:accounting).first
    (render json: {errors: "contact already associated with order"}, status: 400 and return) if acc.key?(:contact_id)
    resp = create_credit_note(contact_params[:credit_note_id], contact_params[:contact_id])
    render json: :ok
  end

  private

  # Only allow a trusted parameter "white list" through.
  def contact_params
    params.require(:contact).permit(:order_id, :contact_id, :credit_note_id)
  end

end
