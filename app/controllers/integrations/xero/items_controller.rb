class Integrations::Xero::ItemsController < ApplicationController
  before_action :authorize_access_request!

  include Userable
  include Integrations::Xeroable

  def index
    if Company.find(current_user.current_company).accounting_stores.present?
      accounts = Store::Accounting.where(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'items').pluck(:blob)
    else
      accounts = []
    end
    render json: accounts.flatten
  end

  # def savedefaults
  #   Store::Accounting.where(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'account_defaults').update_all(blob: account_params)
  #   render json: :ok
  # end

  private

  # Only allow a trusted parameter "white list" through.
  # def account_params
  #   params.require(:account).permit(defaultSalesAccount: {}, defaultPurchaseAccount: {})
  # end

end
