class Integrations::Xero::ConfigurationsController < ApplicationController
  before_action :authorize_access_request!

  rescue_from XeroError, with: :xero_error

  include Userable
  include Integrations::Xeroable

  def index
    root = Store::Accounting.find_by(company_id: current_user.current_company, name: provider, category: 'root')
    if root.blank?
      render json: []
      return
    end

    tenants_count = root.children.count
    organisation_count = Store::Accounting.where(company_id: current_user.current_company, name: provider, category: 'organisations').pluck(:blob).map{|x| x.length}.sum
    accounts_count = Store::Accounting.where(company_id: current_user.current_company, name: provider, category: 'accounts').pluck(:blob).map{|x| x.length}.sum
    contacts_count = Store::Accounting.where(company_id: current_user.current_company, name: provider, category: 'contacts').pluck(:blob).map{|x| x.length}.sum

    render json: [{name: 'Tenants', count: tenants_count}, {name: 'Organisations', count: organisation_count}, {name: 'Accounts', 'count': accounts_count}, {name: 'Contacts', count: contacts_count}]
  end

  def sync
    clear_xero_store
    fetch_tenants
    fetch_organisations
    fetch_accounts
    fetch_contacts
    fetch_items
    create_defaults
    render json: :ok
  end

  private

  def clear_xero_store
    Store::Accounting.where(company_id: current_user.current_company, name: provider).destroy_all
    Store::Accounting.create(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'root')
  end

  def fetch_tenants
    xero = credentials(current_user.current_company)
    refresh_token(xero) if token_expired?(xero)

    resp = Faraday.get('https://api.xero.com/connections') do |req|
      req.headers['Authorization'] = 'Bearer ' + xero.access_token
      req.headers['Accept'] = 'application/json'
      req.headers['Content-Type'] = 'application/json'
    end
    tenants = JSON.parse(resp.body)

    root = Store::Accounting.find_by(company_id: current_user.current_company, name: provider, category: 'root')
    for tenant in tenants do
      ten_store = Store::Accounting.create(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'tenant', blob: tenant)
      root.children.append(ten_store)
    end
  end

  def fetch_organisations
    tenants = Store::Accounting.find_by(company_id: current_user.current_company, name: provider, category: 'root').children
    for tenant in tenants do
      resp = xero_api_get('https://api.xero.com/api.xro/2.0/Organisation', current_user.current_company, tenant.blob['tenantId'])
      organisations = JSON.parse(resp.body)
      org_store = Store::Accounting.create(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'organisations', blob: organisations['Organisations'])
      tenant.children.append(org_store)
    end
  end

  def fetch_accounts
    tenants = Store::Accounting.find_by(company_id: current_user.current_company, name: provider, category: 'root').children
    for tenant in tenants do
      resp = xero_api_get('https://api.xero.com/api.xro/2.0/Accounts', current_user.current_company, tenant.blob['tenantId'])
      accounts = JSON.parse(resp.body)
      acc_store = Store::Accounting.create(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'accounts', blob: accounts['Accounts'])
      tenant.children.append(acc_store)
    end
  end

  def fetch_contacts
    tenants = Store::Accounting.find_by(company_id: current_user.current_company, name: provider, category: 'root').children
    for tenant in tenants do
      resp = xero_api_get('https://api.xero.com/api.xro/2.0/Contacts', current_user.current_company, tenant.blob['tenantId'])
      contacts = JSON.parse(resp.body)
      con_store = Store::Accounting.create(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'contacts', blob: contacts['Contacts'])
      tenant.children.append(con_store)
    end
  end

  def fetch_items
    tenants = Store::Accounting.find_by(company_id: current_user.current_company, name: provider, category: 'root').children
    for tenant in tenants do
      resp = xero_api_get('https://api.xero.com/api.xro/2.0/Items', current_user.current_company, tenant.blob['tenantId'])
      items = JSON.parse(resp.body)
      con_store = Store::Accounting.create(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'items', blob: items['Items'])
      tenant.children.append(con_store)
    end
  end

  def create_defaults
    Store::Accounting.where(company_id: current_user.current_company, name: provider, category: 'account_defaults').destroy_all
    Store::Accounting.create(user_id: current_user.id, company_id: current_user.current_company, name: provider, category: 'account_defaults')
  end

end
