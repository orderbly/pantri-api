class Integrations::Xero::AuthController < ApplicationController
  before_action :authorize_access_request!, only: [:authorize_uri]

  include Userable
  include Integrations::Webable
  include Integrations::Xeroable

  def create
    exchange = request.query_parameters

    exchange_request_body_hash = {
      grant_type: 'authorization_code',
      code: exchange['code'],
      redirect_uri: redirect_url
    }

    resp = Faraday.post(xero_token_endpoint) do |req|
      req.headers['Authorization'] = 'Basic ' + Base64.strict_encode64(ENV['xero_api_client_id'] + ':' + ENV['xero_api_client_secret'])
      req.headers['Content-Type'] = 'application/x-www-form-urlencoded'
      req.body = URI.encode_www_form(exchange_request_body_hash)
    end

    user = User.find(exchange['state'])

    if resp.status == 200
      resp_hash = JSON.parse(resp.body)
      system_time = Time.now().to_i
      Integration.where(company_id: user.current_company, name: ['sage', 'xero']).destroy_all
      Integration.create(company_id: user.current_company, name: provider, access_token: resp_hash['access_token'],
        refresh_token: resp_hash['refresh_token'], expires: system_time + resp_hash['expires_in'], extras: resp_hash)
    end
    Pusher.trigger('integrations', "company_#{user.current_company}",  true)
    render html: "#{close_html}".html_safe
  end
  
  def disconnect
    company = Company.find_by(uuid: params[:company_uuid])
    Integration.where(company_id: company.id, name: provider).destroy_all
    Store::Accounting.where(company_id: company.id, name: provider).destroy_all
    
    Pusher.trigger('integrations', "company_#{user.current_company}",  true)
    render json: {type: 'success'}
  end

  def authorize_uri
    render json: build_authorize_url(current_user)
  end

end
