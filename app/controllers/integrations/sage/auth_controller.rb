class Integrations::Sage::AuthController < ApplicationController
  before_action :authorize_access_request!

  include Userable
  include Integrations::Sageable


  def create
    base64_details = params[:base64_sage_details]
    current_company = params[:current_company]
    sage_company_id = params[:company_id]

    Integration.where(company_id: current_company, name: provider).destroy_all
    Integration.create(
      company_id: params[:current_company],
      name: provider, access_token: nil,
      refresh_token: nil,
      expires: nil,
      extras: {
        basic_token: params[:base64_sage_details],
        company: sage_company_id
      }
    )
    Integrations::SageService::Configurations::ConfigureSageIntegration.new(current_company, current_user.id).call
  end

  def disconnect
    company = Company.find_by(uuid: params[:company_uuid])
    Integration.where(company_id: company.id, name: provider).destroy_all
    Store::Accounting.where(company_id: company.id, name: provider).destroy_all

    render json: {type: 'success'}
  end

  def authorize_uri
    render json: build_authorize_url(current_user)
  end

end
