class Integrations::Sage::AccountsController < ApplicationController
  before_action :authorize_access_request!

  include Userable
  include Integrations::Sageable

  def index
    company = Company.find_by(uuid: params[:company_uuid])
    accounts = Store::Accounting.find_by(company_id: company.id, name: provider, category: 'accounts').blob
    render json: {accounts: accounts.flatten, defaults: (Store::Accounting.find_by(company_id: company.id, name: provider, category: 'account_defaults')&.blob) }
  end

  def savedefaults
    company = Company.find_by(uuid: params[:company_uuid])
    Store::Accounting.where(company_id: company.id, name: provider, category: 'account_defaults').delete_all
    parent_store =  Store::Accounting.find_by(company_id: company.id, name: provider, category: 'root').id
    Store::Accounting.create(user_id: current_user.id, company_id: company.id, name: provider, category: 'account_defaults', parent_id: parent_store, blob: account_params )
  
    render json: :ok
  end

  private

  # Only allow a trusted parameter "white list" through.
  def account_params
    params.require(:account).permit(defaultSalesAccount: {}, defaultPurchaseAccount: {})
  end

end
