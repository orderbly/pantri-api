class Integrations::Sage::ItemsController < ApplicationController
  before_action :authorize_access_request!

  include Userable
  include Integrations::Sageable

  def index
    company = Company.find_by(uuid: params[:company_uuid])
    items = Store::Accounting.find_by(company_id: company.id, name: provider, category: 'items')&.blob&.flatten
    render json: items
  end

  private

  # Only allow a trusted parameter "white list" through.
  def account_params
    params.require(:account).permit(defaultSalesAccount: {}, defaultPurchaseAccount: {})
  end

end
