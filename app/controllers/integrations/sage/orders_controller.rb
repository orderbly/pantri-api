class Integrations::Sage::OrdersController < ApplicationController
  before_action :authorize_access_request!

  include Userable
  include Integrations::Sageable


  def purchase_order
    integration = Integrations::SageService::Orders::PurchaseOrder.new(order_id: current_order.id, sage_one_supplier_id: params[:sage_one_supplier_id]).call
    if integration.type == "success" 
      render json: integration.obj, status: :created
    else
      render json: integration.obj, status: :unprocessable_entity
    end
  end

  def sales_order
    integration = Integrations::SageService::Orders::SalesOrder.new(order_id: current_order.id, sage_one_customer_id: params[:sage_one_customer_id]).call
    if integration.type == "success" 
      render json: integration.obj, status: :created
    else
      render json: integration.obj, status: :unprocessable_entity
    end
  end

  def purchase_return
    integration = Integrations::SageService::CreditNotes::PurchaseReturn.new(credit_note: current_credit_note, sage_one_supplier_id: params[:sage_one_supplier_id]).call
    if integration.type == "success" 
      render json: integration.obj, status: :created
    else
      render json: integration.obj, status: :unprocessable_entity
    end
  end

  def sales_return
    integration = Integrations::SageService::CreditNotes::SalesReturn.new(credit_note: current_credit_note, sage_one_customer_id: params[:sage_one_customer_id]).call
    if integration.type == "success" 
      render json: integration.obj, status: :created
    else
      render json: integration.obj, status: :unprocessable_entity
    end
  end

  private

  def current_credit_note
    CreditNote.find_by(uuid: params[:credit_note_uuid])
  end

  def current_order
    Order.find_by(uuid: params[:order_uuid])
  end

end
