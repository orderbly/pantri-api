class Integrations::Sage::ContactsController < ApplicationController
  before_action :authorize_access_request!

  include Userable
  include Integrations::Sageable

  def suppliers
    if current_company && current_user.companies.find_by(id: current_company.id)
      suppliers = Store::Accounting.where(company_id: current_company, name: provider, category: 'suppliers').pluck(:blob)
      render json: suppliers.flatten
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  def customers
    if current_company && current_user.companies.find_by(id: current_company.id)
      suppliers = Store::Accounting.where(company_id: current_company, name: provider, category: 'customers').pluck(:blob)
      render json: suppliers.flatten
    else
      render json: {}, status: :unprocessable_entity
    end
  end

  def savecontact
    acc = Order.where(id: contact_params[:order_id]).pluck(:accounting).first
    (render json: {errors: "contact already associated with order"}, status: 400 and return) if acc.key?(:contact_id)
    Integrations::SageService::Orders::CreateInvoice.new(order_id: contact_params[:order_id], contact_id: contact_params[:contact_id])
    # resp = create_draft_invoice(contact_params[:order_id], contact_params[:contact_id])
    render json: :ok
  end

  def save_credit_note_contact
    acc = CreditNote.where(id: contact_params[:credit_note_id]).pluck(:accounting).first
    (render json: {errors: "contact already associated with order"}, status: 400 and return) if acc.key?(:contact_id)
    resp = create_credit_note(contact_params[:credit_note_id], contact_params[:contact_id])
    render json: :ok
  end

  private

  def current_company
    Company.find_by(uuid: params[:company_uuid])
  end

  # Only allow a trusted parameter "white list" through.
  def contact_params
    params.require(:contact).permit(:order_id, :contact_id, :credit_note_id, :company_uuid)
  end

end
