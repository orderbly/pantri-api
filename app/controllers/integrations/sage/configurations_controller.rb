class Integrations::Sage::ConfigurationsController < ApplicationController
  before_action :authorize_access_request!

  include Userable
  include Integrations::Sageable

  def index
    company = Company.find_by(uuid: params[:company_uuid])
    root = Store::Accounting.find_by(user_id: current_user.id, company_id: company.id, name: provider, category: 'root')
    
    render json: [] if root.blank?

    accounts_count = Store::Accounting.where(user_id: current_user.id, company_id: company.id, name: provider, category: 'accounts').pluck(:blob).map{|x| x.length}.sum
    suppliers_count = Store::Accounting.where(user_id: current_user.id, company_id: company.id, name: provider, category: 'suppliers').pluck(:blob).map{|x| x.length}.sum
    customers_count = Store::Accounting.where(user_id: current_user.id, company_id: company.id, name: provider, category: 'customers').pluck(:blob).map{|x| x.length}.sum
    items_count = Store::Accounting.where(user_id: current_user.id, company_id: company.id, name: provider, category: 'items').pluck(:blob).map{|x| x.length}.sum

    render json: [
      {name: 'Accounts', count: accounts_count},
      {name: 'Suppliers', count: suppliers_count},
      {name: 'Customers', count: customers_count},
      {name: 'Items', count: items_count}
    ]
  end

  def sync
    company = Company.find_by(uuid: params[:company_uuid])
    Store::Accounting.where(company_id: company.id, name: provider).destroy_all
    Integrations::SageService::Configurations::ConfigureSageIntegration.new(company.id, current_user.id).call
    accounts_count = Store::Accounting.where(user_id: current_user.id, company_id: company.id, name: provider, category: 'accounts').pluck(:blob).map{|x| x.length}.sum
    suppliers_count = Store::Accounting.where(user_id: current_user.id, company_id: company.id, name: provider, category: 'suppliers').pluck(:blob).map{|x| x.length}.sum
    customers_count = Store::Accounting.where(user_id: current_user.id, company_id: company.id, name: provider, category: 'customers').pluck(:blob).map{|x| x.length}.sum
    items_count = Store::Accounting.where(user_id: current_user.id, company_id: company.id, name: provider, category: 'items').pluck(:blob).map{|x| x.length}.sum

    render json: [
      {name: 'Accounts', count: accounts_count},
      {name: 'Suppliers', count: suppliers_count},
      {name: 'Customers', count: customers_count},
      {name: 'Items', count: items_count}
    ]
  end

end
