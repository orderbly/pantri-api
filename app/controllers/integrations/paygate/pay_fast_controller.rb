class Integrations::Paygate::PayFastController < ApplicationController
  # before_action :authorize_access_request!, only: [:details, :paid]

  def details
    payfast_details = {
        merchant_id: Rails.configuration.offsite_payments['payfast']['merchant_id'],
        merchant_key: Rails.configuration.offsite_payments['payfast']['merchant_key'],
        login: Rails.configuration.offsite_payments['payfast']['login']
    }

    render json: @payfast_details
  end

  def paid
    create_notification
    if @notification.acknowledge
      if @notification.complete?
        if (params['custom_str2'] == "account_sale") 
          supplier_id = params['custom_str1'].to_i
          customer_id  = params['custom_str3'].to_i
          total = params["amount_gross"].to_f
          PaymentService::GeneralCompanyPayment.new(customer_id, supplier_id, total).call
        else
          Payment.pay_order(params['custom_int1'], params['amount_gross'])
          Order.where(id: params['custom_int1'].to_i).update_all(status: 3, is_paid: 0)
          Pusher.trigger('payment_update', "#{params['custom_str3']}_order_#{params['custom_int1']}", {order_id: params['custom_int1'].to_i, status: 'payment_complete'})
          render nothing: true
        end
      else
        # Go back to accepted -- client still needs to pay when payment failed
        Order.where(id: params['custom_int1'].to_i).update_all(status: 2)
        Pusher.trigger('payment_update', "order_#{params['custom_int1']}", {order_id: params['custom_int1'].to_i, status: @notification.params['payment_status'], usignature: payload[:usignature]})
        render nothing: true
      end
    else
      head :bad_request
    end
  end

  def success
  end

  def fail
  end

private

  def create_notification
    @notification = OffsitePayments.integration(:pay_fast).notification(request.raw_post, secret: Rails.configuration.offsite_payments['payfast']['password'])
  end
end