class Integrations::Ozow::OzowController < ApplicationController
  # before_action :authorize_access_request!, only: [:details, :paid]

  def paid
    Rails.logger.info "------- Params ----------- #{params}"
    case params["Status"]

    when "Cancelled"
      Pusher.trigger('payment_update',"#{params['Optional4']}_order_#{params['TransactionReference']}", {type: "error", status: "Your payment has been cancelled 🙅‍♀️. Please try again or contact support for assistance."})
      render json: { status: "cancelled", message: "Order has been cancelled"}

    when "Abandoned"
      Pusher.trigger('payment_update',"#{params['Optional4']}_order_#{params['TransactionReference']}", {type: "error", status: "Your payment has been abandoned 🤷‍♀️. Please try again or contact support for assistance."})
      render json: { status: "abandoned", message: "Order has been abandoned"}

    when "PendingInvestigation"
      Pusher.trigger('payment_update',"#{params['Optional4']}_order_#{params['TransactionReference']}", {type: "error", status: "Your payment is pending investigation 🕵️‍♀️. Please contact support for further assistance."})
      render json: { status: "pending investigation", message: "Order is pending investigation"}

    when "Error"
      Pusher.trigger('payment_update',"#{params['Optional4']}_order_#{params['TransactionReference']}", {type: "error", status: "An error has occured 🤦‍♀️. Please try again or contact support for assistance."})
      render json: { status: "error", message: "An error has occured"}

    when "Pending"
      Pusher.trigger('payment_update',"#{params['Optional4']}_order_#{params['TransactionReference']}", {type: "error", status: "Your payment is pending 💆. Please check back later to confirm the status of your order or contact support for assistance."})
      render json: { status: "pending", message: "Order is pending"}

    when "Complete"
      complete_transaction = Integrations::OzowService::CompleteTransaction.new(params).call
      Rails.logger.info "------- Complete Transaction ----------- #{complete_transaction}"
      render json: complete_transaction
    else
      Pusher.trigger('payment_update',"#{params['Optional4']}_order_#{params['TransactionReference']}", {type: "error", status: "An error has occured. We are looking into the matter."})
      render nothing: true
    end
  end

  def success

  end

  def fail

end

private

  def create_notification
    @notification = OffsitePayments.integration(:pay_fast).notification(request.raw_post, secret: Rails.configuration.offsite_payments['payfast']['password'])
  end
end