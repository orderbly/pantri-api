class Integrations::Tradegecko::WebhooksController < ApplicationController
  include Userable
  include Integrations::Tradegeckoable

  def payments

    order = Order.find_by("inventory @> ?", {id: params[:order_id]}.to_json)
    if order.present?
      @integration = Integration.find_by(company_id: order.supplier_id, name: 'tradegecko')
      @gecko = Gecko::Client.new(ENV['TRADEGECKO_CLIENT_ID'], ENV['TRADEGECKO_CLIENT_SECRET'])
      refresh = @gecko.authorize_from_refresh_token(@integration.refresh_token)
      @integration.update(access_token: refresh.token, refresh_token: refresh.refresh_token)

      payment_request = Faraday.get(params["resource_url"]) do |req|
        req.headers['Authorization'] = "Bearer #{@integration.access_token}" 
        req.headers['Content-Type'] = 'application/json'
      end

      payment = JSON.parse(payment_request.body)["payment"]

      if order.status != "paid" && payment["amount"].to_f <= order.balance
        PaymentService::PayOrder.new(order_id: order.id, value: payment["amount"].to_f, transaction_id: "TradeGecko: #{params[:object_id]}", supplier_id: order.supplier_id, customer_id: order.customer_id).call
        Pusher.trigger('orders', "company_#{order.supplier_id}_webhook_order_updates", { customer: { title: order.customer.title}, order_number: order.order_number }.to_json)
        Pusher.trigger('orders', "company_#{order.customer_id}_webhook_order_updates", { customer: { title: order.customer.title}, order_number: order.order_number }.to_json)
        render json: {status: true}
      else
        render json: {status: false}
      end
    else
      render json: {status: false}
    end
  end

  def orders
    order = Order.find_by("inventory @> ?", {id: params[:object_id]}.to_json)
    if order.present?
      if order.status != 'pending'
        render json: {message: 'complete'}
      else
        order.update(status: 2)
        Pusher.trigger('orders', "company_#{order.supplier_id}_webhook_order_updates", { customer: { title: order.customer.title}, order_number: order.order_number }.to_json)
        Pusher.trigger('orders', "company_#{order.customer_id}_webhook_order_updates", { customer: { title: order.customer.title}, order_number: order.order_number }.to_json)
        render json: {message: 'complete'}
      end
    else
      render json: {message: 'complete'}
    end
  end

  def current_company
    Company.find_by(uuid: params[:company_uuid])
  end
  
end
  