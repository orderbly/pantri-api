class Integrations::Tradegecko::ProductsController < ApplicationController
  before_action :authorize_access_request!

  include Userable
  include Integrations::Tradegeckoable

  def retrieve_products
    if current_company && current_user.companies.find_by(id: current_company.id)
      begin
        products = Integrations::TradegeckoService::Products.new(current_company.id).call
        if products
          render json: {status: true}
        else
          raise "Products could not be imported"
        end
      rescue
        render json: {status: false, message: "Products could not be imported" }, status: :unprocessable_entity
      end
    else
      render json: {status: false, message: "Products could not be imported" }, status: :unprocessable_entity
    end
  end

  def current_company
    Company.find_by(uuid: params[:company_uuid])
  end

end
