class Integrations::Tradegecko::ConfigurationsController < ApplicationController
  before_action :authorize_access_request!

  include Userable
  include Integrations::Tradegeckoable

  def sync

  end

  def disconnect
    if current_company && current_user.companies.find_by(id: current_company.id)
      Integration.where(name: 'tradegecko', company_id: current_company.id).destroy_all
      Store::Inventory.where(name: 'tradegecko', company_id: current_company.id).destroy_all
      current_company.update(inventory_blob: {})
      
      Pusher.trigger('integrations', "company_#{current_company.id}",  true)
      render json: true
    else
      render json: {}, status: :unprocessable_entity
    end    
  end

  private

  def current_company
    Company.find_by(uuid: params[:company_uuid])
  end

  # Only allow a trusted parameter "white list" through.
  def contact_params
    params.require(:contact).permit(:order_id, :contact_id, :credit_note_id, :company_uuid)
  end
  
  end
  