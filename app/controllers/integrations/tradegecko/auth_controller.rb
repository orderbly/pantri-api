class Integrations::Tradegecko::AuthController < ApplicationController

  include Integrations::Tradegeckoable
  include Integrations::Webable

  def create
    begin
      pending_integration = PendingIntegration.find_by(name: 'tradegecko')
      company_id = pending_integration.company_id

      url = "https://api.tradegecko.com/oauth/token"
      data = { 
        code: params[:code],
        redirect_uri: ENV['TRADEGECKO_URI'],
        client_id: ENV['TRADEGECKO_CLIENT_ID'],
        client_secret: ENV['TRADEGECKO_CLIENT_SECRET'],
        grant_type:'authorization_code'
      }
      
      resp = Faraday.post(url, data)
      response = JSON.parse(resp.body)
      Rails.logger.info response

      Integration.create(
        company_id: company_id,
        name: 'tradegecko',
        access_token: response['access_token'],
        refresh_token: response['refresh_token'],
        expires: response['expires_at'],
        extras: response
      )
      Rails.logger.info 'access token'
      Rails.logger.info response
      config = Integrations::TradegeckoService::ConfigureTradegecko.new(company_id).call
      if Integration.find_by(company_id: company_id, name: 'tradegecko') && config
        Pusher.trigger('integrations', "company_#{company_id}",  true)
      else 
        integration = Integration.find_by(company_id: company_id, name: 'tradegecko')
        integration.delete if integration.present?
        Pusher.trigger('integrations', "company_#{company_id}",  false)
      end
      render html: "#{close_html}".html_safe
    rescue
      integration = Integration.find_by(company_id: company_id, name: 'tradegecko')
      integration.delete if integration.present?
      Pusher.trigger('integrations', "company_#{company_id}",  false)
      render html: "#{close_html}".html_safe
    end
  end

  def authorize_uri
    if PendingIntegration.find_by(name: 'tradegecko').present?
      render json: {
        message: 'Tradegecko integration is currently not available. Please try again in a few minutes.'
      }, status: :unprocessable_entity
    else
    PendingIntegration.create(
      company_id: params[:company_id],
      name: 'tradegecko'
    )
    render json: build_authorize_url
    end
  end

end
