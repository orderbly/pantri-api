class Integrations::Tradegecko::OrdersController < ApplicationController
    before_action :authorize_access_request!
  
    include Userable
    include Integrations::Tradegeckoable
  
    def sales_order
      if current_company && current_user.companies.find_by(id: current_company.id)
        begin
        products = Integrations::TradegeckoService::SalesOrder.new(
          company_id: current_company.id,
          order_uuid: params[:order_uuid],
          tradegecko_company_id: params[:tradegecko_company_id],
          tradegrecko_address_id: params[:tradegecko_address_id]
        ).call

        if products 
          render json: { status: true }
        else
          render json: {}, status: :unprocessable_entity
        end
      rescue
        render json: {}, status: :unprocessable_entity
      end
      else
        render json: {}, status: :unprocessable_entity
      end
    end
  
    def current_company
      Company.find_by(uuid: params[:company_uuid])
    end
  
  end
  