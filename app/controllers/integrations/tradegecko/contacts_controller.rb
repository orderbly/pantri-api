class Integrations::Tradegecko::ContactsController < ApplicationController
    before_action :authorize_access_request!
  
    include Userable
    include Integrations::Tradegeckoable
  
    def all_contacts
        if current_company && current_user.companies.find_by(id: current_company.id)
        contacts = Store::Inventory.find_by(company_id: current_company.id, name: 'tradegecko', category: 'contacts').blob
        render json: JSON.parse(contacts)
      else
        render json: {}, status: :unprocessable_entity
      end
    end

    # POST / create_contact/:order_uuid
    def create_company
      if current_company && current_user.companies.find_by(id: current_company.id).present?
        new_contact = Integrations::TradegeckoService::NewContact.new(params[:order_uuid]).call
        if new_contact
          render json: Store::Inventory.find_by(company_id: current_company.id, name: 'tradegecko', category: 'contacts').blob
        else
          render json: {}, status: :unprocessable_entity
        end
      else
        render json: {}, status: :unprocessable_entity
      end
    end

    def current_company
      Company.find_by(uuid: params[:company_uuid])
    end
  
  end
  