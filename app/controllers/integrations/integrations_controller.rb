class Integrations::IntegrationsController < ApplicationController
  before_action :authorize_access_request!, only: [ :installed ]

  # GET /integrations/installed
  def installed
    render json: installed_integrations
  end

  private

  def installed_integrations
    [ 
      {
        title: 'Xero',
        category: 'accounting',
        integration: 'xero',
        status: (Integration.where(company_id: current_user.current_company, name: 'xero').count > 0) ? 'Enabled' : 'Disabled'
      },
      {
        title: 'Sage One',
        category: 'accounting',
        integration: 'sage',
        status: (Integration.where(company_id: current_user.current_company, name: 'sage').count > 0) ? 'Enabled' : 'Disabled'
      },
      {
        title: 'OptimoRoute',
        category: 'delivery',
        integration: 'optimo',
        status: (Integration.where(company_id: current_user.current_company, name: 'optimo').count > 0) ? 'Enabled' : 'Disabled'
      },
      {
        title: 'TradeGecko',
        category: 'inventory',
        integration: 'tradegecko',
        status: (Integration.where(company_id: current_user.current_company, name: 'tradegecko').count > 0) ? 'Enabled' : 'Disabled'
      }
    ]
  end

  def current_user
    begin
      current_user ||= User.find(payload['user_id'])
    rescue
      return false
    end
  end

end