class Integrations::Optimo::OptimoController < ApplicationController
  before_action :authorize_access_request!
  include Userable

  def create
    company = Company.find_by(uuid: params[:company_uuid])

    Integration.where(company_id: company.id, name: 'optimo').destroy_all
    Integration.create(
        company_id: company.id,
        name: 'optimo',
        access_token: params[:api_key],
        refresh_token: nil,
        expires: nil,
    )
    render json: {type: 'success'}
  end
  
  def disconnect
    company = Company.find_by(uuid: params[:company_uuid])
    Integration.where(company_id: company.id, name: 'optimo').destroy_all

    render json: {type: 'success'}
  end

  def create_delivery
    no_permission unless current_user.companies.find_by(id: current_company.id)
    delivery = Integrations::OptimoRoute::CreateDelivery.new(params[:order_uuid]).call
    if delivery
      render json: {type: 'success'}
    else
      render json: {
        message: 'Your order could not be integrated into OptimoRoute. Please try again or contact support should the problem persist'
      }, status: :unprocessable_entity
    end
  end

  def current_company
    if Company.find_by(uuid: params[:company_uuid]) 
      @company = Company.find_by(uuid: params[:company_uuid]) 
    else
      no_permission
    end
  end

end