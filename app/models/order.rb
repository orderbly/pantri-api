# == Schema Information
#
# Table name: orders
#
#  id            :bigint           not null, primary key
#  customer_id   :integer
#  supplier_id   :integer
#  delivery_date :datetime
#  notes         :text
#  order_number  :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Order < ApplicationRecord
  # after_commit :send_mail, on: :create
  belongs_to :supplier, class_name: 'Company', foreign_key: 'supplier_id'
  belongs_to :customer, class_name: 'Company', foreign_key: 'customer_id'
  belongs_to :delivery_address, class_name: 'Address', foreign_key: 'delivery_address_id'
  # belongs_to :parent_credit_note, class_name: 'CreditNote', foreign_key: 'parent_credit_note_id'
  has_many :credit_note_claims
  has_many :credit_notes, :through => :credit_note_claims
  has_many :payment_allocations
  has_many :payments, :through => :payment_allocations
  has_many :delivery_allocations
  has_many :delivery_assignees, source: :user, :through => :delivery_allocations
  belongs_to :delivery_user, class_name: 'User', foreign_key: 'delivery_user_id', optional: true

  belongs_to :order_schedule, optional: true

  has_many :order_items
  has_many :addresses, :through => :supplier
  has_many :comments
  has_one :credit_note
  
  accepts_nested_attributes_for :order_items
  enum status: [ :pending, :declined, :accepted, :paid, :cancelled, :partially_paid ]
  enum delivery_status: [ :order_pending, :in_preparation, :delivery_pending, :delivery_in_progress, :delivered, :order_cancelled]
  enum is_paid: [ :true, :false, :partial ]
  
  has_one_attached :signature
  validates :customer, :supplier, presence: true
  scope :by_period, -> (start_date, end_date) { where(:created_at => start_date...end_date) }
  scope :for_date, -> (date) { 
    where("delivery_date >= ?", date.beginning_of_day)
    .where("delivery_date < ?", date.end_of_day) 
  }

  def self.isCustomer(order_id, current_user_id)
    order = Order.find(order_id)
    companies = []
    user_companies = User.find(current_user_id).members.map{|m| companies.push(m.company_id)}
    isCustomer = companies.include? order.customer.id
  end

  def self.isSupplier(order_id, current_user_id)
    order = Order.find(order_id)
    companies = []
    user_companies = User.find(current_user_id).members.map{|m| companies.push(m.company_id)}
    isCustomer = companies.include? order.supplier.id
  end

  def self.account_balance(supplier_id, customer_id)
    Order.where(supplier_id: supplier_id, customer_id: customer_id, status: "accepted", is_paid: "false").pluck(:balance).compact.sum
  end

  def calculate_delivery_fee(delivery_total)
    vat_price = self.supplier.is_vat_registered ? (((delivery_total / 1.15) - delivery_total) * -1).round(2) : 0
    net_price = delivery_total - vat_price
    gross_price = delivery_total
    {"net_price"=>net_price, "vat_price"=> vat_price, "gross_price"=>gross_price}
  end

end