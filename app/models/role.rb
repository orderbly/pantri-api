# == Schema Information
#
# Table name: roles
#
#  id          :bigint           not null, primary key
#  level       :integer          default(1)
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Role < ApplicationRecord
  enum level: [ :admin, :staff, :fulfilment ]
end
