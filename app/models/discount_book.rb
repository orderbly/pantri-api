class DiscountBook < ApplicationRecord
  belongs_to :supplier, class_name: 'Company', foreign_key: 'supplier_id'
  has_many :product_variant_discounts
  has_many :discount_book_memberships
end