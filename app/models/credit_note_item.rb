class CreditNoteItem < ApplicationRecord
  belongs_to :credit_note
  belongs_to :product
  belongs_to :product_variant
  
end
