class DeliveryZone < ApplicationRecord
  belongs_to :company
  has_many :delivery_zone_areas
  has_many :delivery_zone_fees

  validates :name, presence: true, allow_blank: false
  validates :delivery_days, presence: true, allow_blank: false


  enum cut_off_day: [:same_day, :previous_day, :two_days_prior, :three_days_prior]
  enum fee_based_on: [:value, :quantity]
end
