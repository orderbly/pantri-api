# == Schema Information
#
# Table name: accounting_stores
#
# t.bigint "user_id", null: false
# t.bigint "company_id", null: false
# t.bigint "parent_id"
# t.string "name", null: false
# t.string "category"
# t.jsonb "blob", default: "{}", null: false
# t.boolean "active", default: false
# t.datetime "created_at", null: false
# t.datetime "updated_at", null: false
# t.index ["company_id"], name: "index_stores_on_company_id"
# t.index ["parent_id"], name: "index_stores_on_parent_id"
# t.index ["user_id"], name: "index_stores_on_user_id"
    
class Store::Accounting < ApplicationRecord
  self.table_name = 'accounting_stores'

  validates :company, presence: true
  validates :name, presence: true

  belongs_to :user
  belongs_to :company

  has_many :children, class_name: 'Store::Accounting', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'Store::Accounting', optional: true
end
