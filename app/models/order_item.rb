# == Schema Information
#
# Table name: order_items
#
#  id          :bigint           not null, primary key
#  company_id  :bigint
#  order_id    :bigint
#  product_id  :bigint
#  quantity    :float            default(0.0)
#  gross_price :float            default(0.0)
#  net_price   :float            default(0.0)
#  vat_price   :float            default(0.0)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :product_variant
  belongs_to :product
end
