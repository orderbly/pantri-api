class AccountLimitMembership < ApplicationRecord
  belongs_to :customer, class_name: 'Company', foreign_key: 'customer_id'
  belongs_to :supplier, class_name: 'Company', foreign_key: 'supplier_id'
end
