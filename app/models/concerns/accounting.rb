module Accounting
  extend ActiveSupport::Concern
  
  included  do
    has_many :accounting_stores, class_name: "Store::Accounting"
  end
end