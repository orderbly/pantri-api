# == Schema Information
#
# Table name: tags
#
#  id          :bigint           not null, primary key
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Tag < ApplicationRecord
  has_and_belongs_to_many :product

  belongs_to :company
end
