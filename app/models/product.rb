# == Schema Information
#
# Table name: products
#
#  id           :bigint           not null, primary key
#  company_id   :bigint
#  tax_id       :bigint
#  title        :string
#  description  :string
#  image        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  got_variants :boolean
#

# Product --> Jack Black Beer 330 ml
#   Variants --> Lager
#            --> First Light

# Product --> Filter Coffee 250 mg
#   Variants --> Hazelnut
#            --> Columbian

# Product --> Filter Coffee Hazelnut
#   Variants --> 250 mg
#            --> 500 mg

# Product --> T-Shirt
#   Variants --> small
#            --> medium


class Product < ApplicationRecord
  attr_accessor :product_variants_attributes
  has_one_attached :product_image
  before_save :save_image

  belongs_to :company
  belongs_to :tax
  
  has_and_belongs_to_many :tags
  has_many :credit_note_items
  has_many :product_variants
  accepts_nested_attributes_for :product_variants
  
  scope :by_tag, -> (tag) { joins(:tags).where('tags.description ILIKE ?', "%#{tag}%") }

  validates :company, presence: true
  validates :title, presence: true
  validates :tax, presence: true


  def save_image
    if self.product_image.attached?
      self.image = self.product_image.service.send(:object_for, self.product_image.key).public_url
    end
  end

end
