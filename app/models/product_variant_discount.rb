class ProductVariantDiscount < ApplicationRecord
  attr_accessor :primary_product
  belongs_to :discount_book
  belongs_to :product_variant

end
