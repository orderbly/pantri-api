class CreditNote < ApplicationRecord

  belongs_to :supplier, class_name: 'Company', foreign_key: 'supplier_id'
  belongs_to :customer, class_name: 'Company', foreign_key: 'customer_id'
  has_many :credit_note_claims
  has_many :orders, :through => :credit_note_claims
  belongs_to :order
  has_many :credit_note_items

  scope :by_period, -> (start_date, end_date) { where(:created_at => start_date...end_date) }
  scope :for_date, -> (date) { 
    where("created_at >= ?", date.beginning_of_day)
    .where("created_at < ?", date.end_of_day) 
  }

  def self.balance(supplier_id, customer_id)
    CreditNote.where(supplier_id: supplier_id, customer_id: customer_id).pluck(:remaining_balance).compact.sum
  end

end
