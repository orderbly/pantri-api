# == Schema Information
#
# Table name: product_variants
#
#  id                  :bigint           not null, primary key
#  product_id          :bigint
#  description         :string
#  status              :string
#  available_stock     :float            default(0.0)
#  stock               :float            default(0.0)
#  unit_amount         :float            default(0.0)
#  tax_amount          :float            default(0.0)
#  discount_percentage :float            default(0.0)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

# Product --> Jack Black Beer 330 ml
#   Variants --> Lager
#            --> First Light

# Product --> Filter Coffee 250 mg
#   Variants --> Hazelnut
#            --> Columbian

# Product --> Filter Coffee Hazelnut
#   Variants --> 250 mg
#            --> 500 mg

# Product --> T-Shirt
#   Variants --> small
#            --> medium

# pv.unit_amount * (pv.product.tax.percentage * 100)/((pv.product.tax.percentage + 1) * 100)
class ProductVariant < ApplicationRecord
  has_many :order_item
  belongs_to :product, inverse_of: :product_variants
  validates :product, presence: true
  has_many :product_variant_discounts

  def self.verify_stock(id, quantity)
    product_variant = ProductVariant.find(id.to_i)
    if product_variant.track_stock
      return product_variant.available_stock >= quantity.to_i ? true : false
    else
      return true
    end
  end

  def format_unit_amount
    self.unit_amount.to_s
  end

  def format_gross_amount
    self.gross_amount.to_s
  end
  
  def format_tax_amount
    self.tax_amount.to_s
  end

  def format_pricing
    {unit_amount: self.unit_amount.to_s, tax_amount: self.tax_amount.to_s, gross_amount: self.gross_amount.to_s, base_amount: self.base_amount.to_s, margin: self.margin.to_s }
  end

  def product_variant_balance
    return self.unit_amount + self.tax_amount - self.gross_amount
  end

end
