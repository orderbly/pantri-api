# == Schema Information
#
# Table name: companies
#
#  id          :bigint           not null, primary key
#  title       :string
#  address_one :string
#  country     :string
#  city        :string
#  postal_code :string
#  contact     :string
#  website     :string
#  description :string
#  image       :string
#  colour      :string
#  category    :string
#  supplier    :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#


class Company < ApplicationRecord
  include Accounting
  
  before_create :save_image
  after_create :save_email
  
  has_one_attached :company_image
  has_one_attached :company_cover_image
  has_one_attached :company_logo

  has_many :discount_books, foreign_key: "supplier_id"
  belongs_to :default_address, class_name: "Address", foreign_key: "default_address_id", optional: true
  has_many :discount_book_memberships, foreign_key: "customer_id"
  has_many :credit_limits, foreign_key: "customer_id"
  has_many :credit_limits, foreign_key: "supplier_id"
  has_many :custom_clients, foreign_key: "supplier_id"
  has_many :company_memberships, foreign_key: "supplier_id"
  has_many :company_memberships, foreign_key: "customer_id"
  has_many :products
  has_many :taxes
  has_many :members
  has_many :users, through: :members
  has_many :tags
  has_many :integrations
  has_many :payments
  has_many :addresses
  has_many :custom_deliveries, foreign_key: "supplier_id"
  has_many :custom_deliveries, foreign_key: "customer_id"
  has_many :delivery_fee_brackets
  has_many :delivery_zone_areas
  
  accepts_nested_attributes_for :addresses, allow_destroy: true
  validates :title, presence: true, allow_blank: false

  def save_image
    if self.company_image.attached?
      self.image = self.company_image.service.send(:object_for, self.company_image.key).public_url
    end
  end

  def save_email
    self.email_list.push(self.email) if self.email.present?
    self.save!
  end

  def private_suppliers
    CompanyMembership.where(private_customer: self.id).map {|cm| Company.find(cm.supplier_id) }.uniq
  end

  def private_customers
    CompanyMembership.where(private_supplier: self.id).map {|cm| Company.find(cm.customer_id) }.uniq
  end

  def self.create_tax(company_id)
    Tax.create([
      { category: 0, description: 'Standard Tax Rate', percentage: 0.15, company_id: company_id },
      { category: 2, description: 'No Tax', percentage: 0.0, company_id: company_id },
      { category: 1, description: 'Zero Rated Tax', percentage: 0.0, company_id: company_id }
    ])
  end

  def self.has_discount_book(customer_id)
    DiscountBook.where(customer_id: customer_id).any?
  end

end
