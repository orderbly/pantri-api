class CustomDelivery < ApplicationRecord
  belongs_to :delivery_driver, class_name: 'User', foreign_key: 'delivery_driver_id'
  belongs_to :supplier, class_name: 'Company', foreign_key: 'supplier_id'
  belongs_to :customer, class_name: 'Company', foreign_key: 'customer_id'
  belongs_to :address

  enum delivery_status: [ :in_preparation_, :in_preparation, :delivery_pending, :delivery_in_progress, :delivered]
end
