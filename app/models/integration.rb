# == Schema Information
#
# Table name: integrations
#
#    t.bigint "company_id", null: false
#    t.string "name", null: false
#    t.string "access_token"
#    t.string "refresh_token"
#    t.string "expires"
#    t.jsonb "extras", default: "{}", null: false

class Integration < ApplicationRecord
  validates :company, presence: true
  validates :name, presence: true

  belongs_to :company
end
