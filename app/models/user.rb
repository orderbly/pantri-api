# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  company_id      :bigint
#  email           :string
#  password_digest :string
#  first_name      :string
#  last_name       :string
#  username        :string
#  contact_number  :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class User < ApplicationRecord
  has_secure_password
  has_many :members
  has_many :companies, through: :members
  has_many :orders
  has_many :payments
  has_many :custom_deliveries, foreign_key: "delivery_driver_id"

  has_many :delivery_allocations
  has_many :delivery_orders, source: :order, :through => :delivery_allocations

  validates :first_name, :last_name, :email, presence: true
  validates :email, uniqueness: { case_sensitive: false }
  has_many :comments
  has_many :stores
  has_one_attached :avatar_image

  def generate_password_token!
    begin
      self.reset_password_token = SecureRandom.urlsafe_base64
    end while User.exists?(reset_password_token: self.reset_password_token)
    self.reset_password_token_expires_at = 1.day.from_now
    save!
  end

  def clear_password_token!
    self.reset_password_token = nil
    self.reset_password_token_expires_at = nil
    save!
  end

  def self.create_user_for_google(data)  
      User.create(
        first_name: data['given_name'],
        last_name: data['family_name'],
        avatar: data['picture'],
        email: data['email'],
        username: data['name'],
        password: data['sub']
      )    
  end  


  def self.create_invite_membership(email, company_id, role_id, permissions)
    user = User.where('lower(email) = ?', email.downcase).first
    if user.members.map{|m| m.company_id}.include? company_id
      return {type: "error", message: "This membership already exists."}
    else
      membership = Member.new(company_id: company_id, user_id: user.id, role_id: role_id, user_permissions: permissions)
      if membership.save 
        Company.find(company_id).email_list << user.email if role_id.to_i == 1
        Company.find(company_id).save
        return {type: "success", message: "Invite was successful"}
      else
        return {type: "error", message: "Invite was unsuccessful"}
      end
    end
  end

end
