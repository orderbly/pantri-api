class DeliveryZoneArea < ApplicationRecord
  belongs_to :delivery_zone
  belongs_to :company

  enum cut_off_day: [:same_day, :previous_day, :two_days_prior]
end
