class CreditNoteClaim < ApplicationRecord
  belongs_to :order
  belongs_to :credit_note
end
