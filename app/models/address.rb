class Address < ApplicationRecord
  has_many :orders, foreign_key: "delivery_address_id"
  has_many :companies, foreign_key: "default_address_id"
  belongs_to :company, required: false
  belongs_to :custom_client, required: false
  has_many :orders
end
