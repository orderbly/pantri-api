class Payment < ApplicationRecord

  belongs_to :order, optional: true
  belongs_to :supplier, class_name: 'Company', foreign_key: 'supplier_id'
  belongs_to :customer, class_name: 'Company', foreign_key: 'customer_id'
  belongs_to :user

  has_many :payment_allocations
  has_many :orders, :through => :payment_allocations
  validates :customer, :supplier, presence: true

  enum payment_type: [ :bank_transfer, :credit_card, :cash]

  def self.pay_order(id, amount)
    order = Order.find(id)
    payment = Payment.create(supplier_id: order.supplier_id, customer_id: order.customer_id, value: amount, order_id: id)
    PaymentAllocation.create(order_id: id, payment_id: payment.id, value: amount)
    order.value.to_f > amount.to_f ? order.status = 2 : order.status = 0
    order.balance -= amount.to_f
    order.save
  end

  def self.general_payment(supplier_id, customer_id, amount)
    payment = Payment.new(supplier_id: supplier_id, customer_id: customer_id, value: amount)
    payment.save
  end

end



