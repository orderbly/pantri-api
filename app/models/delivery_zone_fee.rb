class DeliveryZoneFee < ApplicationRecord
  belongs_to :delivery_zone

  enum fee_based_on: [:value, :quantity]
end
