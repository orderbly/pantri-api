class CustomClient < ApplicationRecord
  belongs_to :supplier, class_name: 'Company', foreign_key: 'supplier_id'
  has_many :custom_deliveries
  has_many :addresses
end
