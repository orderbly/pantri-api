class OrderSchedule < ApplicationRecord
  
  belongs_to :order
  has_many :orders
  # enum frequency: [ :every_monday, :every_tuesday, :every_wednesday, :every_thursday, :every_friday, :every_saturday, :every_two_weeks, :every_1st_of_the_month, :every_15th_of_the_month]
  enum frequency: [ :daily, :weekly, :monthly]


end
