class Comment < ApplicationRecord
  belongs_to :order
  belongs_to :company
  belongs_to :user

  enum comment_type: [:comment, :activity, :delivery ] 

end
