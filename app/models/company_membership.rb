class CompanyMembership < ApplicationRecord
  belongs_to :private_supplier, foreign_key: "supplier_id", class_name: "Company"
  belongs_to :private_customer, foreign_key: "customer_id", class_name: "Company"
end
