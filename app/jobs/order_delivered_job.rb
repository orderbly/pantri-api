class OrderDeliveredJob < ApplicationJob
  queue_as :default

  def perform(order_id)
    order_breakdown = MailersService::EmailBreakdowns::OrderBreakdown.new(order_id).call
    base_email = Company.find(order_breakdown[:customer][:id]).email || nil
    orders_email = "jean@orderbly.co"
    to_email = Company.find(order_breakdown[:customer][:id]).email_list
    to = [base_email, orders_email, to_email].flatten.compact.uniq
    from = sendgrid_config["emails"]["default_to_email"]
    data = order_breakdown
    template_id = Order.find(order_id).customer.is_custom_client ? sendgrid_config["templates"]["delivered_order_custom_customer_template"] : sendgrid_config["templates"]["delivered_order_template"]
    MailersService::MailerConfig.new(to: to, from: from, data: data, template: template_id).call
  end

end