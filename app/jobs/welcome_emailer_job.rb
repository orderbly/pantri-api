class WelcomeEmailerJob < ApplicationJob
  queue_as :default

  def perform(user)
    to = [user[:email]]
    from = sendgrid_config["emails"]["default_to_email"]
    data = {user: user[:user_name], email: user[:email] }
    template_id = sendgrid_config["templates"]["welcome_template"]
    MailersService::MailerConfig.new(to: to, from: from, data: data, template: template_id).call
  end
end


