class InviteUserJob < ApplicationJob
  queue_as :default

  def perform(email, company)
    invited_company = Company.find(company)
    to = [email]
    from = sendgrid_config["emails"]["default_to_email"]
    data = {company: { title: invited_company.title }}
    template_id = sendgrid_config["templates"]["invite_user"]
    MailersService::MailerConfig.new(to: to, from: from, data: data, template: template_id).call
  end
end
