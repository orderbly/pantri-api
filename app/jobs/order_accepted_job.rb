class OrderAcceptedJob < ApplicationJob
  queue_as :default

  def perform(order_id)
    order_breakdown = MailersService::EmailBreakdowns::OrderBreakdown.new(order_id).call
    to = Company.find(order_breakdown[:customer][:id]).email_list
    from = sendgrid_config["emails"]["default_to_email"]
    data = order_breakdown
    template_id = sendgrid_config["templates"]["order_accepted_template"]
    MailersService::MailerConfig.new(to: to, from: from, data: data, template: template_id).call
  end

end
