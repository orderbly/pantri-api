class PaymentReceivedJob < ApplicationJob
  queue_as :default

  def perform(order_breakdown)
    to = Company.find(order_breakdown[:customer][:id]).email_list
    from = sendgrid_config["emails"]["default_to_email"]
    data = order_breakdown
    template_id = sendgrid_config["templates"]["order_delivered_template"]
    MailersService::MailerConfig.new(to: to, from: from, data: data, template: template_id).call
  end
  
end
