class NewCommentJob < ApplicationJob
  queue_as :default

  def perform(comment)
    to = Company.find(comment[:company_id]).email_list
    from = sendgrid_config["emails"]["default_to_email"]
    data = comment
    template_id = sendgrid_config["templates"]["new_comment_template"]
    MailersService::MailerConfig.new(to: to, from: from, data: data, template: template_id).call
  end

end
