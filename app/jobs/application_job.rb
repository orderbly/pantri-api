class ApplicationJob < ActiveJob::Base

  def sendgrid_config
    Rails.configuration.sendgrid
  end

end
