class OrderSignatureJob < ApplicationJob
  queue_as :default

  def perform(order, signature)
    decoded_data = Base64.decode64(signature)
    signature = { 
      io: StringIO.new(decoded_data),
      content_type: 'image/png',
      filename: 'image.png'
    }
    order.signature.attach(signature)
    order.signature_image = order.signature.service.send(:object_for, order.signature.key).public_url
    order.save
  end
end
