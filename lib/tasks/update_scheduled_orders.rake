namespace :update_scheduled_orders do
    desc "Rake task to get update scheduled orders"
    task :fetch => :environment do
      puts "Updating news Orders"
      OrderScheduleService::UpdateOrderSchedule.new.call
      Integrations::TwilioService.new.send_message
      puts "#{Time.now} — Success!"
    end
  end