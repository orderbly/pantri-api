namespace :update_undelivered_orders_delivery_dates do
  desc "Rake task to update outstanding undelivered orders delivery dates"
  task :fetch => :environment do
    puts "Updating outdated orders"
    DeliveryService::ShiftOutstandingDeliveries.new.call
    puts "#{Time.now} — Success!"
  end
end
