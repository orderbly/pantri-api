namespace :update_delivery_dates do
  desc "Rake task to get shift undelivered orders"
  task :fetch => :environment do
    puts "Updating deliveries"
    OrderScheduleService::UpdateOrderSchedule.new.call
    Integrations::TwilioService.new.send_message
    puts "#{Time.now} — Success!"
  end
end