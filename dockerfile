FROM ruby:2.6.1
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
WORKDIR /usr/app
COPY Gemfile .
COPY Gemfile.lock .
RUN bundle install
COPY ./ .
